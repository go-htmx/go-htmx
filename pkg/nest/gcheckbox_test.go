package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckbox(t *testing.T) {
	b := true
	cb := GCheckBox{BoolRef: &b}
	var control ValueEditor = &cb
	assert.Less(t, 0, len(control.GetHtmlDisplay(nil)))
}
