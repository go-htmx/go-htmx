package nest

import (
	"testing"
)

func TestCheckBox(t *testing.T) {
	comp := &CheckBox{}
	var _ UI = comp
	var _ SortableValueProvider = comp
	var _ Editor = comp
}
