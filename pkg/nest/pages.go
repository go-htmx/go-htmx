package nest

import (
	"errors"
)

// Shows only one of the items.
type Pages struct {
	Component
	Children      []UI                                  // All possible pages must be listed in order to receive incoming information
	PageIndex     int                                   // Current page index
	OnInitialize  func(comp *Pages, ctx *Context) error // Can be used to add pages dynamically
	OnPrepareHTML func(comp *Pages, ctx *Context) error
}

func (pages *Pages) GetChildren() []UI {
	return pages.Children
}

func (pages *Pages) Select(page UI) {
	pages.PageIndex = -1
	for pgIndex, pg := range pages.Children {
		foundPage := (page == pg)
		if foundPage {
			pages.PageIndex = pgIndex
		}
		compget, ok := pg.(ComponentGetter)
		if ok {
			compget.GetComponent().Omitted = !foundPage
		}
	}
}

func (pages *Pages) Init(ctx *Context) error {
	return nil
}

func (pages *Pages) RunOnInitialize(ctx *Context) error {
	if pages.OnInitialize != nil {
		return pages.OnInitialize(pages, ctx)
	}
	return nil
}

func (pages *Pages) RunUserEvents(ctx *Context) error {
	return nil
}

func (pages *Pages) getCurrentPage() (UI, error) {
	pageIndex := pages.PageIndex
	if pageIndex < 0 {
		pageIndex = 0
	}
	if pageIndex >= len(pages.Children) {
		return nil, errors.New("invalid page index")
	} else {
		return pages.Children[pageIndex], nil
	}
}

func (pages *Pages) PrepareHTML(ctx *Context) error {
	var errList error
	if pages.OnPrepareHTML != nil {
		err := pages.OnPrepareHTML(pages, ctx)
		errList = errors.Join(errList, err)
	}
	curPage, err := pages.getCurrentPage()
	errList = errors.Join(errList, err)
	if err == nil {
		err := curPage.PrepareHTML(ctx)
		errList = errors.Join(errList, err)
	}
	return errList
}

func (pages *Pages) GetHTML(ctx *Context) string {
	if pages.Omitted {
		return ""
	}
	curPage, err := pages.getCurrentPage()
	if err != nil {
		return "ERROR"
	}
	return curPage.GetHTML(ctx)
}
