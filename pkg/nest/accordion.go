package nest

import (
	"fmt"
	"html"
	"strings"
)

type Accordion struct {
	Component
	Open      bool   // When true, it is shown open
	Caption   string // Text to show when collapsed
	Child     UI     // Control to show when expanded
	MaxHeight int    // In pixels. If not set, no animation will be made.
}

func (acc *Accordion) GetChildren() []UI {
	return []UI{acc.Child}
}

func (acc *Accordion) Init(ctx *Context) error {
	acc.Open = ctx.GetBool(acc.Component.ID)
	return nil
}

func (acc *Accordion) RunOnInitialize(htmxInfo *Context) error {
	return nil
}

func (acc *Accordion) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (acc *Accordion) PrepareHTML(htmxInfo *Context) error {
	return acc.Child.PrepareHTML(htmxInfo)
}

const accordionStyleTemplate = `
<style>
#%s {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 18px;
  width: 100%%;
  text-align: left;
  border: none;
  outline: none;
  transition: 0.4s;
}

.active, #%s:hover {
  background-color: #ccc;
}

#%s {
	padding 0px 18px;
	max-height: 0px;
	width: 100%%;
	overflow: hidden;
}

#%s.expand {
  	padding: 18px 18px;
	%s
	width: 100%%;
}
</style>
`

func (acc *Accordion) GetHTML(htmxInfo *Context) string {
	if acc.Component.Omitted {
		return ""
	}
	// Handle parameters and state
	curHtmx := htmxInfo.Copy()
	curHtmx.SetBool(acc.Component.ID, !acc.Open)
	curHtmx.SetOTBool(acc.Component.ID+"animate", true)
	buttonid := acc.Component.ID + "b"
	divid := acc.Component.ID + "d"
	animate := htmxInfo.GetBool(acc.Component.ID + "animate")

	// Create list of classes to start with
	classList := make([]string, 0, 3)
	if !animate {
		classList = append(classList, "expand")
	}
	if acc.Component.Class != "" {
		classList = append(classList, acc.Component.Class)
	}
	classAttr := ""
	if len(classList) != 0 {
		classAttr = fmt.Sprintf(` class="%s" `, strings.Join(classList, " "))
	}

	// Define stylesheet
	styleHtml := ""
	if acc.Component.Class == "" {
		maxheight := ""
		if acc.MaxHeight != 0 {
			maxheight = fmt.Sprintf(`max-height: %dpx; 	transition: max-height 0.4s ease-in; overflow: hidden;`, acc.MaxHeight)
		} else {
			maxheight = `max-height: 100%;`
		}
		styleHtml = fmt.Sprintf(accordionStyleTemplate, buttonid, buttonid, divid, divid, maxheight)
	}

	// Make HTML
	buttonHtml := fmt.Sprintf(`<button%s %s id="%s">%s</button>`, classAttr, curHtmx.GetHxAttrs(), buttonid, html.EscapeString(acc.Caption))
	panelHtml := ""
	if acc.Open {
		panelHtml = fmt.Sprintf(`<div%s id="%s">%s</div>`, classAttr, divid, acc.Child.GetHTML(htmxInfo))
		if animate {
			panelHtml += fmt.Sprintf(`<script>setTimeout(function(){document.getElementById('%s').classList.add('expand');},10);</script>`, divid)
		}
	}

	resHTML := fmt.Sprintf(`<div id="%s">%s%s%s</div>`+"\n", acc.Component.ID, styleHtml, buttonHtml, panelHtml)
	return resHTML
}
