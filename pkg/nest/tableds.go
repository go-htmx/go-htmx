package nest

import (
	"fmt"
	"log/slog"
	"strings"
)

// Implements a TableDS that allows sorting on columns and nice formatting of values, and uses a dataset as input
type TableDS struct {
	Component
	Dataset               DataSourceProvider
	Columns               []TableColumn     // Auto-filled, but filling it manually gives you control of what columns to show
	Rows                  []*TableRowDS     // Auto-filled
	SortBy                DatasetSortMethod // Method to sort the rows by
	Refresh               int               // Number of seconds until automatic refresh
	Actions               []RecordAction    // List of actions which can be enabled for each row
	AllowSelect           bool              // If true, a row can be selected by clicking it
	SelectionList         map[string]bool   // List of items that are selected
	Caption               UI                // For WCAG compliance. Put in an Text here.
	PaginationRowsPerPage int               // Has a default, but it is only used if PaginationTotalCount is set
	OnInitialize          func(TableDS *TableDS, ctx *Context) error
	OnPrepareHTML         func(TableDS *TableDS, ctx *Context) error // Always called before GetHTML()

	// Calculated during Load automatically
	pageFirst   int
	pageCurrent int
	pageLast    int
}

type TableRowDS struct {
	Cells   map[FieldName]TableCellDS // Maps fieldname into a cell. This approach means the Table.Columns decides which columns to show and in which order
	Record  RecordAccessor
	keyID   string     // Value of the primary key for this row. Automatically set.
	key     PrimaryKey // Used for referencing into a DataSource
	Omitted bool       // Useful for filtering
}

type TableCellDS struct {
	Value       FieldValue
	Alignment   GFieldAlignment
	AllowWrap   bool   // If true, the row contents may wrap and take up multiple lines
	ExtraInfo   string // Shown if mouse hovers over the text
	CellBGColor string // HTML color for background, including "#", e.g. "#ffffff"
}

func (tab *TableDS) GetChildren() []UI {
	return []UI{tab.Caption}
}

func (tab *TableDS) Init(ctx *Context) error {
	sortByParam := ctx.Get(tab.ID + "sort")
	if sortByParam != "" {
		tab.SortBy = DatasetSortMethod(sortByParam)
	}
	if tab.PaginationRowsPerPage <= 0 {
		tab.PaginationRowsPerPage = 20
	}

	return nil
}

func (tab *TableDS) RunOnInitialize(ctx *Context) error {
	// Filter and sort TableDS, and calculate paging
	tab.pageCurrent = tab.getCurrentTablePage(ctx)
	tab.pageFirst = max(tab.pageCurrent-2, 0)
	tab.pageLast = tab.pageCurrent + 2
	firstRowIndex := tab.pageCurrent * tab.PaginationRowsPerPage
	lastRowIndex := (tab.pageLast+1)*tab.PaginationRowsPerPage - 1
	rows, err := tab.Dataset.GetRecords(ctx, tab.SortBy, firstRowIndex, lastRowIndex)
	if err != nil {
		return err
	}

	tab.pageLast = tab.getPageFromRowNumber(len(rows) + firstRowIndex - 1)
	if len(rows) > tab.PaginationRowsPerPage {
		rows = rows[:tab.PaginationRowsPerPage]
	}

	// Convert fetched rows into tableCells
	tab.Rows = nil
	for _, row := range rows {
		var rowID string
		if row.GetPrimaryKey() == nil {
			slog.Error("TableDS has row without a primary key", slog.String("msgid", "3AK1JH"))
		} else {
			rowID = row.GetPrimaryKey().GetRecordIdentifier()
		}
		newRow := TableRowDS{
			keyID:  rowID,
			key:    row.GetPrimaryKey(),
			Cells:  make(map[FieldName]TableCellDS),
			Record: row,
		}
		for _, fieldName := range row.GetFieldNames() {
			field := row.GetFieldValue(fieldName)
			newRow.Cells[fieldName] = TableCellDS{
				Value: field,
			}
		}
		tab.Rows = append(tab.Rows, &newRow)
	}

	// Table columns
	if tab.Columns == nil {
		for _, fieldinfo := range tab.Dataset.GetFieldList() {
			tab.Columns = append(tab.Columns, TableColumn{
				FieldName: fieldinfo.FieldName,
				Title:     fieldinfo.Caption,
			})
		}
	}

	if tab.OnInitialize != nil {
		return tab.OnInitialize(tab, ctx)
	}
	return nil
}

func (tab *TableDS) RunUserEvents(ctx *Context) error {
	actionrow := ctx.Get(tab.ID + "actionrow")
	actioncaption := ctx.Get(tab.ID + "actioncaption")
	if actionrow != "" && actioncaption != "" {
		for _, action := range tab.Actions {
			if action.Caption != actioncaption || action.OnClick == nil {
				continue
			}

			for _, row := range tab.Rows {
				if row.keyID == actionrow {
					ctx.Clear(tab.ID + "actioncaption")
					ctx.Clear(tab.ID + "actionrow")
					err := action.OnClick(ctx, &action, row.Record)
					if err != nil {
						return err
					}
					break
				}
			}

		}
	}
	return nil
}

func (tab *TableDS) PrepareHTML(ctx *Context) error {
	if tab.OnPrepareHTML != nil {
		err := tab.OnPrepareHTML(tab, ctx)
		if err != nil {
			return err
		}
	}
	if tab.Caption != nil {
		return tab.Caption.PrepareHTML(ctx)
	}

	return nil
}

// Calculates the page number based on the "first row number" parameter in the context
func (tab *TableDS) getCurrentTablePage(ctx *Context) int {
	firstRowIndex := ctx.GetInt(tab.ID + tableParameterSuffixFirstRowIndex)
	return tab.getPageFromRowNumber(firstRowIndex)
}

func (tab *TableDS) getPageFromRowNumber(rowNumber int) int {
	page := 0
	rowsPerPage := tab.PaginationRowsPerPage
	if rowsPerPage == 0 {
		rowsPerPage = 20
	}
	page = rowNumber / rowsPerPage
	return page
}

func (tab *TableDS) GetHTML(htmxInfo *Context) string {
	if tab.Omitted {
		return ""
	}
	if tab.ID == "" {
		return "ID is missing for TableDS"
	}

	htmxInfo = htmxInfo.Copy()
	htmxInfo.HxSelect = "#" + tab.ID
	htmxInfo.HxTarget = htmxInfo.HxSelect
	htmxInfo.HxSwap = "outerHTML"

	// Get parameters from URL
	tab.SelectionList = make(map[string]bool)
	selPK := tab.Dataset.GetCurrentPrimaryKey()
	if selPK != nil {
		tab.SelectionList[selPK.GetRecordIdentifier()] = true
	}

	// Generate HTML
	sl := make([]string, 0)
	sl = append(sl, fmt.Sprintf(`<div id="%s">`, e(tab.ID)))
	sl = append(sl, fmt.Sprintf(`<table class="%s" id="%s" `, GenerateComponentClassName("Table"), e(tab.ID+"Table")))
	if tab.Refresh != 0 {
		newhtmxinfo := htmxInfo.Copy()
		newhtmxinfo.HxTrigger = fmt.Sprintf("every %ds", tab.Refresh)
		htmxattrs := newhtmxinfo.GetHxAttrs()
		sl = append(sl, htmxattrs)
	}
	sl = append(sl, ">\n")

	if htmxInfo.RequireWCAG && tab.Caption == nil {
		slog.Warn("WCAG problem: Table has no caption", slog.String("id", tab.ID), slog.String("msgid", "WCAG03"))
	}
	if tab.Caption != nil {
		sl = append(sl, fmt.Sprintf(`<caption>%s</caption>`, tab.Caption.GetHTML(htmxInfo)))
	}

	sl = append(sl, "<colgroup>")
	if len(tab.Actions) != 0 {
		sl = append(sl, "<col />")
	}
	for _, col := range tab.Columns {
		colstyles := ""
		if col.ColumnWidth != "" {
			colstyles += "width: " + col.ColumnWidth
		}
		sl = append(sl, "<col style=\""+e(colstyles)+"\">")
	}
	sl = append(sl, "</colgroup>\n")

	sl = append(sl, "<thead>")
	sl = append(sl, "<tr>")
	if len(tab.Actions) != 0 {
		sl = append(sl, fmt.Sprintf(`<th>%s</th>`, e(T("Actions"))))
	}
	for _, col := range tab.Columns {
		// TableDS header cell <th>
		thHtmxInfo := htmxInfo.Copy()
		thHtmxInfo.Set(tab.ID+"sort", string(col.FieldName))
		attrs := thHtmxInfo.GetHxAttrs()
		sl = append(sl, fmt.Sprintf(`<th %s `, attrs))
		if col.ExtraInfo != "" {
			sl = append(sl, fmt.Sprintf(` title="%s" `, e(col.ExtraInfo)))
		}
		sl = append(sl, fmt.Sprintf(` id="%s" style="text-align: center; cursor:pointer; ">`, e(tab.ID+"col"+string(col.FieldName))))
		sl = append(sl, e(col.Title))
		if string(col.FieldName) == string(tab.SortBy) {
			sl = append(sl, fmt.Sprintf(` <span title="%s">&#8595;</span>`, e("Sorted ascending - click a column header to sort")))
		}
		sl = append(sl, "</th>")
	}
	sl = append(sl, "</tr>\n</thead>\n")
	sl = append(sl, "<tbody>\n")
	for _, row := range tab.Rows {
		if row.Omitted {
			continue
		}
		rowid := fmt.Sprintf("%srow%s", tab.ID, row.keyID)
		attrs := ""
		if tab.AllowSelect {
			rowSelectionID := row.keyID
			isRowSelected := tab.SelectionList[rowSelectionID]
			if isRowSelected {
				attrs = fmt.Sprintf(` class="%s" `, GenerateComponentClassName("GTableRowSelected"))
			} else {
				if row.key == nil {
					slog.Error("Primary key missing for TableDS row", slog.String("msgid", "KJSEHA"))
				}
				newHtmxCtx := tab.Dataset.GetContextWithNewState(htmxInfo, DsRead, row.key)
				attrs = newHtmxCtx.GetHxAttrs()
			}
		}
		sl = append(sl, fmt.Sprintf(`<tr id="%s" %s>`, e(rowid), attrs))
		if len(tab.Actions) != 0 {
			sl = append(sl, "<td>")
			for _, action := range tab.Actions {
				if !action.IsVisible(htmxInfo, &action, row.Record) {
					continue
				}

				ctxAction := htmxInfo.Copy()

				if action.ConfirmationQuestion != "" {
					ctxAction.HxConfirm = action.ConfirmationQuestion
				}

				ctxAction.SetOT(tab.ID+"actionrow", row.keyID)
				ctxAction.SetOT(tab.ID+"actioncaption", action.Caption)
				sl = append(sl, fmt.Sprintf(`<button %s>%s</button>`, ctxAction.GetHxAttrs(), e(action.Caption)))
			}
			sl = append(sl, "</td>")
		}

		for _, col := range tab.Columns {
			cell, found := row.Cells[col.FieldName]
			if found {
				styles := ""
				alignment := cell.Alignment
				if alignment == FaAuto {
					_, ok := cell.Value.(FieldValueAggregator)
					if ok {
						alignment = FaRight
					}
				}

				switch alignment {
				case FaLeft:
					styles += ` text-align: left; `
				case FaCenter:
					styles += ` text-align: center; `
				case FaRight:
					styles += ` text-align: right; `
				}
				if cell.AllowWrap {
					styles += "white-space: normal; "
				}
				if cell.CellBGColor != "" {
					styles += fmt.Sprintf("background-color: %s; ", e(cell.CellBGColor))
				}
				sl = append(sl, "<td ")
				if styles != "" {
					sl = append(sl, fmt.Sprintf(` style="%s"`, e(styles)))
				}
				if cell.ExtraInfo != "" {
					sl = append(sl, fmt.Sprintf(` title="%s"`, e(cell.ExtraInfo)))
				}
				sl = append(sl, ">")
				if cell.Value != nil {
					sl = append(sl, cell.Value.GetAsHtml(htmxInfo.Locale))
				}
				sl = append(sl, "</td>")
			} else {
				sl = append(sl, "<td></td>")
			}
		}
		sl = append(sl, "</tr>\n")
	}
	sl = append(sl, "</tbody>\n</Table>\n")

	// Pagination
	if tab.pageLast != 0 { // More than one page
		styles := `margin: 0 5px; cursor: pointer; `
		stylesInactive := `margin: 0 5px; visibility: hidden;`

		var pageHtml []string
		if tab.pageCurrent > 0 {
			linkContext := htmxInfo.Copy()
			linkContext.SetIntNonZero(tab.ID+tableParameterSuffixFirstRowIndex, (tab.pageCurrent-1)*tab.PaginationRowsPerPage)
			hxstuff := linkContext.GetHxAttrs()
			pageHtml = append(pageHtml, fmt.Sprintf(`<span style="%s" %s>&nbsp;Previous&nbsp;</span>`, styles, hxstuff))
		} else {
			pageHtml = append(pageHtml, fmt.Sprintf(`<span style="%s">&nbsp;Previous&nbsp;</span>`, stylesInactive))
		}
		for p := tab.pageFirst; p <= tab.pageLast; p++ {
			active := ""
			if p == tab.pageCurrent {
				active = " font-weight: bold;"
			}
			linkContext := htmxInfo.Copy()
			linkContext.SetIntNonZero(tab.ID+tableParameterSuffixFirstRowIndex, p*tab.PaginationRowsPerPage)
			hxstuff := linkContext.GetHxAttrs()
			pageHtml = append(pageHtml, fmt.Sprintf(`<span style="%s %s"  onmouseover="this.style.color='#8080ff';" onmouseout="this.style.color='black';" %s>&nbsp;%d&nbsp;</span>`, styles, active, hxstuff, p+1))
		}
		if tab.pageCurrent < tab.pageLast {
			linkContext := htmxInfo.Copy()
			linkContext.SetIntNonZero(tab.ID+tableParameterSuffixFirstRowIndex, (tab.pageCurrent+1)*tab.PaginationRowsPerPage)
			hxstuff := linkContext.GetHxAttrs()
			pageHtml = append(pageHtml, fmt.Sprintf(`<span style="%s" %s>&nbsp;Next&nbsp;</span>`, styles, hxstuff))
		} else {
			pageHtml = append(pageHtml, fmt.Sprintf(`<span style="%s">&nbsp;Next&nbsp;</span>`, stylesInactive))
		}
		sl = append(sl, fmt.Sprintf(`
		<div style="display: flex; justify-content: center; margin-top: 20px;">
			<nav aria-label="Page navigation">
					%s
			</nav>
		</div>`, strings.Join(pageHtml, "\n")))
	}
	sl = append(sl, "</div>\n")

	return strings.Join(sl, "")
}
