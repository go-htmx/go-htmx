package nest

import (
	"errors"
	"fmt"
	"log/slog"
)

// LabelEdit is used in User Interfaces for tables, forms etc.

type LabelEdit struct {
	Component
	Caption      string      // Caption to show on the UI for this field
	FieldName    string      // Machine readable text ID of this field
	IsReadOnly   bool        // Default is false
	LayoutMode   int         // 0 = Automatic. 1=Label above edit. 2=edit left to label
	AutoFocus    bool        // If true, try to autofocus on this one in some situations
	LinkURL      string      // Makes the cell contents clickable, will navigate to this URL
	ExtraInfo    string      // Extra info that can be shown as a hint or multiline explanation
	ErrorMessage string      // If set, this field is considered to have an error. Useful for handling incoming http requests and parsing fields.
	Editor       ValueEditor // Value to be saved/retrieved
}

func (field *LabelEdit) GetHtmlDisplay(locale Locale) string {
	return field.Editor.GetHtmlDisplay(locale)
}

func (field *LabelEdit) GetHtmlEdit(idName, fieldName, extraAttrs string, readonly bool) string {
	if field.FieldName != fieldName {
		slog.Error("FieldName mismatch for LabelEdit", slog.String("fieldname", fieldName), slog.String("field.fieldname", field.FieldName), slog.String("msgid", "R8KK61"))
	}
	return field.Editor.GetHtmlEdit(idName, fieldName, extraAttrs, readonly)
}

func (field *LabelEdit) SetHtmlValue(value string) error {
	if field.Editor == nil {
		field.ErrorMessage = "Editor component missing"
		return errors.New("missing component in LabelEdit")
	}
	err := field.Editor.SetHtmlValue(value)
	if err != nil {
		field.ErrorMessage = err.Error()
	}
	return err
}

func (field *LabelEdit) GetFieldName() string {
	if field.FieldName == "" {
		field.FieldName = field.ID
	}
	return field.FieldName
}

func (field *LabelEdit) Init(ctx *Context) error {
	if field.FieldName == "" {
		field.FieldName = field.ID
	}
	return nil
}

func (field *LabelEdit) RunOnInitialize(htmxInfo *Context) error {
	return nil
}

func (field *LabelEdit) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (field *LabelEdit) PrepareHTML(htmxInfo *Context) error {
	return nil
}

func (field *LabelEdit) GetHTML(htmxInfo *Context) string {
	if field.Omitted {
		return ""
	}
	if field.FieldName == "" {
		slog.Error("FieldName is not set for LabelEdit", slog.String("caption", field.Caption), slog.String("msgid", "R8KK65"))
	}

	// EDIT
	editPartStr := "EDIT control is missing"
	errorPartStr := ""
	{
		// Editor for field value
		if field.Editor != nil {
			// Additional attributes
			attrs := " "
			if field.AutoFocus {
				attrs = " autofocus "
			}
			editPartStr = field.Editor.GetHtmlEdit(field.FieldName, field.FieldName, attrs, field.IsReadOnly)
		}
		if field.LinkURL != "" {
			editPartStr = fmt.Sprintf(`<a href="%s">%s</a>`, e(field.LinkURL), editPartStr)
		}
		if field.ErrorMessage != "" {
			errorPartStr = fmt.Sprintf(`<br><span class="%s" style="color: red; ">%s</span>`, GenerateComponentClassName("GFormError"), e(field.ErrorMessage))
		}
	}

	layout := field.LayoutMode
	if layout == 0 {
		layout = 1
	}
	layoutStr := ""
	switch layout {
	case 1:
		layoutStr = fmt.Sprintf(`<label for="#%s">%s:</label><br>%s%s`, e(field.FieldName), e(field.Caption), editPartStr, errorPartStr)
	case 2:
		layoutStr = fmt.Sprintf(`<label>%s <span style="user-select: none; cursor:pointer;">%s</span></label>%s`, editPartStr, e(field.Caption), errorPartStr)
	}
	return fmt.Sprintf(`<div id="%s" title="%s">%s</div>`, e(field.FieldName+"div"), e(field.ExtraInfo), layoutStr)
}
