package nest

import (
	"fmt"
)

// LabelEdit2 is used in User Interfaces for tables, forms etc.

type LabelEdit2 struct {
	Component
	Caption      string // Caption to show on the UI for this field
	ExtraInfo    string // Extra info that can be shown as a hint or multiline explanation
	Editor       Editor // Editor for a value
	OnInitialize func(edit *LabelEdit2, ctx *Context) error
	OnIncoming   func(edit *LabelEdit2, ctx *Context) error
}

func (field *LabelEdit2) GetChildren() []UI {
	return []UI{field.Editor}
}

func (field *LabelEdit2) Init(ctx *Context) error {
	return nil
}

func (field *LabelEdit2) RunOnInitialize(htmxInfo *Context) error {
	if field.OnInitialize != nil {
		return field.OnInitialize(field, htmxInfo)
	}
	return nil
}

func (field *LabelEdit2) RunUserEvents(htmxInfo *Context) error {
	if field.OnIncoming != nil {
		return field.OnIncoming(field, htmxInfo)
	}
	return nil
}

func (field *LabelEdit2) PrepareHTML(htmxInfo *Context) error {
	if field.Editor == nil {
		return nil
	}
	return field.Editor.PrepareHTML(htmxInfo)
}

func (field *LabelEdit2) GetHTML(htmxInfo *Context) string {
	if field.Omitted {
		return ""
	}
	if field.Editor == nil {
		return "NO EDITOR"
	}

	errorHtml := ""
	err := field.Editor.GetValidationResult(htmxInfo)
	if err != nil {
		errorHtml = fmt.Sprintf(`<br><span class="%s" style="color: red; ">%s</span>`, GenerateComponentClassName("GFormError"), e(err.Error()))
	}

	editorID := field.Editor.GetID()
	labelstr := ""
	if field.Caption != "" {
		labelstr = fmt.Sprintf(`<label for="%s">%s:</label><br>`, e(editorID), e(field.Caption))
	}

	return fmt.Sprintf(`<div id="%s" title="%s">%s%s%s</div>`+"\n",
		e(field.ID), e(field.ExtraInfo), labelstr, field.Editor.GetHTML(htmxInfo), errorHtml)
}
