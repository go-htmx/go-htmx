package nest

import (
	"errors"
	"strings"
)

// Executes all the UI methods on SubComponents and is used to
// concatenate multiple components without adding additional HTML or styling.
type Concat struct {
	Component
	Children     []UI
	OnInitialize func(container *Concat, htmxCtx *Context) error // SubComponents have to be initialized inside this handler
}

func (container *Concat) Init(ctx *Context) error {
	return nil
}

func (container *Concat) GetChildren() []UI {
	return container.Children
}

// SubComponents have to initialize themselves inside OnInitialize.
func (container *Concat) RunOnInitialize(htmxCtx *Context) error {
	if container.OnInitialize != nil {
		return container.OnInitialize(container, htmxCtx)
	}
	return nil
}

func (container *Concat) RunUserEvents(htmxCtx *Context) error {
	return nil
}

func (container *Concat) PrepareHTML(htmxCtx *Context) error {
	var err error
	for _, component := range container.Children {
		err = errors.Join(err, component.PrepareHTML(htmxCtx))
	}

	return err
}

func (container *Concat) GetHTML(ctx *Context) string {
	if container.Component.Omitted {
		return ""
	}
	var sl []string
	for _, component := range container.Children {
		sl = append(sl, component.GetHTML(ctx))
	}

	return strings.Join(sl, "")
}
