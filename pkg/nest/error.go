package nest

// This is a useful alternative for using errors.New() because it will not force you to start messages with lowercase.
type ErrorItem string

func (e *ErrorItem) Error() string {
	return string(*e)
}

// Useful for returning errors that the user should see, with uppercase initial etc., which is not allowed when using errors.New().
func UserErrorMsg(UserMessage string) *ErrorItem {
	return (*ErrorItem)(&UserMessage)
}
