package nest

import (
	"errors"
	"fmt"
	"html"
	"strings"
)

// Simple HTML components to support H1, H2, H3, P and texts with links

// HEADING 1 (H1)

type Heading1 struct {
	Component
	OnInitialize  func(heading *Heading1, ctx *Context) error
	OnPrepareHTML func(heading *Heading1, ctx *Context) error // You can set Html in this.

	Text string
}

func (part *Heading1) Init(ctx *Context) error {
	return nil
}

func (part *Heading1) RunOnInitialize(htmxInfo *Context) error {
	if part.OnInitialize != nil {
		return part.OnInitialize(part, htmxInfo)
	}
	return nil
}

func (part *Heading1) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (part *Heading1) PrepareHTML(htmxInfo *Context) error {
	if part.OnPrepareHTML != nil {
		return part.OnPrepareHTML(part, htmxInfo)
	}
	return nil
}

func (part *Heading1) GetHTML(htmxInfo *Context) string {
	if part.Omitted {
		return ""
	}
	classHtml := ""
	if part.Class != "" {
		classHtml = fmt.Sprintf(` class="%s"`, html.EscapeString(part.Class))
	}
	partID := ""
	if part.ID != "" {
		partID = fmt.Sprintf(` id="%s"`, html.EscapeString(part.ID))
	}
	return fmt.Sprintf(`<h1%s%s>%s</h1>`, partID, classHtml, ebr(part.Text))
}

// HEADING 2 (H2)

type Heading2 struct {
	Component
	OnInitialize  func(heading *Heading2, ctx *Context) error
	OnPrepareHTML func(heading *Heading2, ctx *Context) error // You can set Html in this.

	Text string
}

func (part *Heading2) Init(ctx *Context) error {
	return nil
}

func (part *Heading2) RunOnInitialize(htmxInfo *Context) error {
	if part.OnInitialize != nil {
		return part.OnInitialize(part, htmxInfo)
	}
	return nil
}

func (part *Heading2) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (part *Heading2) PrepareHTML(htmxInfo *Context) error {
	if part.OnPrepareHTML != nil {
		return part.OnPrepareHTML(part, htmxInfo)
	}
	return nil
}

func (part *Heading2) GetHTML(htmxInfo *Context) string {
	if part.Omitted {
		return ""
	}
	classHtml := ""
	if part.Class != "" {
		classHtml = fmt.Sprintf(` class="%s"`, html.EscapeString(part.Class))
	}
	partID := ""
	if part.ID != "" {
		partID = fmt.Sprintf(` id="%s"`, html.EscapeString(part.ID))
	}
	return fmt.Sprintf(`<h2%s%s>%s</h2>`, partID, classHtml, ebr(part.Text))
}

// HEADING 3 (H3)

type Heading3 struct {
	Component
	OnInitialize  func(heading *Heading3, ctx *Context) error
	OnPrepareHTML func(heading *Heading3, ctx *Context) error // You can set Html in this.

	Text string
}

func (part *Heading3) Init(ctx *Context) error {
	return nil
}

func (part *Heading3) RunOnInitialize(htmxInfo *Context) error {
	if part.OnInitialize != nil {
		return part.OnInitialize(part, htmxInfo)
	}
	return nil
}

func (part *Heading3) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (part *Heading3) PrepareHTML(htmxInfo *Context) error {
	if part.OnPrepareHTML != nil {
		return part.OnPrepareHTML(part, htmxInfo)
	}
	return nil
}

func (part *Heading3) GetHTML(htmxInfo *Context) string {
	if part.Omitted {
		return ""
	}
	classHtml := ""
	if part.Class != "" {
		classHtml = fmt.Sprintf(` class="%s"`, html.EscapeString(part.Class))
	}
	partID := ""
	if part.ID != "" {
		partID = fmt.Sprintf(` id="%s"`, html.EscapeString(part.ID))
	}
	return fmt.Sprintf(`<h3%s%s>%s</h3>`, partID, classHtml, ebr(part.Text))
}

// PARAGRAPH (P)

type Paragraph struct {
	Component
	OnInitialize  func(p *Paragraph, ctx *Context) error // Can be used to add items to TextParts
	OnIncoming    func(p *Paragraph, ctx *Context) error // User event stuff can be put here
	OnPrepareHTML func(p *Paragraph, ctx *Context) error // You can set Html in this.

	Text      string        // If set, this is the text to show
	TextParts []UI          // If set, these parts will be queried for HTML to be inserted between <p>...</p>. Use Text or Image or CustomHtml components here.
	Attrs     AttributeList // Can be used to add attributes to the surrounding html tag
}

func (part *Paragraph) GetChildren() []UI {
	return part.TextParts
}

func (part *Paragraph) Init(ctx *Context) error {
	return nil
}

func (part *Paragraph) RunOnInitialize(htmxInfo *Context) error {
	if part.OnInitialize != nil {
		return part.OnInitialize(part, htmxInfo)
	}
	return nil
}

func (part *Paragraph) RunUserEvents(htmxInfo *Context) error {
	if part.OnIncoming != nil {
		return part.OnIncoming(part, htmxInfo)
	}
	return nil
}

func (part *Paragraph) PrepareHTML(htmxInfo *Context) error {
	var firstError error
	if part.OnPrepareHTML != nil {
		firstError = part.OnPrepareHTML(part, htmxInfo)
	}
	for _, p := range part.TextParts {
		err := p.PrepareHTML(htmxInfo)
		firstError = errors.Join(err)
	}
	return firstError
}

func (part *Paragraph) GetHTML(htmxInfo *Context) string {
	if part.Omitted {
		return ""
	}
	classHtml := ""
	if part.Class != "" {
		classHtml = fmt.Sprintf(` class="%s"`, html.EscapeString(part.Class))
	}
	partID := ""
	if part.ID != "" {
		partID = fmt.Sprintf(` id="%s"`, html.EscapeString(part.ID))
	}
	TextBody := ebr(part.Text)
	for _, p := range part.TextParts {
		TextBody += p.GetHTML(htmxInfo)
	}
	return fmt.Sprintf(`<p%s%s%s>%s</p>`, part.Attrs.GetHTML(htmxInfo), partID, classHtml, TextBody)
}

func (part *Paragraph) GetSortableRepresentation() (string, float64) {
	return part.Text, 0
}

// TEXT (Plain text, can have a link)

// This generates an <a href...>text</a> part, and does not use HTMX for the link
type Text struct {
	Component
	Text      string
	TextParts []UI          // If you want to put Text items into Text items, you can do this here.
	URL       string        // If set, <a href...></a> will be around the text
	NewWindow bool          // If URL is set, and this is true, it will use target="_blank"
	Attrs     AttributeList // Can be used to add attributes to the surrounding html tag

	OnInitialize  func(p *Text, ctx *Context) error // Can be used to add items to TextParts
	OnIncoming    func(p *Text, ctx *Context) error
	OnPrepareHTML func(p *Text, ctx *Context) error // You can set Html in this.
}

func (part *Text) GetChildren() []UI {
	return part.TextParts
}

func (part *Text) Init(ctx *Context) error {
	return nil
}

func (part *Text) RunOnInitialize(htmxInfo *Context) error {
	if part.OnInitialize != nil {
		return part.OnInitialize(part, htmxInfo)
	}
	return nil
}

func (part *Text) RunUserEvents(htmxInfo *Context) error {
	if part.OnIncoming != nil {
		return part.OnIncoming(part, htmxInfo)
	}
	return nil
}

func (part *Text) PrepareHTML(htmxInfo *Context) error {
	var firstError error
	if part.OnPrepareHTML != nil {
		firstError = part.OnPrepareHTML(part, htmxInfo)
	}
	for _, p := range part.TextParts {
		err := p.PrepareHTML(htmxInfo)
		firstError = errors.Join(firstError, err)
	}
	return firstError
}

func (part *Text) GetHTML(htmxInfo *Context) string {
	if part.Omitted {
		return ""
	}
	attrClass := ""
	if part.Class != "" {
		attrClass = fmt.Sprintf(` class="%s" `, html.EscapeString(part.Class))
	}
	partID := ""
	if part.ID != "" {
		partID = fmt.Sprintf(` id="%s"`, html.EscapeString(part.ID))
	}
	text := ebr(part.Text)
	for _, p := range part.TextParts {
		text += p.GetHTML(htmxInfo)
	}
	if part.URL != "" {
		target := ""
		if part.NewWindow {
			target = ` target="_blank"`
		}
		return fmt.Sprintf(`<a%s%s%s%s href="%s">%s</a>`, part.Attrs.GetHTML(htmxInfo), target, partID, attrClass, html.EscapeString(part.URL), text)
	} else {
		// No URL
		return fmt.Sprintf(`<span%s%s%s>%s</span>`, part.Attrs.GetHTML(htmxInfo), partID, attrClass, text)
	}
}

func (part *Text) GetSortableRepresentation() (string, float64) {
	str := part.Text
	value := 0.0
	for _, part := range part.TextParts {
		sortable, ok := part.(SortableValueProvider)
		if ok {
			sortStr, sortVal := sortable.GetSortableRepresentation()
			str = fmt.Sprintf("%s~%s", str, sortStr)
			value += sortVal
		}
	}
	return str, value
}

// Sortable representation of a number.
// Similar to Text, but takes a floating point number which can be converted from integer.
// Useful for formatting numbers well, and for having it sortable.
type Number struct {
	Component
	Value         float64
	Decimals      uint8 // Default is to have no decimals
	OnInitialize  func(num *Number, ctx *Context) error
	OnPrepareHTML func(num *Number, ctx *Context) error
}

func (part *Number) Init(ctx *Context) error {
	return nil
}

func (part *Number) RunOnInitialize(ctx *Context) error {
	if part.OnInitialize != nil {
		return part.OnInitialize(part, ctx)
	}
	return nil
}

func (part *Number) RunUserEvents(ctx *Context) error {
	return nil
}

func (part *Number) PrepareHTML(ctx *Context) error {
	if part.OnPrepareHTML != nil {
		return part.OnPrepareHTML(part, ctx)
	}
	return nil
}

func (part *Number) GetHTML(ctx *Context) string {
	var str string
	if ctx.Locale != nil {
		str = ctx.Locale.FormatFloat(part.Value, int(part.Decimals))
	} else {
		str = fmt.Sprintf("%v", part.Value)
	}
	attrClass := ""
	if part.Class != "" {
		attrClass = fmt.Sprintf(` class="%s"`, e(part.Class))
	}
	attrID := ""
	if part.ID != "" {
		attrID = fmt.Sprintf(` id="%s"`, e(part.ID))
	}
	return fmt.Sprintf(`<span%s%s>%s</span>`, attrClass, attrID, e(str))
}

func (part *Number) GetSortableRepresentation() (string, float64) {
	return "", part.Value
}

// <UL>/<OL>
// This generates an ordered or unordered list
type List struct {
	Component
	IsOrdered bool // If false, use <li>. If true, use <ol>
	ListItems []UI

	OnInitialize  func(p *List, ctx *Context) error // Can be used to add items to ListItems
	OnIncoming    func(p *List, ctx *Context) error
	OnPrepareHTML func(p *List, ctx *Context) error // You can set Html in this.
}

func (part *List) GetChildren() []UI {
	return part.ListItems
}

func (part *List) Init(ctx *Context) error {
	return nil
}

func (part *List) RunOnInitialize(htmxInfo *Context) error {
	if part.OnInitialize != nil {
		return part.OnInitialize(part, htmxInfo)
	}
	return nil
}

func (part *List) RunUserEvents(htmxInfo *Context) error {
	if part.OnIncoming != nil {
		return part.OnIncoming(part, htmxInfo)
	}
	return nil
}

func (part *List) PrepareHTML(htmxInfo *Context) error {
	var firstError error
	if part.OnPrepareHTML != nil {
		firstError = part.OnPrepareHTML(part, htmxInfo)
	}
	for _, li := range part.ListItems {
		err := li.PrepareHTML(htmxInfo)
		firstError = errors.Join(firstError, err)
	}
	return firstError
}

func (part *List) GetHTML(htmxInfo *Context) string {
	if part.Omitted {
		return ""
	}
	attrClass := ""
	if part.Class != "" {
		attrClass = fmt.Sprintf(` class="%s" `, html.EscapeString(part.Class))
	}
	partID := ""
	if part.ID != "" {
		partID = fmt.Sprintf(` id="%s"`, html.EscapeString(part.ID))
	}
	var sl []string
	for _, li := range part.ListItems {
		sl = append(sl, "<li>", li.GetHTML(htmxInfo), "</li>\n")
	}
	template := "<ul%s%s>\n%s</ul>\n"
	if part.IsOrdered {
		template = "<ol%s%s>\n%s</ol>\n"
	}
	return fmt.Sprintf(template, partID, attrClass, strings.Join(sl, ""))
}

// <BR> Linebreak - useful for inserting as a TextPart
type LineBreak struct {
	Component
	Omitted bool // If true, no HTML is generated.
}

func (part *LineBreak) GetHTML(htmxInfo *Context) string {
	if part.Omitted {
		return ""
	}
	return "<br>"
}

// <HR> horizontal ruler
type HR struct {
	Component
	Omitted bool // If true, no HTML is generated.
}

var _ UI = &HR{}

func (part *HR) GetHTML(htmxInfo *Context) string {
	if part.Omitted {
		return ""
	}
	return "<hr>"
}
