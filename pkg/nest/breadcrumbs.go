package nest

import (
	"fmt"
	"html"
	"strings"
)

var defaultBreadcrumbsPartClass = GenerateComponentClassName("breadcrumbs-part")

type BreadCrumbs struct {
	Component
	Crumbs        []BreadCrumbsPart                         // Must be set
	ClassPart     string                                    // If set, set as CSS class for each part
	SeparatorHtml string                                    // HTML to be inserted between the parts. A suitable value could be " / ".
	OnPrepareHTML func(bc *BreadCrumbs, ctx *Context) error // Can be used to configure the breadcrumb
}

type BreadCrumbsPart struct {
	Caption string
	URL     string   // If set, clicking the breadcrumb will activate a new URL
	Context *Context // If set, clicking the breadcrumb will activate this Context
}

func (acc *BreadCrumbs) Init(ctx *Context) error {
	return nil
}

func (obj *BreadCrumbs) RunOnInitialize(htmxInfo *Context) error {
	return nil
}

func (obj *BreadCrumbs) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (obj *BreadCrumbs) PrepareHTML(htmxInfo *Context) error {
	if obj.OnPrepareHTML != nil {
		return obj.OnPrepareHTML(obj, htmxInfo)
	}
	return nil
}

func (obj *BreadCrumbs) GetHTML(htmxInfo *Context) string {
	if obj.Component.Omitted {
		return ""
	}

	partClassAttr := obj.getPartClassAttribute()
	sl := make([]string, len(obj.Crumbs))
	for idx, crumb := range obj.Crumbs {
		idp := ""
		if obj.Component.ID != "" {
			idp = fmt.Sprintf(` id="%s%d" `, html.EscapeString(obj.Component.ID), idx)
		}

		attrTitle := fmt.Sprintf(` title="%s" `, html.EscapeString(crumb.Caption))

		if crumb.URL != "" {
			sl[idx] = fmt.Sprintf(`<span%s %s %s><a href="%s">%s</a></span>`, attrTitle, idp, partClassAttr, html.EscapeString(crumb.URL), crumb.Caption)
		} else if crumb.Context != nil {
			sl[idx] = fmt.Sprintf(`<span%s %s %s %s">%s</a>`, attrTitle, idp, partClassAttr, crumb.Context.GetHxAttrs(), crumb.Caption)
		} else {
			sl[idx] = fmt.Sprintf(`<span%s %s %s>%s</span>`, attrTitle, idp, partClassAttr, crumb.Caption)
		}
	}
	fullClass := ""
	if obj.Component.Class != "" {
		fullClass = fmt.Sprintf(` class="%s" `, html.EscapeString(obj.Component.Class))
	}
	idPart := ""
	if obj.Component.ID != "" {
		idPart = fmt.Sprintf(` id="%s" `, html.EscapeString(obj.Component.ID))
	}
	return fmt.Sprintf(`<span %s %s>%s</span>`, idPart, fullClass, strings.Join(sl, obj.SeparatorHtml))
}

func (bc *BreadCrumbs) getPartClassAttribute() string {
	classes := []string{defaultBreadcrumbsPartClass, bc.Component.Class}

	var escapedClasses []string
	for _, class := range classes {
		if class != "" {
			escapedClasses = append(escapedClasses, html.EscapeString(class))
		}
	}

	return fmt.Sprintf(` class="%s" `, strings.Join(escapedClasses, " "))
}
