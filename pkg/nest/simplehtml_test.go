package nest

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInterfaces(t *testing.T) {
	{
		comp := &Heading1{}
		var _ UI = comp
	}
	{
		comp := &Heading2{}
		var _ UI = comp
	}
	{
		comp := &Heading3{}
		var _ UI = comp
	}
	{
		comp := &Paragraph{}
		var _ UI = comp
		var _ SortableValueProvider = comp
		var _ UIProvider = comp
	}
	{
		comp := &Text{
			Text: "abc",
			TextParts: []UI{
				&Text{
					Text: "def",
				},
			},
		}
		var _ UI = comp
		var sprov SortableValueProvider = comp
		str, _ := sprov.GetSortableRepresentation()
		assert.Equal(t, "abc~def", str)
		var _ UIProvider = comp
	}
	{
		comp := &List{}
		var _ UI = comp
		var _ UIProvider = comp
	}
	{
		comp := &LineBreak{}
		var _ UI = comp
	}
}

func TestNumber(t *testing.T) {
	comp := &Number{
		Component: Component{ID: "b"},
		Value:     2.3,
		Decimals:  2,
	}
	var rproc UI = comp
	rproc.SetID(rproc.GetID() + "a")
	ctx := &Context{}
	ctx.Locale = NewLocale("en_US", "UTC")
	err := rproc.RunOnInitialize(ctx)
	assert.Equal(t, nil, err)
	err = rproc.RunUserEvents(ctx)
	assert.Equal(t, nil, err)
	err = rproc.RunOnInitialize(ctx)
	assert.Equal(t, nil, err)
	str := rproc.GetHTML(ctx)
	assert.Equal(t, `<span id="ba">2.30</span>`, str)
	var svp SortableValueProvider = comp
	sortStr, sortValue := svp.GetSortableRepresentation()
	assert.Equal(t, "", sortStr)
	assert.Equal(t, 2.3, sortValue)
}

func TestNumberInTextSorting(t *testing.T) {
	var items []*Text
	for number := range 20 {
		comp := &Text{
			TextParts: []UI{
				&Text{Text: "Item "},
				&Number{Value: float64(15 - number)},
			},
		}
		items = append(items, comp)
	}
	ctx := &Context{}
	assert.Equal(t, `<span><span>Item </span><span>15</span></span>`, items[0].GetHTML(ctx))
	assert.Equal(t, `<span><span>Item </span><span>1</span></span>`, items[14].GetHTML(ctx))
	assert.Equal(t, `<span><span>Item </span><span>-4</span></span>`, items[19].GetHTML(ctx))
	sort.Slice(items, func(i, j int) bool {
		str1, flt1 := items[i].GetSortableRepresentation()
		str2, flt2 := items[j].GetSortableRepresentation()
		return str1 < str2 || (str1 == str2 && flt1 < flt2)
	})
	assert.Equal(t, `<span><span>Item </span><span>-4</span></span>`, items[0].GetHTML(ctx))
	assert.Equal(t, `<span><span>Item </span><span>1</span></span>`, items[5].GetHTML(ctx))
	assert.Equal(t, `<span><span>Item </span><span>15</span></span>`, items[19].GetHTML(ctx))
}
