package nest

import (
	"testing"
)

func TestEditString(t *testing.T) {
	comp := &EditString{}
	var _ SortableValueProvider = comp
	var _ UI = comp
	var _ Editor = comp
}
