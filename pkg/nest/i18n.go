package nest

import (
	"fmt"
	"log/slog"
	"os"
	"strconv"
	"strings"
	"time"

	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

// Returns language.Tag for current environment
func GetLanguage() string {
	envLocale := os.Getenv("LANG") // LANG environment variable is commonly used on UNIX systems
	if envLocale == "" {
		return "en_US"
	}
	return envLocale
}

func NewLocale(langCode string, location string) Locale {
	loc, err2 := time.LoadLocation(location)
	if err2 != nil {
		slog.Error("Unknown location", slog.String("location", location), slog.String("msgid", "J33NSE"))
	}

	lang, err := language.Parse(langCode)
	if err != nil {
		slog.Error("Unknown language code", slog.String("langcode", langCode), slog.String("msgid", "J3KNSE"))
	}

	res := LocaleInfo{
		Language: lang,
		Location: loc,
	}

	switch langCode {
	case "da", "da_DK":
		res.Format = Locale_da_DK
	case "en", "en_US":
		res.Format = Locale_en_US
	default:
		slog.Error("Language code is not built into this library, yet.", slog.String("msgid", "JAN2NZ"))
	}
	if res.Format.NumberPrinter == nil {
		res.Format.NumberPrinter = message.NewPrinter(res.Language)
	}
	return &res
}

type LocaleInfo struct {
	Format   LocaleFormat
	Location *time.Location // Used for timezone calculations
	Language language.Tag
}

// Returns info about locale
func (loc *LocaleInfo) GetInfo() *LocaleInfo {
	return loc
}

func (loc *LocaleInfo) FormatInt(val int) string {
	return loc.Format.NumberPrinter.Sprintf("%d", val)
}

func (loc *LocaleInfo) FormatFloat(val float64, decimals int) string {
	format := "%." + strconv.Itoa(decimals) + "f"
	return loc.Format.NumberPrinter.Sprintf(format, val)
}

func (loc *LocaleInfo) FormatTimeHHMM(value time.Time) string {
	return value.Format(loc.Format.TimeFormatHHMM)
}

func (loc *LocaleInfo) FormatTimeHHMMSS(value time.Time) string {
	return value.Format(loc.Format.TimeFormatHHMMSS)
}

func (loc *LocaleInfo) FormatDateTimeYMDHMS(value time.Time) string {
	return value.Format(loc.Format.DateTimeFormatYMDHMS)
}

func (loc *LocaleInfo) FormatDurationMMSS(value time.Duration) string {
	totalSeconds := int(value.Seconds())
	minutes := totalSeconds / 60
	seconds := totalSeconds % 60
	return fmt.Sprintf(loc.Format.DurationFormatMMSS, minutes, seconds)
}

func (loc *LocaleInfo) FormatDurationMM(value time.Duration) string {
	minutes := int(value.Minutes())
	return fmt.Sprintf(loc.Format.DurationFormatMM, minutes)
}

func (loc *LocaleInfo) FormatDurationHHMM(value time.Duration) string {
	totalMinutes := int(value.Minutes())
	hours := totalMinutes / 60
	minutes := totalMinutes % 60
	return fmt.Sprintf(loc.Format.DurationFormatHHMM, hours, minutes)
}

func (loc *LocaleInfo) ParseInt(strparam string) (int, error) {
	str := strings.TrimSpace(strparam)
	str = strings.ReplaceAll(str, string(loc.Format.ThousandsSeparator), "")
	intval, err := strconv.ParseInt(str, 10, 64)
	return int(intval), err
}

func (loc *LocaleInfo) ParseFloat(str string) (float64, error) {
	specialCharacterNotFoundElsewhere := "\x01"
	str = strings.TrimSpace(str)
	str = strings.ReplaceAll(str, loc.Format.DecimalSeparator, specialCharacterNotFoundElsewhere)
	str = strings.ReplaceAll(str, loc.Format.ThousandsSeparator, "")
	str = strings.ReplaceAll(str, specialCharacterNotFoundElsewhere, ".")
	return strconv.ParseFloat(str, 64)
}

type LocaleFormat struct {
	DecimalSeparator     string
	ThousandsSeparator   string
	DateFormat           string // According to time.Time.Format()
	TimeFormatHHMM       string // According to time.Time.Format()
	TimeFormatHHMMSS     string // According to time.Time.Format()
	DateTimeFormatYMDHMS string // According to time.Time.Format()
	DurationFormatMMSS   string
	DurationFormatMM     string
	DurationFormatHHMM   string
	NumberPrinter        *message.Printer
}

var Locale_da_DK = LocaleFormat{
	DecimalSeparator:     ",",
	ThousandsSeparator:   ".",
	DateFormat:           "02.01.2006",
	TimeFormatHHMM:       "15:04",
	TimeFormatHHMMSS:     "15:04:05",
	DateTimeFormatYMDHMS: "02.01.2006 15:04:05",
	DurationFormatMMSS:   "%dm %ds",
	DurationFormatMM:     "%dm",
	DurationFormatHHMM:   "%dt %dm",
}

var Locale_en_US = LocaleFormat{
	DecimalSeparator:     ".",
	ThousandsSeparator:   ",",
	DateFormat:           "01/02/2006",
	TimeFormatHHMM:       "03:04 PM",
	TimeFormatHHMMSS:     "03:04:05 PM",
	DateTimeFormatYMDHMS: "01/02/2006 03:04:05 PM",
	DurationFormatMMSS:   "%dm %ds",
	DurationFormatMM:     "%dm",
	DurationFormatHHMM:   "%dh %dm",
}

var textTranslator func(message string) string // Set this in order to have localization on built-in messages

// Set translation mechanism for all things using this framework. Only set this once, and before any translation is needed.
func SetTextTranslator(f func(string) string) {
	if textTranslator != nil {
		slog.Error("Text translator may only be set once in an application.", slog.String("msgid", "TT28AB"))
	}
	if f == nil {
		slog.Error("Text translator must be not nil.", slog.String("msgid", "TT29AB"))
	}
	textTranslator = f
}

// Translate a text to another language using the chosen translation system
// No mutexes, assumes that the translation system never changes during execution.
func T(message string) string {
	translate := textTranslator
	if translate == nil {
		return message
	}
	return translate(message)
}
