package nest

// Implements a UI which is just custom HTML.
// Note, do not use interactive components that implement the UI interface, to produce the HTML inside this.
type CustomHtml struct {
	Component
	OnInitialize  func(control *CustomHtml, ctx *Context) error // For initializing
	OnIncoming    func(control *CustomHtml, ctx *Context) error // Use this for user event handling
	OnPrepareHTML func(control *CustomHtml, ctx *Context) error // You can set Html in this.
	Html          string                                        // This value will become the result of rendering this component.
}

func (ch *CustomHtml) Init(ctx *Context) error {
	return nil
}

func (ch *CustomHtml) RunOnInitialize(htmxInfo *Context) error {
	if ch.OnInitialize != nil {
		return ch.OnInitialize(ch, htmxInfo)
	}
	return nil
}

func (ch *CustomHtml) RunUserEvents(htmxInfo *Context) error {
	if ch.OnIncoming != nil {
		return ch.OnIncoming(ch, htmxInfo)
	}
	return nil
}

func (ch *CustomHtml) PrepareHTML(htmxInfo *Context) error {
	if ch.OnPrepareHTML != nil {
		return ch.OnPrepareHTML(ch, htmxInfo)
	}
	return nil
}

func (ch *CustomHtml) GetHTML(htmxInfo *Context) string {
	if ch.Omitted {
		return ""
	}
	return ch.Html
}
