package nest

// General purpose types used in this library

type DatasetSortMethod string // Machine-readable. Can be a field name but even if it looks like a field name, the backend may include more than one field in the sort order.
type DatasetFilter string     // Machine-readable. Usually a search text to filter records like in SQL: field like '%filter%'. May be upgraded to a more advanced data structure later.

// General-purpose type for lists of machine-readable values and corresponding human-readable title
type ValueAndTitle struct {
	Value string // Machine-readable value, e.g. "1000.23" or "123"
	Title string // Human-readable value, e.g. "1.000,23" or "Rotating"
}

// Used in various places where a subset of an array would be used
type Range struct {
	First int // Offset 0, so default is to include the first.
	Last  int // If 0, include all. Otherwise, this is the number of the last record.
}
