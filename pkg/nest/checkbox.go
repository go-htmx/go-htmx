package nest

import (
	"fmt"
	"html"
)

// Component equivalent to an <input>.
type CheckBox struct {
	Component
	Caption            string             // Appended right after the checkbox, if available
	ReadOnly           bool               // If true, the edit box becomes read/only
	Changed            bool               // true if checkbox was changed as part of current request
	Required           bool               // Must be checked
	Form               UIRegistrator      // Specify a reference to the form that this edit belongs to
	DataSource         DataSourceProvider // If connected, it will load its values from there
	DataSourceField    FieldName          // Required for DataSource
	ValidationDisabled bool               // If true, Required is not checked
	Checked            bool

	OnChange      func(cb *CheckBox, ctx *Context) error // Called right after OnSet() but only if value has changed
	OnPrepareHTML func(cb *CheckBox, ctx *Context) error // Called on load
}

func (ed *CheckBox) SetValueDefault(ctx *Context) {
	ed.SetState(ctx, false)
}

func (ed *CheckBox) GetState(ctx *Context) bool {
	ed.Checked = ctx.GetBool(ed.Component.ID)
	return ed.Checked
}

func (ed *CheckBox) SetState(ctx *Context, state bool) {
	ed.Checked = state
	ctx.SetBool(ed.Component.ID, state)
}

func (ed *CheckBox) GetValidationResult(htmxInfo *Context) error {
	if ed.ValidationDisabled {
		return nil
	}
	if ed.Required && !ed.GetState(htmxInfo) {
		return UserErrorMsg(T("Checkbox must be checked."))
	}
	return nil
}

func (ed *CheckBox) EnableValidation(enabled bool) {
	ed.ValidationDisabled = !enabled
}

func (ed *CheckBox) Init(ctx *Context) error {
	if ed.Form != nil {
		ed.Form.RegisterComponent(ed)
	}
	ed.Checked = ed.GetState(ctx)
	ed.Changed = ctx.GetBool(ed.Component.ID + "changed")
	return nil
}

func (ed *CheckBox) RunOnInitialize(ctx *Context) error {
	if ed.DataSource != nil && ed.DataSourceField != "" {
		field := ed.DataSource.GetField(ctx, ed.DataSourceField)
		if ed.DataSource.GetStateShouldFormsLoadNewData(ctx) {
			valstr := field.GetAsString()
			ed.SetState(ctx, valstr != "" && valstr != "0")
		} else {
			ed.Checked = ed.GetState(ctx)
			if ed.Checked {
				field.SetAsString("1")
			} else {
				field.SetAsString("0")
			}
		}
		if ed.DataSource.GetStateReadOnly(ctx) {
			ed.ReadOnly = true
		}
	}
	return nil
}

func (ed *CheckBox) RunUserEvents(ctx *Context) error {
	if ed.OnChange != nil {
		if ctx.GetBool(ed.ID + "changed") {
			ctx.Clear(ed.ID + "changed")
			err := ed.OnChange(ed, ctx)
			if err != nil {
				// Revert the change
				ed.SetState(ctx, !ed.GetState(ctx))
			}
			return err
		}
	}
	return nil
}

func (ed *CheckBox) PrepareHTML(htmxInfo *Context) error {
	if ed.OnPrepareHTML != nil {
		return ed.OnPrepareHTML(ed, htmxInfo)
	}
	return nil
}

func (ed *CheckBox) GetHTML(htmxInfo *Context) string {
	if ed.Component.Omitted {
		return ""
	}
	fieldname := ed.Component.ID
	attrReadOnly := ""
	if ed.ReadOnly {
		attrReadOnly = " disabled "
	}
	attrClass := ""
	if ed.Component.Class != "" {
		attrClass = fmt.Sprintf(` class="%s" `, html.EscapeString(ed.Component.Class))
	}
	attrChecked := ""
	if ed.GetState(htmxInfo) {
		attrChecked = " checked "
	}
	caption := ""
	if ed.Caption != "" {
		caption = fmt.Sprintf(`<label for="%s"%s>%s</label>`, html.EscapeString(ed.Component.ID), attrClass, html.EscapeString(ed.Caption))
	}
	htmxInfo = htmxInfo.Copy()
	htmxInfo.SetOTBool(fieldname+"changed", true)
	htmxInfo.Clear(ed.Component.ID)
	return fmt.Sprintf(`<input type="checkbox" id="%s" name="%s" value="1" %s%s%s %s>%s`,
		html.EscapeString(ed.Component.ID), html.EscapeString(fieldname), attrClass, attrReadOnly, attrChecked, htmxInfo.GetHxAttrs(), caption)
}

func (f *CheckBox) GetSortableRepresentation() (string, float64) {
	if f.Checked {
		return "", 1
	}
	return "", 0
}
