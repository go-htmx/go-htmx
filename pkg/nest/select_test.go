package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGSelect(t *testing.T) {
	comp := &Select{}
	var control UI = comp
	assert.NotEqual(t, "", control.GetHTML(&Context{}))
	var _ Editor = comp
}
