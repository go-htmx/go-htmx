package nest

import (
	"fmt"
	"strings"
)

var _ UI = &ModalDialog{}

// Inserting this into your page will make the page show a message in a modal dialog, which the user can close by clicking OK.
// This is useful for reporting the result of POST/PATCH actions sent using HTMX.
type ModalDialog struct {
	Component
	Headline string
	BodyText string
}

func (dialog *ModalDialog) GetHTML(htmxInfo *Context) string {
	var sl []string
	sl = append(sl, fmt.Sprintf(`<dialog open class="%s">`, e(dialog.Class)))
	if dialog.Headline != "" {
		sl = append(sl, fmt.Sprintf(`<h2>%s</h2>`, e(dialog.Headline)))
	}
	if dialog.BodyText != "" {
		body := ebr(dialog.BodyText)
		sl = append(sl, fmt.Sprintf(`<p>%s</p>`, body))
	}
	sl = append(sl, `<form method="dialog">`)
	sl = append(sl, fmt.Sprintf(`<button>%s</button>`, e(`OK`)))
	sl = append(sl, `</form>`)
	sl = append(sl, `</dialog>`)
	return strings.Join(sl, "\n")
}
