package nest

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"html"
	"image"
	"log/slog"

	_ "image/png"
)

// Passive component that inserts <img> tag.
// Supports inlining images into HTML.
type Image struct {
	Component
	Style       string        // Will be appended to the <img style=".."> if present
	Width       Length        // Should be set to achieve optimal performance. Is set automatically for PNG images in Images variable.
	Height      Length        // Should be set to achieve optimal performance. Is set automatically for PNG images in Images variable.
	LinkURL     string        // If set, the image becomes clickable using <a href="">
	LazyLoading bool          // If true, this image will only be loaded if it is going into the viewport (loading="lazy")
	Zoom        float64       // If specified, this factor is applied to Width and Height in the HTML
	Caption     UI            // Put below image using <figcaption>, this is an alternative for using .Alt
	Title       string        // becomes <img title="..."> if set, i.e. mouse-over text
	AltText     string        // Alternative text for accessibility, required by WCAG
	Attrs       AttributeList // Can be used to add attributes to the surrounding html tag

	// Only set either SrcURL or the Image* fields
	SrcURL        string // If set, this URL will be specified as the source for the image
	Image         []byte // If set, this will be sent inline in the HTML. Recommended for small(!) images embedded using //go:embed
	ImageMimeType string // Defaults to "image/png"

	OnInitialize  func(image *Image, ctx *Context) error
	OnClick       func(image *Image, ctx *Context) error // If set, the image becomes clickable
	OnPrepareHTML func(image *Image, ctx *Context) error
}

func (img *Image) GetChildren() []UI {
	return []UI{img.Caption}
}

func (img *Image) Init(htmxInfo *Context) error {
	return nil
}

func (img *Image) RunOnInitialize(htmxInfo *Context) error {
	if img.OnInitialize != nil {
		return img.OnInitialize(img, htmxInfo)
	}
	return nil
}

func (img *Image) RunUserEvents(htmxInfo *Context) error {
	if img.OnClick != nil && htmxInfo.GetBool(img.ID) {
		return img.OnClick(img, htmxInfo)
	}
	return nil
}

func (img *Image) PrepareHTML(htmxInfo *Context) error {
	var errList error
	if img.OnPrepareHTML != nil {
		err := img.OnPrepareHTML(img, htmxInfo)
		errList = errors.Join(errList, err)
	}
	if img.Caption != nil {
		err := img.Caption.PrepareHTML(htmxInfo)
		errList = errors.Join(errList, err)
	}
	return errList
}

func (img *Image) GetHTML(htmxInfo *Context) string {
	if img.Omitted {
		return ""
	}
	if img.Attrs == nil {
		img.Attrs = make(AttributeList)
	}
	if img.LinkURL != "" && img.OnClick != nil {
		slog.Error("Image component has both LinkURL and OnClick set", slog.String("id", img.ID))
	}
	if img.Image != nil && img.SrcURL != "" {
		slog.Error("Image component has both .Image and .SrcURL set", slog.String("id", img.ID))
	}
	if img.Image == nil && (img.Width.IsZero() || img.Height.IsZero()) {
		slog.Warn("Image component has no known size. This affects performance of showing the web page.", slog.String("id", img.ID))
	}
	if img.Image != nil {
		base64stuff := base64.StdEncoding.EncodeToString(img.Image)
		mime := img.ImageMimeType
		if mime == "" {
			mime = "image/png"
		}
		img.Attrs["src"] = fmt.Sprintf(`data:%s;base64,%s`, mime, base64stuff)
		// Auto-width and auto-height
		if mime == "image/png" && (img.Width.IsZero() || img.Height.IsZero()) {
			reader := bytes.NewReader(img.Image)
			png, _, err := image.Decode(reader)
			if err != nil {
				slog.Error("Error decoding image", slog.String("error", err.Error()), slog.String("msgid", "PNG8NA"))
				return "ERROR"
			}
			img.Width = Length{Value: float64(png.Bounds().Dx()), Unit: Pixels}
			img.Height = Length{Value: float64(png.Bounds().Dy()), Unit: Pixels}
		}
	} else if img.SrcURL != "" {
		img.Attrs["src"] = img.SrcURL
	}
	zoom := img.Zoom
	if zoom == 0 {
		zoom = 1
	}
	if !img.Width.IsZero() {
		ll := img.Width
		ll.Value *= zoom
		img.Attrs["width"] = ll.String()
	}
	if !img.Height.IsZero() {
		ll := img.Height
		ll.Value *= zoom
		img.Attrs["height"] = ll.String()
	}
	if img.AltText != "" {
		img.Attrs["alt"] = img.AltText
	}
	if img.ID != "" {
		img.Attrs["id"] = img.ID
	}
	if img.Class != "" {
		img.Attrs["class"] = img.Class
	}
	if img.Title != "" {
		img.Attrs["title"] = img.Title
	}
	attrHx := ""
	attrStyle := ""
	if img.OnClick != nil {
		htmxInfo = htmxInfo.Copy()
		htmxInfo.SetOTBool(img.ID, true)
		attrHx = htmxInfo.GetHxAttrs()
		attrStyle = `cursor:pointer; `
	} else if img.LinkURL != "" {
		attrStyle = `cursor:pointer; `
	}
	if img.Style != "" {
		attrStyle += img.Style
	}
	if attrStyle != "" {
		img.Attrs["style"] = attrStyle
	}
	if htmxInfo.RequireWCAG && (img.Attrs["alt"] == "" && img.Attrs["role"] == "" && img.Attrs["longdesc"] == "" && img.Caption == nil) {
		slog.Warn("WCAG violation: Image must have alt attribute.", slog.String("id", img.ID), slog.String("msgid", "WCAG01"))
	}
	res := fmt.Sprintf(`<img %s%s>`, img.Attrs.GetHTML(htmxInfo), attrHx)
	if img.Caption != nil {
		captionHtml := img.Caption.GetHTML(htmxInfo)
		res = fmt.Sprintf("<figure>%s<figcaption>%s</figcaption></figure>", res, captionHtml)
	}
	if img.LinkURL != "" {
		res = fmt.Sprintf(`<a href="%s">%s</a>`, html.EscapeString(img.LinkURL), res)
	}
	return res
}
