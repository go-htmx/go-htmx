package nest

import (
	"errors"
	"fmt"
	"log/slog"
	"sort"
	"strings"
)

type DataSourceState uint8

const dsUninitialized DataSourceState = 0 // Default value for variables, do not use. Is not exported from package.
const DsBrowse DataSourceState = 1        // Browsing data, no single-record activity
const DsRead DataSourceState = 2          // No record is being edited
const DsNew DataSourceState = 3           // A blank record is being edited for insertion
const DsEdit DataSourceState = 4          // An existing record is being edited

// Non-visual component. Must be on a visible panel somewhere.
type DataSource[T any] struct {
	Component
	OnInitialize func(ctx *Context, ds *DataSource[T]) error // Executed before event handlers

	SortMethods  map[DatasetSortMethod]string                                                     // Must be set in order to have sorting work
	OnGetRecords func(ctx *Context, ds *DataSource[T], filter FilterAndSort) ([]Record[T], error) // Returns array of structs. Remember to look into DataSource.GetFilter()
	OnGetSingle  func(ctx *Context, ds *DataSource[T]) (Record[T], error)                         // Returns struct with data
	OnDelete     func(ctx *Context, ds *DataSource[T]) error                                      // Must delete the specified record
	OnGetBlank   func(ctx *Context, ds *DataSource[T]) Record[T]                                  // Must return *struct with default values in it, this is the template for OnInsert()
	OnInsert     func(ctx *Context, ds *DataSource[T], record Record[T]) (PrimaryKey, error)      // Must return new primary key
	OnUpdate     func(ctx *Context, ds *DataSource[T], record Record[T]) error                    // record is the struct type used in the database

	// Private stuff
	currentPK       PrimaryKey
	currentState    DataSourceState
	currentRecord   Record[T]
	formsShouldLoad bool // If true, a form should reload data from the current record. If false, the form should keep the current content.
	isInitialized   bool // Set to true by Init()
	fields          []FieldNameAndCaption
	filter          DatasetFilter
}

type FilterAndSort struct {
	SortMethod  DatasetSortMethod // How to sort the data
	Filter      DatasetFilter     // How to filter the data
	FilterRange Range             // After sorting and filtering, which records to pick
}

func (ds *DataSource[T]) SetFilter(ctx *Context, filter DatasetFilter) {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (SetFilter)")
	}
	ds.filter = filter
	ctx.Set(ds.ID+"filter", string(ds.filter))
}

func (ds *DataSource[T]) GetFilter() DatasetFilter {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (GetFilter)")
	}
	return ds.filter
}

// This function is nice to have as the last code in OnGetRecords() if filtering and sorting is not done in the database backend
// Just feed it with a []T and 3 helper functions, and it will sort, filter and convert.
// If sorter==nil, it is assumed that the array is already sorted.
// If getStringToFilter==nil, no filtering will be done.
func (ds *DataSource[T]) DoSortFilterRangeConvert(array []T, filter FilterAndSort, sorter func(i, j int) bool, getStringToFilter func(T) string, convert func(*T) Record[T]) ([]Record[T], error) {
	if convert == nil {
		slog.Error("DoSortFilterRangeConvert requires a converter", slogCallerInfo(), slog.String("msgid", "PPQOWE"))
		return nil, nil
	}

	// Split the filter text into multiple texts
	filterTexts := strings.Split(strings.ToLower(strings.TrimSpace(string(filter.Filter))), " ")

	// Sort the array as specified
	if sorter != nil {
		sort.Slice(array, sorter)
	}

	var res []Record[T]
	counter := 0
	for _, item := range array {
		// Filter away items not matching search keywords
		if len(filterTexts) != 0 && getStringToFilter != nil {
			itemText := getStringToFilter(item)
			sourceText := strings.ReplaceAll(strings.ToLower(itemText), " ", "")
			found := true
			for _, filterItem := range filterTexts {
				if filterItem == "" {
					continue
				}
				found = strings.Contains(sourceText, filterItem)
				if !found {
					break
				}
			}
			if !found {
				continue
			}
		}

		// Only return records within the correct range
		if counter >= filter.FilterRange.First {
			if filter.FilterRange.Last != 0 && counter > filter.FilterRange.Last {
				break
			}
			// Convert to correct type
			res = append(res, convert(&item))
		}
		counter++
	}
	return res, nil
}

func (ds *DataSource[T]) DoBrowse(ctx *Context, filter DatasetFilter) error {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (DoBrowse)")
	}
	if ds.GetState() == DsBrowse && ds.filter == filter {
		return nil
	}
	ds.SetFilter(ctx, filter)
	ds.setMustLoad(ctx)
	err := ds.SetState(ctx, DsBrowse)
	if err != nil {
		return err
	}
	ctx.Restart()
	return nil
}

// Handle that the user pressed New button to create a new record
func (ds *DataSource[T]) DoNew(ctx *Context) error {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (DoNew)")
	}
	if ds.GetState() == DsNew {
		return nil
	}
	ds.setMustLoad(ctx)
	err := ds.SetState(ctx, DsNew)
	if err != nil {
		return err
	}
	ctx.Restart()
	return nil
}

// Handle that the user pressed Edit button to edit a new record
func (ds *DataSource[T]) DoEdit(ctx *Context) error {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (DoEdit)")
	}
	if ds.GetState() == DsEdit {
		return nil
	}
	ds.setMustLoad(ctx)
	err := ds.SetState(ctx, DsEdit)
	if err != nil {
		return err
	}
	ctx.Restart()
	return nil
}

// Handle that the user pressed Edit button to edit a new record
func (ds *DataSource[T]) DoSave(ctx *Context) error {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (DoSave)")
	}
	state := ds.GetState()
	switch state {
	case DsNew:
		if ds.OnInsert == nil {
			return errors.New("onInsert is not implemented")
		}
		if ds.currentRecord.Data == nil {
			slog.Error("OrgData is not specified for record to insert", slog.String("msgid", "45YA92"))
			return errors.New("orgdata is not implemented, yet")
		} else {
			newpk, err := ds.OnInsert(ctx, ds, ds.currentRecord)
			if err != nil {
				return err
			}
			err = ds.setStateAndPrimaryKey(ctx, DsRead, newpk)
			if err != nil {
				return err
			}
			ctx.Restart()
			return nil
		}
	case DsEdit:
		if ds.OnUpdate == nil {
			return errors.New("onUpdate is not implemented")
		}
		err := ds.OnUpdate(ctx, ds, ds.currentRecord)
		if err != nil {
			return err
		}
		err = ds.setStateAndPrimaryKey(ctx, DsRead, ds.currentPK)
		if err != nil {
			return err
		}
		ctx.Restart()
		return nil
	default:
		slog.Error("DoSave was called but state was not in edit mode", slog.Int("state", int(ds.currentState)), slog.String("msgid", "KJAETV"))
	}

	return errors.New("illegal state")
}

// Handle that the user pressed Cancel button to stop editing a record
func (ds *DataSource[T]) DoCancel(ctx *Context) error {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (DoCancel)")
	}
	if ds.GetCurrentPrimaryKey() != nil {
		if ds.GetState() == DsRead {
			return nil
		}
		err := ds.SetState(ctx, DsRead)
		if err != nil {
			return err
		}
	} else {
		if ds.GetState() == DsBrowse {
			return nil
		}
		err := ds.SetState(ctx, DsBrowse)
		if err != nil {
			return err
		}
	}
	ctx.Restart()
	return nil
}

// Handle that the user pressed Delete button to delete a record
func (ds *DataSource[T]) DoDelete(ctx *Context) error {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (DoDelete)")
	}
	if ds.GetCurrentPrimaryKey() == nil {
		return errors.New("no primary key when trying to delete current record")
	}
	err := ds.OnDelete(ctx, ds)
	if err != nil {
		return err
	}
	// This will clear the primary key
	err = ds.SetState(ctx, DsBrowse)
	if err != nil {
		return err
	}
	ctx.Restart()
	return nil
}

func (ds *DataSource[T]) GetStateReadOnly(ctx *Context) bool {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (GetStateReadOnly)")
	}
	state := ds.GetState()
	writemode := (state == DsEdit || state == DsNew)
	return !writemode
}

// If true, a form should reload data from the current record. If false, the form should keep the current content.
func (ds *DataSource[T]) GetStateShouldFormsLoadNewData(ctx *Context) bool {
	return ds.formsShouldLoad
}

func (ds *DataSource[T]) GetField(ctx *Context, fieldName FieldName) FieldValue {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (DataSource.GetField called from somewhere)")
		panic("DataSource must come before other things in the UI")
	}
	field, found := ds.currentRecord.Fields[fieldName]
	if !found {
		panic(fmt.Sprintf("Datasource field \"%s\" is not set", fieldName))
	}
	return field
}

func (ds *DataSource[T]) CanDoBrowse(ctx *Context) bool {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (CanDoBrowse)")
	}
	return ds.GetState() != DsBrowse
}

// Returns false if a delete button should not be made available to the enduser
func (ds *DataSource[T]) CanDoDelete(ctx *Context) bool {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (CanDoDelete)")
	}
	state := ds.GetState()
	return ds.OnDelete != nil && state == DsRead
}

// Returns false if a new button should not be made available to the enduser
func (ds *DataSource[T]) CanDoNew(ctx *Context) bool {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (CanDoNew)")
	}
	state := ds.GetState()
	return ds.OnInsert != nil && (state == DsBrowse || state == DsRead)
}

// Returns false if an edit button should not be made available to the enduser
func (ds *DataSource[T]) CanDoEdit(ctx *Context) bool {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (CanDoEdit)")
	}
	state := ds.GetState()
	return ds.OnUpdate != nil && state != DsBrowse && state != DsNew && state != DsEdit
}

// Returns false if a cancel button should not be made available to the enduser
func (ds *DataSource[T]) CanDoCancel(ctx *Context) bool {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (CanDoCancel)")
	}
	state := ds.GetState()
	return (state == DsEdit) || (state == DsNew)
}

// Returns false if a cancel button should not be made available to the enduser
func (ds *DataSource[T]) CanDoSave(ctx *Context) bool {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (CanDoSave)")
	}
	state := ds.GetState()
	return (state == DsNew) || (state == DsEdit)
}

func (ds *DataSource[T]) GetContextWithNewState(ctx *Context, state DataSourceState, newPK PrimaryKey) *Context {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (GetContextWithNewState)")
	}
	newhtmxinfo := ctx.Copy()
	newhtmxinfo.Set(ds.ID+"sel", newPK.GetAsString())
	newhtmxinfo.SetInt(ds.ID+"state", int(state))
	return newhtmxinfo

}

// Returns current record as primary key.
// If nil, the current record may be something being entered by the user
func (ds *DataSource[T]) GetCurrentPrimaryKey() PrimaryKey {
	return ds.currentPK
}

// Assumes that state has higher priority over newPK
// caller is for debugging only
func (ds *DataSource[T]) setStateAndPrimaryKey(ctx *Context, newState DataSourceState, newPK PrimaryKey) error {
	if newState == ds.currentState && newPK == ds.currentPK {
		return nil
	}

	// Handle state changes
	switch newState {
	case DsBrowse:
		newPK = nil
	case DsRead:
		if newPK == nil {
			return errors.New("cannot read without a primary key value")
		}
	case DsNew:
		newPK = nil
	case DsEdit:
		if newPK == nil {
			return errors.New("cannot go into edit mode without selected data")
		}
	default:
		slog.Error("Invalid state", slog.Int("state", int(newState)), slog.String("msgid", "JK2SQ9"))
	}

	// On success, save new state
	oldPK := ds.currentPK
	oldState := ds.currentState
	ds.currentState = newState
	ds.currentPK = newPK
	if ds.currentPK == nil {
		ctx.Clear(ds.ID + "sel")
	} else {
		ctx.Set(ds.ID+"sel", ds.currentPK.GetAsString())
	}
	ctx.SetIntNonZero(ds.ID+"state", int(ds.currentState))

	var err error
	// Retrieve new record if required
	if newPK == nil {
		ds.currentRecord = Record[T]{}
	} else {
		if newPK != ds.currentPK {
			var rec Record[T]
			rec, err = ds.OnGetSingle(ctx, ds)
			if err == nil {
				ds.currentRecord = rec
			}
		}
	}

	// Rollback if error
	if err != nil {
		ds.currentState = oldState
		ds.currentPK = oldPK
		return err
	}

	return nil
}

// Sets the current record to the specified item.
// if parameter is nil, the current record is considered to be a blank unlisted record maybe under creation
func (ds *DataSource[T]) SetCurrentPrimaryKey(ctx *Context, pk PrimaryKey) error {
	if pk == ds.currentPK {
		return nil
	}
	if pk != nil {
		return ds.setStateAndPrimaryKey(ctx, DsRead, pk)
	} else if ds.currentState != DsNew {
		return ds.setStateAndPrimaryKey(ctx, DsBrowse, nil)
	}
	return nil
}

func (ds *DataSource[T]) GetState() DataSourceState {
	return ds.currentState
}

// Tries to switch state to new state, but it may return an error, like if you try to enter an edit state on a read/only dataset
func (ds *DataSource[T]) SetState(ctx *Context, state DataSourceState) error {
	if state == ds.currentState {
		return nil
	}
	return ds.setStateAndPrimaryKey(ctx, state, ds.GetCurrentPrimaryKey())
}

func (ds *DataSource[T]) GetSortMethods() map[DatasetSortMethod]string {
	return ds.SortMethods
}

func (ds *DataSource[T]) GetFieldList() []FieldNameAndCaption {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (GetFieldList)")
	}
	return ds.fields
}

func (ds *DataSource[T]) GetRecord(ctx *Context) (RecordAccessor, error) {
	if ds.currentPK == nil {
		rec := ds.GetNewBlankRecord(ctx)
		return &rec, nil
	}
	if ds.OnGetSingle != nil {
		rec, err := ds.OnGetSingle(ctx, ds)
		return &rec, err
	}
	if ds.OnGetRecords != nil {
		arr, err := ds.OnGetRecords(ctx, ds, FilterAndSort{})
		if err != nil {
			return nil, err
		}
		for _, rec := range arr {
			if rec.PrimaryKey.GetRecordIdentifier() == ds.currentPK.GetRecordIdentifier() {
				return &rec, nil
			}
		}
	}
	return nil, errors.New("no record by this ID")
}

func (ds *DataSource[T]) GetRecords(ctx *Context, sortMethod DatasetSortMethod, firstRecord int, lastRecord int) ([]RecordAccessor, error) {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (GetRecords)")
	}
	if ds.OnGetRecords == nil {
		return nil, errors.New("OnGetRecords is not implemented")
	}
	recList, err := ds.OnGetRecords(ctx, ds, FilterAndSort{
		SortMethod: sortMethod,
		Filter:     ds.GetFilter(),
		FilterRange: Range{
			First: firstRecord,
			Last:  lastRecord,
		},
	})
	if err != nil {
		return nil, err
	}
	var res []RecordAccessor
	for _, rec := range recList {
		res = append(res, &rec)
	}
	return res, nil
}

func (ds *DataSource[T]) GetNewBlankRecord(ctx *Context) Record[T] {
	if ds.OnGetBlank == nil {
		slog.Error("DataSource.OnGetBlank is not set before calling it")
		return Record[T]{}
	}
	err := ds.SetCurrentPrimaryKey(ctx, nil)
	if err != nil {
		slog.Error("Setting primary key failed", slog.String("msgid", "O45UH1"))
	}
	return ds.OnGetBlank(ctx, ds)
}

// Inserts record and if successful, it becomes the new current record
func (ds *DataSource[T]) InsertRecord(ctx *Context, rec Record[T]) error {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (InsertRecord)")
	}
	if ds.OnInsert == nil {
		return errors.New("DataSource.InsertRecord requires OnInsert to be set")
	}
	pk, err := ds.OnInsert(ctx, ds, rec)
	if err != nil {
		return err
	}
	ds.SetCurrentPrimaryKey(ctx, pk)
	return nil
}

func (ds *DataSource[T]) UpdateRecord(ctx *Context, rec Record[T]) error {
	if !ds.isInitialized {
		slog.Error("DataSource must come before other things in the UI (UpdateRecord)")
	}
	if ds.OnUpdate == nil {
		return errors.New("DataSource.UpdateRecord requires OnUpdate to be set")
	}
	return ds.OnUpdate(ctx, ds, rec)
}

// Separate function in order to make debugging easier
func (ds *DataSource[T]) setMustLoad(ctx *Context) {
	ctx.SetOTBool(ds.ID+"load", true)
}

func (ds *DataSource[T]) Init(ctx *Context) error {
	ds.filter = DatasetFilter(ctx.Get(ds.ID + "filter"))

	ds.formsShouldLoad = ctx.GetBool(ds.ID + "load")
	ctx.Clear(ds.ID + "load")

	return nil
}

func (ds *DataSource[T]) RunOnInitialize(ctx *Context) error {
	newState := DataSourceState(ctx.GetInt(ds.ID + "state"))
	if newState == dsUninitialized {
		newState = DsBrowse
	}
	if !ctx.GetBool(ds.ID + "stick") {
		// We cannot keep the edit or new states if one-time parameters are lost due to navigation
		switch newState {
		case DsEdit:
			newState = DsRead
		case DsNew:
			newState = DsBrowse
		}
	}

	// Set a one-time parameter to ensure, that we only stay in New and Edit modes as long as one-time parameters from this UI sticks
	ctx.SetOTBool(ds.ID+"stick", true)

	if newState == DsRead {
		ds.formsShouldLoad = true
	}
	var err error
	var newPK PrimaryKey
	newPKvalue := ctx.Get(ds.ID + "sel")
	if newPKvalue != "" {
		blank := ds.OnGetBlank(ctx, ds)
		newPK = blank.PrimaryKey
		newPK.SetAsString(newPKvalue)
	}
	if newPK != nil && newState == DsBrowse {
		newState = DsRead
	}
	err = ds.setStateAndPrimaryKey(ctx, newState, newPK)
	if err != nil {
		return err
	}

	if ds.OnInitialize != nil {
		err = ds.OnInitialize(ctx, ds)
		if err != nil {
			return err
		}
	}

	// Ensure we have a valid field list
	if len(ds.fields) == 0 {
		emptyRecord := ds.OnGetBlank(ctx, ds)
		for fieldname := range emptyRecord.Fields {
			ds.fields = append(ds.fields, FieldNameAndCaption{
				FieldName: fieldname,
				Caption:   string(fieldname),
			})
		}
		sort.Slice(ds.fields, func(i, j int) bool {
			return ds.fields[i].FieldName < ds.fields[j].FieldName
		})
	}

	// Some data must be set after OnInitialize
	if ds.currentPK == nil {
		ds.currentRecord = ds.GetNewBlankRecord(ctx)
	} else {
		rec, err := ds.OnGetSingle(ctx, ds)
		if err != nil {
			return err
		}
		ds.currentRecord = rec
	}
	ds.isInitialized = true
	return nil
}
