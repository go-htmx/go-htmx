package nest

type DatasetPanel struct {
	Component
	DataSource              DataSourceProvider // Provides all the data in the system
	DataExport              Exporter           // Handles data export form the data source
	ShowOnlyCancelSaveOrNot bool               // If true, either only Cancel and Save are shown, or they are not shown. Useful if UI shows either table or form but not both
	ShowSearch              bool               // If true, a search input will be shown, too
	ShowExport              bool               // If true, an export button will be shown

	// Private stuff
	ui             *Panel
	btnNew         *Button
	btnEdit        *Button
	btnDelete      *Button
	btnBrowse      *Button
	btnCancel      *Button
	btnSave        *Button
	btnExport      *Button
	editSearch     *EditString
	panelSearch    *Panel
	btnSearch      *Button
	btnSearchClear *Button
	download       *Download

	OnInitialize func(ctx *Context, panel *DatasetPanel) error
}

var _ UI = &DatasetPanel{}
var _ UIProvider = &DatasetPanel{}

func (panel *DatasetPanel) GetChildren() []UI {
	if panel.ui == nil {
		return nil
	}
	return []UI{panel.ui}
}

func (panel *DatasetPanel) Init(ctx *Context) error {
	panel.btnNew = &Button{
		Component: Component{ID: panel.ID + "new"},
		Caption:   T("New"),
		OnInitialize: func(button *Button, ctx *Context) error {
			button.Disabled = !panel.DataSource.CanDoNew(ctx)
			return nil
		},
		OnClick: func(button *Button, ctx *Context) error {
			return panel.DataSource.DoNew(ctx)
		},
	}
	panel.btnEdit = &Button{
		Component: Component{ID: panel.ID + "edit"},
		Caption:   T("Edit"),
		OnInitialize: func(button *Button, ctx *Context) error {
			button.Disabled = !panel.DataSource.CanDoEdit(ctx)
			return nil
		},
		OnClick: func(button *Button, ctx *Context) error {
			return panel.DataSource.DoEdit(ctx)
		},
	}
	panel.btnDelete = &Button{
		Component:            Component{ID: panel.ID + "delete"},
		Caption:              T("Delete"),
		ConfirmationQuestion: T("Are you sure?"),
		OnInitialize: func(button *Button, ctx *Context) error {
			button.Disabled = !panel.DataSource.CanDoDelete(ctx)
			return nil
		},
		OnClick: func(button *Button, ctx *Context) error {
			return panel.DataSource.DoDelete(ctx)
		},
	}
	panel.btnBrowse = &Button{
		Component: Component{ID: panel.ID + "browse"},
		Caption:   T("Browse"),
		OnInitialize: func(button *Button, ctx *Context) error {
			button.Disabled = !panel.DataSource.CanDoBrowse(ctx)
			return nil
		},
		OnClick: func(button *Button, ctx *Context) error {
			return panel.DataSource.DoBrowse(ctx, DatasetFilter(panel.editSearch.GetValue(ctx)))
		},
	}
	panel.btnCancel = &Button{
		Component: Component{ID: panel.ID + "cancel"},
		Caption:   T("Cancel"),
		OnInitialize: func(button *Button, ctx *Context) error {
			button.Disabled = !panel.DataSource.CanDoCancel(ctx)
			return nil
		},
		OnClick: func(button *Button, ctx *Context) error {
			return panel.DataSource.DoCancel(ctx)
		},
	}
	panel.btnSave = &Button{
		Component: Component{ID: panel.ID + "save"},
		Caption:   T("Save"),
		OnInitialize: func(button *Button, ctx *Context) error {
			button.Disabled = !panel.DataSource.CanDoSave(ctx)
			return nil
		},
		OnClick: func(button *Button, ctx *Context) error {
			return panel.DataSource.DoSave(ctx)
		},
	}
	panel.btnExport = &Button{
		Component: Component{ID: panel.ID + "export"},
		Caption:   T("Export"),
		OnInitialize: func(button *Button, ctx *Context) error {
			button.Omitted = !panel.ShowExport
			return nil
		},
		OnClick: func(button *Button, ctx *Context) error {
			panel.download.Enabled = true
			return nil
		},
	}
	panel.editSearch = &EditString{
		Component:   Component{ID: panel.ID + "search"},
		Placeholder: T("Search"),
	}
	panel.btnSearch = &Button{
		Component: Component{ID: panel.ID + "searchbtn"},
		Caption:   T("Search"),
		OnClick: func(button *Button, ctx *Context) error {
			return panel.DataSource.DoBrowse(ctx, DatasetFilter(panel.editSearch.GetValue(ctx)))
		},
	}
	panel.btnSearchClear = &Button{
		Component: Component{ID: panel.ID + "searchclear"},
		Caption:   T("Clear"),
		OnClick: func(button *Button, ctx *Context) error {
			panel.editSearch.SetValue(ctx, "")
			return panel.DataSource.DoBrowse(ctx, "")
		},
	}
	panel.panelSearch = &Panel{
		Style:      PanelStyle{AvoidSpacing: true},
		Horizontal: true,
		Children: []UI{
			panel.editSearch,
			panel.btnSearch,
			panel.btnSearchClear,
		},
		OnInitialize: func(pan *Panel, ctx *Context) error {
			pan.Omitted = !panel.ShowSearch
			return nil
		},
	}
	panel.download = &Download{
		OnDownload: func(comp *Download, ctx *Context) (ContextFrameworkFileDownload, error) {
			records, err := panel.DataSource.GetRecords(ctx, DatasetSortMethod(panel.DataSource.GetFilter()), 0, 0)
			if err != nil {
				return ContextFrameworkFileDownload{}, err
			}

			if panel.DataExport == nil {
				panel.DataExport = &DataExportCSV{Filename: "download"}
			}

			filterDescription := ""
			if panel.DataSource.GetFilter() != "" {
				filterDescription = "Filter was used in Export. Not all data is present."
			}

			return panel.DataExport.Export(records, filterDescription)
		},
	}
	panel.ui = &Panel{
		Horizontal: true,
		Children: []UI{
			panel.btnBrowse,
			panel.btnNew,
			panel.btnEdit,
			panel.btnDelete,
			panel.btnCancel,
			panel.btnSave,
			panel.btnExport,
			panel.panelSearch,
			panel.download,
		},
	}
	ctx.InitNew(panel, panel.ui)
	return nil
}

func (panel *DatasetPanel) RunOnInitialize(ctx *Context) error {
	if panel.OnInitialize != nil {
		return panel.OnInitialize(ctx, panel)
	}
	return nil
}

func (panel *DatasetPanel) RunUserEvents(ctx *Context) error {
	return nil
}

func (panel *DatasetPanel) PrepareHTML(ctx *Context) error {
	if panel.ShowOnlyCancelSaveOrNot {
		panel.btnCancel.Omitted = panel.btnCancel.Disabled && panel.btnSave.Disabled
		panel.btnSave.Omitted = panel.btnCancel.Omitted
		panel.btnNew.Omitted = !panel.btnCancel.Omitted
		panel.btnEdit.Omitted = panel.btnNew.Omitted
		panel.btnDelete.Omitted = panel.btnNew.Omitted
		panel.panelSearch.Omitted = panel.btnEdit.Omitted || panel.panelSearch.Omitted
	}
	return panel.ui.PrepareHTML(ctx)
}

func (panel *DatasetPanel) GetHTML(ctx *Context) string {
	return panel.ui.GetHTML(ctx)
}
