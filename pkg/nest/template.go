package nest

import (
	"bytes"
	"errors"
	"html/template"
)

// Template system for quick embedding of interactive components in custom HTML.
// Example template:
//   <html><body><h1>{{.heading}}</h1><p>{{:mybutton1}}</body></html>

// Implements custom HTML with embedded components
type Template struct {
	Component
	OnInitialize  func(control *Template, ctx *Context) error // Called on RunOnInitialize()
	OnIncoming    func(control *Template, ctx *Context) error // Called on RunUserEvents()
	OnPrepareHTML func(control *Template, ctx *Context) error // You can set Html in this.
	Template      *template.Template                          // Pre-parsed template
	Html          string                                      // HTML template. Use {{.Name}} to insert values from context. Use {{.Name}} to insert a component from the Components array
	Children      map[string]UI                               // Maps componentname to components that can be inserted into the HTML
	Data          map[string]any                              // Contains any data that is other than UI and thus doesn't need to be pre-processed before inserting into the template
	parsedtempl   string
}

func (ch *Template) Init(ctx *Context) error {
	return nil
}

func (ch *Template) RunOnInitialize(htmxInfo *Context) error {
	if ch.OnInitialize != nil {
		return ch.OnInitialize(ch, htmxInfo)
	}
	return nil
}

func (ch *Template) RunUserEvents(htmxInfo *Context) error {
	if ch.OnIncoming != nil {
		return ch.OnIncoming(ch, htmxInfo)
	}
	return nil
}

func (ch *Template) GetChildren() []UI {
	var list []UI
	for _, comp := range ch.Children {
		list = append(list, comp)
	}
	return list
}

// This is the error-handled part of building the HTML
func (ch *Template) PrepareHTML(htmxInfo *Context) error {
	if ch.OnPrepareHTML != nil {
		err := ch.OnPrepareHTML(ch, htmxInfo)
		if err != nil {
			return err
		}
	}
	if ch.Omitted {
		return nil
	}
	var err error
	for _, comp := range ch.Children {
		err = errors.Join(err, comp.PrepareHTML(htmxInfo))
	}
	if err != nil {
		return err
	}

	data := make(map[string]any)
	for key, value := range ch.Data {
		data[key] = value
	}
	for compName, comp := range ch.Children {
		data[compName] = template.HTML(comp.GetHTML(htmxInfo))
	}

	var templ *template.Template
	if ch.Template != nil {
		templ = ch.Template
	} else {
		templ, err = template.New("").Parse(ch.Html)
		if err != nil {
			return err
		}
	}
	var buf bytes.Buffer
	err = templ.Execute(&buf, data)
	if err != nil {
		return err
	}
	ch.parsedtempl = buf.String()
	return nil
}

// This is about concatenating the HTML strings
func (ch *Template) GetHTML(htmxInfo *Context) string {
	if ch.Omitted {
		return ""
	}

	return ch.parsedtempl
}
