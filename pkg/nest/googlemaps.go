package nest

import (
	"fmt"
	"net/url"
)

// Implements a UI which is just custom HTML
type GoogleMap struct {
	Component
	ApiKey        string // Provided by Google
	Signature     string // Your app signature to be verified by Google
	Width, Height int    // E.g. "300"
	InitialZoom   int    //  Zoom levels between 0 (the lowest zoom level, in which the entire world can be seen on one map) and 21+ (down to streets and individual buildings) are possible within the default roadmap view. Building outlines, where available, appear on the map around zoom level 17.
	Query         string // What to show, e.g. an address
	Interactive   bool
	MapType       int // 0=roadmap, 1=satellite, 2=terrain, 3=hybrid
	OnInitialize  func(ctx *Context) error
}

func (m *GoogleMap) Init(ctx *Context) error {
	return nil
}

func (m *GoogleMap) RunOnInitialize(ctx *Context) error {
	if m.OnInitialize != nil {
		return m.OnInitialize(ctx)
	}
	return nil
}

func (m *GoogleMap) RunUserEvents(ctx *Context) error {
	return nil
}

func (m *GoogleMap) PrepareHTML(ctx *Context) error {
	return nil
}

func (m *GoogleMap) GetHTML(htmxInfo *Context) string {
	if m.Omitted {
		return ""
	}

	if m.Interactive {
		url := fmt.Sprintf(`https://www.google.com/maps/embed/v1/place?key=%s&q=%s`, url.QueryEscape(m.ApiKey), url.QueryEscape(m.Query))
		htmlstr := fmt.Sprintf(`<iframe width="%d" height="%d" style="border:0"
		loading="lazy" allowfullscreen referrerpolicy="no-referrer-when-downgrade"
		src="%s"></iframe>`, m.Width, m.Height, e(url))
		return htmlstr
	}

	mapType := "roadmap"
	switch m.MapType {
	case 1:
		mapType = "satellite"
	case 2:
		mapType = "terrain"
	case 3:
		mapType = "hybrid"
	}

	url := fmt.Sprintf(`https://maps.googleapis.com/maps/api/staticmap?center=%s&maptype=%s&zoom=%d&size=%dx%d&key=%s&signature=%s`,
		url.QueryEscape(m.Query), url.QueryEscape(mapType), m.InitialZoom, m.Width, m.Height, url.QueryEscape(m.ApiKey), url.QueryEscape(m.Signature))
	htmlstr := fmt.Sprintf(`<img width="%d" height="%d" style="border:0"
	loading="lazy" allowfullscreen referrerpolicy="no-referrer-when-downgrade"
	src="%s"></iframe>`, m.Width, m.Height, e(url))
	return htmlstr

}
