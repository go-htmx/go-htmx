package nest

import (
	"fmt"
)

// Deprecated
type GCheckBox struct {
	BoolRef *bool
}

func (f *GCheckBox) GetHtmlDisplay(Locale) string {
	if f.BoolRef == nil {
		return "N/A"
	}
	if *f.BoolRef {
		return "✓"
	}
	return "-"
}

func (f *GCheckBox) GetHtmlEdit(idName, fieldName, extraAttrs string, ReadOnly bool) string {
	checked := ""
	if f.BoolRef != nil && *f.BoolRef {
		checked = " checked "
	}
	if ReadOnly {
		extraAttrs += " readonly "
	}
	return fmt.Sprintf(`<input type="checkbox" style="cursor:pointer;" id="%s" name="%s" %s %s>`, idName, fieldName, extraAttrs, checked)
}

func (f *GCheckBox) SetHtmlValue(value string) error {
	if value == "" {
		*f.BoolRef = false
		return nil
	}
	*f.BoolRef = true
	return nil
}

func (f *GCheckBox) GetSortableRepresentation() (string, float64) {
	if f.BoolRef == nil {
		return "-", 0
	}
	if *f.BoolRef {
		return "", 1
	}
	return "", 0
}
