package nest

import (
	"fmt"
	"log/slog"
	"strings"
)

func recurseInit(ctx *Context, comp UI) {
	if comp == nil {
		return
	}

	// Only initialize if not already initialized
	doInitialize := true
	compGet, ok := comp.(ComponentGetter)
	if ok {
		embComp := compGet.GetComponent()
		if embComp.DisableInitialization {
			return
		}
		if embComp.initStage >= ctx.Framework.initPhase {
			doInitialize = false
		}
		embComp.initStage = ctx.Framework.initPhase
	}

	// Do initialization
	if doInitialize {
		err := comp.Init(ctx)
		if err != nil {
			ctx.AddErrorMessageForUser(err.Error())
			slog.Warn("Error during Init of components", slog.String("error", err.Error()), slog.String("msgid", "J2C92C"))
		}
	}

	// Initialize subcomponents
	prov, ok := comp.(UIProvider)
	if ok {
		list := prov.GetChildren()
		for _, item := range list {
			recurseInit(ctx, item)
		}
	}
}

func recurseRunOnInitialize(ctx *Context, comp UI) {
	if comp == nil {
		return
	}

	// Only initialize if not already initialized
	doInitialize := true
	compGet, ok := comp.(ComponentGetter)
	if ok {
		embComp := compGet.GetComponent()
		if embComp.DisableInitialization {
			return
		}
		if embComp.initStage >= ctx.Framework.initPhase {
			doInitialize = false
		}
		embComp.initStage = ctx.Framework.initPhase
	}

	// Do initialization
	if doInitialize {
		err := comp.RunOnInitialize(ctx)
		if err != nil {
			ctx.AddErrorMessageForUser(err.Error())
			slog.Warn("Error during RunOnInitialize of components", slog.String("error", err.Error()), slog.String("msgid", "J2C92A"))
		}
	}

	// Initialize subcomponents
	prov, ok := comp.(UIProvider)
	if ok {
		list := prov.GetChildren()
		for _, item := range list {
			recurseRunOnInitialize(ctx, item)
		}
	}
}

func recurseCallUserEvents(ctx *Context, comp UI) {
	if comp == nil {
		return
	}

	// Only initialize if not already initialized
	doInitialize := true
	compGet, ok := comp.(ComponentGetter)
	if ok {
		embComp := compGet.GetComponent()
		if embComp.DisableInitialization {
			return
		}
		if embComp.initStage >= ctx.Framework.initPhase {
			doInitialize = false
		}
		embComp.initStage = ctx.Framework.initPhase
	}

	// Call
	if doInitialize {
		err := comp.RunUserEvents(ctx)
		if err != nil {
			ctx.AddErrorMessageForUser(err.Error())
			slog.Warn("Error during RunUserEvents of components", slog.String("error", err.Error()), slog.String("msgid", "J2C92B"))
		}
	}

	// Recuse into subcomponents
	prov, ok := comp.(UIProvider)
	if ok {
		list := prov.GetChildren()
		for _, item := range list {
			recurseCallUserEvents(ctx, item)
		}
	}
}

// New initializes the component.
// Used for adding components dynamically to a UI.
// Parent can be nil, but then component must have an ID.
func (ctx *Context) InitNew(parent UIProvider, component UI) {
	if ctx.Framework.initPhase != stageRunOnInitialize && ctx.Framework.initPhase != stageInit {
		slog.Error("Function called from wrong phase in UI building. Dynamic components must be created in RunOnInitialize.",
			slogCallerInfo(), slog.String("msgid", "2JAHR1"))
	}
	if component.GetID() == "" {
		if parent == nil {
			slog.Error("InitNew called with nil parent and a component that has no ID", slogCallerInfo(), slog.String("msgid", "WA3ZJX"))
		} else {
			parentID := parent.GetID()
			if parentID == "" {
				slog.Error("InitNew called with parent that has no ID", slogCallerInfo(), slog.String("msgid", "WA3ZJY"))
			} else {
				// Make list of existing IDs
				list := parent.GetChildren()
				idlist := make(map[string]bool, len(list))
				for _, item := range list {
					idlist[item.GetID()] = true
				}

				// Find new ID
				counter := 1
				newID := ""
				for {
					idCandidate := fmt.Sprintf(`%s%d_`, parentID, counter)
					_, found := idlist[idCandidate]
					if !found {
						newID = idCandidate
						break
					}
					counter++
				}

				component.SetID(newID)
				setDefaultIDsPart(component, strings.TrimSuffix(newID, "_"))
			}
		}
	}

	if ctx.Framework.initPhase >= stageInit {
		recurseInit(ctx, component)
	}
	if ctx.Framework.initPhase >= stageRunOnInitialize {
		recurseRunOnInitialize(ctx, component)
	}
}
