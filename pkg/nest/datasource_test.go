package nest

import (
	"testing"
)

func TestDataSource(t *testing.T) {
	type mytype struct{ a, b int }
	comp := &DataSource[mytype]{}
	var _ DataSourceProvider = comp // Required for interaction with data aware components
	var _ UI = comp                 // Required for the component to save/load data to/from the browser
}
