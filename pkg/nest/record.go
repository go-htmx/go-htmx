package nest

import (
	"log/slog"
	"reflect"
)

type FieldName string

type FieldNameAndCaption struct {
	FieldName FieldName
	Caption   string
}

type Record[T any] struct {
	Data       *T                       // Points to struct that contains the actual data
	PrimaryKey PrimaryKey               // Value of primary key
	Fields     map[FieldName]FieldValue // Editors for OrgData
}

func (record *Record[T]) GetPrimaryKey() PrimaryKey {
	return record.PrimaryKey
}

func (record *Record[T]) GetFieldValue(fieldName FieldName) FieldValue {
	return record.Fields[fieldName]
}

func (record *Record[T]) GetFieldNames() []FieldName {
	var res []FieldName
	for fieldName := range record.Fields {
		res = append(res, fieldName)
	}
	return res
}

type RecordAccessor interface {
	GetPrimaryKey() PrimaryKey
	GetFieldValue(FieldName) FieldValue
	GetFieldNames() []FieldName
}

// Action for a database record, e.g. "copy", "plan", "view" og something else. Can be used by tables or forms.
type RecordAction struct {
	Caption              string
	ConfirmationQuestion string
	IsVisible            func(ctx *Context, action *RecordAction, Record RecordAccessor) bool  // Return true if action should be visible for this record. If absent, it is considered visible.
	OnClick              func(ctx *Context, action *RecordAction, Record RecordAccessor) error // Is called if the action is activated for the selected record
}

// Adds members of struct as fields using reflect library.
func StructToFieldValues(str any) map[FieldName]FieldValue {
	// Get the reflection type and value of the struct.
	val := reflect.ValueOf(str).Elem()

	// Ensure the value is a struct
	valKind := val.Kind()
	if valKind != reflect.Struct {
		slog.Error("Expected a struct in convertStructToRecord", slog.Int("kind", int(valKind)))
		return nil
	}

	rec := make(map[FieldName]FieldValue)
	// Iterate through the fields of the struct.
	for i := 0; i < val.NumField(); i++ {
		field := val.Field(i)
		fieldType := val.Type().Field(i)
		fieldName := fieldType.Name

		var newField FieldValue

		if field.CanAddr() {
			fieldKind := field.Kind()
			switch fieldKind {
			case reflect.String:
				strvalptr := field.Addr().Interface().(*string)
				newField = &ValueStr{StrRef: strvalptr}
			case reflect.Int:
				intf := field.Addr().Interface()
				intvalptr, ok := intf.(*int)
				if ok {
					newField = &ValueInt{IntRef: intvalptr}
				}
			case reflect.Float64:
				floatf := field.Addr().Interface()
				floatptr, ok := floatf.(*float64)
				if ok {
					newField = &ValueFloat{FloatRef: floatptr}
				}
			case reflect.Bool:
				boolf := field.Addr().Interface()
				boolptr, ok := boolf.(*bool)
				if ok {
					newField = &ValueBool{BoolRef: boolptr}
				}
			default:
				slog.Warn("Field has unrecognized type", slog.String("fieldname", fieldName), slog.String("msgid", "LSHG24"))
			}
		}
		rec[FieldName(fieldName)] = newField
	}
	return rec
}
