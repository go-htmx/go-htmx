package nest

import (
	"errors"
	"fmt"
	"html"
	"log/slog"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

// Implements a table that allows sorting on columns and nice formatting of values
type Table struct {
	Component
	Columns               []TableColumn
	Rows                  []*TableRow
	SortBy                FieldName                                            // Field name to sort the rows by
	SortDescending        bool                                                 // If false, the order is ascending
	Refresh               int                                                  // Number of seconds until automatic refresh
	AllowSelect           bool                                                 // If true, a row can be selected by clicking it
	SelectionFieldName    FieldName                                            // If empty, selection list is based on PrimaryKeyFieldName. But if multiple rows share a field value, all these can be selected with one click by settings this.
	SelectionList         map[string]bool                                      // List of items that are selected
	Search                string                                               // If set, items will be sorted by relevance for the search term
	Caption               UI                                                   // For WCAG compliance. Put in an Text here.
	LastLineIsTotal       bool                                                 // If true, the last line will be treated as a total line, not included in sorting
	PaginationTotalCount  int                                                  // Usually set in OnPrepareHTML()
	PaginationRowsPerPage int                                                  // Has a default, but it is only used if PaginationTotalCount is set
	OnInitialize          func(table *Table, ctx *Context) error               // Called for .RunOnInitialize()
	OnIncoming            func(table *Table, ctx *Context) error               // Called for .RunUserEvents
	OnPrepareHTML         func(table *Table, ctx *Context) error               // Always called before GetHTML()
	OnLoadPage            func(table *Table, ctx *Context, firstRow int) error // If OnPrepareHTML sets PaginationTotalCount, then this event handler should be used to set only the rows specified by the pagination. Remember to use table.SortBy for sort order.
	HtmxReloadLocal       bool                                                 // Makes reloads of HTMX inside this panel reload the panel, but not anything outside. This is achieved using hx-select and hx-target
}

type TableColumn struct {
	FieldName   FieldName
	Title       string
	ExtraInfo   string // Shown on mouse-over
	ColumnWidth string // HTML sizes, e.g. 100px or "10em" for approx. 10 characters wide.
}

func (tab *Table) GetChildren() []UI {
	res := []UI{tab.Caption}
	for _, row := range tab.Rows {
		res = append(res, row)
	}
	return res
}

func (tab *Table) Init(ctx *Context) error {
	return nil
}

func (tab *Table) RunOnInitialize(ctx *Context) error {
	if tab.OnInitialize != nil {
		return tab.OnInitialize(tab, ctx)
	}
	return nil
}

func (tab *Table) RunUserEvents(ctx *Context) error {
	if tab.OnIncoming != nil {
		return tab.OnIncoming(tab, ctx)
	}
	return nil
}

const tableParameterSuffixFirstRowIndex = "FirstRowIndex"

func getPageFromRowNumber(tab *Table, rowNumber int) int {
	page := 0
	rowsPerPage := tab.PaginationRowsPerPage
	if rowsPerPage == 0 {
		rowsPerPage = 20
	}
	page = rowNumber / rowsPerPage
	return page
}

// Calculates the page number based on the "first row number" parameter in the context
func getTablePage(ctx *Context, tab *Table) int {
	firstRowIndex := ctx.GetInt(tab.ID + tableParameterSuffixFirstRowIndex)
	return getPageFromRowNumber(tab, firstRowIndex)
}

func (tab *Table) PrepareHTML(ctx *Context) error {
	if tab.Omitted {
		return nil
	}
	var err error
	if tab.OnPrepareHTML != nil {
		err = errors.Join(err, tab.OnPrepareHTML(tab, ctx))
	}
	if tab.OnLoadPage != nil {
		page := getTablePage(ctx, tab)
		err = errors.Join(err, tab.OnLoadPage(tab, ctx, page*tab.PaginationRowsPerPage))
	}
	if tab.Caption != nil {
		err = errors.Join(tab.Caption.PrepareHTML(ctx))
	}
	for _, row := range tab.Rows {
		err = errors.Join(err, row.PrepareHTML(ctx))
	}
	return err
}

// Key identifies the row. Key must use alphanumeric stuff.
func (tab *Table) NewRow(key string) *TableRow {
	if !hasOnlySafeCharacters(key) {
		slog.Error("Key used for row in Table does not seem safe", slog.String("tableid", tab.ID), slog.String("key", key), slog.String("msgid", "JVGDK1"))
	}
	if tab.Rows == nil {
		tab.Rows = make([]*TableRow, 0)
	}
	row := &TableRow{
		Component: Component{ID: fmt.Sprintf("%srow%s", tab.ID, key)},
		keyID:     key,
		Cells:     make(map[FieldName]*TableCell),
	}
	tab.Rows = append(tab.Rows, row)
	return row
}

// Key identifies the row. Key must use alphanumeric stuff.
func (tab *Table) GetRow(key string) *TableRow {
	for _, row := range tab.Rows {
		if row.keyID == key {
			return row
		}
	}
	return nil
}

// Same as .NewRow() but with integer parameter. Both can be mixed in the same table.
func (tab *Table) NewRowInt(key int) *TableRow {
	return tab.NewRow(strconv.Itoa(key))
}

func sortGTable(def *Table) {
	if def.SortBy == "" {
		return
	}

	customSort := func(i, j int) bool {
		a, founda := def.Rows[i].Cells[def.SortBy]
		b, foundb := def.Rows[j].Cells[def.SortBy]
		var keya1, keyb1 string
		var keya2, keyb2 float64
		if founda {
			keya1, keya2 = a.GetSortableRepresentation()
		}
		if foundb {
			keyb1, keyb2 = b.GetSortableRepresentation()
		}
		if def.SortDescending {
			return keya1 > keyb1 || (keya1 == keyb1 && keya2 > keyb2)
		} else {
			return keyb1 > keya1 || (keya1 == keyb1 && keyb2 > keya2)
		}
	}
	if def.Search != "" {
		customSort = func(i, j int) bool {
			keya2 := def.Rows[i].AltSortKey
			keyb2 := def.Rows[j].AltSortKey
			return keya2 > keyb2
		}
	}

	if def.LastLineIsTotal {
		sort.Slice(def.Rows[:len(def.Rows)-1], customSort)
	} else {
		sort.Slice(def.Rows, customSort)
	}
}

// Converts map of selected items into a comma separated string
func getGTableSelectionlist(selectionList map[string]bool) string {
	if len(selectionList) > 0 {
		arr := make([]string, 0, len(selectionList))
		for key, value := range selectionList {
			if value {
				arr = append(arr, key)
			}
		}
		return strings.Join(arr, ",")
	}
	return ""
}

func updateTotalRow2(def *Table) {
	if !def.LastLineIsTotal {
		return
	}
	rowWithTotalsIndex := len(def.Rows) - 1
	rowWithTotals := def.Rows[rowWithTotalsIndex]
	for fieldId, cell := range rowWithTotals.Cells {
		if !cell.TotalIsSum {
			continue
		}
		cellFloat, convOK := cell.Value.(*GEditFloat)
		if !convOK {
			continue
		}
		colSum := 0.0
		for rowIndex, row := range def.Rows {
			if rowIndex == rowWithTotalsIndex {
				continue
			}
			pcell, ok := row.Cells[fieldId].Value.(FieldValueAggregator)
			if ok {
				colSum += pcell.GetFloat64()
			}
		}
		cellFloat.Float64Ref = &colSum
	}
}

func removeHTMLTags(input string) string {
	// Compile a regular expression to match HTML tags
	re := regexp.MustCompile(`<[^>]*>`)
	// Replace all HTML tags with an empty string
	result := re.ReplaceAllString(input, "")
	return result
}

func (def *Table) GetHTML(ctx *Context) string {
	if def.Omitted {
		return ""
	}
	if def.ID == "" {
		return "ID is missing for table"
	}
	if def.PaginationTotalCount != 0 && def.LastLineIsTotal {
		return "Total rows and pagination cannot be used both at the same time"
	}
	ctxCells := ctx.Copy()
	htmxInfo := ctx.Copy()
	// The following was previously part of the code, but was commented out, because hxselect is inherited in the DOM,
	// which breaks components in cells. A new way to fix this could be to use htmx tags that prevent inheritance.
	//	htmxInfo.HxSelect = "#" + def.ID
	//	htmxInfo.HxTarget = htmxInfo.HxSelect
	//	htmxInfo.HxSwap = "outerHTML"
	if def.HtmxReloadLocal {
		htmxInfo = htmxInfo.CopyForLocalReload(def.ID)
	}

	// Fix total rows
	updateTotalRow2(def)

	// Get parameters from URL
	orderparam := htmxInfo.Get("order")
	if orderparam != "" {
		def.SortDescending = orderparam == "desc"
	}
	sortByParam := FieldName(htmxInfo.Get("field"))
	if sortByParam != "" {
		def.SortBy = sortByParam
	}
	selectedList := htmxInfo.Get(def.ID + "selection")
	if selectedList != "" {
		def.SelectionList = make(map[string]bool)
		arr := strings.Split(selectedList, ",")
		for _, item := range arr {
			def.SelectionList[item] = true
		}
	}

	// Filter and sort table
	if def.Search != "" {
		for _, row := range def.Rows {
			searchStr := ""
			for _, cell := range row.Cells {
				valueprov, ok := cell.Value.(ValueEditor)
				if ok {
					searchStr += " " + removeHTMLTags(valueprov.GetHtmlDisplay(nil))
				}
			}
			row.AltSortKey = searchMatch(def.Search, searchStr) // Reference to tools is not ok
			row.Omitted = row.AltSortKey == 0
		}
		if def.LastLineIsTotal {
			def.Rows[len(def.Rows)-1].Omitted = true
		}
		sortGTable(def)
	} else if def.SortBy != "" {
		sortGTable(def)
	}

	// Generate HTML
	sl := make([]string, 0)
	sl = append(sl, fmt.Sprintf(`<div id="%s">`, e(def.ID)))

	// <table>
	sl = append(sl, fmt.Sprintf(`<table class="%s" id="%s" `, GenerateComponentClassName("Table"), e(def.ID+"table")))
	if def.Refresh != 0 {
		newhtmxinfo := htmxInfo.Copy()
		newhtmxinfo.HxTrigger = fmt.Sprintf("every %ds", def.Refresh)
		htmxattrs := newhtmxinfo.GetHxAttrs()
		sl = append(sl, htmxattrs)
	}
	sl = append(sl, ">\n")

	if htmxInfo.RequireWCAG && def.Caption == nil {
		slog.Warn("WCAG problem: Table has no caption", slog.String("id", def.ID), slog.String("msgid", "WCAG02"))
	}
	if def.Caption != nil {
		sl = append(sl, fmt.Sprintf(`<caption>%s</caption>`, def.Caption.GetHTML(htmxInfo)))
	}

	sl = append(sl, "<colgroup>")
	for _, col := range def.Columns {
		colstyles := ""
		if col.ColumnWidth != "" {
			colstyles += "width: " + col.ColumnWidth
		}
		sl = append(sl, "<col style=\""+e(colstyles)+"\">")

	}
	sl = append(sl, "</colgroup>\n")
	sl = append(sl, "<thead>\n")
	sl = append(sl, "<tr>")
	for _, col := range def.Columns {
		newSortDesc := def.SortDescending != (col.FieldName == def.SortBy) // xor
		sortorder := "asc"
		if newSortDesc {
			sortorder = "desc"
		}
		// Table header cell <th>
		thHtmxInfo := htmxInfo.Copy()
		thHtmxInfo.Set("order", sortorder)
		thHtmxInfo.Set("field", string(col.FieldName))
		attrs := thHtmxInfo.GetHxAttrs()
		sl = append(sl, fmt.Sprintf(`<th %s `, attrs))
		if col.ExtraInfo != "" {
			sl = append(sl, fmt.Sprintf(` title="%s" `, e(col.ExtraInfo)))
		}
		sl = append(sl, fmt.Sprintf(` id="%s" style="text-align: center; cursor:pointer; ">`, e(def.ID+"col"+string(col.FieldName))))
		sl = append(sl, e(col.Title))
		if col.FieldName == def.SortBy {
			if def.SortDescending {
				sl = append(sl, fmt.Sprintf(` <span title="%s">&#8593;</span>`, e("Sorted descending - click a column header to sort")))
			} else {
				sl = append(sl, fmt.Sprintf(` <span title="%s">&#8595;</span>`, e("Sorted ascending - click a column header to sort")))
			}
		}
		sl = append(sl, "</th>\n")
	}
	sl = append(sl, "</tr>\n</thead>\n")
	sl = append(sl, "<tbody>\n")
	for rowindex, row := range def.Rows {
		if row.Omitted {
			continue
		}
		if row.ID == "" {
			slog.Error("Internal error in Table component: Row does not have an ID.", slog.String("msgid", "AH2RKJ"))
		}

		isFoot := def.LastLineIsTotal && rowindex == len(def.Rows)-1
		if isFoot {
			sl = append(sl, "<tfoot>\n")
			sl = append(sl, fmt.Sprintf(`<tr id="%s" class="%s">`, GenerateComponentClassName("GTableRowFooter"), e(row.ID)))
		} else {
			attrs := ""
			if def.AllowSelect {
				rowSelectionID := row.keyID
				if def.SelectionFieldName != "" {
					keystr, keyfloat := row.Cells[def.SelectionFieldName].GetSortableRepresentation()
					rowSelectionID = fmt.Sprintf("%s%f", keystr, keyfloat)
				}
				founda, foundb := def.SelectionList[rowSelectionID]
				if foundb && founda {
					attrs = fmt.Sprintf(` class="%s" `, GenerateComponentClassName("GTableRowSelected"))
				} else {
					selmap := make(map[string]bool)
					selmap[rowSelectionID] = true
					newhtmxinfo := ctxCells.Copy()
					newhtmxinfo.Set(def.ID+"selection", getGTableSelectionlist(selmap))
					attrs = newhtmxinfo.GetHxAttrs()
				}
			}
			sl = append(sl, fmt.Sprintf(`<tr id="%s" %s>`, e(row.ID), attrs))
		}
		for _, col := range def.Columns {
			cell, found := row.Cells[col.FieldName]
			if found {
				sl = append(sl, cell.GetHTML(ctxCells))
			} else {
				sl = append(sl, "<td></td>")
			}
		}
		sl = append(sl, "</tr>\n")
		if isFoot {
			sl = append(sl, "</tfoot>\n")
		}
	}
	sl = append(sl, "</tbody>\n</table>\n")

	// Pagination
	if def.PaginationTotalCount != 0 {
		page := getTablePage(htmxInfo, def)
		firstpage := page - 2
		lastpage := page + 2
		totalLastPage := getPageFromRowNumber(def, def.PaginationTotalCount-1)
		if firstpage < 0 {
			lastpage -= firstpage
			firstpage = 0
		}
		lastpage = min(lastpage, totalLastPage)
		if lastpage > 0 {

			styles := `margin: 0 5px; cursor: pointer; `

			var pageHtml []string
			if page > 0 {
				linkContext := htmxInfo.Copy()
				linkContext.SetIntNonZero(def.ID+tableParameterSuffixFirstRowIndex, (page-1)*def.PaginationRowsPerPage)
				hxstuff := linkContext.GetHxAttrs()
				pageHtml = append(pageHtml, fmt.Sprintf(`<span style="%s" %s>%s</span>`, styles, hxstuff, html.EscapeString(T("Previous"))))
			}
			for p := firstpage; p <= lastpage; p++ {
				active := ""
				if p == page {
					active = " font-weight: bold;"
				}
				linkContext := htmxInfo.Copy()
				linkContext.SetIntNonZero(def.ID+tableParameterSuffixFirstRowIndex, p*def.PaginationRowsPerPage)
				hxstuff := linkContext.GetHxAttrs()
				pageHtml = append(pageHtml, fmt.Sprintf(`<span style="%s %s"  onmouseover="this.style.color='#8080ff';" onmouseout="this.style.color='black';" %s>%d</span>`, styles, active, hxstuff, p+1))
			}
			if page < lastpage {
				linkContext := htmxInfo.Copy()
				linkContext.SetIntNonZero(def.ID+tableParameterSuffixFirstRowIndex, (page+1)*def.PaginationRowsPerPage)
				hxstuff := linkContext.GetHxAttrs()
				pageHtml = append(pageHtml, fmt.Sprintf(`<span style="%s" %s>%s</span>`, styles, hxstuff, html.EscapeString(T("Next"))))
			}
			sl = append(sl, fmt.Sprintf(`
		<div style="display: flex; justify-content: center; margin-top: 20px;">
			<nav aria-label="Page navigation">
					%s
			</nav>
		</div>`, strings.Join(pageHtml, "\n")))
		}
	}
	sl = append(sl, "</div>\n")

	return strings.Join(sl, "")
}
