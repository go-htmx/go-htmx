package nest

import (
	"errors"
	"fmt"
	"log/slog"
)

// Provides a button that toggles something on/off, e.g. "edit mode". For instance, switch between "edit pen" and "save checkmark"

type Button struct {
	Component
	Clicked              bool
	Caption              string // Defaults to Click me
	Disabled             bool   // If true, the button is not clickable
	Default              bool   // If true, this button will react to pressing Enter. Only one button can be default.
	Action               string // Deprecated. If set, this will be set as one-time Context parameter named "action".
	Method               string // Defaults to GET
	ConfirmationQuestion string // If set, the user is asked this question to confirm the button click
	OnInitialize         func(button *Button, ctx *Context) error
	OnClick              func(button *Button, ctx *Context) error // Only called if button is clicked, before OnPrepareHTML()
	OnPrepareHTML        func(button *Button, ctx *Context) error
}

func (btn *Button) GetSortableRepresentation() (string, float64) {
	return btn.Caption, 0
}

func (btn *Button) Init(ctx *Context) error {
	btn.Clicked = ctx.GetBool(btn.ID)
	return nil
}

func (btn *Button) RunOnInitialize(ctx *Context) (err error) {
	if btn.OnInitialize != nil {
		return btn.OnInitialize(btn, ctx)
	}
	return nil
}

func (btn *Button) RunUserEvents(ctx *Context) (err error) {
	if btn.OnClick != nil {
		if ctx.GetBool(btn.ID) {
			ctx.Clear(btn.ID)
			err = btn.OnClick(btn, ctx)
		}
	}
	return err
}

func (btn *Button) PrepareHTML(ctx *Context) error {
	if btn.OnPrepareHTML != nil {
		return btn.OnPrepareHTML(btn, ctx)
	}
	if btn.OnClick != nil && btn.ID == "" {
		return errors.New("internal error: cannot use Button with OnClick event handler but no fixed ID value")
	}
	return nil
}

// Returns HTML representing the Select structure, activating HTMX request upon change
func (btn *Button) GetHTML(ctx *Context) string {
	ctx = ctx.Copy()
	if btn.ConfirmationQuestion != "" {
		ctx.HxConfirm = btn.ConfirmationQuestion
	}
	if btn.Omitted {
		return ""
	}
	if btn.ID == "" {
		slog.Error("Button missing its ID", slog.String("msgid", "H1BTN9"))
	}
	if btn.Class == "" {
		btn.Class = GenerateComponentClassName("Button")
	}
	curHtmx := ctx.Copy()
	if btn.Action != "" {
		curHtmx.SetOT("action", btn.Action)
	} else {
		curHtmx.SetOTBool(btn.ID, true)
	}
	attrDisabled := ""
	if btn.Disabled {
		attrDisabled = " disabled "
	}
	caption := btn.Caption
	if caption == "" {
		caption = T("Click me")
	}
	javaScriptDefault := ""
	if btn.Default {
		javaScriptDefault = fmt.Sprintf(`
		 <script>
    document.addEventListener('keydown', function (event) {
      if (event.key === 'Enter') {
        // Trigger the button click when Enter is pressed
        const defaultButton = document.getElementById('%s');
        if (defaultButton) {
          defaultButton.click();
        }
      }
    });
  	</script>
		`, e(btn.ID))
	}
	hxattrs := curHtmx.GetHxAttrs()
	return fmt.Sprintf(`<button id="%s" class="%s" %s %s>%s</button>%s`,
		e(btn.ID), e(btn.Class), hxattrs, attrDisabled, e(caption), javaScriptDefault)
}
