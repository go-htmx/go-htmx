package nest

import (
	"errors"
	"fmt"
	"html"
	"log/slog"
	"strconv"
	"strings"
	"unicode/utf8"
)

// Component equivalent to an <input>.
type EditString struct {
	Component
	Form               UIRegistrator      // Specify a reference to the form that this edit belongs to
	DataSource         DataSourceProvider // If connected, it will load its values from there
	DataSourceField    FieldName          // Required for DataSource
	Required           bool               // If true, validation will fail if there is no text.
	ReadOnly           bool               // If true, the edit box becomes read/only
	Length             int                // Number of characters that should be visible in the input field, if possible and non-zero.
	MaxLength          int                // Inputs longer than this number of characters are not allowed.
	Attrs              AttributeList      // Can be used to add attributes to the surrounding html tag
	Placeholder        string             // Text shown inside the edit when it is empty. E.g. to explain what to put in. Careful: WCAG guidelines says you should also have a label to an edit field.
	ValidationDisabled bool
	value              string

	OnInitialize func(ed *EditString, ctx *Context) error
	OnValidate   func(ed *EditString, ctx *Context, value string) error // Checks if the string is ok - and if not, returns a suitable error message
}

var _ Editor = &EditString{}

func (ed *EditString) GetValidationResult(ctx *Context) error {
	if ed.ValidationDisabled && ed.DataSource == nil {
		return nil
	}
	if ed.Required && strings.TrimSpace(ed.value) == "" {
		return UserErrorMsg(T("This field is required."))
	}
	if ed.MaxLength != 0 {
		charCount := utf8.RuneCountInString(ed.value)
		if charCount > ed.MaxLength {
			return UserErrorMsg(fmt.Sprintf(T("Text is too long. It must be max %d characters."), ed.MaxLength))
		}
	}
	if ed.OnValidate != nil {
		err := ed.OnValidate(ed, ctx, ed.value)
		if err != nil {
			return err
		}
	}
	if ed.DataSource != nil {
		if ed.DataSourceField == "" {
			slog.Error("DataSourceField is missing for EditNumber", slog.String("msgid", "435PO3"))
		}
		field := ed.DataSource.GetField(ctx, ed.DataSourceField)
		if field == nil {
			return errors.New("Unknown field " + string(ed.DataSourceField))
		}
		return field.Validate(ctx.Locale, strings.TrimSpace(ed.value))
	}
	return nil
}

func (ed *EditString) EnableValidation(enabled bool) {
	ed.ValidationDisabled = !enabled
}

// Changes the current value for the input
func (ed *EditString) SetValue(ctx *Context, value string) {
	if ed.Component.ID == "" {
		slog.Error("You must set ID on an edit component before calling .SetValue()", slog.String("msgid", "B1AGMT"))
	}
	ed.value = value
	ctx.Set(ed.ID, value)
}

// Gets the current value in the input
func (ed *EditString) GetValue(ctx *Context) string {
	if ed.Component.ID == "" {
		slog.Error("You must set ID on an edit component before calling .GetValue()", slog.String("msgid", "5Y8DJK"))
	}
	return ed.value
}

// Sets edit to its default starting point, which is an empty string
func (ed *EditString) SetValueDefault(ctx *Context) {
	ed.SetValue(ctx, "")
}

func (ed *EditString) Init(ctx *Context) error {
	if ed.Form != nil {
		ed.Form.RegisterComponent(ed)
	}

	ed.value = ctx.Get(ed.ID)
	return nil
}

func (ed *EditString) RunOnInitialize(ctx *Context) error {
	// After datasource has been initialized, we can access it
	if ed.DataSource != nil && ed.DataSourceField != "" {
		field := ed.DataSource.GetField(ctx, ed.DataSourceField)
		if field == nil {
			return errors.New("field unknown " + string(ed.DataSourceField))
		}
		if ed.DataSource.GetStateShouldFormsLoadNewData(ctx) {
			ed.value = field.GetAsString()
		} else {
			ed.value = ctx.Get(ed.ID)
			field.SetAsString(ed.value)
		}
		if ed.DataSource.GetStateReadOnly(ctx) {
			ed.ReadOnly = true
		}
	}
	if ed.OnInitialize != nil {
		return ed.OnInitialize(ed, ctx)
	}
	return nil
}

func (ed *EditString) RunUserEvents(ctx *Context) error {
	return nil
}

func (ed *EditString) PrepareHTML(ctx *Context) error {
	// Remove this parameter because it is autosubmitted when HTML is generated
	ctx.Clear(ed.ID)
	return nil
}

func (ed *EditString) GetHTML(ctx *Context) string {
	attrReadOnly := ""
	if ed.ReadOnly {
		attrReadOnly = " readonly "
	}
	if ed.Attrs == nil {
		ed.Attrs = make(AttributeList)
	}
	_, found := ed.Attrs["type"]
	if !found {
		ed.Attrs["type"] = "text"
	}
	if ed.Length != 0 {
		ed.Attrs["size"] = strconv.Itoa(ed.Length)
	}
	if ed.Class != "" {
		ed.Attrs["class"] = ed.Class
	}
	if ed.Placeholder != "" {
		ed.Attrs["placeholder"] = ed.Placeholder
	}
	ed.Attrs["id"] = ed.ID
	ed.Attrs["value"] = ed.value
	res := fmt.Sprintf(`<input %s%s>`, ed.Attrs.GetHTML(ctx), attrReadOnly)

	IDenc := html.EscapeString(ed.ID)
	paramNameEnc := html.EscapeString(OTprefix + ed.ID)
	res = res + fmt.Sprintf(
		`<script>
	if (!document.body.%shasListenerAttached) {
		document.body.addEventListener('htmx:configRequest', %shandleConfigRequest);
		document.body.%shasListenerAttached = true;
	}

	function %shandleConfigRequest(evt) {
		const element = document.getElementById('%s');
		if (element.value) {
        	evt.detail.parameters['%s'] = element.value;
    	} else {
        	delete evt.detail.parameters['%s'];
	    }
	}
	</script>`,
		IDenc, IDenc, IDenc, IDenc, IDenc, paramNameEnc, paramNameEnc)
	return res
}

func (f *EditString) GetSortableRepresentation() (string, float64) {
	return f.value, 0
}
