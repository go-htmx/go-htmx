package nest

import (
	"encoding/json"
	"errors"
	"fmt"
)

// Implements quickchart.io online chart service, based on Chart.js.
type Chartjs struct {
	Component
	Width, Height Length
	Data          ChartJsParameters // Configure this with the data you want to show
	Attrs         AttributeList     // Possibility to set custom attributes
	OnInitialize  func(chart *Chartjs, ctx *Context) error
	OnPrepareHTML func(chart *Chartjs, ctx *Context) error

	// Private data
	jsonData []byte
}

func (chart *Chartjs) Init(ctx *Context) error {
	return nil
}

func (chart *Chartjs) RunOnInitialize(ctx *Context) error {
	if chart.OnInitialize != nil {
		return chart.OnInitialize(chart, ctx)
	}
	return nil
}

func (chart *Chartjs) RunUserEvents(ctx *Context) error {
	return nil
}

func (chart *Chartjs) PrepareHTML(ctx *Context) error {
	if chart.Omitted {
		return nil
	}

	if chart.OnPrepareHTML != nil {
		err := chart.OnPrepareHTML(chart, ctx)
		if err != nil {
			return err
		}
	}

	if chart.Data.Type == "" {
		return errors.New("missing type for Chartjs")
	}

	if len(chart.Data.Data.Datasets) == 0 {
		return errors.New("missing data for Chartjs")
	}

	var err error
	chart.jsonData, err = json.Marshal(chart.Data)
	if err != nil {
		return err
	}

	return nil
}

func (chart *Chartjs) GetHTML(ctx *Context) string {
	if chart.Omitted {
		return ""
	}

	if chart.Data.Type == "" {
		return "Missing type"
	}
	if len(chart.Data.Data.Datasets) == 0 {
		return "Missing data"
	}
	if chart.Width.Unit != Pixels {
		return "Width must use px"
	}
	if chart.Height.Unit != Pixels {
		return "Height must use px"
	}

	// Script to dynamically load chart.js
	script := `
	  // Dynamically load Chart.js if not already loaded
    if (typeof Chart === 'undefined') {
        const script = document.createElement('script');
        script.src = "https://cdn.jsdelivr.net/npm/chart.js";
        script.onload = function () {
            renderChart();
        };
        document.head.appendChild(script);
    } else {
        renderChart();
    }
`
	// Script that renders the chart
	script = script + fmt.Sprintf(
		`function renderChart() { `+"\n"+
			`const ctx = document.getElementById('%s').getContext('2d');`+"\n"+
			`new Chart(ctx, %s); `+"\n"+
			`}`+"\n",
		chart.ID, string(chart.jsonData))

	lines := fmt.Sprintf(`<canvas id="%s" width="%.0f" height="%.0f"></canvas>`+"<script>\n%s\n</script>\n",
		e(chart.ID), chart.Width.Value, chart.Height.Value, script)

	return lines
}

type ChartJsParameters struct {
	Type ChartJsType `json:"type"`
	Data ChartJsData `json:"data"`
}

type ChartJsDataset struct {
	Label string    `json:"label"`
	Data  []float64 `json:"data"`
}

type ChartJsData struct {
	Labels   []string         `json:"labels"`
	Datasets []ChartJsDataset `json:"datasets"`
}

type ChartJsType string

const CjBar ChartJsType = "bar"
const CjLine ChartJsType = "line"
const CjScatter ChartJsType = "scatter"
const CjPolarArea ChartJsType = "polarArea"
const CjBubble ChartJsType = "bubble"
