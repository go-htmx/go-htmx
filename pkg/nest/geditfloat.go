package nest

import (
	"fmt"
	"log/slog"
	"math"
	"strconv"
)

// Deprecated, use ValueFloat or EditNumber instead, with DataSource
type GEditFloat struct {
	LimitedInputRange bool    // If true, inputs are validated against limits
	LimitLow          float64 // Lowest value that is allowed, if LimitedInputRange is true
	LimitHigh         float64 // Highest value that is allowed, if LimitedInputRange is true
	Length            int     // Number of characters that are allowed to be input
	ZeroIsNull        bool    // Makes zero be treated as a non-value
	DebugInfo         string  // if filled, this will be used with log messages
	Decimals          int

	// One of the following must be specified
	OnGet      func(*GEditFloat) float64
	OnSet      func(*GEditFloat, float64) error
	Float64Ref *float64
}

func (f *GEditFloat) getFormatStr() string {
	return "%" + fmt.Sprintf(`.%df`, f.Decimals)
}

func (f *GEditFloat) GetHtmlDisplay(locale Locale) string {
	if f.Float64Ref == nil {
		return ""
	}
	str := locale.FormatFloat(*f.Float64Ref, f.Decimals)
	return e(str)
}

func (f *GEditFloat) GetHtmlEdit(idName, fieldName, extraAttrs string, ReadOnly bool) string {
	style := "text-align: right;"
	length := f.Length
	if length == 0 {
		length = 12
	}
	if f.LimitedInputRange {
		extraAttrs = fmt.Sprintf(`%s min="%f" max="%f" `, extraAttrs, f.LimitLow, f.LimitHigh)
	}
	if length != 0 {
		style += fmt.Sprintf(" width: %dch;", length)
	}
	if ReadOnly {
		extraAttrs += " readonly "
	}
	stepSize := math.Pow10(-f.Decimals)
	return fmt.Sprintf(`<input type="number" step="%f" id="%s" name="%s" style="%s" %s value="%s">`, stepSize, e(idName), e(fieldName), e(style), extraAttrs, e(f.getString()))
}

func (f *GEditFloat) SetHtmlValue(value string) error {
	return f.setString(value)
}

func (f *GEditFloat) GetFloat64() float64 {
	return f.getFloat64()
}

func (f *GEditFloat) getFloat64() float64 {
	if f.OnGet != nil {
		return f.OnGet(f)
	}
	if f.Float64Ref != nil {
		return *f.Float64Ref
	}
	slog.Error("Float64 reference is missing", slog.String("msgid", "2GR5O3"))
	return 0
}

func (f *GEditFloat) setFloat64(float float64) {
	if f.OnSet != nil {
		f.OnSet(f, float)
		return
	}
	if f.Float64Ref != nil {
		*f.Float64Ref = float
		return
	}
	slog.Error("Float64 reference is missing", slog.String("msgid", "2SR5O2"))
}

func (f *GEditFloat) getString() string {
	val := f.getFloat64()
	if f.ZeroIsNull && val == 0 {
		return ""
	}
	return fmt.Sprintf(f.getFormatStr(), val)
}

func (f *GEditFloat) setString(value string) error {
	if value == "" {
		f.setFloat64(0)
		return nil
	}

	val, err := strconv.ParseFloat(value, 64)
	if err != nil {
		return err
	}
	if f.LimitedInputRange {
		if val < f.LimitLow {
			return UserErrorMsg(fmt.Sprintf(`Value %f is below lower limit %f`, val, f.LimitLow))
		}
		if val > f.LimitHigh {
			return UserErrorMsg(fmt.Sprintf(`Value %f is above upper limit %f`, val, f.LimitHigh))
		}
	}
	f.setFloat64(val)
	return nil
}

func (f *GEditFloat) GetSortableRepresentation() (string, float64) {
	return "", f.getFloat64()
}
