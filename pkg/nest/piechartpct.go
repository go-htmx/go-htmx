package nest

import (
	"fmt"
	"html"
	"math"
)

type PieChartPct struct {
	Component
	Percent         float64 // Number 0-100. Defaults to zero.
	Color           string  // HTML/CSS format, e.g. "#ff0000" or "red". Defaults to black.
	BackgroundColor string  // HTML/CSS format, defaults to "none"
	Width           string  // Defaults to "100px"
	Height          string  // Defaults to "100px"
	Border          bool    // If not set, the pie does not use the Color for border
}

const pieChartTemplateBordered = `
  <svg width="%s" height="%s" viewBox="0 0 320 320">
	<!-- First slice,filled -->
	<path d="M160 160 L160 20 A140 140 0 0 1 %.1f %.1f z" fill="%s" stroke="%s" stroke-width="10"></path>
  
	<!-- Second slice, empty -->
	<path d="M160 160 L%.1f %.1f A140 140 0 1 1 160 20 z" fill="%s" stroke="%s" stroke-width="10"></path>
  </svg>
  `

func (part *PieChartPct) Init(ctx *Context) error {
	return nil
}

func (part *PieChartPct) RunOnInitialize(htmxInfo *Context) error {
	return nil
}

func (part *PieChartPct) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (part *PieChartPct) PrepareHTML(htmxInfo *Context) error {
	return nil
}

func (pie *PieChartPct) GetHTML(ctx *Context) string {
	if pie.Omitted {
		return ""
	}
	if pie.Width == "" {
		pie.Width = "100px"
	}
	if pie.Height == "" {
		pie.Height = "100px"
	}
	if pie.Color == "" {
		pie.Color = "black"
	}
	if pie.BackgroundColor == "" {
		pie.BackgroundColor = "none"
	}
	if pie.Percent < 0 {
		pie.Percent = 0
	}
	if pie.Percent > 100 {
		pie.Percent = 100
	}
	x := math.Cos((pie.Percent-25)/50*math.Pi)*140.0 + 160.0
	y := math.Sin((pie.Percent-25)/50*math.Pi)*140.0 + 160.0
	strokeColor := ""
	if pie.Border {
		strokeColor = pie.Color
	}
	return fmt.Sprintf(pieChartTemplateBordered, html.EscapeString(pie.Width), html.EscapeString(pie.Height), x, y, pie.Color, strokeColor, x, y, pie.BackgroundColor, strokeColor)
}
