package nest

import (
	"fmt"
	"html"
)

// Panel with 1 element that the user can resize by dragging the edge
// Note, resizing does not sustain the next user interactivity. This can be implemented in a future version.
type Resizable struct {
	Component
	Child           UI     // Required
	Width           Length // Required
	Height          Length // Required
	Padding         Length // Defaults to no padding
	DisableResizing bool   // If true, resizing is disabled, and the width and height are fixed
	Border          Border
}

func (panel *Resizable) Init(htmxInfo *Context) error {
	// Get adjusted size from client
	pHeight := panel.ID + "Height"
	pWidth := panel.ID + "Width"
	pH := float64(htmxInfo.GetInt(pHeight))
	pW := float64(htmxInfo.GetInt(pWidth))
	if pH != 0 {
		panel.Height = Px(pH)
	}
	if pW != 0 {
		panel.Width = Px(pW)
	}

	// Clearing is required because JavaScript will add these parameters to the URL
	htmxInfo.Clear(pHeight)
	htmxInfo.Clear(pWidth)
	return nil
}

func (panel *Resizable) RunOnInitialize(htmxInfo *Context) error {
	return nil
}

func (panel *Resizable) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (panel *Resizable) PrepareHTML(htmxInfo *Context) error {
	if panel.Child != nil {
		return panel.Child.PrepareHTML(htmxInfo)
	}
	return nil
}

func (panel *Resizable) GetChildren() []UI {
	return []UI{panel.Child}
}

func (panel *Resizable) GetHTML(htmxInfo *Context) string {
	if panel.Omitted || panel.Child == nil {
		return ""
	}
	if panel.Width.IsZero() {
		panel.Width = Px(300)
	}
	if panel.Height.IsZero() {
		panel.Height = Px(150)
	}
	IDenc := html.EscapeString(panel.ID)
	attrClass := ""
	if panel.Class != "" {
		attrClass = fmt.Sprintf(` class="%s" `, html.EscapeString(panel.Class))
	}
	attrResize := ` resize: both; overflow: auto; `
	if panel.DisableResizing {
		attrResize = ""
	}
	htmlStr := panel.Child.GetHTML(htmxInfo)
	borderHtml := panel.Border.String()
	res := fmt.Sprintf(`<div id="%s" %s style="width: %s; height: %s; padding: %s; %s %s">%s</div>`+
		`<script>
		if (!document.body.%shasListenerAttached) {
			document.body.addEventListener('htmx:configRequest', %shandleConfigRequest);
			document.body.%shasListenerAttached = true;
		}

		function %shandleConfigRequest(evt) {
			const resizableDiv = document.getElementById('%s');
			evt.detail.parameters['%sWidth'] = resizableDiv.clientWidth;
			evt.detail.parameters['%sHeight'] = resizableDiv.clientHeight;
		}
	</script>`,
		IDenc, attrClass, panel.Width.String(), panel.Height.String(), panel.Padding.String(), borderHtml, attrResize, htmlStr, IDenc, IDenc, IDenc, IDenc, IDenc, IDenc, IDenc)
	return res
}
