package nest

import (
	"fmt"
	"log/slog"
	"unicode/utf8"
)

// Deprecated. Use EditString with dataprovider, instead.
type GEditString struct {
	OnValidate func(string) error // Checks if the string is ok - and if not, returns a suitable error message
	IsHTML     bool               // if set, the stored string is HTML, and the control is read/only
	Length     int                // Number of characters that should be visible in the input field, if possible and non-zero.
	MaxLength  int                // Inputs longer than this number of characters are not allowed.
	DebugInfo  string             // if filled, this will be used with log messages

	// Either set StringRef, or OnGet/OnSet
	OnGet     func(*GEditString) string
	OnSet     func(*GEditString, string) error
	StringRef *string
}

func (f *GEditString) GetHtmlDisplay(Locale) string {
	if f.IsHTML {
		return f.getString()
	}
	return e(f.getString())
}

func (f *GEditString) GetHtmlEdit(idName, fieldName, extraAttrs string, ReadOnly bool) string {
	if f.IsHTML {
		return f.getString()
	}
	if f.Length != 0 {
		extraAttrs += fmt.Sprintf(` size="%d" `, f.Length)
	}
	if ReadOnly {
		extraAttrs += " readonly "
	}
	return fmt.Sprintf(`<input type="text" id="%s" name="%s" %s value="%s">`, idName, fieldName, extraAttrs, e(f.getString()))
}

func (f *GEditString) SetHtmlValue(value string) error {
	if f.MaxLength != 0 {
		charCount := utf8.RuneCountInString(value)
		if charCount > f.MaxLength {
			return UserErrorMsg(fmt.Sprintf("Text is too long. It must be max %d characters.", f.MaxLength))
		}
	}
	if f.OnValidate != nil {
		err := f.OnValidate(value)
		if err != nil {
			return err
		}
	}
	if f.OnSet != nil {
		return f.OnSet(f, value)
	}
	if f.StringRef == nil {
		if f.OnGet != nil {
			// OnGet is present, but OnSet is not, so probably this just doesn't want to save anything.
			return nil
		}
		slog.Error("Control is missing .StringRef", slog.String("debuginfo", f.DebugInfo), slog.String("msgid", "0KCMQX"))
		return UserErrorMsg("Internal error, see server log. 0KCMQX")
	}
	*f.StringRef = value
	return nil
}

func (f *GEditString) GetSortableRepresentation() (string, float64) {
	return f.getString(), 0
}

func (f *GEditString) getString() string {
	if f.OnGet != nil {
		return f.OnGet(f)
	}
	if f.StringRef != nil {
		return *f.StringRef
	}
	panic("GEditString.getString() is missing a reference")
}
