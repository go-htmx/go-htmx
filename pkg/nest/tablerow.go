package nest

import (
	"errors"
	"fmt"
	"log/slog"
)

type TableRow struct {
	Component
	Cells      map[FieldName]*TableCell // Maps fieldname into a cell. This approach means the Table.Columns decides which columns to show and in which order
	keyID      string                   // Value of the primary key for this row. Automatically set.
	AltSortKey float64                  // Useful for search, sorting by relevance
}

func (row *TableRow) SetID(id string) {
	slog.Error("Framework is trying to set ID for a TableRow, this should not succeed", slog.String("msgid", "2LKJAA"))
}

func (row *TableRow) GetChildren() []UI {
	row.setCellIDs()
	var res []UI
	for _, cell := range row.Cells {
		res = append(res, cell)
	}
	return res
}

func (row *TableRow) Init(ctx *Context) error {
	return nil
}

func (row *TableRow) RunOnInitialize(ctx *Context) error {
	row.setCellIDs()
	return nil
}

func (row *TableRow) RunUserEvents(ctx *Context) error {
	row.setCellIDs()
	var err error
	for _, cell := range row.Cells {
		err = errors.Join(err, cell.RunUserEvents(ctx))
	}
	return err
}

// This function needs to be done several times, because the Table user can add cells dynamically, and the IDs must be present when needed.
func (row *TableRow) setCellIDs() {
	for fieldname, cell := range row.Cells {
		if cell.GetID() == "" {
			cell.SetID(fmt.Sprintf("%s_%s_", row.ID, fieldname))
		}
	}
}

func (row *TableRow) PrepareHTML(ctx *Context) error {
	row.setCellIDs()
	var err error
	for _, cell := range row.Cells {
		err = errors.Join(err, cell.PrepareHTML(ctx))
	}
	return err
}

func (row *TableRow) GetHTML(ctx *Context) string {
	return ""
}
