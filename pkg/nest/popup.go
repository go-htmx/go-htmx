package nest

import (
	"fmt"
	"html"
)

// Allows a user interface to be shown in a popup.
// Set Visible to True in a Button.OnClick event handler to make it show itself.
type Popup struct {
	Component
	Visible bool
	Control UI
}

func (obj *Popup) GetChildren() []UI {
	return []UI{obj.Control}
}

func (obj *Popup) Init(ctx *Context) error {
	return nil
}

func (obj *Popup) RunOnInitialize(htmxInfo *Context) error {
	return obj.Control.RunOnInitialize(htmxInfo)
}

func (obj *Popup) RunUserEvents(htmxInfo *Context) error {
	return obj.Control.RunUserEvents(htmxInfo)
}

func (obj *Popup) PrepareHTML(htmxInfo *Context) error {
	return obj.Control.PrepareHTML(htmxInfo)
}

func (obj *Popup) GetHTML(htmxInfo *Context) string {
	if !obj.Visible || obj.Control == nil || obj.Omitted {
		return ""
	}

	class := GenerateComponentClassName("modal-content")
	classOK := GenerateComponentClassName("close-btn")
	classModal := GenerateComponentClassName("modal")
	if obj.Class != "" {
		class = obj.Class
		classOK = obj.Class + "ok"
		classModal = obj.Class + "modal"
	}

	IDhtml := html.EscapeString(obj.ID)
	res := obj.Control.GetHTML(htmxInfo)
	res = fmt.Sprintf(`<dialog id="%s" class="%s">
    <div class="%s">
	<span id="%sclose" class="%s">&times;</span>
        %s<button id="%sok">OK</button>
	</div>
	</dialog>
	<script id="%sscript">
		var modal = document.getElementById('%s');
		var okButton = document.getElementById('%sok');
		okButton.addEventListener('click', () => {
			const modal = document.getElementById('%s');
			modal.close();
		});
			
		modal.showModal();

		document.body.addEventListener('htmx:afterRequest', (evt) => {
			const modal = document.getElementById('%s');
			if (modal) {
				modal.showModal();
			}
		},{ once: true });
	</script>
	`, IDhtml, html.EscapeString(classModal), html.EscapeString(class),
		IDhtml, html.EscapeString(classOK), res, IDhtml,
		IDhtml, IDhtml, IDhtml, IDhtml, IDhtml)
	return res
}
