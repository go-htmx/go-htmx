package nest

import (
	"fmt"
	"math"
	"math/rand/v2"
)

// Allows files to be downloaded
// E.g. make a Button{} that on its OnClick sets the Download.Enabled to true. This will make the button download using Download.OnDownload()
type Download struct {
	Component
	Enabled    bool // If not enabled, this component is passive. If enabled, it will produce HTML that triggers client to download a file from OnDownload()
	OnDownload func(comp *Download, ctx *Context) (ContextFrameworkFileDownload, error)
}

func (comp *Download) PrepareHTML(ctx *Context) error {
	if ctx.GetBool(comp.ID) && comp.OnDownload != nil {
		fileContents, err := comp.OnDownload(comp, ctx)
		if err != nil {
			ctx.Framework.DownloadFile = ContextFrameworkFileDownload{
				MimeType:     "text/plain; charset=utf-8",
				FileName:     "Error.txt",
				FileContents: []byte(fmt.Sprintf("Error occurred on the server:\n%s", err.Error())),
			}
			return err
		}
		ctx.Framework.DownloadFile = fileContents
		return nil
	}
	return nil
}

// Returns HTML representing the Select structure, activating HTMX request upon change
func (comp *Download) GetHTML(ctx *Context) string {
	if !comp.Enabled {
		return ""
	}

	suffix := rand.IntN(math.MaxInt)
	template :=
		`<script>
		// Trigger download of a file
		const a%d = document.createElement('a');
		a%d.href = '%s';
		a%d.download = '';  // Leave empty to use the default file name
		document.body.appendChild(a%d);
		a%d.click();
		document.body.removeChild(a%d);
	</script>`
	ctx = ctx.Copy()
	ctx.SetOTBool(comp.ID, true)
	ctx.SetOTBool(SkipTemplateParameter, true)
	url := ctx.getURL()
	return fmt.Sprintf(template, suffix, suffix, url, suffix, suffix, suffix, suffix)
}
