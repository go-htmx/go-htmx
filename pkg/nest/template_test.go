package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTempl(t *testing.T) {
	comp := &Template{}
	var _ UIProvider = comp
	var control UI = comp
	control.SetID("asdf" + control.GetID())
	assert.Greater(t, 1, len(control.GetHTML(nil)))
	assert.Equal(t, nil, control.RunUserEvents(&Context{}))
	assert.Equal(t, nil, control.PrepareHTML(&Context{}))
}
