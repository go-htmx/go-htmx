package nest

import (
	"errors"
	"fmt"
	"html"
	"log/slog"
	"math"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"
)

//*******************************************************************

type ValueInt struct {
	IntRef         *int    // Only set one of the Int*Ref values
	Int64Ref       *int64  // Only set one of the Int*Ref values
	Int32Ref       *int32  // Only set one of the Int*Ref values
	Int16Ref       *int16  // Only set one of the Int*Ref values
	Int8Ref        *int8   // Only set one of the Int*Ref values
	UIntRef        *uint   // Only set one of the Int*Ref values
	UInt64Ref      *uint64 // Only set one of the Int*Ref values
	UInt32Ref      *uint32 // Only set one of the Int*Ref values
	UInt16Ref      *uint16 // Only set one of the Int*Ref values
	UInt8Ref       *uint8  // Only set one of the Int*Ref values
	ZeroIsNull     bool    // If true, a zero value is represented by an empty string
	Required       bool    // If ZeroIsNull, this means that Zero is not a valid value
	Min, Max       int     // If Min,Max=0, then no validation is performed
	SelectionList  []ValueAndTitle
	OnValidate     func(intval int) error                      // If set, it will be called to add extra validation. Function must return error with user message if validation doesn't passe.
	OnGetHTMLValue func(value *ValueInt, locale Locale) string // Provides value as read/only HTML. Usually not set, as this is handled automatically.
}

var _ FieldValue = &ValueInt{}

func (value *ValueInt) GetSelectionList() []ValueAndTitle {
	return value.SelectionList
}

func (value *ValueInt) GetAsInt() (int, error) {
	var intval int
	if value.IntRef != nil {
		intval = *value.IntRef
	} else if value.Int64Ref != nil {
		intval = int(*value.Int64Ref)
	} else if value.Int32Ref != nil {
		intval = int(*value.Int32Ref)
	} else if value.Int16Ref != nil {
		intval = int(*value.Int16Ref)
	} else if value.Int8Ref != nil {
		intval = int(*value.Int8Ref)
	} else if value.UIntRef != nil {
		intval = int(*value.UIntRef)
	} else if value.UInt64Ref != nil {
		intval = int(*value.UInt64Ref)
	} else if value.UInt32Ref != nil {
		intval = int(*value.UInt32Ref)
	} else if value.UInt16Ref != nil {
		intval = int(*value.UInt16Ref)
	} else if value.UInt8Ref != nil {
		intval = int(*value.UInt8Ref)
	} else {
		slog.Error("At least one IntRef must be set in ValueInt")
		return 0, errors.New("valueInt has no reference to integer variable")
	}
	return intval, nil
}

func (value *ValueInt) GetAsString() string {
	intval, err := value.GetAsInt()
	if err != nil {
		return ""
	}
	if value.ZeroIsNull && intval == 0 {
		return ""
	}
	return strconv.Itoa(intval)
}

func (value *ValueInt) SetAsString(strval string) error {
	var intval int64
	if !value.ZeroIsNull || strval != "" {
		intv, err := strconv.ParseInt(strval, 10, 64)
		if err != nil {
			return err
		}
		intval = intv
	}
	err := value.validateInt(int(intval))
	if err != nil {
		return err
	}
	if value.IntRef != nil {
		*value.IntRef = int(intval)
	} else if value.Int64Ref != nil {
		*value.Int64Ref = intval
	} else if value.Int32Ref != nil {
		if intval > math.MaxInt32 || intval < math.MinInt32 {
			return errors.New("Number out of range")
		}
		*value.Int32Ref = int32(intval)
	} else if value.Int16Ref != nil {
		if intval > math.MaxInt16 || intval < math.MinInt16 {
			return errors.New("Number out of range")
		}
		*value.Int16Ref = int16(intval)
	} else if value.Int8Ref != nil {
		if intval > math.MaxInt8 || intval < math.MinInt8 {
			return errors.New("Number out of range")
		}
		*value.Int8Ref = int8(intval)
	} else if value.UIntRef != nil {
		if intval < 0 {
			return errors.New("Number out of range")
		}
		*value.UIntRef = uint(intval)
	} else if value.UInt64Ref != nil {
		if intval < 0 || intval > math.MaxInt {
			return errors.New("Number out of range")
		}
		*value.UInt64Ref = uint64(intval)
	} else if value.UInt32Ref != nil {
		if intval > math.MaxUint32 || intval < 0 {
			return errors.New("Number out of range")
		}
		*value.UInt32Ref = uint32(intval)
	} else if value.UInt16Ref != nil {
		if intval > math.MaxUint16 || intval < 0 {
			return errors.New("Number out of range")
		}
		*value.UInt16Ref = uint16(intval)
	} else if value.UInt8Ref != nil {
		if intval > math.MaxUint8 || intval < 0 {
			return errors.New("Number out of range")
		}
		*value.UInt8Ref = uint8(intval)
	} else {
		return errors.New("intref is missing in ValueInt")
	}
	return nil
}

func (value *ValueInt) validateInt(intval int) error {
	if value.OnValidate != nil {
		err := value.OnValidate(int(intval))
		if err != nil {
			return err
		}
	}
	if value.ZeroIsNull && value.Required && intval == 0 {
		return UserErrorMsg(T("Value must be specified"))
	}
	if value.Min != 0 || value.Max != 0 {
		if int(intval) < value.Min {
			return UserErrorMsg(fmt.Sprintf(T("Value is below minimum of %d."), value.Min))
		}
		if int(intval) > value.Max {
			return UserErrorMsg(fmt.Sprintf(T("Value is above maximum of %d."), value.Max))
		}
	}
	if value.Int32Ref != nil {
		if intval > math.MaxInt32 {
			return UserErrorMsg(fmt.Sprintf(T("Number is larger than max=%d"), math.MaxInt32))
		}
		if intval < math.MinInt32 {
			return UserErrorMsg(fmt.Sprintf(T("Number is smaller than min=%d"), math.MinInt32))
		}
	} else if value.Int16Ref != nil {
		if intval > math.MaxInt16 {
			return UserErrorMsg(fmt.Sprintf(T("Number is larger than max=%d"), math.MaxInt16))
		}
		if intval < math.MinInt16 {
			return UserErrorMsg(fmt.Sprintf(T("Number is smaller than min=%d"), math.MinInt16))
		}
	} else if value.Int8Ref != nil {
		if intval > math.MaxInt8 {
			return UserErrorMsg(fmt.Sprintf(T("Number is larger than max=%d"), math.MaxInt8))
		}
		if intval < math.MinInt8 {
			return UserErrorMsg(fmt.Sprintf(T("Number is smaller than min=%d"), math.MinInt8))
		}
	} else if value.UIntRef != nil {
		if intval < 0 {
			return UserErrorMsg(fmt.Sprintf(T("Number is smaller than min=%d"), 0))
		}
	} else if value.UInt64Ref != nil {
		if intval < 0 {
			return UserErrorMsg(fmt.Sprintf(T("Number is smaller than min=%d"), 0))
		}
	} else if value.UInt32Ref != nil {
		if intval > math.MaxUint32 {
			return UserErrorMsg(fmt.Sprintf(T("Number is larger than max=%d"), math.MaxUint32))
		}
		if intval < 0 {
			return UserErrorMsg(fmt.Sprintf(T("Number is smaller than min=%d"), 0))
		}
	} else if value.UInt16Ref != nil {
		if intval > math.MaxUint16 {
			return UserErrorMsg(fmt.Sprintf(T("Number is larger than max=%d"), math.MaxUint16))
		}
		if intval < 0 {
			return UserErrorMsg(fmt.Sprintf(T("Number is smaller than min=%d"), 0))
		}
	} else if value.UInt8Ref != nil {
		if intval > math.MaxUint8 {
			return UserErrorMsg(fmt.Sprintf(T("Number is larger than max=%d"), math.MaxUint8))
		}
		if intval < 0 {
			return UserErrorMsg(T("Number is negative but must be 0 or positive"))
		}
	}

	return nil
}

func (value *ValueInt) Validate(locale Locale, strval string) error {
	var intval int
	if strval != "" || !value.ZeroIsNull {
		var err error
		intval, err = locale.ParseInt(strval)
		if err != nil {
			return errors.New("Not a valid integer value: " + strval)
		}
	}
	return value.validateInt(intval)
}

func (value *ValueInt) GetAsLocalizedString(locale Locale) string {
	intval, err := value.GetAsInt()
	if err != nil {
		return ""
	}
	return locale.FormatInt(intval)
}

func (value *ValueInt) SetAsLocalizedString(locale Locale, param string) error {
	intval, err := locale.ParseInt(param)
	if err != nil {
		return err
	}
	return value.SetAsString(strconv.Itoa(intval))
}

func (value *ValueInt) GetAsHtml(locale Locale) string {
	if value.OnGetHTMLValue != nil {
		return value.OnGetHTMLValue(value, locale)
	}
	valstr := value.GetAsString()
	val := value.GetAsLocalizedString(locale)
	if value.ZeroIsNull && val == "0" {
		return ""
	}
	for _, sel := range value.SelectionList {
		if sel.Value == valstr {
			val = sel.Title
			return html.EscapeString(val)
		}
	}

	return fmt.Sprintf(`<div style="text-align: right;">%s</div>`, html.EscapeString(val))
}

//*******************************************************************

type ValueFloat struct {
	FloatRef   *float64
	Decimals   int
	ZeroIsNull bool                       // If true, a zero value is represented by an empty string
	Min, Max   float64                    // If Min,Max=0, then no validation is performed
	OnValidate func(intval float64) error // If set, it will be called to add extra validation. Function must return error with user message if validation doesn't passe.
}

var _ FieldValue = &ValueFloat{}

func (value *ValueFloat) GetSelectionList() []ValueAndTitle {
	return nil
}

func (value *ValueFloat) GetAsString() string {
	if value.FloatRef == nil {
		slog.Error("ValueFloat is used without a float reference", slog.String("msgid", "98JBQ"))
		return ""
	}
	if value.ZeroIsNull && *value.FloatRef == 0 {
		return ""
	}
	return fmt.Sprintf(`%v`, *value.FloatRef)
}

func (value *ValueFloat) validateFloat(val float64) error {
	if value.OnValidate != nil {
		err := value.OnValidate(val)
		if err != nil {
			return err
		}
	}
	if value.Min != 0 || value.Max != 0 {
		if val < value.Min {
			return UserErrorMsg(fmt.Sprintf(T("Value is below minimum of %v."), value.Min))
		}
		if val > value.Max {
			return UserErrorMsg(fmt.Sprintf(T("Value is above maximum of %v."), value.Max))
		}
	}
	return nil
}

func (value *ValueFloat) Validate(locale Locale, strval string) error {
	val, err := locale.ParseFloat(strval)
	if err != nil {
		return err
	}
	return value.validateFloat(val)
}

func (value *ValueFloat) SetAsString(strval string) error {
	var floatval float64
	if !value.ZeroIsNull || strval != "" {
		var err error
		floatval, err = strconv.ParseFloat(strval, 64)
		if err != nil {
			return err
		}
	}
	err := value.validateFloat(floatval)
	if err != nil {
		return err
	}
	if value.FloatRef != nil {
		*value.FloatRef = floatval
	} else {
		return errors.New("floatref is missing in ValueFloat")
	}
	return nil
}

func (value *ValueFloat) GetAsLocalizedString(locale Locale) string {
	return locale.FormatFloat(*value.FloatRef, value.Decimals)
}

func (value *ValueFloat) SetAsLocalizedString(locale Locale, param string) error {
	if value.FloatRef == nil {
		return errors.New("valueFloat.FloatRef is missing")
	}
	fl, err := locale.ParseFloat(param)
	if err != nil {
		return err
	}
	*value.FloatRef = fl
	return nil
}

func (value *ValueFloat) GetAsHtml(locale Locale) string {
	if value.ZeroIsNull && *value.FloatRef == 0 {
		return ""
	}
	valuestr := locale.FormatFloat(*value.FloatRef, value.Decimals)
	return fmt.Sprintf(`<div style="text-align: right;">%s</div>`, valuestr)
}

//*******************************************************************

type ValueStr struct {
	StrRef         *string
	LengthMin      int
	LengthMax      int
	Required       bool // Same as LengthMin=1, but with different error message
	TrimSpace      bool // If true, spaces at start and end are trimmed
	SelectionList  []ValueAndTitle
	OnValidate     func(strval string) error                   // If set, it will be called to add extra validation. Function must return error with user message if validation doesn't passe.
	OnGetHTMLValue func(value *ValueStr, locale Locale) string // Provides value as read/only HTML. Usually not set, as this is handled automatically.
}

var _ FieldValue = &ValueStr{}

func (value *ValueStr) GetSelectionList() []ValueAndTitle {
	return value.SelectionList
}

func (value *ValueStr) GetAsString() string {
	if value.StrRef == nil {
		return ""
	}
	return *value.StrRef
}

func (value *ValueStr) validateStr(strval string) error {
	if value.Required && strval == "" {
		return UserErrorMsg(T("Value must be provided"))
	}
	strlen := utf8.RuneCountInString(strval)
	if value.OnValidate != nil {
		err := value.OnValidate(strval)
		if err != nil {
			return err
		}
	}
	if strlen < value.LengthMin {
		return UserErrorMsg(fmt.Sprintf(T("Text is shorter than the minimum length of %d."), value.LengthMin))
	}
	if value.LengthMax != 0 && strlen > value.LengthMax {
		return UserErrorMsg(fmt.Sprintf(T("Text is longer than the maximum length of %d."), value.LengthMax))
	}
	return nil
}

func (value *ValueStr) Validate(locale Locale, strval string) error {
	return value.validateStr(strval)
}

func (value *ValueStr) SetAsString(strval string) error {
	err := value.validateStr(strval)
	if err != nil {
		return err
	}
	if value.StrRef != nil {
		if value.TrimSpace {
			strval = strings.TrimSpace(strval)
		}
		*value.StrRef = strval
	} else {
		return errors.New("strref is missing in ValueStr")
	}
	return nil
}

func (value *ValueStr) GetAsHtml(locale Locale) string {
	if value.OnGetHTMLValue != nil {
		return value.OnGetHTMLValue(value, locale)
	}
	val := value.GetAsString()
	for _, sel := range value.SelectionList {
		if sel.Value == val {
			val = sel.Title
			break
		}
	}
	return html.EscapeString(val)
}

func (value *ValueStr) GetAsLocalizedString(locale Locale) string {
	if value.StrRef == nil {
		slog.Error("ValueStr.StrRef is nil.", slog.String("msgid", "JPOIUQ"))
	}
	return *value.StrRef
}

func (value *ValueStr) SetAsLocalizedString(locale Locale, param string) error {
	if value.StrRef == nil {
		return errors.New("valueStr.StrRef is missing")
	}
	*value.StrRef = param
	return nil
}

//*******************************************************************

type ValueBool struct {
	BoolRef        *bool
	Required       bool
	StringFalse    string                                       // Defaults to minus
	StringTrue     string                                       // Defaults to checkmark
	OnValidate     func(intval int) error                       // If set, it will be called to add extra validation. Function must return error with user message if validation doesn't passe.
	OnGetHTMLValue func(value *ValueBool, locale Locale) string // Provides value as read/only HTML. Usually not set, as this is handled automatically.
}

var _ FieldValue = &ValueBool{}

func (value *ValueBool) GetSelectionList() []ValueAndTitle {
	return nil
}

func (value *ValueBool) GetAsString() string {
	if value.BoolRef == nil {
		return ""
	}
	if *value.BoolRef {
		return "1"
	} else {
		return "0"
	}
}

func (value *ValueBool) SetAsString(strval string) error {
	boolval := strval != "" && strval != "0"
	err := value.validateBool(boolval)
	if err != nil {
		return err
	}
	*value.BoolRef = boolval
	return nil
}

func (value *ValueBool) validateBool(val bool) error {
	if value.Required && !val {
		return UserErrorMsg(T("This must be set"))
	}
	return nil
}

func (value *ValueBool) Validate(locale Locale, strval string) error {
	val := !(strval == "" || strval == "0")
	return value.validateBool(val)
}

func (value *ValueBool) GetAsLocalizedString(locale Locale) string {
	if value.BoolRef == nil {
		return T("N/A")
	}
	if *value.BoolRef {
		if value.StringTrue != "" {
			return value.StringTrue
		} else {
			return "✓"
		}
	} else {
		if value.StringFalse != "" {
			return value.StringFalse
		} else {
			return "-"
		}
	}
}

func (value *ValueBool) SetAsLocalizedString(locale Locale, param string) error {
	if value.BoolRef == nil {
		return errors.New("valueStr.StrRef is missing")
	}
	param = strings.ToLower(strings.TrimSpace(param))
	*value.BoolRef = !(param == "" || param == "0" || param == "false" || param == "-")
	return nil
}

func (value *ValueBool) GetAsHtml(locale Locale) string {
	if value.OnGetHTMLValue != nil {
		return value.OnGetHTMLValue(value, locale)
	}
	if value.BoolRef == nil {
		return e(T("N/A"))
	}
	if *value.BoolRef {
		if value.StringTrue != "" {
			return e(value.StringTrue)
		} else {
			return "✓"
		}
	} else {
		if value.StringFalse != "" {
			return e(value.StringFalse)
		} else {
			return "-"
		}
	}
}

//*******************************************************************

type ValueDateTime struct {
	RefTime        *time.Time
	Required       bool
	Min, Max       time.Time
	FormatLayout   string                                           // If not set, a European default layout will be applied
	OnValidate     func(intval int) error                           // If set, it will be called to add extra validation. Function must return error with user message if validation doesn't passe.
	OnGetHTMLValue func(value *ValueDateTime, locale Locale) string // Provides value as read/only HTML. Usually not set, as this is handled automatically.
}

var _ FieldValue = &ValueDateTime{}

func (value *ValueDateTime) GetSelectionList() []ValueAndTitle {
	return nil
}

func (value *ValueDateTime) GetAsString() string {
	if value.RefTime == nil {
		return ""
	}
	return value.RefTime.Format(time.RFC3339)
}

func (value *ValueDateTime) SetAsString(strval string) error {
	parsedTime, err := time.Parse(time.RFC3339, strval)
	if err != nil {
		return err
	}
	err = value.validateDateTime(parsedTime)
	if err != nil {
		return err
	}
	*value.RefTime = parsedTime
	return nil
}

func (value *ValueDateTime) GetAsLocalizedString(locale Locale) string {
	if value.RefTime == nil {
		slog.Error("valueDateTime.RefTime is nil.", slog.String("msgid", "JPO3UQ"))
	}
	format := value.FormatLayout
	if format == "" {
		format = locale.GetInfo().Format.TimeFormatHHMMSS
	}
	return value.RefTime.Format(format)
}

func (value *ValueDateTime) SetAsLocalizedString(locale Locale, param string) error {
	if value.RefTime == nil {
		return errors.New("valueDateTime.RefTime is missing")
	}
	format := value.FormatLayout
	if format == "" {
		format = locale.GetInfo().Format.TimeFormatHHMMSS
	}
	parsedTime, err := time.Parse(format, param)
	if err != nil {
		return err
	}

	*value.RefTime = parsedTime
	return nil
}

func (value *ValueDateTime) validateDateTime(_ time.Time) error {
	return nil
}

func (value *ValueDateTime) Validate(locale Locale, strval string) error {
	val, err := time.Parse(time.RFC3339, strval)
	if err != nil {
		return err
	}
	return value.validateDateTime(val)
}

func (value *ValueDateTime) GetAsHtml(locale Locale) string {
	if value.OnGetHTMLValue != nil {
		return value.OnGetHTMLValue(value, locale)
	}
	if value.RefTime == nil {
		return "N/A"
	}
	fmtLayout := value.FormatLayout
	if fmtLayout == "" {
		fmtLayout = "02/01/2006 15:04" // Default is European format
	}
	return value.RefTime.Format(fmtLayout)
}

//*******************************************************************

// ValueAny doesn't really work with most editors, but with the right type-cast,
// It can be used to provide complex data structures through RecordAccessor
type ValueAny[X any] struct {
	Ref *X
}

var _ FieldValue = &ValueAny[int]{}

func (value *ValueAny[X]) GetSelectionList() []ValueAndTitle {
	return nil
}

func (value *ValueAny[X]) GetAsString() string {
	return ""
}

func (value *ValueAny[X]) SetAsString(strval string) error {
	return UserErrorMsg(T("Cannot set this value as a string"))
}

func (value *ValueAny[X]) Validate(locale Locale, strval string) error {
	return UserErrorMsg(T("Cannot validate ValueAny"))
}

func (value *ValueAny[X]) GetAsLocalizedString(locale Locale) string {
	return ""
}

func (value *ValueAny[X]) SetAsLocalizedString(locale Locale, param string) error {
	return UserErrorMsg(T("Cannot set ValueAny as localized string"))
}

func (value *ValueAny[X]) GetAsHtml(locale Locale) string {
	return ""
}
