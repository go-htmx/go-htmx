package nest

import (
	"errors"
	"log/slog"
)

// ***** Form component *****

// Non-visual component which must be placed somewhere on the page where it is used
type Form struct {
	Component
	DataSource DataSourceProvider                   // If set, this form will consider itself a datasource editor
	OnSubmit   func(ctx *Context, form *Form) error // If error is returned, the form stays active
	OnValidate func(ctx *Context, form *Form) error // Not needed if validation is done on each edit control

	state                int  // 0=New blank form, 1=editing, 2=validating
	parts                []UI // All panels, editors etc. that belong to the form
	primaryKey           PrimaryKey
	primaryKeyIdentifier string
}

// To be called from a submit button. Returns validation errors as error value
func (form *Form) Submit(ctx *Context) error {
	// Switch to always-validate mode
	form.setState(ctx, 2)

	// Validate form
	validationError := form.validate(ctx)
	if validationError != nil {
		return validationError
	}

	// Run submission
	if form.OnSubmit == nil {
		return errors.New("onsubmit is not set on form component")
	}
	err := form.OnSubmit(ctx, form)
	if err != nil {
		return err
	}

	// On successful submit, clear all data about the form in the context
	form.Clear(ctx)
	form.setState(ctx, 0)
	form.SetPrimaryKey(ctx, nil)
	return nil
}

// Clears all edit fields, sets them to default, also call Clear() before loading data into edit fields.
func (form *Form) Clear(ctx *Context) {
	form.setState(ctx, 1)
	for _, editor := range form.getAllEditors() {
		editor.EnableValidation(false)
		editor.SetValueDefault(ctx)
	}
}

// Setting the primary key means it can be retrieved again. If it changes, the form is cleared. Can be useful for database editing forms
func (form *Form) SetPrimaryKey(ctx *Context, pk PrimaryKey) {
	if pk == nil {
		ctx.Clear(form.ID + "key")
		form.primaryKeyIdentifier = ""
		form.primaryKey = nil
		return
	}
	if form.primaryKey == nil || pk.GetRecordIdentifier() != form.primaryKeyIdentifier {
		form.Clear(ctx)
		form.primaryKey = pk
		form.primaryKeyIdentifier = pk.GetRecordIdentifier()
		ctx.Set(form.ID+"key", form.primaryKeyIdentifier)
	}
}

// Is nil if no primary key was set
func (form *Form) GetPrimaryKey() PrimaryKey {
	return form.primaryKey
}

func (form *Form) getState(ctx *Context) int {
	return ctx.GetInt(form.ID + "state")
}

func (form *Form) setState(ctx *Context, state int) {
	if state == 0 {
		ctx.Clear(form.ID + "state")
	} else {
		ctx.SetInt(form.ID+"state", state)
	}
}

func (form *Form) RegisterComponent(comp UI) {
	if form.ID == "" {
		slog.Error("Form component has no ID, probably because it was not included in the UI", slog.String("msgid", "JWA34H"))
	}
	form.parts = append(form.parts, comp)
}

func (form *Form) Init(ctx *Context) error {
	form.state = form.getState(ctx)
	form.primaryKeyIdentifier = ctx.Get(form.ID + "key")
	if form.state == 0 {
		form.Clear(ctx)
		form.setState(ctx, 1)
	}
	return nil
}

func (form *Form) RunOnInitialize(ctx *Context) error {
	return nil
}

func (form *Form) RunUserEvents(ctx *Context) error {
	return nil
}

func (form *Form) PrepareHTML(ctx *Context) error {
	form.state = form.getState(ctx)
	switch form.state {
	case 0:
		// Form has been submitted successfully
		form.setValidation(false)
	case 1:
		// After first cycles, keep values in edit fields, but do not validate
		form.setValidation(false)
	case 2:
		// After pressing submit button, do validating each time
		form.setValidation(true)
	default:
		slog.Error("Form state error", slog.Int("state", form.state))
	}
	return nil
}

func (form *Form) GetHTML(ctx *Context) string {
	return ""
}

// Recursively go through all panels etc. to find all controls of type Editor
func (form *Form) getAllEditorsFromUI(controls []UI) []Editor {
	var res []Editor
	for _, prov := range controls {
		editor, ok := prov.(Editor)
		if ok {
			res = append(res, editor)
		}
		prov, ok := prov.(UIProvider)
		if ok {
			subs := prov.GetChildren()
			subeds := form.getAllEditorsFromUI(subs)
			res = append(res, subeds...)
		}
	}
	return res
}

// Return list of all Editors that are included for the form
func (form *Form) getAllEditors() []Editor {
	res := form.getAllEditorsFromUI(form.parts)
	return res
}

func (form *Form) setValidation(validate bool) {
	for _, edit := range form.getAllEditors() {
		edit.EnableValidation(validate)
	}
}

func (form *Form) validate(ctx *Context) error {
	editorList := form.getAllEditors()
	var err error
	for _, edit := range editorList {
		edit.EnableValidation(true)
		err = errors.Join(err, edit.GetValidationResult(ctx))
	}
	if form.OnValidate != nil {
		err = errors.Join(err, form.OnValidate(ctx, form))
	}
	return err
}
