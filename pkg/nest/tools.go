package nest

import (
	"html"
	"strings"
	"unicode"

	"golang.org/x/exp/rand"
)

// Shortcut for html.EscapeString()
func e(str string) string {
	return html.EscapeString(str)
}

// Shortcut for html.EscapeString() with linebreaks
func ebr(str string) string {
	// First escape the HTML to avoid security issues like XSS
	escapedString := html.EscapeString(str)

	// Replace newlines with <br> tags
	res := strings.ReplaceAll(escapedString, "\n", "<br>")

	// Replace double-spaces with &nbsp; constructs, to ensure wider spacing
	for {
		reslen := len(res)
		res = strings.ReplaceAll(res, "  ", "&nbsp; ")
		if len(res) == reslen {
			break
		}
	}
	return res
}

// This increases safety on providing identifiers in some cases
func hasOnlySafeCharacters(input string) bool {
	for _, char := range input {
		if !unicode.IsPrint(char) {
			return false
		}
		switch char {
		case '"', '\'', '%', '<', '>', '&', ':', '/', '`', '*', '?', '|':
			return false
		}
	}
	return true
}

// Create a random DOM ID, in case one is needed but none is provided
// Randomness ensures that it fails, if the source code tries to depend on the auto-generated value
func getRandomDomID() string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, 10)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
