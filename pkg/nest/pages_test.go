package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGPages(t *testing.T) {
	var control UI = &Pages{
		Children: []UI{
			&CustomHtml{Html: "Hej"},
		},
	}
	assert.Equal(t, "Hej", control.GetHTML(&Context{}))
}
