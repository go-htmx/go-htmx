package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestButton(t *testing.T) {
	btn := &Button{}
	var control UI = btn
	assert.Equal(t, nil, control.RunUserEvents(&Context{}))
	assert.Equal(t, nil, control.PrepareHTML(&Context{}))
	assert.Less(t, 0, len(control.GetHTML(&Context{})))
	var _ SortableValueProvider = btn
}
