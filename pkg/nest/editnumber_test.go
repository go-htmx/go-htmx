package nest

import (
	"testing"
)

func TestEditNumber(t *testing.T) {
	comp := &EditNumber{}
	var _ SortableValueProvider = comp
	var _ UI = comp
	var _ Editor = comp
}
