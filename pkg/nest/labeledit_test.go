package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLabeledEdit(t *testing.T) {
	comp := &LabelEdit{Caption: "Test 2345"}
	var control UI = comp
	assert.Less(t, 1, len(control.GetHTML(nil)))
	var editor ValueEditor = comp
	editor.SetHtmlValue("123")
}
