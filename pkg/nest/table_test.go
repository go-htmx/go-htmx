package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTable(t *testing.T) {
	comp := &Table{}
	var control UI = comp
	assert.Equal(t, nil, control.RunUserEvents(&Context{}))
	assert.Equal(t, nil, control.PrepareHTML(&Context{}))
	assert.NotEqual(t, "", control.GetHTML(&Context{}))
	var _ UIProvider = comp
}
