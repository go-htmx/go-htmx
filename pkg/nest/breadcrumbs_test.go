package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBreadCrumbs(t *testing.T) {
	comp := BreadCrumbs{
		Crumbs: []BreadCrumbsPart{
			{Caption: "Part 1", URL: ".."},
			{Caption: "Part 2", URL: ".."},
			{Caption: "Part 3", URL: ".."},
		},
	}
	var ht UI = &comp
	assert.NotEqual(t, "", ht.GetHTML(&Context{}))
}
