package nest

// Implements built-in search in certain components

import (
	"strings"
	"unicode"
)

func alphaNumericParts(str string) []string {
	var result []string
	var currentSequence []rune

	for _, char := range str {
		if unicode.IsLetter(char) || unicode.IsDigit(char) {
			currentSequence = append(currentSequence, char)
		} else {
			if len(currentSequence) > 0 {
				result = append(result, string(currentSequence))
				currentSequence = nil
			}
		}
	}

	// Append the last sequence if it exists
	if len(currentSequence) > 0 {
		result = append(result, string(currentSequence))
	}

	return result
}

// Returns 0 if no match. Higher value at a great match
func searchMatch(SearchCriteria, Item string) float64 {
	// Rename common letters in order to make Åbenrå = Aabenraa in Danish
	Item = strings.ReplaceAll(Item, "å", "aa")
	Item = strings.ReplaceAll(Item, "Å", "Aa")
	SearchCriteria = strings.ReplaceAll(SearchCriteria, "å", "aa")
	SearchCriteria = strings.ReplaceAll(SearchCriteria, "Å", "Aa")

	// Perfect matches
	if SearchCriteria == Item {
		return 999
	}
	ItemUpper := strings.ToUpper(Item)
	if strings.ToUpper(SearchCriteria) == ItemUpper {
		return 998
	}

	// Partial matches
	Items := alphaNumericParts(Item)
	result := -float64(len(Item)) / 700
	words := alphaNumericParts(SearchCriteria)
	for wordindex, word := range words {
		partresult := 0.0
		countFound := 0
		for itindex, it := range Items {
			if it == "" {
				continue
			}
			countFound++
			weight := (float64(wordindex)+float64(itindex))/10 + float64(len(it))/1000
			thisMatch := 0.0
			wordUpper := strings.ToUpper(word)
			itUpper := strings.ToUpper(it)
			if strings.HasPrefix(it, word) {
				// Case sensitive
				thisMatch = 4.0
			} else if strings.HasPrefix(itUpper, wordUpper) {
				thisMatch = 3.0
			} else if strings.Contains(it, word) {
				// Case sensitive
				thisMatch = 2.0
			} else if strings.Contains(itUpper, wordUpper) {
				thisMatch = 1.0
			}
			if thisMatch > 0 {
				partresult += (thisMatch - weight) / float64(len(Items))
			}
		}
		if countFound != 0 && partresult == 0 {
			return 0
		}
		result += partresult
	}
	if len(Items) != 0 {
		result = result / float64(len(Items))
	}
	return max(0, result+100)
}
