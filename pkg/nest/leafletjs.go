package nest

import (
	"fmt"
	"html"
	"log/slog"
	"math"
	"strconv"
	"strings"
)

/*
 This file implements support for Leaflet.js - a map viewer
 This code requires the web page to have loaded jshtmx.js
 The component will:
 1) Run JavaScript to fetch data for the map via JSON
 2) Show the data on a map
 3) Allow you to filter, which parts of the data you want to show
 4) Allow you to have separate JavaScript code that interacts with this
*/

type LeafletJsMap struct {
	Component

	// Map object
	Width, Height Length // Height is mandatory.

	// One of the following can be set to provide zoom level
	Bounds     GeoBounds // What part of the map to show. If not set, it will auto-center
	DiameterKM float64   // km in diameter.

	// Data to show
	Data LeafletjsMapData

	// Datasource will affect the Data.Marker
	DataSource          DataSourceProvider // If connected, it will load its values from there
	DataSourceLatitude  FieldName          // Required for DataSource
	DataSourceLongitude FieldName          // Required for DataSource

	OnInitialize  func(mymap *LeafletJsMap, ctx *Context) error
	OnPrepareHTML func(mymap *LeafletJsMap, ctx *Context) error

	EnableInteractiveMarker bool // If true, allows single interactive marker
}

var _ UI = &LeafletJsMap{}

type LeafletjsPoint struct {
	Coor         GeoPoint
	Radius       float64 // If zero, a marker will be used, instead
	Transparency float64 // Opposite of Opacity because default is zero
	FillColor    Color   // Default color will be applied. E.g. "red"
	Color        Color   // Default color will be applied
	Weight       float64 // Weight of border
	Title        string  // Tooltip
}

type LeafletjsPolyline struct {
	Color        Color
	Transparency float64 // Opposite of Opacity because default is zero
	Coors        []GeoPoint
}

type LeafletjsMapData struct {
	PointList []LeafletjsPoint // Points to draw
	Polylines []LeafletjsPolyline
	Marker    LeafletjsPoint
}

func (mapobj *LeafletJsMap) Init(ctx *Context) error {
	lat := ctx.GetFloat(mapobj.ID + "_lat")
	long := ctx.GetFloat(mapobj.ID + "_lng")
	if lat != 0.0 || long != 0.0 {
		mapobj.Data.Marker.Coor = GeoPoint{
			Latitude:  lat,
			Longitude: long,
		}
	}
	ctx.Clear(mapobj.ID + "_lat")
	ctx.Clear(mapobj.ID + "_lng")

	return nil
}

func (mapobj *LeafletJsMap) RunOnInitialize(ctx *Context) error {
	if mapobj.DataSource != nil {
		fieldLatitude := mapobj.DataSource.GetField(ctx, mapobj.DataSourceLatitude)
		fieldLongitude := mapobj.DataSource.GetField(ctx, mapobj.DataSourceLongitude)
		if mapobj.DataSource.GetStateShouldFormsLoadNewData(ctx) {
			lat, err1 := strconv.ParseFloat(fieldLatitude.GetAsString(), 64)
			long, err2 := strconv.ParseFloat(fieldLongitude.GetAsString(), 64)
			if err1 == nil && err2 == nil {
				mapobj.Data.Marker.Coor = GeoPoint{
					Latitude:  lat,
					Longitude: long,
				}
			}
		} else {
			lat := ctx.GetFloat(mapobj.ID + "_lat")
			long := ctx.GetFloat(mapobj.ID + "_lng")
			if lat != 0.0 || long != 0.0 {
				mapobj.Data.Marker.Coor = GeoPoint{
					Latitude:  lat,
					Longitude: long,
				}
			}
			fieldLatitude.SetAsString(fmt.Sprintf("%.7f", mapobj.Data.Marker.Coor.Latitude))
			fieldLongitude.SetAsString(fmt.Sprintf("%.7f", mapobj.Data.Marker.Coor.Longitude))
		}
		mapobj.EnableInteractiveMarker = !mapobj.DataSource.GetStateReadOnly(ctx)
	}
	if mapobj.OnInitialize != nil {
		return mapobj.OnInitialize(mapobj, ctx)
	}
	return nil
}

func (mapobj *LeafletJsMap) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (mapobj *LeafletJsMap) PrepareHTML(htmxInfo *Context) error {
	if mapobj.OnPrepareHTML != nil {
		err := mapobj.OnPrepareHTML(mapobj, htmxInfo)
		if err != nil {
			return err
		}
	}
	return nil
}

func geoPointToJs(coor GeoPoint) string {
	return fmt.Sprintf("[%.8f,%.8f]", coor.Latitude, coor.Longitude)
}

func geoBoundsTojs(bounds GeoBounds) string {
	return fmt.Sprintf("[%s,%s]", geoPointToJs(bounds[0]), geoPointToJs(bounds[1]))
}

const loadLeafletJavaScript = `
function loadLeaflet() {
	const initializeMap = () => {
		%s
		%s
		%s
	};

    if (typeof L !== 'undefined') {
        // Reset the map container to prevent reuse issues
        const mapContainer = document.getElementById('%s');
        if (mapContainer) {
            mapContainer.innerHTML = '';
            mapContainer._leaflet_id = null; // Reset Leaflet's internal reference
        }

        // Wait briefly to ensure the DOM is fully updated
        setTimeout(initializeMap, 1);
        return Promise.resolve();
    }

	const script = document.createElement('script');
	script.src = "https://unpkg.com/leaflet@1.9.4/dist/leaflet.js";
	script.integrity = "sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=";
	script.crossOrigin = "";
	script.onload = () => {
		const link = document.createElement('link');
		link.rel = "stylesheet";
		link.href = "https://unpkg.com/leaflet@1.9.4/dist/leaflet.css";
		document.head.appendChild(link);

		setTimeout(initializeMap, 1);
	};
	script.onerror = () => {
		console.error("Failed to load Leaflet");
		reject(new Error("Failed to load Leaflet"));
	};
	document.head.appendChild(script);
}
loadLeaflet();
`

// Gets the HTML for the HTMX request to show a map
func (mapobj *LeafletJsMap) GetHTML(htmxinfo *Context) string {
	if mapobj.Omitted {
		return ""
	}
	if mapobj.ID == "" {
		slog.Error("Missing ID value for leaflet map", slog.String("msgid", "R5071W"))
		mapobj.ID = "LeafletMap"
	}
	if mapobj.Height.IsZero() {
		slog.Error("Missing height for leaflet map", slog.String("ID", mapobj.ID), slog.String("msgid", "R5171W"))
		// Non-rounded sizes so that they can be recognized in the browser developer tools
		mapobj.Height = Px(253)
		mapobj.Width = Px(404)
	}

	leafletMapID := getRandomDomID()

	// Automatic center
	if mapobj.Bounds.IsZero() && mapobj.DiameterKM != 0 {
		// Calculate bounds using a diameter
		fullPointList := mapobj.Data.PointList
		if !mapobj.Data.Marker.Coor.IsZero() {
			fullPointList = append(fullPointList, mapobj.Data.Marker)
		}
		countPoints := float64(len(fullPointList))

		if countPoints != 0 {
			var lat, long float64
			for _, point := range fullPointList {
				lat += point.Coor.Latitude
				long += point.Coor.Longitude
			}
			lat = lat / countPoints
			long = long / countPoints
			distlat := mapobj.DiameterKM / 110.574
			distlong := mapobj.DiameterKM / 111.320 / math.Cos(math.Pi/180.0*lat)
			mapobj.Bounds = GeoBounds{
				GeoPoint{Latitude: lat - distlat/2, Longitude: long - distlong/2},
				GeoPoint{Latitude: lat + distlat/2, Longitude: long + distlong/2},
			}
		}
	}
	if mapobj.Bounds.IsZero() {
		mapobj.Bounds = GeoBounds{}
		// Only set bounds automatically, if not already specified by caller
		for _, point := range mapobj.Data.PointList {
			mapobj.Bounds.Expand(point.Coor)
		}
		if !mapobj.Data.Marker.Coor.IsZero() {
			mapobj.Bounds.Expand(mapobj.Data.Marker.Coor)
		}

		if mapobj.Bounds.IsZero() {
			mapobj.Bounds.Expand(GeoPoint{
				Latitude:  45,
				Longitude: 23,
			})
			mapobj.Bounds.Expand(GeoPoint{
				Latitude:  52,
				Longitude: 40,
			})
		}

		mapobj.Bounds.ZoomOut(1.1)
	}

	// Add map
	var sl []string
	style := fmt.Sprintf("height: %s; width: %s;", mapobj.Height.String(), mapobj.Width.String())
	sl = append(sl, fmt.Sprintf(`<div style="%s" id="%s"></div>`, html.EscapeString(style), html.EscapeString(leafletMapID)))
	sl = append(sl, `<script>`)
	sl = append(sl, fmt.Sprintf(`
		var map%s;
		var marker%s;
		`,
		mapobj.ID, mapobj.ID))

	createMapWithBoundsJs := fmt.Sprintf(
		`map%s=L.map('%s').fitBounds(%s);`,
		mapobj.ID, leafletMapID, geoBoundsTojs(mapobj.Bounds),
	)

	setTileLayer := fmt.Sprintf(
		`	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				attribution: '© OpenStreetMap contributors'
			}).addTo(map%s);
		`, mapobj.ID)

	var linesl []string
	// Draw line between points
	for _, polyline := range mapobj.Data.Polylines {
		var pl []string
		for _, coor := range polyline.Coors {
			pl = append(pl, geoPointToJs(coor))
		}
		color := polyline.Color
		if color == "" {
			color = "blue"
		}
		jsline := fmt.Sprintf(`L.polyline([%s], {opacity: %.2f, color: '%s'}).addTo(map%s);`,
			strings.Join(pl, ","), 1.0-polyline.Transparency, color, mapobj.ID)
		linesl = append(linesl, jsline)
	}

	// Draw points
	for _, point := range mapobj.Data.PointList {
		linesl = append(linesl, addPointToMap(point, mapobj.ID, ""))
	}

	if !mapobj.Data.Marker.Coor.IsZero() {
		linesl = append(linesl,
			addPointToMap(mapobj.Data.Marker, mapobj.ID, "marker"+mapobj.ID),
		)
	}
	if mapobj.EnableInteractiveMarker {
		linesl = append(linesl,
			fmt.Sprintf(`
				if (!document.body.%shasListenerAttached) {
				document.body.addEventListener('htmx:configRequest', function(evt) {
					if (marker%s) {
						evt.detail.parameters['%s%s_lat'] = marker%s.getLatLng().lat;
						evt.detail.parameters['%s%s_lng'] = marker%s.getLatLng().lng;
					}
				});
				document.body.%shasListenerAttached = true;
				}
				`,
				mapobj.ID, mapobj.ID,
				OTprefix, mapobj.ID, mapobj.ID,
				OTprefix, mapobj.ID, mapobj.ID,
				mapobj.ID,
			),
			fmt.Sprintf(`
				map%s.on('click', function(e) {
					if (marker%s) {
						map%s.removeLayer(marker%s);
					}
					
					marker%s = L.marker([e.latlng.lat, e.latlng.lng], {
						draggable: true
					}).addTo(map%s);
				});
				`,
				mapobj.ID, mapobj.ID, mapobj.ID, mapobj.ID, mapobj.ID, mapobj.ID,
			),
		)
	}

	sl = append(sl, fmt.Sprintf(loadLeafletJavaScript,
		createMapWithBoundsJs, setTileLayer, strings.Join(linesl, "\n"),
		leafletMapID,
	))

	sl = append(sl, `</script>`)
	return strings.Join(sl, "\n")
}

// Generates javaScript that adds a point to the map
func addPointToMap(point LeafletjsPoint, id string, variableName string) string {
	var assignment string
	if variableName != "" {
		assignment = variableName + " ="
	}
	if point.Radius == 0 {
		return fmt.Sprintf(
			`%s L.marker(%s,{title: '%s'}).addTo(map%s);`,
			assignment, geoPointToJs(point.Coor), jsEscape(point.Title), id,
		)
	} else {
		color := colorWithDefault(point.Color, ColorRed)
		fillColor := colorWithDefault(point.FillColor, ColorRed)
		return fmt.Sprintf(
			`%s L.circle(%s,{radius: %.0f, fillOpacity: %.2f, color: '%s', fillColor: '%s', weight: %v}).addTo(map%s);`,
			assignment, geoPointToJs(point.Coor), point.Radius, 1.0-point.Transparency, color, fillColor, point.Weight, id,
		)
	}
}

// Escapes a string for use inside ” in JavaScript
func jsEscape(text string) string {
	return strings.ReplaceAll(text, "'", "\\'")
}

func colorWithDefault(value Color, defaultValue Color) Color {
	if value == "" {
		return defaultValue
	}
	return value
}
