package nest

import (
	"fmt"
	"html"
	"log/slog"
	"math"
	"strings"
)

type OpenStreetMap struct {
	Component

	// Map object
	Width, Height Length // Height is mandatory.

	// One of the following can be set to provide zoom level
	Bounds     GeoBounds // What part of the map to show. If not set, it will auto-center
	DiameterKM float64   // km in diameter.

	// Data to show
	Marker GeoPoint

	OnInitialize  func(mymap *OpenStreetMap, ctx *Context) error
	OnPrepareHTML func(mymap *OpenStreetMap, ctx *Context) error
}

var _ UI = &OpenStreetMap{}

func (mapobj *OpenStreetMap) RunOnInitialize(ctx *Context) error {
	if mapobj.OnInitialize != nil {
		return mapobj.OnInitialize(mapobj, ctx)
	}
	return nil
}

func (mapobj *OpenStreetMap) RunUserEvents(ctx *Context) error {
	return nil
}

func (mapobj *OpenStreetMap) PrepareHTML(ctx *Context) error {
	if mapobj.OnPrepareHTML != nil {
		err := mapobj.OnPrepareHTML(mapobj, ctx)
		if err != nil {
			return err
		}
	}

	// Automatic center
	if mapobj.Bounds.IsZero() && mapobj.DiameterKM != 0 && !mapobj.Marker.IsZero() {
		lat := mapobj.Marker.Latitude
		long := mapobj.Marker.Longitude
		distlat := mapobj.DiameterKM / 110.574
		distlong := mapobj.DiameterKM / 111.320 / math.Cos(math.Pi/180.0*lat)
		mapobj.Bounds = GeoBounds{
			{Latitude: lat - distlat/2, Longitude: long - distlong/2},
			{Latitude: lat + distlat/2, Longitude: long + distlong/2},
		}
	}

	return nil
}

// Gets the HTML for the HTMX request to show a map
func (mapobj *OpenStreetMap) GetHTML(ctx *Context) string {
	if mapobj.Omitted {
		return ""
	}
	if mapobj.Height.IsZero() {
		slog.Error("Missing height for map", slog.String("ID", mapobj.ID), slog.String("msgid", "R5172A"))
		// Non-rounded sizes so that they can be recognized in the browser developer tools
		mapobj.Height = Px(253)
		mapobj.Width = Px(404)
	}

	params := make(map[string]string)
	params["bbox"] = fmt.Sprintf(`%.7f,%.7f,%.7f,%.7f`, mapobj.Bounds[0].Longitude, mapobj.Bounds[0].Latitude, mapobj.Bounds[1].Longitude, mapobj.Bounds[1].Latitude)
	params["layer"] = "mapnik"
	if !mapobj.Marker.IsZero() {
		params["marker"] = fmt.Sprintf(`%.7f,%.7f`, mapobj.Marker.Latitude, mapobj.Marker.Longitude)
	}

	var paramList []string
	for key, value := range params {
		paramList = append(paramList, fmt.Sprintf(`%s=%s`, html.EscapeString(key), html.EscapeString(value)))
	}

	osrurl := `https://www.openstreetmap.org/export/embed.html?` + strings.Join(paramList, "&")

	attrs := AttributeList{
		"id":          mapobj.ID,
		"width":       mapobj.Width.String(),
		"height":      mapobj.Height.String(),
		"frameborder": "0",
		"scrolling":   "no",
		"src":         osrurl,
	}

	return fmt.Sprintf(`<iframe %s></iframe>`+"\n", attrs.GetHTML(ctx))
}
