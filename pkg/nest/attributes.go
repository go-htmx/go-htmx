package nest

import (
	"fmt"
	"html"
)

type AttributeList map[string]string

func (list *AttributeList) Add(attribute string, values string) {
	if *list == nil {
		*list = make(AttributeList)
	}
	(*list)[attribute] = (*list)[attribute] + " " + values
}

func (list *AttributeList) GetHTML(ctx *Context) string {
	if list == nil || *list == nil {
		return ""
	}
	attr := ""
	for attribute, value := range *list {
		attr = fmt.Sprintf(`%s %s="%s" `, attr, html.EscapeString(attribute), html.EscapeString(value))
	}
	return attr
}
