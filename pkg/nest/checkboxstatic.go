package nest

import (
	"fmt"
)

// Provides a checkbox that is checked or not

type CheckBoxStatic struct {
	ID    string // Set automatically, if needed
	Class string // Defaults to GCheckBox
}

// Returns HTML, activating HTMX request upon change
// If method is empty, "GET" is default
func (btn *CheckBoxStatic) GetHtml(htmxInfo *Context) string {
	if btn.ID == "" {
		btn.ID = getRandomDomID()
	}
	if btn.Class == "" {
		btn.Class = GenerateComponentClassName("GCheckBox")
	}
	curHtmx := htmxInfo.Copy()
	isSet := htmxInfo.GetBool(btn.ID)
	curHtmx.SetBool(btn.ID, !isSet)
	setHtml := ""
	if isSet {
		setHtml = "checked"
	}
	return fmt.Sprintf(`<input id="%s" class="%s" style="cursor:pointer;" type="checkbox" %s %s>`,
		e(btn.ID), e(btn.Class), curHtmx.GetHxAttrs(), setHtml)
}
