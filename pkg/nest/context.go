package nest

import (
	"context"
	"fmt"
	"html"
	"log/slog"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
)

/*
This struct helps:

1. Set HTMX attributes like hx-get, hx-target etc.
2. Saving state data in the client-side HTML.
3. Error handling during preparation of website, so that errors can be shown to end-user nicely
*/
type Context struct {
	// Incoming request information
	IsHtmxRequest       bool            // True, if the current request that is being handled, was made by the client-side HTMX library
	IncomingMethod      string          // Method from the incoming request, usually GET, POST, PUT, DELETE
	HxCurrentBrowserURL string          // Provided by htmx, only valid when IsHtmxRequest is true
	Context             context.Context // Useful for accessing username, permission lists etc., will be set from the http.Request

	// Information transport from incoming request to outgoing HTML. Use Set() and Get() to access this.
	params   map[string]string // URL-transported items
	formdata map[string]string // Data received using POST/PUT/DELETE etc., this will not be used in GetHxAttr()

	// Preparations of hx-* attributes for outgoing HTML. Set these before calling .GetHxAttrs()
	UrlPrefix   string // URL for hx-get etc., but only up til "?". Will be set automatically to incoming URL.
	HxTarget    string
	HxSelect    string
	HxSwap      string
	HxIndicator string
	HxTrigger   string
	HxMethod    string // If set, this becomes the default for .GetHxAttrs()
	HxConfirm   string // If set, the user must confirm the text stored here, before even is triggered

	// Private storage
	hxSelectOobItems []oobFilter // Empty by default, add to this using .AddOob() functions

	RequireWCAG bool // If yes, all components should report error to slog if WCAG is violated. https://www.w3.org/WAI/standards-guidelines/wcag/
	Locale      Locale

	Framework *ContextFramework

	idAutoIncrement int
}

type ContextFrameworkFileDownload struct {
	MimeType     string
	FileName     string
	FileContents []byte // Must be set in order for this struct to be considered filled in
}

// Contains items that should not be copied on Context.Copy()
type ContextFramework struct {
	// List of errors to show to the end-user as a modal dialog or similar.
	// Will be filled during processing of incoming request to webserver, and automatically added by Context.RequestHandler
	ErrorListForUser   []string
	RestartWithContext *Context // If set, the page-building must restart after RunUserEvents, with this new Context.

	RedirectURL  string                       // Can be used to do a client-side redirect to a new location
	Location     string                       // Allows you to do a client-side redirect that does not do a full page reload
	HxPush       string                       // Will make the server show a new URL
	Refresh      bool                         // If set to “true” the client-side will do a full refresh of the page
	DownloadFile ContextFrameworkFileDownload // If set, this file will be downloaded instead of starting to build HTML

	initPhase initStage // Used for keeping track of initialization phase. This is just to have extra checks for initialization sequences.
}

type oobFilter struct {
	IdPrefix    string // This must be the prefix of an hx-select in order to trigger this item
	HxSelectOob string // This is the value to pass to HxSelectOob, if this is triggered
}

const OTprefix = "OT~"                                      // Used for variables that will be submitted once to the server but not kept around
const SkipTemplateParameter = OTprefix + "HtmxSkipTemplate" // If this is set as a parameter, the default request handler should let the framework handle the response and not put the framework-generated HTML into a template before sending it to the browser.

// Create Context from incoming Request
func NewContext(r *http.Request) Context {
	htmxInfo := Context{
		UrlPrefix: r.URL.Path,
		Context:   r.Context(),
		Framework: &ContextFramework{},
	}

	// Retrieve parameters from URL
	htmxInfo.formdata = make(map[string]string)
	params := r.URL.Query()
	for key := range params {
		value := params.Get(key)
		if strings.HasPrefix(key, OTprefix) {
			htmxInfo.formdata[key[len(OTprefix):]] = value
		} else {
			htmxInfo.Set(key, value)
		}
	}

	// Retrieve parameters from URL patterns like /resource/{resourceid}
	varMap := mux.Vars(r)
	for key, value := range varMap {
		htmxInfo.Set(key, value)
	}

	// Retrieve parameters from POST data, they will overwrite everything else
	switch r.Method {
	case "POST", "PUT", "PATCH", "DELETE":
		r.ParseForm()
		for key, values := range r.Form {
			if len(values) > 0 {
				pkey := key
				value := values[0]
				if strings.HasPrefix(key, OTprefix) {
					pkey = pkey[len(OTprefix):]
				}
				htmxInfo.formdata[pkey] = value
			}
		}
	}

	htmxInfo.IsHtmxRequest = r.Header.Get("HX-Request") != "" || htmxInfo.GetBool(SkipTemplateParameter)
	htmxInfo.HxCurrentBrowserURL = r.Header.Get("HX-Current-URL")
	htmxInfo.IncomingMethod = r.Method
	htmxInfo.Locale = NewLocale("en_US", "UTC")
	return htmxInfo
}

// Should only be called in Button.OnClick or similar, which are protected against running twice.
// Useful after RunUserEvents, in case an event handler has modified the database data, so that
// the number of components in the UI has changed. For instance, if a button click adds more parts to the UI,
// then the http request handler must first build the UI that has the button Onclick handler, then run the OnClick handler,
// and then the database data was changed, and then the http handler must rebuild the UI based on updated database data.
func (htmxInfo *Context) Restart() {
	htmxInfo.Framework.RestartWithContext = htmxInfo.Copy()
}

// Makes the framework redirect the browser to another URL using HTTP response, and not create HTML
func (htmxInfo *Context) RedirectURL(url string) {
	htmxInfo.Framework.RedirectURL = url
}

// Location makes the framework trigger a client side redirection without reloading the whole page.
// Instead of changing the page’s location it will create a new history entry and issue an ajax
// request to the value of the header and push the path into history.
func (htmxInfo *Context) Location(url string) {
	htmxInfo.Framework.Location = url
}

// Make the browser show this URL instead of the actual URL. Should not include server name but just start with /something
func (htmxInfo *Context) HxPush(url string) {
	htmxInfo.Framework.HxPush = url
}

// Request a full client-side refresh of the page
func (htmxInfo *Context) Refresh() {
	htmxInfo.Framework.Refresh = true
}

// Adds that any hx-select on an ID with the specified prefix must add an hx-select-oob with the specified value.
// This can be called multiple times, adding to the list of IDs in the hx-select-oob
// hxSelectPrefix is without "#"
func (htmxInfo *Context) AddOob(hxSelectPrefix, hxSelectOob string) {
	if hxSelectPrefix == "" {
		slog.Error("idPrefix was empty in call to AddOob()", slog.String("msgid", "7FGC9O"))
	} else {
		hxSelectPrefix = strings.TrimPrefix(hxSelectPrefix, "#")
	}
	htmxInfo.hxSelectOobItems = append(htmxInfo.hxSelectOobItems, oobFilter{
		IdPrefix:    "#" + hxSelectPrefix,
		HxSelectOob: hxSelectOob,
	})
}

// Makes the response to an HTMX request set the new URL to the old URL, but with the parameter changed
func (htmxInfo *Context) SwitchURL(paramKey string) {
	u, err := url.Parse(htmxInfo.HxCurrentBrowserURL)
	if err != nil {
		slog.Error("Error parsing URL", slog.String("url", htmxInfo.HxCurrentBrowserURL), slog.String("error", err.Error()), slog.String("msgid", "KZIUQN"))
		return
	}
	paramValue := htmxInfo.Get(paramKey)

	query := u.Query()
	if paramValue == "" {
		// Remove query parameter
		query.Del(paramKey)
	} else {
		// Add query parameters
		query.Set(paramKey, paramValue)
	}
	u.RawQuery = query.Encode()
	finalURI := u.RequestURI()
	htmxInfo.HxPush(finalURI)
}

// Creates all the HTMX attributes needed according to the htmxinfo object.
func (htmxInfo *Context) GetHxAttrs() string {
	method := htmxInfo.HxMethod
	if method == "" {
		method = "GET"
	}
	var sl []string
	sl = append(sl, " ")
	if htmxInfo.HxSwap != "" {
		sl = append(sl, fmt.Sprintf(`hx-swap="%s"`, e(htmxInfo.HxSwap)))
	}
	if htmxInfo.HxTarget != "" {
		sl = append(sl, fmt.Sprintf(`hx-target="%s"`, e(htmxInfo.HxTarget)))
	}
	if htmxInfo.HxIndicator != "" {
		sl = append(sl, fmt.Sprintf(`hx-indicator="%s"`, e(htmxInfo.HxIndicator)))
	}
	if htmxInfo.HxTrigger != "" {
		sl = append(sl, fmt.Sprintf(`hx-trigger="%s"`, e(htmxInfo.HxTrigger)))
	}
	if htmxInfo.HxConfirm != "" {
		sl = append(sl, fmt.Sprintf(`hx-confirm="%s"`, e(htmxInfo.HxConfirm)))
	}
	if htmxInfo.HxSelect != "" {
		sl = append(sl, fmt.Sprintf(`hx-select="%s"`, e(htmxInfo.HxSelect)))
		// Hx-Select-Oob
		var ooblist []string
		for _, item := range htmxInfo.hxSelectOobItems {
			if strings.HasPrefix(htmxInfo.HxSelect, item.IdPrefix) {
				ooblist = append(ooblist, item.HxSelectOob)
			}
		}
		if len(ooblist) != 0 {
			sl = append(sl, fmt.Sprintf(`hx-select-oob="%s"`, e(strings.Join(ooblist, ","))))
		}
	}
	url := htmxInfo.getURL()
	switch method {
	case "GET":
		sl = append(sl, fmt.Sprintf(`hx-get="%s"`, e(url)))
	case "POST":
		sl = append(sl, fmt.Sprintf(`hx-post="%s"`, e(url)))
	case "PATCH":
		sl = append(sl, fmt.Sprintf(`hx-patch="%s"`, e(url)))
	case "PUT":
		sl = append(sl, fmt.Sprintf(`hx-put="%s"`, e(url)))
	case "DELETE":
		sl = append(sl, fmt.Sprintf(`hx-delete="%s"`, e(url)))
	default:
		sl = append(sl, fmt.Sprintf(`<!-- illegal method %s -->`, e(method)))
		slog.Error("Illegal method encountered in GetHxHtml", slog.String("method", method), slog.String("msgid", "ARP0JH"))
	}

	sl = append(sl, " ")
	return strings.Join(sl, " ")
}

// Will add or remove a setting from the list of parameters in htmxinfo
func (htmxInfo *Context) Set(param string, value string) {
	if htmxInfo.params == nil {
		htmxInfo.params = make(map[string]string)
	}
	noPrefixParamName := strings.TrimPrefix(param, OTprefix)
	delete(htmxInfo.formdata, noPrefixParamName)
	delete(htmxInfo.params, noPrefixParamName)
	delete(htmxInfo.params, OTprefix+noPrefixParamName)
	if value != "" {
		htmxInfo.params[param] = value
	}
}

// Set a one-time value, which will be sent on the next request, but the xt framework will treat it as form data
func (htmxInfo *Context) SetOT(param string, value string) {
	htmxInfo.Set(OTprefix+param, value)
}

// Like .Set() but with bool
func (htmxInfo *Context) SetBool(param string, value bool) {
	if value {
		htmxInfo.Set(param, "1")
	} else {
		htmxInfo.Clear(param)
	}
}

// Like .SetOT() but with bool
func (htmxInfo *Context) SetOTBool(param string, value bool) {
	if value {
		htmxInfo.SetOT(param, "1")
	} else {
		htmxInfo.SetOT(param, "0")
	}
}

// Like Set() but with integer value, for ease of conversion.
func (htmxInfo *Context) SetInt(param string, value int) {
	htmxInfo.Set(param, strconv.Itoa(value))
}

// Like SetInt() but if value is zero, parameter is removed
func (htmxInfo *Context) SetIntNonZero(param string, value int) {
	if value == 0 {
		htmxInfo.Clear(param)
	} else {
		htmxInfo.Set(param, strconv.Itoa(value))
	}
}

// Like SetOT() but with integer value, for ease of conversion.
func (htmxInfo *Context) SetOTInt(param string, value int) {
	htmxInfo.SetOT(param, strconv.Itoa(value))
}

// Useful for transmitting a limited number of integers as one parameter. They will be transmitted as a comma separated list
func (htmxInfo *Context) SetIntArray(param string, values []int) {
	var sl []string
	for _, valint := range values {
		sl = append(sl, strconv.Itoa(valint))
	}
	htmxInfo.Set(param, strings.Join(sl, ","))
}

// Returns true if this parameter exists. When submitting form data, some parameters may exist but have empty string.
func (htmxInfo *Context) Has(param string) bool {
	_, found := htmxInfo.params[param]
	if found {
		return true
	}
	_, found2 := htmxInfo.formdata[param]
	return found2
}

// Get parameter from URL, form submission or similar, i.e. the data that is transported from HTML to the server.
// Form submission parameters have higher priority.
func (htmxInfo *Context) Get(param string) string {
	if htmxInfo.formdata != nil {
		value, found := htmxInfo.formdata[param]
		if found {
			return value
		}
	}
	if htmxInfo.params != nil {
		value, found := htmxInfo.params[param]
		if found {
			return value
		}
	}
	return ""
}

// GetFloat Like Get(), but converts to float. Returns 0.0 if no parameter present.
func (htmxInfo *Context) GetFloat(param string) float64 {
	value := htmxInfo.Get(param)
	valfloat, _ := strconv.ParseFloat(value, 64)
	return valfloat
}

// Like Get(), but converts to integer. Returns 0 if no parameter present.
func (htmxInfo *Context) GetInt(param string) int {
	value := htmxInfo.Get(param)
	valint, _ := strconv.Atoi(value)
	return valint
}

// Like Get(), but returns true if parameter exists and is not an empty string
func (htmxInfo *Context) GetBool(param string) bool {
	value := htmxInfo.Get(param)
	return value != "" && value != "0" && value != " "
}

// If the parameter contains a comma separated list of integers, this will return the list. No integers gives nil.
func (htmxInfo *Context) GetIntArray(param string) []int {
	value := htmxInfo.Get(param)
	var res []int
	for _, intstr := range strings.Split(value, ",") {
		valInt, err := strconv.Atoi(intstr)
		if err != nil {
			return nil
		}
		res = append(res, valInt)
	}
	return res
}

func (htmxInfo *Context) Clear(param string) {
	htmxInfo.Set(param, "")
}

// Gets all parameters that match the prefix. If prefix=="" then all parameters are fetched.
// The return value is a copy of the data.
func (htmxInfo *Context) GetParams(prefix string) (copyOfParams map[string]string) {
	copyOfParams = make(map[string]string, len(htmxInfo.params))
	for key, value := range htmxInfo.params {
		if strings.HasPrefix(key, prefix) {
			copyOfParams[key] = value
		}
	}
	for key, value := range htmxInfo.formdata {
		if strings.HasPrefix(key, prefix) {
			copyOfParams[key] = value
		}
	}
	return copyOfParams
}

// Makes a copy, which can be modified
// This is usually done for each element where you need to call .GetHxAttrs()
// The copy represents the desired HX-action and the state after executing the action.
func (htmxInfo *Context) Copy() *Context {
	res := *htmxInfo // makes a copy
	res.params = make(map[string]string)
	for key, val := range htmxInfo.params {
		res.params[key] = val
	}
	res.formdata = make(map[string]string)
	for key, val := range htmxInfo.formdata {
		res.formdata[key] = val
	}
	if htmxInfo.hxSelectOobItems != nil {
		res.hxSelectOobItems = make([]oobFilter, len(htmxInfo.hxSelectOobItems))
		copy(res.hxSelectOobItems, htmxInfo.hxSelectOobItems)
	}

	return &res
}

// Creates a URL according to the htmxRequest, preserving all incoming data.
// Use other functions to modify the Context before calling this function, if needed.
func (htmxInfo *Context) getURL() string {
	res := htmxInfo.UrlPrefix + "?"

	// Extract and sort the keys
	keys := make([]string, 0, len(htmxInfo.params))
	for key := range htmxInfo.params {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	// Construct the URL query string in sorted key order
	var pairs []string
	for _, key := range keys {
		value := htmxInfo.Get(key)
		pairs = append(pairs, fmt.Sprintf("%s=%s", url.QueryEscape(key), url.QueryEscape(value)))
	}
	res += strings.Join(pairs, "&")

	return res
}

// During processing of incoming HTMX request, especially PATCH/POST requests,
// this can be used to save error messages that must be shown when generating HTML code later.
func (htmxInfo *Context) AddErrorMessageForUser(msg string) {
	htmxInfo.Framework.ErrorListForUser = append(htmxInfo.Framework.ErrorListForUser, msg)
}

func getDocHtml(htmxInfo *Context, doc UI) string {
	htmlstr := doc.GetHTML(htmxInfo)
	dialogstr := ""
	if len(htmxInfo.Framework.ErrorListForUser) != 0 {
		msg := strings.Join(htmxInfo.Framework.ErrorListForUser, "\n")
		dialog := ModalDialog{
			Component: Component{Class: "error"},
			Headline:  "Error",
			BodyText:  msg,
		}
		dialogstr = fmt.Sprintf(`<div id="errormessages">%s</div>`, dialog.GetHTML(htmxInfo))
		fmt.Println(T("Error message occured in user interface"), msg)
	}
	htmlstr = fmt.Sprintf(`<div id="errormessage">%s</div>%s`, dialogstr, htmlstr)
	return htmlstr
}

// Makes a copy of the Context, which will only reload the DOM element identified by ID, with hx-swap=outerHTML
func (htmxInfo *Context) CopyForLocalReload(ID string) *Context {
	if ID == "" {
		slog.Error("ID is empty in htmxReloadLocal", slog.String("msgid", "28934S"))
	}
	htmxInfo = htmxInfo.Copy()
	htmxInfo.HxTarget = "#" + ID
	htmxInfo.HxSelect = "#" + ID
	htmxInfo.HxSwap = "outerHTML"
	htmxInfo.AddOob(htmxInfo.HxSelect, "errormessages")
	return htmxInfo
}

func createAndProcessPage(ctxvar **Context, factory UIFactory) UI {
	iterationCounter := 0
	for {
		ctx := *ctxvar
		iterationCounter++
		if iterationCounter == 100 {
			slog.Error("Framework has restarted 100 times. Aborting.", slog.String("url", ctx.UrlPrefix), slog.String("msgid", "AHKJ2A"))
			return &Paragraph{Text: "End-less loop on server. Could not deliver page."}
		}

		// Build page and call functions in the right order
		doc := factory()
		ctx.Framework.initPhase = stageSettingIDs
		setDefaultIDs(doc)
		ctx.Framework.initPhase = stageInit
		recurseInit(ctx, doc)
		ctx.Framework.initPhase = stageRunOnInitialize
		recurseRunOnInitialize(ctx, doc)
		ctx.Framework.initPhase = stageCallUserEvents
		recurseCallUserEvents(ctx, doc)
		ctx.Framework.initPhase = stagePrepareHTML

		if ctx.Framework.RestartWithContext == nil {
			err := doc.PrepareHTML(ctx)
			if err != nil {
				ctx.AddErrorMessageForUser(err.Error())
				slog.Error("Error loading data for UI", slog.String("error", err.Error()), slog.String("msgid", "J2C8GQ"))
			}
			if iterationCounter >= 5 {
				slog.Warn("Framework has restarted many times before delivering the page. The max limit is 100.", slog.String("url", ctx.UrlPrefix), slog.Int("count", iterationCounter), slog.String("msgid", "AHKJ28"))
			} else if iterationCounter >= 3 {
				slog.Debug("Framework has restarted many times before delivering the page. The max limit is 100.", slog.String("url", ctx.UrlPrefix), slog.Int("count", iterationCounter), slog.String("msgid", "AHKJ29"))
			}
			ctx.Framework.initPhase = stageGetHTML
			return doc
		}
		*ctxvar = ctx.Framework.RestartWithContext
		ctx = *ctxvar
		ctx.Framework.RestartWithContext = nil
		ctx.idAutoIncrement = 0

		// Move OneTime parameters
		var listOfOtKeys []string
		for key := range ctx.params {
			if strings.HasPrefix(key, OTprefix) {
				listOfOtKeys = append(listOfOtKeys, key)
			}
		}
		for _, key := range listOfOtKeys {
			value := ctx.params[key]
			newKey := strings.TrimPrefix(key, OTprefix)
			ctx.formdata[newKey] = value
			delete(ctx.params, key)
		}
	}
}

// Handles webserver request handling, based on a webpage described as a data structure.
func (htmxInfo *Context) RequestHandler(w http.ResponseWriter, factory UIFactory) {
	doc := createAndProcessPage(&htmxInfo, factory)

	if len(htmxInfo.Framework.ErrorListForUser) == 0 && htmxInfo.Framework.RedirectURL != "" {
		if htmxInfo.IsHtmxRequest {
			w.Header().Set("HX-Redirect", htmxInfo.Framework.RedirectURL)
		} else {
			h := w.Header()
			h.Set("Location", htmxInfo.Framework.RedirectURL)
			h.Set("Content-Type", "text/html; charset=utf-8")
			w.WriteHeader(302)
			body := fmt.Sprintf(`<html><body><a href="%s">Click here</a>.</body></html>`,
				html.EscapeString(htmxInfo.Framework.RedirectURL))
			fmt.Fprintln(w, body)
		}
		return
	}

	if htmxInfo.Framework.DownloadFile.FileContents != nil {
		if htmxInfo.Framework.DownloadFile.MimeType == "" {
			slog.Error("No mime-type set for downloading file", slog.String("msgid", "MIM5HA"))
			w.Header().Set("Content-Type", "application/octet-stream")
		} else {
			w.Header().Set("Content-Type", htmxInfo.Framework.DownloadFile.MimeType)
		}
		if htmxInfo.Framework.DownloadFile.FileName != "" {
			w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="%s"`, htmxInfo.Framework.DownloadFile.FileName))
		}
		w.Write(htmxInfo.Framework.DownloadFile.FileContents)
		return
	}

	if htmxInfo.IsHtmxRequest && htmxInfo.Framework.Location != "" {
		w.Header().Set("HX-Location", htmxInfo.Framework.Location)
	}

	if htmxInfo.IsHtmxRequest && htmxInfo.Framework.HxPush != "" {
		w.Header().Set("HX-Push", htmxInfo.Framework.HxPush)
	}

	if htmxInfo.IsHtmxRequest && htmxInfo.Framework.Refresh {
		w.Header().Set("HX-Refresh", "true")
	}

	setDefaultIDs(doc)
	htmlstr := getDocHtml(htmxInfo, doc)

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	_, err := w.Write([]byte(htmlstr))
	if err != nil {
		slog.Error("Cannot send webpage", slog.String("error", err.Error()), slog.String("msgid", "KVGUEC"))
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// Same as RequestHandler, but suitable for applying the result to a template
func (ctx *Context) RequestHandlerForTemplate(factory UIFactory) string {
	doc := createAndProcessPage(&ctx, factory)
	setDefaultIDs(doc)
	return getDocHtml(ctx, doc)
}
