package nest

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSearchmatch(t *testing.T) {
	type testcase struct {
		str    string
		search string
		goal   float64
	}
	testdata := []testcase{
		{goal: 999.0, search: "Ullerslev", str: "Ullerslev"},
		{goal: 103.0, search: "ul", str: "Ullerslev"},
		{goal: 103.9, search: "lev ler", str: "Ullerslev"},
		{goal: 998.0, search: "ullerslev", str: "Ullerslev"},
		{goal: 100.2, search: "ege", str: "AspIT Skive College"},
		{goal: 103.0, search: "ege", str: "Egehøj"},
		{goal: 99.9, search: "liv", str: "2024-06-30 20:05 jacob   Imported  Plan 1667  Skanderborg Imported dataimport mandag skanderborg v3 live køretid.csv"},
	}
	for _, tc := range testdata {
		assert.InDelta(t, tc.goal, searchMatch(tc.search, tc.str), 0.1, fmt.Sprintf("Searching for %s in %s", tc.search, tc.str))
	}
}

func TestAlphanumericParts(t *testing.T) {
	assert.Equal(t, []string{"Hello", "World"}, alphaNumericParts("Hello, World"))
	assert.Equal(t, []string{"123", "Hello"}, alphaNumericParts("-.123   Hello "))
	assert.Equal(t, []string{"ege"}, alphaNumericParts("ege"))
	assert.Equal(t, []string{"AspIT", "Skive", "College"}, alphaNumericParts("AspIT Skive College"))
}
