package nest

import (
	"log/slog"
	"runtime"
)

// Retrieve info from stack about who called a function, and return it as an attribute for slog.Error()
func slogCallerInfo() slog.Attr {
	// Capture caller information
	pc, file, line, ok := runtime.Caller(2) // 2 means the caller of the caller of this function
	var callerInfo slog.Attr
	if ok {
		function := runtime.FuncForPC(pc)
		callerInfo = slog.Group("caller",
			slog.String("caller", function.Name()),
			slog.String("source", file),
			slog.Int("line", line),
		)
	} else {
		callerInfo = slog.Group("caller", slog.String("info", "unknown caller"))
	}
	return callerInfo
}
