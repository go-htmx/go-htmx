package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLeafletjs(t *testing.T) {
	comp := &LeafletJsMap{
		Component: Component{ID: "mymap"},
		Width:     Px(300),
		Height:    Px(300),
	}
	var control UI = comp
	assert.NotEqual(t, "", control.GetHTML(&Context{}))
}
