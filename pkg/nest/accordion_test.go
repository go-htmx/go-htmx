package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAccordion(t *testing.T) {
	acc := &Accordion{
		Caption: "Test",
		Child:   &Button{},
	}
	var proc UI = acc
	proc.PrepareHTML(&Context{})
	var coll UIProvider = acc
	test := coll.GetChildren()
	assert.Equal(t, 1, len(test))
}
