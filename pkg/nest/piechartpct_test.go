package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPieChartSvg(t *testing.T) {
	var control UI = &PieChartPct{
		Percent: 50,
	}
	str := control.GetHTML(nil)
	assert.Contains(t, str, "<path")

}
