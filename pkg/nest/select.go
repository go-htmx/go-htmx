package nest

import (
	"errors"
	"fmt"
	"html"
	"log/slog"
	"sort"
	"strings"
)

// Tools to help making <select> HTML

type Select struct {
	Component
	value              string
	FullWidth          bool               // If set, the input will be asked to be 100% wide
	DefaultValue       string             // If no value is known (empty string), this value will be set, instead
	Changed            bool               // Set to true if value was changed by the user
	Required           bool               // If true, Value must be something else than an empty string
	ValidationDisabled bool               // If true, Required is ignored
	Form               UIRegistrator      // Specify a reference to the form that this edit belongs to
	DataSource         DataSourceProvider // If connected, it will load its values from there
	DataSourceField    FieldName          // Required for DataSource
	Sorted             bool               // If true, the list of options is sorted by the component
	ReadOnly           bool               // If true, the value cannot be changed
	Options            []ValueAndTitle
	OnInitialize       func(control *Select, ctx *Context) error
	OnChange           func(control *Select, ctx *Context, OldValue string) error
	OnPrepareHTML      func(control *Select, ctx *Context) error // Should be used to fill the Options, if it is not static
}

func (sel *Select) SetValueDefault(ctx *Context) {
	sel.SetValue(ctx, "")
}

func (sel *Select) GetValue() string {
	return sel.value
}

func (sel *Select) SetValue(ctx *Context, value string) {
	sel.value = value
	ctx.Set(sel.ID, value)
}

func (ed *Select) GetValidationResult(ctx *Context) error {
	if ed.ValidationDisabled {
		return nil
	}
	if ed.Required && ed.GetValue() == "" {
		return UserErrorMsg(T("Make a choice from the list"))
	}
	if ed.DataSource != nil {
		if ed.DataSourceField == "" {
			slog.Error("DataSourceField is missing for Select", slog.String("msgid", "435PO2"))
		}
		field := ed.DataSource.GetField(ctx, ed.DataSourceField)
		if field == nil {
			return errors.New("Unknown field " + string(ed.DataSourceField))
		}
		return field.Validate(ctx.Locale, strings.TrimSpace(ed.value))
	}
	return nil
}

func (ed *Select) EnableValidation(enabled bool) {
	ed.ValidationDisabled = !enabled
}

// For convenience, if .Options is not set directly
func (sel *Select) AddOption(value, title string) {
	sel.Options = append(sel.Options, ValueAndTitle{
		Title: title,
		Value: value,
	})
}

func (sel *Select) Init(ctx *Context) error {
	sel.value = ctx.Get(sel.ID)

	if sel.value == "" {
		sel.value = sel.DefaultValue
		ctx.Set(sel.ID, sel.value)
	}
	sel.Changed = ctx.GetBool(sel.ID + "changed")
	return nil
}

func (sel *Select) RunOnInitialize(ctx *Context) error {
	if sel.DataSource != nil && sel.DataSourceField != "" {
		field := sel.DataSource.GetField(ctx, sel.DataSourceField)
		if len(sel.Options) == 0 {
			sel.Options = field.GetSelectionList()
		}

		if sel.DataSource.GetStateShouldFormsLoadNewData(ctx) {
			sel.value = field.GetAsString()
		} else {
			sel.value = ctx.Get(sel.ID)
			field.SetAsString(sel.value)
		}
		if sel.DataSource.GetStateReadOnly(ctx) {
			sel.ReadOnly = true
		}
	}

	if sel.Form != nil {
		sel.Form.RegisterComponent(sel)
	}
	if sel.OnInitialize != nil {
		return sel.OnInitialize(sel, ctx)
	}
	return nil
}

func (sel *Select) RunUserEvents(ctx *Context) error {
	if sel.OnChange != nil {
		if ctx.GetBool(sel.ID + "changed") {
			oldval := ctx.Get(sel.ID + "old")
			sel.value = ctx.Get(sel.ID)
			ctx.Clear(sel.ID + "changed")
			err := sel.OnChange(sel, ctx, oldval)
			if err != nil {
				sel.SetValue(ctx, oldval)
				return err
			}
		}
	}
	return nil
}

func (sel *Select) PrepareHTML(ctx *Context) error {
	if sel.OnPrepareHTML != nil {
		return sel.OnPrepareHTML(sel, ctx)
	}
	ctx.Set(sel.ID, sel.value)
	return nil
}

func (sel *Select) GetHTML(ctx *Context) string {
	if sel.Omitted {
		return ""
	}

	// Auto-set default to last selection, if load method is a GET
	selectedValue := sel.value

	// Find out styles
	styleSelect := ""
	if sel.FullWidth {
		styleSelect = `width: 100%;`
	}
	if styleSelect != "" {
		styleSelect = fmt.Sprintf(` style="%s" `, html.EscapeString(styleSelect))
	}

	cssClass := ""
	if sel.Class != "" {
		cssClass = fmt.Sprintf(` class="%s"`, html.EscapeString(sel.Class))
	}

	// Find out if the current value is in the list
	found := false
	for _, option := range sel.Options {
		if option.Value == selectedValue {
			found = true
			break
		}
	}
	if !found {
		sel.Options = append([]ValueAndTitle{{Title: selectedValue, Value: selectedValue}}, sel.Options...)
	}

	// Sort options list
	if sel.Sorted {
		sort.Slice(sel.Options, func(i, j int) bool {
			return sel.Options[i].Title < sel.Options[j].Title
		})
	}

	disabledAttr := ""
	if sel.ReadOnly {
		disabledAttr = " disabled "
	}

	// Remember current default value when sending new value
	ctx = ctx.Copy()
	ctx.SetOTBool(sel.ID+"changed", true)
	ctx.SetOT(sel.ID+"old", selectedValue)

	htmxattrs := ""
	if ctx.UrlPrefix != "" {
		ctx.Clear(sel.ID)
		if ctx.HxIndicator == "" {
			ctx.HxIndicator = fmt.Sprintf(`#%s`, sel.ID)
		}
		htmxattrs = ctx.GetHxAttrs()
	}

	// Build HTML
	var sl []string
	sl = append(sl, fmt.Sprintf(`<select id="%s" name="%s" %s %s %s%s>`,
		e(sel.ID), e(sel.ID), htmxattrs, styleSelect, cssClass, disabledAttr))
	for _, option := range sel.Options {
		selected := ""
		if selectedValue == option.Value {
			selected = " selected "
		}
		sl = append(sl, fmt.Sprintf(`<option value="%s" %s>%s</option>`, e(option.Value), selected, e(option.Title)))
	}
	sl = append(sl, `</select>`)

	return strings.Join(sl, "\n")
}
