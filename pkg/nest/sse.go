package nest

// EXPERIMENTAL with HTMX 2.0. Does not seem to work, yet.

import (
	"context"
	"fmt"
	"html"
	"log/slog"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"
)

// Very simple HTMX SSE system, requires that the HTTP SSE connection is handled by the same go instance as the web page.
type SseSimple struct {
	Component
	Child   UI                                                           // This will be updated through the SSE channel
	Backend *SseBackend                                                  // This must be set in the http router using 	r.HandleFunc(sseBackend.Url, sseBackend.GetHttpHandler())
	Handler func(ctx context.Context, triggerUpdate func(), SseID SseId) // Started once for each incoming http that receives UI updates. Use triggerUpdate() to update the UI using the Control.
}

func (sse *SseSimple) GetChildren() []UI {
	return []UI{sse.Child}
}

func (see *SseSimple) Init(ctx *Context) error {
	return nil
}

func (sse *SseSimple) RunOnInitialize(htmxInfo *Context) error {
	return nil
}

func (sse *SseSimple) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (sse *SseSimple) PrepareHTML(htmxInfo *Context) error {
	return nil
}

func (sse *SseSimple) GetHTML(htmxInfo *Context) string {
	if sse.Omitted {
		return ""
	}
	htmltxt := ""
	// This requires HTMX 1.x
	sseid := generateSseID()
	sse.Backend.RegisterHandler(sseid, sse.Child, sse.Handler)
	return fmt.Sprintf(`<div id="%s" hx-ext="sse" sse-connect="%s?%s=%s" sse-swap="message" hx-swap="innerHTML">%s</div>`,
		html.EscapeString(sse.ID),
		html.EscapeString(sse.Backend.Url),
		html.EscapeString(SseParameterName),
		html.EscapeString(string(sseid)),
		htmltxt)
}

type SseBackendClient struct {
	created time.Time // used to clean out outdated data
	control UI
	handler func(ctx context.Context, triggerUpdate func(), SseID SseId)
}

type SseBackend struct {
	Url       string // Your http router must route this to the backend.GetHttpHandler()
	mu        sync.RWMutex
	instances map[SseId]*SseBackendClient
}

const timeoutHandler = time.Hour * 24

func (backend *SseBackend) RegisterHandler(sseid SseId, control UI, handler func(ctx context.Context, triggerUpdate func(), SseID SseId)) {
	backend.mu.Lock()
	if backend.instances == nil {
		backend.instances = make(map[SseId]*SseBackendClient)
	}
	backend.instances[sseid] = &SseBackendClient{
		created: time.Now(),
		control: control,
		handler: handler,
	}
	for sid, inst := range backend.instances {
		age := time.Since(inst.created)
		if age > timeoutHandler || age < (-2)*time.Hour {
			delete(backend.instances, sid)
			// Only remove one item at a time to ensure map consistency
			break
		}
	}
	backend.mu.Unlock()
}

func (backend *SseBackend) GetHttpHandler() func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		// Get URL parameter
		sseid := SseId(r.URL.Query().Get(SseParameterName))
		if sseid == "" {
			http.Error(w, "SSE parameter is missing", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Connection", "keep-alive")
		w.Header().Set("Transfer-Encoding", "chunked")

		// Flusher interface to support streaming
		flusher, ok := w.(http.Flusher)
		if !ok {
			http.Error(w, "Streaming unsupported!", http.StatusInternalServerError)
			return
		}

		backend.mu.RLock()
		instance := backend.instances[sseid]
		backend.mu.RUnlock()

		if instance == nil {
			// Incoming SSE connection rejected, server app was restarted. Please reload the browser window.
			messagestr := "Reload page, please"
			_, err := w.Write([]byte(messagestr))
			if err != nil {
				slog.Error("HTMX SSE http handler encountered an error writing to the http connection", slog.String("error", err.Error()), slog.String("msgid", "72AVZ9"))
			}
			return
		}

		if instance.control == nil {
			slog.Error("SSE UI component has no control assigned", slog.String("msgid", "B8SFE1"))
			return
		}

		items := make(chan UI, 3)
		go instance.handler(r.Context(), func() { items <- instance.control }, sseid)

		// Ensure a first update immediately
		items <- instance.control

		// Forward all incoming messages in the channel to the http client
		for builder := range items {
			htmxInfo := NewContext(r)
			err := builder.RunUserEvents(&htmxInfo)
			if err != nil {
				slog.Error("SSE handler could not process incoming for UI object", slog.String("error", err.Error()), slog.String("msgid", "SSEH8F"))
			}
			err = builder.PrepareHTML(&htmxInfo)
			if err != nil {
				slog.Error("SSE handler could not load UI object", slog.String("error", err.Error()), slog.String("msgid", "SSEH8G"))
			}
			htmlstr := builder.GetHTML(&htmxInfo)
			htmloneline := strings.ReplaceAll(htmlstr, "\n", " ")
			messagestr := fmt.Sprintf("data: %s\n\n", htmloneline) // TO-DO htmlstr must have linebreaks removed
			_, err = w.Write([]byte(messagestr))
			if err != nil {
				slog.Error("HTMX SSE http handler encountered an error writing to the http connection", slog.String("error", err.Error()), slog.String("msgid", "72APZ9"))
				break
			}
			flusher.Flush()
		}
	}
}

type SseId string

const SseParameterName = "SSE~ID"

func generateSseID() SseId {
	const letters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	var randomLetters [25]byte

	for index := range randomLetters {
		randomLetters[index] = letters[rand.Intn(len(letters))]
	}

	return SseId(randomLetters[:])
}
