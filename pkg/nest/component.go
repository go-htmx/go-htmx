package nest

type initStage uint8

const (
	stageNotInitialized  initStage = 0
	stageSettingIDs      initStage = 1
	stageInit            initStage = 2
	stageRunOnInitialize initStage = 3
	stageCallUserEvents  initStage = 4
	stagePrepareHTML     initStage = 5
	stageGetHTML         initStage = 6
)

// Component contains properties shared by all components.
// Embed it into component when creating a new one.
type Component struct {
	ID                    string    // HTML ID of the component
	Class                 string    // CSS class
	Omitted               bool      // If true, no HTML is generated
	DisableInitialization bool      // If true, this component and its children will not be initialized. Useful for paged UIs.
	initStage             initStage // Initialization stage. If zero it is not initialized, yet.
}

func (component *Component) GetID() string {
	return component.ID
}

func (component *Component) SetID(id string) {
	component.ID = id
}

// Default implementation for this function, when Component is used in another struct
func (component *Component) Init(ctx *Context) error {
	return nil
}

// Default implementation for this function, when Component is used in another struct
func (component *Component) RunOnInitialize(ctx *Context) error {
	return nil
}

// Default implementation for this function, when Component is used in another struct
func (component *Component) RunUserEvents(ctx *Context) error {
	return nil
}

// Default implementation for this function, when Component is used in another struct
func (component *Component) PrepareHTML(ctx *Context) error {
	return nil
}

// Default implementation for this function, when Component is used in another struct
func (component *Component) GetHTML(ctx *Context) string {
	return ""
}

func (component *Component) GetComponent() *Component {
	return component
}

type ComponentGetter interface {
	UI
	GetComponent() *Component
}
