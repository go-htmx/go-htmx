package nest

import (
	"errors"
	"fmt"
	"html"
	"log/slog"
	"strconv"
	"strings"
	"time"
)

const htmlDateValueFormat = time.DateOnly

var _ SortableValueProvider = &EditDate{}
var _ UI = &EditDate{}
var _ Editor = &EditDate{}

type EditDate struct {
	Component
	Form                 UIRegistrator      // Specify a reference to the form that this edit belongs to
	DataSource           DataSourceProvider // If connected, it will load its values from there
	DataSourceField      FieldName          // Required for DataSource
	Required             bool               // If true, validation will fail if there is no number.
	ReadOnly             bool               // If true, the edit box becomes read/only
	ValidationDisabled   bool               // If true, validation will return no error messages
	ConfirmationQuestion string             // If set, the user is asked this question to confirm the date change
	DefaultValue         time.Time
	MinDate              time.Time
	MaxDate              time.Time
	OnChange             func(editDate *EditDate, ctx *Context) error

	value  time.Time
	locale Locale
}

func (edit *EditDate) GetSortableRepresentation() (string, float64) {
	timestamp := edit.value.Unix()
	return strconv.Itoa(int(timestamp)), float64(timestamp)
}

func (ed *EditDate) EnableValidation(enabled bool) {
	ed.ValidationDisabled = !enabled
}

func (ed *EditDate) GetStringValue() string {
	return ed.value.Format(time.RFC3339)
}

func (ed *EditDate) GetValue() time.Time {
	return ed.value
}

func (ed *EditDate) SetValue(ctx *Context, value string) error {
	if strings.TrimSpace(value) == "" {
		ed.value = time.Time{}
		ctx.Set(ed.ID, value)
		return nil
	}

	var err error
	ed.value, err = time.Parse(time.RFC3339, value)
	if err != nil {
		return err
	}
	ctx.Set(ed.ID, value)

	return nil
}

func (ed *EditDate) SetValueDefault(ctx *Context) {
	ed.SetValue(ctx, "")
}

func (ed *EditDate) GetValidationResult(ctx *Context) error {
	if ed.ValidationDisabled && ed.DataSource == nil {
		return nil
	}
	if ed.value.IsZero() {
		if ed.Required {
			return UserErrorMsg(T("This field is required."))
		}
	} else {
		dayDuration := 24 * time.Hour
		valueDate := ed.GetValue().Truncate(dayDuration)
		minDate := ed.MinDate.Truncate(dayDuration)
		maxDate := ed.MaxDate.Truncate(dayDuration)

		if !ed.MinDate.IsZero() || !ed.MaxDate.IsZero() {
			if valueDate.Before(minDate) || valueDate.After(maxDate) {
				format := ed.locale.GetInfo().Format.DateFormat
				minValue := ed.MinDate.Format(format)
				maxValue := ed.MaxDate.Format(format)

				return UserErrorMsg(
					fmt.Sprintf(T("Specify a date between %s and %s."), minValue, maxValue),
				)
			}
		}
	}
	if ed.DataSource != nil {
		if ed.DataSourceField == "" {
			slog.Error("DataSourceField is missing for EditDate", slog.String("msgid", "436EO3"))
		}
		field := ed.DataSource.GetField(ctx, ed.DataSourceField)
		if field == nil {
			return errors.New("Unknown field " + string(ed.DataSourceField))
		}
		return field.Validate(ctx.Locale, ed.GetStringValue())
	}
	return nil
}

func (ed *EditDate) Init(ctx *Context) error {
	if ed.Form != nil {
		ed.Form.RegisterComponent(ed)
	}
	ed.locale = ctx.Locale

	err := ed.SetValue(ctx, ctx.Get(ed.ID))
	if err != nil {
		return err
	}

	if ed.GetValue().IsZero() {
		ed.value = ed.DefaultValue
		ctx.Set(ed.ID, ed.GetStringValue())
	}

	return nil
}

func (ed *EditDate) RunOnInitialize(ctx *Context) error {
	// After datasource has been initialized, we can access it
	if ed.DataSource != nil && ed.DataSourceField != "" {
		field := ed.DataSource.GetField(ctx, ed.DataSourceField)
		if field == nil {
			return errors.New("field unknown " + string(ed.DataSourceField))
		}
		if ed.DataSource.GetStateShouldFormsLoadNewData(ctx) {
			err := ed.SetValue(ctx, field.GetAsString())
			if err != nil {
				return err
			}
		} else {
			field.SetAsString(ed.GetStringValue())
		}
		if ed.DataSource.GetStateReadOnly(ctx) {
			ed.ReadOnly = true
		}
	}
	return nil
}

func (ed *EditDate) RunUserEvents(ctx *Context) error {
	if ed.OnChange != nil && ctx.GetBool(ed.ID+"changed") {
		ctx.Clear(ed.ID + "changed")

		prevValue := ed.GetValue()
		newValue := ctx.Get(ed.ID)
		err := ed.SetValue(ctx, newValue)
		if err != nil {
			return err
		}

		err = ed.OnChange(ed, ctx)
		if err != nil {
			ed.value = prevValue
		}
		return err
	}
	return nil
}

func (ed *EditDate) GetHTML(htmxCtx *Context) string {
	htmxCtx = htmxCtx.Copy()
	htmxCtx.SetOTBool(ed.Component.ID+"changed", true)
	htmxCtx.Clear(ed.Component.ID)

	if ed.ConfirmationQuestion != "" {
		htmxCtx.HxConfirm = ed.ConfirmationQuestion
	}

	attrReadOnly := ""
	if ed.ReadOnly {
		attrReadOnly = " readonly "
	}
	attrClass := ""
	if ed.Class != "" {
		attrClass = fmt.Sprintf(` class="%s" `, html.EscapeString(ed.Class))
	}

	minAttr := ""
	if !ed.MinDate.IsZero() {
		minHtmlValue := ed.MinDate.Format(htmlDateValueFormat)
		minAttr = fmt.Sprintf(` min="%s"`, minHtmlValue)
	}

	maxAttr := ""
	if !ed.MaxDate.IsZero() {
		maxHtmlValue := ed.MaxDate.Format(htmlDateValueFormat)
		maxAttr = fmt.Sprintf(` max="%s"`, maxHtmlValue)
	}

	valueAttr := ""
	if !ed.GetValue().IsZero() {
		valueAttr = fmt.Sprintf(` value="%s"`, e(ed.value.Format(htmlDateValueFormat)))
	}

	htmxAttrs := htmxCtx.GetHxAttrs()
	res := fmt.Sprintf(`<input type="date" id="%s" %s%s%s%s%s %s>`,
		ed.ID, attrClass, attrReadOnly, minAttr, maxAttr, valueAttr, htmxAttrs)

	IDenc := html.EscapeString(ed.ID)
	paramNameEnc := html.EscapeString(OTprefix + ed.ID)
	res = res + fmt.Sprintf(
		`<script>
		if (!document.body.%shasListenerAttached) {
			document.body.addEventListener('htmx:configRequest', %shandleConfigRequest);
			document.body.%shasListenerAttached = true;
		}
	
		function %shandleConfigRequest(evt) {
			const element = document.getElementById('%s');
			if (element.value) {
				const date = new Date(element.value);
				const formattedDate = date.toISOString();
				evt.detail.parameters['%s'] = formattedDate;
			} else {
				delete evt.detail.parameters['%s'];
			}
		}
		</script>`,
		IDenc, IDenc, IDenc, IDenc, IDenc, paramNameEnc, paramNameEnc)
	return res
}
