package nest

/**** COMPONENT INTERFACES ****/

// This interface allows proper handling of requests for interactive components
// 1) GetID() and SetID() are used to automatically set IDs for components, that do not have a predefined ID
// 2) Init() is called by the framework, to ensure that all components are initialized, e.g. fetch data from context. Do not access other components from within Init().
// 3) RunOnInitialize() is called by the framework. This is used to run OnInitialize() event handlers, which can contain UI-specific code.
// 4) RunUserEvents() is called by the framework to run code that handles user events, e.g. Button.OnClick().
// 5) PrepareHTML() is called to prepare HTML for sending to user. For example it may parse and execute a template. The parent component calls this, usually only if GetHTML is expected to be called.
// 6) GetHTML() is called to provide actual HTML. Called by parent component.
// If this interface is implemented, do not expect it to work well without calling RunUserEvents() and Load() first.
type UI interface {
	GetID() string                      // Returns the current ID of the component
	SetID(string)                       // Implementation must set its ID to this parameter, unless another parameter has been specified
	Init(ctx *Context) error            // The initialization of a component's inner workings, will be run before any event handlers are run.
	RunOnInitialize(ctx *Context) error // All initialization work before handling user-changes. E.g. build up sub-components that must also process incoming. Return error to be shown to the end-user.
	RunUserEvents(ctx *Context) error   // This part should call event handlers like "OnClick". Process incoming request before we start generating HTML. Return error to be used by parents. Must always be called.
	PrepareHTML(ctx *Context) error     // After all have processed incoming requests, start loading data for the HTML generation. Return error to be shown to the end-user. This might not be called on a component if it is certain that .GetHTML won't be called afterwards.
	GetHTML(ctx *Context) string
}

// Sometimes an event handler changes the state in ways that requires the page processing to restart,
// Similar to a server-side redirect, where the redirect happens without involving the client, and the client will not notice.
// For instance, if clicking a button "Add 1 row" would add more components, that all would need initialization.
type UIFactory func() UI

// For recursively accessing all components, we need this interface.
// All components that have subcomponents should implement this, so that it is possible to quickly iterate the entire tree
type UIProvider interface {
	UI
	GetChildren() []UI // Returns list of controls that are placed on top of this control
}

/**** EDITORS to be used in forms ****/

type Editor interface {
	UI                                      // Editors should return validation result from PrepareHTML()
	GetValidationResult(ctx *Context) error // If empty, the value is fine
	EnableValidation(bool)                  // Default for components should be true
	SetValueDefault(ctx *Context)           // Sets its content to default value, remove parameters from context
}

type UIRegistrator interface {
	RegisterComponent(UI) // This function makes it easy to specify on an edit control, which form component it belongs to. Can also be used with Panels.
}

/**** TABLE/FORM editors - DEPRECATED ****/

// For values inside Forms and Tables: Interface for showing, editing and sorting a value.
// This is not based on UI, because the type of HTML that is produced, depends on the edit mode.
type ValueEditor interface {
	// HTML UI capabilities
	GetHtmlDisplay(Locale) string                                           // Get HTML that shows value
	GetHtmlEdit(idName, fieldName, extraAttrs string, readonly bool) string // Get <input> or <select> or similar that makes it possible to edit the value. extraAttrs are extra attributes to insert, like class="" or style="".
	SetHtmlValue(value string) error                                        // After submitting an HTML form, and parsing the results, the resulting value can be inserted here to set a new value
}

// Used in forms in order to find out which component should have .SetHtmlValue() called.
type ValueEditorField interface {
	ValueEditor
	GetFieldName() string // Must return a field name
}

// Same as ValueEditor, but sortable, useful for table cells so that the table can be sorted.
type SortableValueProvider interface {
	GetSortableRepresentation() (string, float64) // Sorting will be done by strings first, and floating point values afterwards. Null values should return ("-",0) and zero-numbers should return ("",0). In case of values that identify a record, the result value of this should also be unique.
}

/**** DATASET providers for data-aware components ****/

// Dataset providers help getting data from databases or other sources into data-aware components.
// Fetches data, sorts by a fieldname and filter by a string.
// It is up to the implementation to decide, how filtering should work.
// lastRecord can be set to math.MaxInt
type DataSourceProvider interface {
	GetFieldList() []FieldNameAndCaption
	GetField(ctx *Context, fieldName FieldName) FieldValue
	GetCurrentPrimaryKey() PrimaryKey
	SetCurrentPrimaryKey(ctx *Context, newPK PrimaryKey) error
	GetRecord(ctx *Context) (RecordAccessor, error)
	GetSortMethods() map[DatasetSortMethod]string // strings are human-readable texts. If nil is returned, there is only sortmethod=="".
	GetRecords(ctx *Context, sortMethod DatasetSortMethod, firstRecord int, lastRecord int) ([]RecordAccessor, error)
	SetFilter(ctx *Context, filter DatasetFilter)
	GetFilter() DatasetFilter

	// Do state changes
	DoBrowse(ctx *Context, filter DatasetFilter) error // Removes current record, state is only about listing records
	DoNew(ctx *Context) error
	DoEdit(ctx *Context) error
	DoSave(ctx *Context) error
	DoCancel(ctx *Context) error
	DoDelete(ctx *Context) error

	// Provide info whether certain actions can be done, e.g. use result for disabling buttons
	CanDoBrowse(ctx *Context) bool // This is always doable, but if already in browse mode, this will return false
	CanDoNew(ctx *Context) bool
	CanDoEdit(ctx *Context) bool
	CanDoSave(ctx *Context) bool
	CanDoCancel(ctx *Context) bool
	CanDoDelete(ctx *Context) bool

	// Helpers for edit controls
	GetStateShouldFormsLoadNewData(ctx *Context) bool                                      // Used by controls to know, if their Init() should load their data from the datasource
	GetStateReadOnly(ctx *Context) bool                                                    // If true, edit controls should all be read/only
	GetContextWithNewState(ctx *Context, state DataSourceState, newPK PrimaryKey) *Context // Used for quick navigation. Does not support saving of data.
}

// Interface to load/save values in a variable.
// In future, localized formats will be implemented, where Validate() will work on localized representation (e.g. "1.000,23")
// and SaveAsString will work on machine-readable representation (e.g. "1000.23")
type FieldValue interface {
	// Machine readable values
	GetAsString() string                       // Get as machine-readable value, e.g. "1000.23"
	SetAsString(string) error                  // Set as machine-readable value, e.g. "1000.23". Returns error message that explains why this string cannot be set.
	GetSelectionList() []ValueAndTitle         // Possible selection list
	GetAsLocalizedString(Locale) string        // Get as human-readable/editable value, e.g. "1.000,23"
	SetAsLocalizedString(Locale, string) error // Set as human-readable/editable value, e.g. "1.000,23"
	Validate(Locale, string) error             // Validates the string for possible saving, returns error message if it is not valid. This is a more extensive validation.

	// Regional formatting etc., read/only text, maybe with symbols
	GetAsHtml(Locale) string
}

type Locale interface {
	GetInfo() *LocaleInfo
	FormatInt(val int) string                     // Formats integer using local separators
	FormatFloat(val float64, decimals int) string // Formats float64 using local separators
	ParseInt(str string) (int, error)             // Parses integer using local separators
	ParseFloat(str string) (float64, error)       // Parses float64 using local separators
}

// For summary rows in tables: This interface provides the ability to calculate sums over a number of items
type FieldValueAggregator interface {
	GetFloat64() float64
}
