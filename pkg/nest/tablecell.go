package nest

import (
	"fmt"
)

type TableCell struct {
	Component
	Content     UI                    // Set either Value or Content. Content can be interactive components, but then it must be added to the table in OnInitialize event handler
	Value       SortableValueProvider // Set either Value or Content
	Alignment   GFieldAlignment
	AllowWrap   bool   // If true, the row contents may wrap and take up multiple lines
	ExtraInfo   string // Shown if mouse hovers over the text
	CellBGColor string // HTML color for background, including "#", e.g. "#ffffff"
	LinkURL     string // Makes the cell contents clickable, will navigate to this URL
	TotalIsSum  bool   // Only valid for total rows, this makes Table sum all the rows and show it using one of the existing formatting rules
}

func (cell *TableCell) GetChildren() []UI {
	if cell.Content == nil {
		return nil
	}
	return []UI{cell.Content}
}

func (cell *TableCell) GetSortableRepresentation() (string, float64) {
	var keystr string
	var keyfloat float64
	if cell.Value != nil {
		keystr, keyfloat = cell.Value.GetSortableRepresentation()
	}
	if cell.Content != nil {
		sorter, ok := cell.Content.(SortableValueProvider)
		if ok {
			keystr, keyfloat = sorter.GetSortableRepresentation()
		}
	}
	return keystr, keyfloat
}

func (cell *TableCell) Init(ctx *Context) error {
	return nil
}

func (cell *TableCell) RunOnInitialize(ctx *Context) error {
	return nil
}

func (cell *TableCell) RunUserEvents(ctx *Context) error {
	return nil
}

func (cell *TableCell) PrepareHTML(ctx *Context) error {
	if cell.Content != nil {
		return cell.Content.PrepareHTML(ctx)
	}
	return nil
}

func (cell *TableCell) GetHTML(ctx *Context) string {
	styles := ""
	alignment := cell.Alignment
	if alignment == FaAuto {
		_, ok := cell.Value.(FieldValueAggregator)
		if ok {
			alignment = FaRight
		}
	}
	switch alignment {
	case FaLeft:
		styles += ` text-align: left; `
	case FaCenter:
		styles += ` text-align: center; `
	case FaRight:
		styles += ` text-align: right; `
	}
	if cell.AllowWrap {
		styles += "white-space: normal; "
	}
	if cell.CellBGColor != "" {
		styles += fmt.Sprintf("background-color: %s; ", e(cell.CellBGColor))
	}

	attrStyles := ""
	linkStart := ""
	linkEnd := ""
	attrTitle := ""
	cellContentsValueEditor := ""
	cellContentsContent := ""

	if styles != "" {
		attrStyles = fmt.Sprintf(` style="%s"`, e(styles))
	}
	if cell.ExtraInfo != "" {
		attrTitle = fmt.Sprintf(` title="%s"`, e(cell.ExtraInfo))
	}
	if cell.LinkURL != "" {
		linkStart = fmt.Sprintf(`<a href="%s">`, e(cell.LinkURL))
		linkEnd = "</a>"
	}
	htmlprov, ok := cell.Value.(ValueEditor)
	if ok {
		cellContentsValueEditor = htmlprov.GetHtmlDisplay(ctx.Locale)
	}
	if cell.Content != nil {
		cellContentsContent = cell.Content.GetHTML(ctx)
	}
	attrID := ""
	if cell.ID != "" {
		attrID = fmt.Sprintf(` id="%s"`, e(cell.ID))
	}

	return fmt.Sprintf(`<td%s%s%s>%s%s%s%s</td>`, attrID, attrStyles, attrTitle, linkStart, cellContentsValueEditor, cellContentsContent, linkEnd)
}

func NewGTableCellHtml(Value string) *TableCell {
	return &TableCell{
		Content: &CustomHtml{Html: Value},
	}
}

func NewGTableCellInt(Value int, ZeroIsNull bool) *TableCell {
	return &TableCell{
		Value: &GEditInt{
			ZeroIsNull: ZeroIsNull,
			IntRef:     &Value,
		},
	}
}

func NewGTableCellBool(Value bool) *TableCell {
	return &TableCell{
		Value: &GCheckBox{
			BoolRef: &Value,
		},
	}
}

func NewGTableCellFloat(Value float64, Decimals int, ZeroIsNull bool) *TableCell {
	return &TableCell{
		Value: &GEditFloat{
			ZeroIsNull: ZeroIsNull,
			Decimals:   Decimals,
			Float64Ref: &Value,
		},
	}
}

func NewGTableCellText(Value string) *TableCell {
	return &TableCell{
		Content: &Text{Text: Value},
	}
}

// Creates a cell that sums up all the fields above in the table, only valid for total rows.
func NewGTableTotalSum() *TableCell {
	value := &GEditFloat{}
	return &TableCell{
		Value:      value,
		TotalIsSum: true,
	}
}
