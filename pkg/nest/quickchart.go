package nest

import (
	"encoding/json"
	"errors"
	"fmt"
	"log/slog"
	"net/url"
	"strings"
)

// Implements quickchart.io online chart service, based on Chart.js.
type QuickChart struct {
	Component
	Width, Height Length
	AltTitle      string            // For visually impaired
	Data          ChartJsParameters // Configure this with the data you want to show
	Attrs         AttributeList     // Possibility to set custom attributes
	OnInitialize  func(chart *QuickChart, ctx *Context) error
	OnPrepareHTML func(chart *QuickChart, ctx *Context) error

	// Private data
	jsonData []byte
}

func (chart *QuickChart) Init(ctx *Context) error {
	return nil
}

func (chart *QuickChart) RunOnInitialize(ctx *Context) error {
	if chart.OnInitialize != nil {
		return chart.OnInitialize(chart, ctx)
	}
	return nil
}

func (chart *QuickChart) RunUserEvents(ctx *Context) error {
	return nil
}

func (chart *QuickChart) PrepareHTML(ctx *Context) error {
	if chart.Omitted {
		return nil
	}

	if chart.OnPrepareHTML != nil {
		err := chart.OnPrepareHTML(chart, ctx)
		if err != nil {
			return err
		}
	}

	if chart.Data.Type == "" {
		return errors.New("missing type for QuickChart")
	}

	if len(chart.Data.Data.Datasets) == 0 {
		return errors.New("missing data for QuickChart")
	}

	var err error
	chart.jsonData, err = json.Marshal(chart.Data)
	if err != nil {
		return err
	}

	if ctx.RequireWCAG && chart.AltTitle == "" {
		slog.Warn("WCAG policy not complied to", slog.String("id", chart.ID), slog.String("msgid", "I881AI"))
	}

	return nil
}

func (chart *QuickChart) GetHTML(ctx *Context) string {
	if chart.Omitted {
		return ""
	}

	if chart.Data.Type == "" {
		return "Missing type"
	}
	if len(chart.Data.Data.Datasets) == 0 {
		return "Missing data"
	}
	if chart.Width.Unit != Pixels {
		return "Width must use px"
	}
	if chart.Height.Unit != Pixels {
		return "Height must use px"
	}

	params := make(map[string]string)
	params["chart"] = string(chart.jsonData)
	params["width"] = fmt.Sprintf("%.0f", chart.Width.Value)
	params["height"] = fmt.Sprintf("%.0f", chart.Height.Value)

	var paramList []string
	for key, value := range params {
		paramList = append(paramList, fmt.Sprintf(`%s=%s`, key, url.QueryEscape(value)))
	}

	chartUrl := `https://quickchart.io/chart?` + strings.Join(paramList, "&")

	attrs := AttributeList{
		"id":     chart.ID,
		"width":  chart.Width.String(),
		"height": chart.Height.String(),
		"src":    chartUrl,
		"alt":    chart.AltTitle,
	}

	return fmt.Sprintf(`<img %s>`, attrs.GetHTML(ctx))
}
