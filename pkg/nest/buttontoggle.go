package nest

import (
	"errors"
	"fmt"
	"log/slog"
)

// Provides a button that toggles something on/off, e.g. "edit mode". For instance, switch between "edit pen" and "save checkmark"

type ButtonToggle struct {
	Component
	HtmlDefault string // Shown normally. E.g. a <button>, a text or an image.
	HtmlActive  string // Shown if this has been activated
}

func (toggle *ButtonToggle) GetState(ctx *Context) bool {
	return ctx.GetBool(toggle.Component.ID)
}

func (toggle *ButtonToggle) SetState(ctx *Context, state bool) {
	ctx.SetBool(toggle.Component.ID, state)
}

func (toggle *ButtonToggle) Init(ctx *Context) error {
	return nil
}

func (toggle *ButtonToggle) RunOnInitialize(htmxInfo *Context) error {
	return nil
}

func (toggle *ButtonToggle) RunUserEvents(htmxInfo *Context) error {
	return nil
}

func (toggle *ButtonToggle) PrepareHTML(htmxInfo *Context) error {
	if toggle.Component.ID == "" {
		slog.Error("ButtonToggle must have an ID", slog.String("msgid", "6WESJK"))
		return errors.New("internal error, see server log")
	}
	return nil
}

// Returns HTML representing the Select structure, activating HTMX request upon change
// If method is empty, "GET" is default
func (toggle *ButtonToggle) GetHTML(ctx *Context) string {
	curValue := toggle.GetState(ctx)
	curHtmx := ctx.Copy()
	htmlStr := toggle.HtmlDefault
	if htmlStr == "" {
		// Default off HTML
		htmlStr = "OFF"
	}
	if !curValue {
		curHtmx.SetBool(toggle.Component.ID, true)
	} else {
		htmlStr = toggle.HtmlActive
		if htmlStr == "" {
			// Default HTML
			htmlStr = "ON"
		}
		curHtmx.SetBool(toggle.Component.ID, false)
	}
	htmxattrs := curHtmx.GetHxAttrs()
	str := fmt.Sprintf(`<span id="%s" %s class="%s" style="cursor:pointer;">%s</span>`, e(toggle.Component.ID), htmxattrs, GenerateComponentClassName("ButtonToggle"), htmlStr)
	return str
}
