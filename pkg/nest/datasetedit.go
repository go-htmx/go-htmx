package nest

import (
	"errors"
	"log/slog"
)

// Deprecated. Implements a dataset editor for database tables or similar, with tables and forms. Use DataSource instead. See cmd/dataprovider
type DatasetEdit struct {
	Component
	Table         *Table
	Form          *FormHtml
	ui            *Panel // All the ui that the user interface is composed of
	btnNew        *Button
	btnEdit       *Button
	btnDelete     *Button
	btnCancel     *Button
	EnableSearch  bool      // If true, there will be a search input field
	DisableEdit   bool      // Will disable any kind of editing
	ExtraButtons  []*Button // The button row will have extra buttons if they are listed here. Note, this may not work well with the form.
	OnInitialize  func(ed *DatasetEdit, ctx *Context) error
	OnUpdate      func(ed *DatasetEdit, ctx *Context, selected int) error    // Called after loading incoming into form
	OnUpdateStr   func(ed *DatasetEdit, ctx *Context, selected string) error // Same as OnUpdate but with string
	OnInsert      func(ed *DatasetEdit, ctx *Context) error                  // Called after loading incoming into form
	OnDelete      func(ed *DatasetEdit, ctx *Context, selected int) error    // Implement record deletion
	OnDeleteStr   func(ed *DatasetEdit, ctx *Context, selected string) error // Same as OnDelete but with string
	OnPrepareHTML func(ed *DatasetEdit, ctx *Context) error                  // Called before GetHTML() but after update/insert/delete
}

func (editor *DatasetEdit) GetChildren() []UI {
	if editor.ui != nil {
		return []UI{editor.ui}
	}
	res := []UI{editor.Form, editor.Table}
	for _, button := range editor.ExtraButtons {
		res = append(res, button)
	}
	return res
}

// Returns the ID of the selected item as integer
func (editor *DatasetEdit) GetSelectionInt(ctx *Context) int {
	selection := ctx.GetInt(editor.Table.ID + "selection")
	ctx.SetInt(editor.Form.ID+"selection", selection)
	return selection
}

// Returns the ID of the selected item as string
func (editor *DatasetEdit) GetSelectionStr(ctx *Context) string {
	selection := ctx.Get(editor.Table.ID + "selection")
	ctx.Set(editor.Form.ID+"selection", selection)
	return selection
}

func (editor *DatasetEdit) SetSelectionStr(ctx *Context, selection string) {
	ctx.Set(editor.Table.ID+"selection", selection)
	ctx.Set(editor.Form.ID+"selection", selection)
}

func (editor *DatasetEdit) SetSelectionInt(ctx *Context, selection int) {
	ctx.SetInt(editor.Table.ID+"selection", selection)
	ctx.SetInt(editor.Form.ID+"selection", selection)
}

func (editor *DatasetEdit) getMode(ctx *Context) string {
	return ctx.Get(editor.ID + "mode")
}

func (editor *DatasetEdit) setMode(ctx *Context, mode string) {
	ctx.Set(editor.ID+"mode", mode)
}

func (editor *DatasetEdit) Init(ctx *Context) error {
	if editor.Table == nil {
		return UserErrorMsg("Internal error, no table for dataset editor")
	}
	if editor.Form == nil {
		return UserErrorMsg("Internal error, no form for dataset editor")
	}
	editor.Table.AllowSelect = true
	editor.Table.ID = editor.ID + "table"
	editor.Form.ID = editor.ID + "form"

	editor.btnNew = &Button{
		Component: Component{ID: editor.ID + "btnNew"},
		Caption:   T("New"),
		OnClick: func(button *Button, ctx *Context) error {
			editor.setMode(ctx, "new")
			editor.SetSelectionStr(ctx, "")
			ctx.Restart()
			return nil
		},
	}

	editor.btnDelete = &Button{
		Component:            Component{ID: editor.ID + "btnDelete"},
		Caption:              T("Delete"),
		ConfirmationQuestion: T("Are you sure that you want to delete this?"),
		OnClick: func(button *Button, ctx *Context) error {
			if editor.OnDelete == nil && editor.OnDeleteStr == nil {
				editor.Form.ErrorMessage = "OnDelete is not implemented for this form"
				slog.Error("OnDelete is not implemented", slog.String("msgid", "WUGZLD"))
				return UserErrorMsg("OnDelete is not implemented")
			}
			if editor.Table.Omitted {
				slog.Error("DatasetEditor table is omitted", slog.String("msgid", "WUG7LD"))
			}
			if editor.GetSelectionStr(ctx) == "" {
				slog.Error("DatasetEditor invoked delete with no selection", slog.String("msgid", "WUG6LD"))
			}
			if editor.OnDelete != nil {
				return editor.OnDelete(editor, ctx, editor.GetSelectionInt(ctx))
			}
			if editor.OnDeleteStr != nil {
				return editor.OnDeleteStr(editor, ctx, editor.GetSelectionStr(ctx))
			}
			editor.SetSelectionStr(ctx, "")
			ctx.Restart()
			return nil
		},
	}

	editor.btnEdit = &Button{
		Component: Component{ID: editor.ID + "btnEdit"},
		Caption:   T("Edit"),
		OnClick: func(button *Button, ctx *Context) error {
			editor.setMode(ctx, "edit")
			ctx.Restart()
			return nil
		},
	}

	editor.btnCancel = &Button{
		Component: Component{ID: editor.ID + "btnCancel"},
		Caption:   T("Cancel"),
		OnClick: func(button *Button, ctx *Context) error {
			editor.setMode(ctx, "")
			editor.SetSelectionStr(ctx, "")
			ctx.Restart()
			return nil
		},
	}

	panelButtons := &Panel{
		Horizontal: true,
		Children: []UI{
			editor.btnNew,
			editor.btnEdit,
			editor.btnDelete,
			editor.btnCancel,
		},
	}
	for _, button := range editor.ExtraButtons {
		panelButtons.Children = append(panelButtons.Children, button)
	}

	editor.ui = &Panel{
		Children: []UI{
			panelButtons,
			&HR{},
			editor.Table,
			editor.Form,
		},
	}
	ctx.InitNew(editor, editor.ui)

	editor.Form.OnSubmit = func(form *FormHtml, ctx *Context) error {
		switch editor.getMode(ctx) {
		case "new":
			if editor.OnInsert != nil {
				err := editor.OnInsert(editor, ctx)
				if err != nil {
					return err
				}
				editor.setMode(ctx, "")
			} else {
				editor.Form.ErrorMessage = "OnInsert is not implemented"
				slog.Error("OnInsert is not implemented on dataseteditor", slog.String("editorid", editor.ID), slog.String("msgid", "QL2GUX"))
			}
		case "edit":
			if editor.OnUpdate != nil {
				err := editor.OnUpdate(editor, ctx, editor.GetSelectionInt(ctx))
				if err != nil {
					return err
				}
				editor.setMode(ctx, "")
			} else if editor.OnUpdateStr != nil {
				err := editor.OnUpdateStr(editor, ctx, editor.GetSelectionStr(ctx))
				if err != nil {
					return err
				}
				editor.setMode(ctx, "")
			} else {
				editor.Form.ErrorMessage = "OnInsert is not implemented"
				slog.Error("OnUpdate is not implemented on dataseteditor", slog.String("editorid", editor.ID), slog.String("msgid", "QL2GVX"))
			}
		}
		return nil
	}

	return nil
}

func (editor *DatasetEdit) RunOnInitialize(ctx *Context) error {
	if editor.OnInitialize != nil {
		return editor.OnInitialize(editor, ctx)
	}
	return nil
}

func (editor *DatasetEdit) RunUserEvents(ctx *Context) error {
	return nil
}

func (editor *DatasetEdit) PrepareHTML(ctx *Context) error {
	var errList error
	if editor.OnPrepareHTML != nil {
		errList = editor.OnPrepareHTML(editor, ctx)
	}

	switch editor.getMode(ctx) {
	case "new":
		editor.Form.ButtonTitle = "Create new"
		editor.btnNew.Disabled = true
		editor.btnCancel.Disabled = false
		editor.btnEdit.Disabled = true
		editor.btnDelete.Disabled = true
		editor.Form.Omitted = false
		editor.Table.Omitted = true
	case "edit":
		editor.Form.ButtonTitle = "Save changes"
		editor.btnNew.Disabled = true
		editor.btnCancel.Disabled = false
		editor.btnEdit.Disabled = true
		editor.btnDelete.Disabled = true
		editor.Form.Omitted = false
		editor.Table.Omitted = true
	case "": // Table mode
		selectionAvailable := editor.GetSelectionStr(ctx) != ""
		editor.btnNew.Disabled = (editor.OnInsert == nil)
		editor.btnCancel.Disabled = false
		editor.btnEdit.Disabled = !selectionAvailable || ((editor.OnUpdate == nil) && (editor.OnUpdateStr == nil))
		editor.btnDelete.Disabled = !selectionAvailable
		editor.Form.Omitted = true
		editor.Table.Omitted = false
	}

	err := editor.ui.PrepareHTML(ctx)
	errList = errors.Join(errList, err)
	return errList
}

func (editor *DatasetEdit) GetHTML(htmxInfo *Context) string {
	if editor.Omitted {
		return ""
	}
	if editor.ID == "" {
		slog.Error("DatasetEdit must have an ID", slog.String("msgid", "CCE194"))
	}
	return editor.ui.GetHTML(htmxInfo)

}
