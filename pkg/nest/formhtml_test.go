package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGForm(t *testing.T) {
	f := &FormHtml{
		Component: Component{ID: "Myform"},
		Child:     &Panel{},
	}
	var control UI = f
	assert.NotEqual(t, "", control.GetHTML(&Context{}))
	assert.Equal(t, nil, control.RunUserEvents(&Context{}))
	assert.Equal(t, nil, control.PrepareHTML(&Context{}))
}
