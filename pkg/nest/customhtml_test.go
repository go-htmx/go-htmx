package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCustomHtml(t *testing.T) {
	comp := &CustomHtml{Html: "Test"}
	var control UI = comp
	assert.Less(t, 1, len(control.GetHTML(nil)))
	assert.Equal(t, nil, control.RunUserEvents(&Context{}))
	assert.Equal(t, nil, control.PrepareHTML(&Context{}))
}
