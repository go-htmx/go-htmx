package nest

import (
	"errors"
	"fmt"
	"log/slog"
	"slices"
	"sort"
	"strings"
)

// MultiSelect is a component that allows selecting multiple options from a dropdown list
// It displays selected options as labels with an 'x' button to remove them.
// This component allows search across its options when ShowSearch is true.
type MultiSelect struct {
	Component
	Title              string             // Text that will be shown in the select
	FullWidth          bool               // If set, the component will be asked to be 100% wide
	Required           bool               // If true, at least one value must be selected
	ValidationDisabled bool               // If true, Required is ignored
	Form               UIRegistrator      // Specify a reference to the form that this component belongs to
	DataSource         DataSourceProvider // If connected, it will load its values from there
	DataSourceField    FieldName          // Required for DataSource
	Sorted             bool               // If true, the list of options is sorted by the component
	ReadOnly           bool               // If true, the values cannot be changed
	Options            []ValueAndTitle    // Available options for selection
	MaxSelections      int                // Maximum number of selections allowed (0 means unlimited)
	ShowSearch         bool               // If true, a search input will be shown, too

	values         []string        // The currently selected values
	shownOptions   []ValueAndTitle // Currently shown options
	ui             *Panel
	editSearch     *EditString
	panelSearch    *Panel
	btnSearch      *Button
	btnSearchClear *Button

	OnInitialize  func(control *MultiSelect, ctx *Context) error
	OnChange      func(control *MultiSelect, ctx *Context, values []string) error
	OnPrepareHTML func(control *MultiSelect, ctx *Context) error // Should be used to fill the Options, if it is not static
}

var _ UI = &MultiSelect{}
var _ UIProvider = &MultiSelect{}

// SetValueDefault clears all selections
func (ms *MultiSelect) SetValueDefault(ctx *Context) {
	ms.SetValues(ctx, []string{})
}

// GetValues returns the currently selected values
func (ms *MultiSelect) GetValues() []string {
	return ms.values
}

// SetValues sets the selected values and updates the context
func (ms *MultiSelect) SetValues(ctx *Context, values []string) {
	ms.values = values
	ctx.Set(ms.ID+"list", strings.Join(values, ","))
}

// AddValue adds a single value to the selection if not already selected
func (ms *MultiSelect) AddValue(ctx *Context, value string) {
	if slices.Contains(ms.values, value) {
		return
	}

	// Check if we've reached the maximum selections
	if ms.MaxSelections > 0 && len(ms.values) >= ms.MaxSelections {
		return
	}

	ms.values = append(ms.values, value)
	ctx.Set(ms.ID+"list", strings.Join(ms.values, ","))
}

// RemoveValue removes a value from the selection
func (ms *MultiSelect) RemoveValue(ctx *Context, value string) {
	var newValues []string
	for _, v := range ms.values {
		if v != value {
			newValues = append(newValues, v)
		}
	}
	ms.values = newValues
	ctx.Set(ms.ID+"list", strings.Join(ms.values, ","))
}

// GetValidationResult validates the component's state
func (ms *MultiSelect) GetValidationResult(ctx *Context) error {
	if ms.ValidationDisabled {
		return nil
	}
	if ms.Required && len(ms.values) == 0 {
		return UserErrorMsg(T("Select at least one option from the list"))
	}
	if ms.DataSource != nil {
		if ms.DataSourceField == "" {
			slog.Error("DataSourceField is missing for MultiSelect", slog.String("msgid", "MS8JK2"))
		}
		field := ms.DataSource.GetField(ctx, ms.DataSourceField)
		if field == nil {
			return errors.New("Unknown field " + string(ms.DataSourceField))
		}
		// For MultiSelect, we validate each value separately
		for _, val := range ms.values {
			if err := field.Validate(ctx.Locale, strings.TrimSpace(val)); err != nil {
				return err
			}
		}
	}
	return nil
}

// EnableValidation enables or disables validation
func (ms *MultiSelect) EnableValidation(enabled bool) {
	ms.ValidationDisabled = !enabled
}

// AddOption adds an option to the Options list
func (ms *MultiSelect) AddOption(value, title string) {
	ms.Options = append(ms.Options, ValueAndTitle{
		Title: title,
		Value: value,
	})
}

// FilterShownOptions allows to filter options in select.
func (ms *MultiSelect) FilterShownOptions(searchString string) {
	if searchString == "" {
		ms.shownOptions = ms.Options
	}

	ms.shownOptions = make([]ValueAndTitle, 0)

	for _, option := range ms.Options {
		if strings.Contains(option.Title, searchString) {
			ms.shownOptions = append(ms.shownOptions, option)
		}
	}
}

func (ms *MultiSelect) GetChildren() []UI {
	if ms.ui == nil {
		return nil
	}
	return []UI{ms.ui}
}

// Init initializes the component from context
func (ms *MultiSelect) Init(ctx *Context) error {
	valueStr := ctx.Get(ms.ID + "list")
	if valueStr != "" {
		ms.values = strings.Split(valueStr, ",")
	} else {
		ms.values = []string{}
	}

	ms.editSearch = &EditString{
		Component:   Component{ID: ms.ID + "search"},
		Placeholder: T("Search"),
	}
	ms.btnSearch = &Button{
		Component: Component{ID: ms.ID + "searchbtn"},
		Caption:   T("Search"),
		OnClick: func(button *Button, ctx *Context) error {
			ms.FilterShownOptions(ms.editSearch.GetValue(ctx))
			return nil
		},
	}
	ms.btnSearchClear = &Button{
		Component: Component{ID: ms.ID + "searchclear"},
		Caption:   T("Clear"),
		OnClick: func(button *Button, ctx *Context) error {
			ms.editSearch.SetValue(ctx, "")
			ms.FilterShownOptions("")
			return nil
		},
	}
	ms.panelSearch = &Panel{
		Style:      PanelStyle{AvoidSpacing: true},
		Horizontal: true,
		Children: []UI{
			ms.editSearch,
			ms.btnSearch,
			ms.btnSearchClear,
		},
		OnInitialize: func(pan *Panel, ctx *Context) error {
			pan.Omitted = !ms.ShowSearch
			return nil
		},
	}
	ms.ui = &Panel{
		Horizontal: true,
		Children: []UI{
			ms.panelSearch,
		},
	}

	ctx.InitNew(ms, ms.panelSearch)

	return nil
}

// RunOnInitialize runs initialization logic
func (ms *MultiSelect) RunOnInitialize(ctx *Context) error {
	if ms.DataSource != nil && ms.DataSourceField != "" {
		field := ms.DataSource.GetField(ctx, ms.DataSourceField)
		if len(ms.Options) == 0 {
			ms.Options = field.GetSelectionList()
		}

		if ms.DataSource.GetStateShouldFormsLoadNewData(ctx) {
			valueStr := field.GetAsString()
			if valueStr != "" {
				ms.values = strings.Split(valueStr, ",")
			} else {
				ms.values = []string{}
			}
		} else {
			valueStr := ctx.Get(ms.ID + "list")
			if valueStr != "" {
				ms.values = strings.Split(valueStr, ",")
			} else {
				ms.values = []string{}
			}
			field.SetAsString(strings.Join(ms.values, ","))
		}

		if ms.DataSource.GetStateReadOnly(ctx) {
			ms.ReadOnly = true
		}

		ctx.Set(ms.ID+"list", strings.Join(ms.values, ","))
	}

	if ms.Form != nil {
		ms.Form.RegisterComponent(ms)
	}

	if ms.OnInitialize != nil {
		return ms.OnInitialize(ms, ctx)
	}

	return nil
}

// RunUserEvents processes user events
func (ms *MultiSelect) RunUserEvents(ctx *Context) error {
	ms.FilterShownOptions(ms.editSearch.GetValue(ctx))

	// Handle adding a specific value
	if addVal := ctx.Get(ms.ID); addVal != "" {
		oldValues := make([]string, len(ms.values))
		copy(oldValues, ms.values)

		ms.AddValue(ctx, addVal)
		ctx.Clear(ms.ID)

		if ms.OnChange != nil {
			err := ms.OnChange(ms, ctx, ms.values)
			if err != nil {
				ms.values = oldValues
				ctx.Set(ms.ID+"list", strings.Join(oldValues, ","))
				return err
			}
		}
	}

	// Handle removing a specific value
	if removeVal := ctx.Get(ms.ID + "remove"); removeVal != "" {
		oldValues := make([]string, len(ms.values))
		copy(oldValues, ms.values)

		ms.RemoveValue(ctx, removeVal)
		ctx.Clear(ms.ID + "remove")

		if ms.OnChange != nil {
			err := ms.OnChange(ms, ctx, ms.values)
			if err != nil {
				ms.values = oldValues
				ctx.Set(ms.ID+"list", strings.Join(oldValues, ","))
				return err
			}
		}
	}

	return nil
}

// PrepareHTML prepares for HTML rendering
func (ms *MultiSelect) PrepareHTML(ctx *Context) error {
	err := ms.ui.PrepareHTML(ctx)
	if err != nil {
		return err
	}

	if ms.OnPrepareHTML != nil {
		return ms.OnPrepareHTML(ms, ctx)
	}

	return nil
}

// GetHTML renders the component as HTML
func (ms *MultiSelect) GetHTML(ctx *Context) string {
	if ms.Omitted {
		return ""
	}

	// Sort shown options if needed
	if ms.Sorted {
		sort.Slice(ms.shownOptions, func(i, j int) bool {
			return ms.shownOptions[i].Title < ms.shownOptions[j].Title
		})
	}

	containerStyle := ""
	if ms.FullWidth {
		containerStyle = "width: 100%;"
	}

	containerClass := ""
	if ms.Class != "" {
		containerClass = fmt.Sprintf(` class="%s"`, e(ms.Class))
	}

	ctx = ctx.Copy()

	var multiSelectHTML []string

	multiSelectHTML = append(multiSelectHTML, fmt.Sprintf(`<div %s style="%s">`,
		containerClass, e(containerStyle)))

	// Map of selected values for quick lookup
	optionsMap := make(map[string]string)
	for _, val := range ms.values {
		for _, opt := range ms.Options {
			if opt.Value == val {
				optionsMap[opt.Value] = opt.Title
			}
		}
	}

	multiSelectHTML = append(multiSelectHTML, `<div style="display: flex;">`)

	// Only show add button if MaxSelections is not reached
	canAdd := ms.MaxSelections <= 0 || len(ms.values) < ms.MaxSelections
	if canAdd {
		multiSelectHTML = append(multiSelectHTML, fmt.Sprintf(`<select name="%s" %s>`,
			e(ms.ID), ctx.GetHxAttrs()))

		multiSelectHTML = append(multiSelectHTML, fmt.Sprintf(`<option value="">%s</option>`, e(ms.Title)))

		// Add available options (excluding already selected ones)
		for _, opt := range ms.shownOptions {
			if _, selected := optionsMap[opt.Value]; selected {
				continue
			}

			multiSelectHTML = append(multiSelectHTML, fmt.Sprintf(`<option value="%s">%s</option>`,
				e(opt.Value), e(opt.Title)))
		}

		multiSelectHTML = append(multiSelectHTML, `</select>`)
	}

	multiSelectHTML = append(multiSelectHTML, ms.ui.GetHTML(ctx))

	multiSelectHTML = append(multiSelectHTML, `</div>`)

	multiSelectHTML = append(multiSelectHTML, fmt.Sprintf(`<div class="%s">`, GenerateComponentClassName("multiselect-selected-labels")))
	for _, val := range ms.values {
		title := optionsMap[val]

		// If read-only, just show labels without remove buttons
		if ms.ReadOnly {
			multiSelectHTML = append(multiSelectHTML, fmt.Sprintf(`<span>%s</span>`,
				e(title)))
		} else {
			removeCtx := ctx.Copy()
			removeCtx.SetOT(ms.ID+"remove", val)

			multiSelectHTML = append(multiSelectHTML, fmt.Sprintf(`<span class="%s">%s<button class="%s" type="button" %s>&times;</button></span>`,
				GenerateComponentClassName("multiselect-label"), e(title), GenerateComponentClassName("multiselect-remove"), removeCtx.GetHxAttrs()))
		}
	}
	multiSelectHTML = append(multiSelectHTML, `</div>`)

	// Close container
	multiSelectHTML = append(multiSelectHTML, `</div>`)

	multiSelectHTML = append(multiSelectHTML, getMultiSelectStyles())

	return strings.Join(multiSelectHTML, "\n")
}

// Helper to get selected option titles
func (ms *MultiSelect) GetSelectedTitles() []string {
	var titles []string
	valueMap := make(map[string]struct{})

	for _, val := range ms.values {
		valueMap[val] = struct{}{}
	}

	for _, opt := range ms.Options {
		if _, exists := valueMap[opt.Value]; exists {
			titles = append(titles, opt.Title)
		}
	}

	return titles
}

func getMultiSelectStyles() string {
	return fmt.Sprintf(`<style>
.%s {
  display: flex;
  flex-wrap: wrap;
  gap: 0.25rem;
}
.%s {
  display: inline-flex;
  align-items: center;
  padding: 0.25rem 1rem;
  background-color: #e1e8ed;
  border-radius: 1rem;
  font-size: 1rem;
  margin-right: 0.25rem;
}
.%s {
  background: none;
  border: none;
  color: #666;
  cursor: pointer;
  font-size: 1rem;
  margin-left: 0.25rem;
  padding: 0 0.25rem;
}
.%s:hover {
  color: #f00;
}
</style>`, GenerateComponentClassName("multiselect-selected-labels"), GenerateComponentClassName("multiselect-label"), GenerateComponentClassName("multiselect-remove"), GenerateComponentClassName("multiselect-remove"))
}
