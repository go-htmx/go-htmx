package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGPanel(t *testing.T) {
	v := &Panel{}
	var control UI = v
	var container UIProvider = v
	assert.Equal(t, "", control.GetHTML(&Context{}))
	assert.Equal(t, 0, len(container.GetChildren()))
	assert.Equal(t, nil, control.RunUserEvents(&Context{}))
	assert.Equal(t, nil, control.PrepareHTML(&Context{}))
}
