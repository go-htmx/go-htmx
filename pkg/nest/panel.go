package nest

import (
	"errors"
	"fmt"
	"html"
	"log/slog"
	"strings"
)

const effectVisiblePanel = ` border-radius: 3px; background-color: #f0f0f0; `

/* Typical scrollbar use:
* Subdivide screen vertically: Height100pct=true
 */
type PanelStyle struct {
	AvoidSpacing bool   // Avoid spacing between elements
	Padding      bool   // Avoid padding inside the panel, i.e. element distance to panel border
	ShowBorder   bool   // Shows a line around everything inside.
	VisiblePanel bool   // Makes the area look raised
	Height100Pct bool   // Use if the panel should use the full vertical height of the parent. This is required to make scrollbars work.
	Scrollbar    bool   // If true, a scrollbar can be used if the panel is too big
	GrowIndex    int    // Which item should be expanded or changed to use scrollbar. -1 means last item and will be default if Height100Pct is true.
	SpaceBetween Length // If empty, a default spacing is applied. Can be set to values like "7px"
}

// Panel for vertically handling items
type Panel struct {
	Component
	Horizontal        bool                                   // Default is vertical
	HtmxReloadLocal   bool                                   // BUGGY - does not show error messages and state preservation is challenged. Makes reloads of HTMX inside this panel reload the panel, but not anything outside. This is achieved using hx-select and hx-target
	HtmxReloadPartial string                                 // BUGGY - does not show error messages and state preservation is challenged. Makes reloads of HTMX inside this panel reload only the part identified by the ID specified in this variable. This is achieved using hx-select and hx-target
	Style             PanelStyle                             // Defines the looks of the panel
	Attrs             AttributeList                          // Can be used to add attributes to the surrounding html tag
	Refresh           float64                                // Number of seconds until automatic refresh
	Height, Width     Length                                 // Not set on most panels, but for subdividing the screen into percentages etc. this is useful
	Form              UIRegistrator                          // Optional link to form, to include this panel in the form. Only apply to top level panel in the form.
	OnInitialize      func(panel *Panel, ctx *Context) error // Can be used to dynamically add items to the panel
	OnHandleEvents    func(panel *Panel, ctx *Context) error // Run during RunUserEvents
	OnPrepareHTML     func(panel *Panel, ctx *Context) error // Called before loading Children on the panel
	Children          []UI                                   // List of controls on the panel
}

func (comp *Panel) Init(ctx *Context) error {
	if comp.Form != nil {
		comp.Form.RegisterComponent(comp)
	}
	return nil
}

func (panel *Panel) RunOnInitialize(htmxInfo *Context) error {
	if panel.OnInitialize != nil {
		return panel.OnInitialize(panel, htmxInfo)
	}
	return nil
}

func (panel *Panel) RunUserEvents(htmxInfo *Context) error {
	if panel.OnHandleEvents != nil {
		return panel.OnHandleEvents(panel, htmxInfo)
	}
	return nil
}

func (panel *Panel) PrepareHTML(htmxInfo *Context) error {
	var firstErr error = nil
	if panel.OnPrepareHTML != nil {
		firstErr = panel.OnPrepareHTML(panel, htmxInfo)
	}
	for _, control := range panel.Children {
		firstErr = errors.Join(firstErr, control.PrepareHTML(htmxInfo))
	}
	return firstErr
}

func (panel *Panel) GetChildren() []UI {
	return panel.Children
}

func (panel *Panel) GetHTML(htmxInfo *Context) string {
	if panel.Component.Omitted {
		return ""
	}
	if panel.HtmxReloadLocal {
		htmxInfo = htmxInfo.CopyForLocalReload(panel.ID)
	} else if panel.HtmxReloadPartial != "" {
		htmxInfo = htmxInfo.CopyForLocalReload(panel.HtmxReloadPartial)
	}
	htmxattrs := ""
	if panel.Refresh != 0 {
		htmxInfo = htmxInfo.Copy()
		htmxInfo.HxTrigger = fmt.Sprintf("every %f.2s", panel.Refresh)
		htmxattrs = htmxInfo.GetHxAttrs()
	}
	border := ""
	if panel.Style.ShowBorder {
		border = " border: 1px solid #000; "
	}
	shadow := ""
	if panel.Style.VisiblePanel {
		shadow = effectVisiblePanel
	}
	margin := " padding: 0; margin: 0; "
	if panel.Style.Padding || panel.Style.VisiblePanel || panel.Style.ShowBorder {
		margin = " padding: 5px; margin: 0px; "
	}
	growItemIndex := panel.Style.GrowIndex
	if growItemIndex == -1 {
		panel.Style.Height100Pct = true
	}
	if panel.Style.Height100Pct && growItemIndex == 0 {
		growItemIndex = -1
	}
	scrollbar := ""
	if !panel.Horizontal {
		if panel.Style.Scrollbar && growItemIndex == 0 {
			scrollbar = ` overflow: auto; width: fit-content; `
		} else {
			scrollbar = ` width: fit-content; `
		}
		if panel.Style.Height100Pct {
			scrollbar += ` height: 100%; `
		}
	} else {
		if panel.Style.Height100Pct {
			scrollbar = ` height: 100%; `
		} else {
			scrollbar = ` height: fit-content; `
		}
		if panel.Style.Scrollbar && growItemIndex == 0 {
			scrollbar += ` overflow-x: auto; `
		}
	}
	domID := ""
	if panel.ID != "" {
		domID = fmt.Sprintf(`id="%s"`, e(panel.ID))
	}
	scrollbarIndex := growItemIndex
	if scrollbarIndex == -1 {
		scrollbarIndex = len(panel.Children)
	}
	containerStyle := " display: flex; flex-direction: column; "
	if panel.Horizontal {
		containerStyle = " display: flex; width: fit-content; "
	}
	foundSomething := false
	var sl []string
	ctrlIndexVisible := 0
	for ctrlIndex, ctrl := range panel.Children {
		sectionHtml := ctrl.GetHTML(htmxInfo)
		if sectionHtml != "" {
			// Find space between items
			spaceBetween := panel.Style.SpaceBetween
			if spaceBetween.IsNull() {
				if !panel.Horizontal {
					spaceBetween = Px(5)
				} else {
					spaceBetween = Px(15)
				}
			}
			if panel.Style.AvoidSpacing || ctrlIndexVisible == 0 {
				spaceBetween = Px(0)
			}

			styleScrollbar := ""
			if ctrlIndex+1 == scrollbarIndex {
				if panel.Style.Scrollbar {
					styleScrollbar = " flex-grow: 1; overflow: auto; "
				} else {
					// Makes it possible to use scrollbars inside the element by using .ScrollBarIndex without setting scrollbars
					styleScrollbar = " flex-grow: 1; overflow: hidden;  "
				}
			}
			style := ""
			if !panel.Horizontal {
				style = fmt.Sprintf(`padding: 0px; margin: %s 0px 0px 0px; %s`, spaceBetween.String(), styleScrollbar)
			} else {
				style = fmt.Sprintf(`padding: 0px; margin: 0px 0px 0px %s; flex: 0 0 auto; %s`, spaceBetween.String(), styleScrollbar)
			}
			if ctrl == nil {
				slog.Error("nil control was added to Panel", slog.String("id", panel.ID), slog.String("msgid", "8H2NZ0"))
			}
			foundSomething = true
			sl = append(sl, fmt.Sprintf(`<div style="%s" %s>%s</div>`, html.EscapeString(style), htmxattrs, sectionHtml))
			ctrlIndexVisible++
		}
	}
	if !foundSomething {
		return ""
	}
	styleWidth := ""
	if !panel.Width.IsZero() {
		styleWidth = fmt.Sprintf(" width: %s; ", panel.Width.String())
	}
	styleHeight := ""
	if !panel.Height.IsZero() {
		styleHeight = fmt.Sprintf(" height: %s; ", panel.Height.String())
	}

	classAttr := ""
	if panel.Class != "" {
		classAttr = fmt.Sprintf(`class="%s"`, e(panel.Class))
	}

	fullStyle := fmt.Sprintf("%s%s%s%s%s%s%s", styleWidth, styleHeight, containerStyle, margin, border, shadow, scrollbar)
	return fmt.Sprintf(`<div%s %s %s style="%s">%s</div>`, panel.Attrs.GetHTML(htmxInfo), domID, classAttr, e(fullStyle), strings.Join(sl, "\n"))
}
