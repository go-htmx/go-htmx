package nest

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestLocale(t *testing.T) {
	localeInfo := LocaleInfo{
		Format: Locale_da_DK,
	}
	var err error
	localeInfo.Location, err = time.LoadLocation("Europe/Copenhagen")
	assert.Equal(t, nil, err)
	var _ Locale = &localeInfo
}
