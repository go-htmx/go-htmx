package nest

import (
	"fmt"
	"strconv"
	"strings"
)

// Implements a tabbed selector by putting <button>s next to each other
type Tabs struct {
	Component
	Value        string // Value of the currently selected tab
	Changed      bool   // Set to true if tabs were changed
	TabList      []ValueAndTitle
	OnInitialize func(tabs *Tabs, ctx *Context) error
	OnChange     func(tabs *Tabs, ctx *Context) error
}

func (tabs *Tabs) Init(ctx *Context) error {
	tabValueCtx := ctx.Get(tabs.ID)
	if tabValueCtx != "" {
		tabs.Value = tabValueCtx
	}

	tabs.Changed = ctx.GetBool(tabs.ID + "changed")

	// Automatically assign values, if all are empty
	allAreEmpty := true
	for _, tab := range tabs.TabList {
		if tab.Value != "" {
			allAreEmpty = false
			break
		}
	}
	if allAreEmpty {
		for tabIndex := range tabs.TabList {
			str := ""
			if tabIndex != 0 {
				str = strconv.Itoa(tabIndex)
			}
			tabs.TabList[tabIndex].Value = str
		}
	}
	return nil
}

func (tabs *Tabs) RunOnInitialize(ctx *Context) error {
	if tabs.OnInitialize != nil {
		return tabs.OnInitialize(tabs, ctx)
	}
	return nil
}

func (tabs *Tabs) RunUserEvents(ctx *Context) error {
	if tabs.OnChange != nil {
		if ctx.GetBool(tabs.ID + "changed") {
			ctx.Clear(tabs.ID + "changed")
			return tabs.OnChange(tabs, ctx)
		}
	}
	return nil
}

func (tabs *Tabs) PrepareHTML(ctx *Context) error {
	ctx.Set(tabs.ID, tabs.Value)
	return nil
}

func (tabs *Tabs) GetHTML(ctx *Context) string {
	if tabs.Omitted {
		return ""
	}
	class := tabs.Class
	if class == "" {
		class = GenerateComponentClassName("TabsButton")
	}
	attrClassButton := fmt.Sprintf(` class="%s" `, e(class))
	attrClassButtonSelected := fmt.Sprintf(` class="%s" `, e(class+"Selected"))

	var lines []string
	for tabIndex, tab := range tabs.TabList {
		buttonID := fmt.Sprintf(`%sn%d`, tabs.ID, tabIndex)
		attrClass := attrClassButton
		newHtmxInfo := ctx.Copy()
		newHtmxInfo.SetOTBool(tabs.ID+"changed", true)
		newHtmxInfo.Set(tabs.ID, tab.Value)
		hxattrs := newHtmxInfo.GetHxAttrs()
		if tab.Value == tabs.Value {
			attrClass = attrClassButtonSelected
			hxattrs = ""
		}
		s := fmt.Sprintf(`<button id="%s" %s %s>%s</button>`, e(buttonID), attrClass, hxattrs, e(tab.Title))
		lines = append(lines, s)
	}
	return fmt.Sprintf(`<p class="%s" id="%s">%s</p>`+"\n", e(class+"P"), e(tabs.ID), strings.Join(lines, ""))
}
