package nest

type GFieldAlignment int

const (
	FaAuto GFieldAlignment = iota
	FaLeft
	FaRight
	FaCenter
)
