package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGEditString(t *testing.T) {
	var control ValueEditor = &GEditString{Length: 20, OnGet: func(*GEditString) string { return "Hello, World" }}
	assert.Less(t, 1, len(control.GetHtmlDisplay(nil)))
}
