package nest

import "fmt"

// Sets IDs on all elements in the document tree, that do not already have an ID, based on their position in the document tree
// basePrefix should be empty, if it is applied to the top level document
func setDefaultIDsPart(doc UI, basePrefix string) {
	if doc == nil {
		return
	}
	foundID := false
	id := doc.GetID()
	if id != "" {
		basePrefix = id + "_" // Extra underscore as we don't know if the ID ends with digits
		foundID = true
	}

	if !foundID {
		doc.SetID(basePrefix + "_")
	}

	lister, ok2 := doc.(UIProvider)
	if ok2 {
		list := lister.GetChildren()
		formatter := "%s%01d"
		if len(list) >= 100 {
			formatter = "%s_%d"
		} else if len(list) >= 10 {
			formatter = "%s%.02d"
		}
		for listIndex, item := range list {
			newPrefix := fmt.Sprintf(formatter, basePrefix, listIndex)
			setDefaultIDsPart(item, newPrefix)
		}
	}
}

func setDefaultIDs(doc UI) {
	BasePrefix := "HC"
	setDefaultIDsPart(doc, BasePrefix)
}

func (ctx *Context) NewID() string {
	const BasePrefix = "HC"

	id := fmt.Sprintf("%s_%d", BasePrefix, ctx.idAutoIncrement)
	ctx.idAutoIncrement++

	return id
}
