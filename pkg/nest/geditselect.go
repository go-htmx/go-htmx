package nest

import (
	"fmt"
	"log/slog"
	"slices"
	"strconv"
	"strings"
)

// Deprecated. Use Select component with DataSource, instead.
type GEditSelect struct {
	Representations map[int]string // For not-null values, show the value as the correponding item in the list
	ZeroIsInvalid   bool           // If true, the value zero is not considered to be a valid value, and the user must pick another value
	AskUserToSelect bool           // If true, the default is to show that the user must select a value.
	DebugInfo       string         // if filled, this will be used with log messages

	// One of the following must be specified, or OnGet/OnSet must be specified
	OnGet    func(*GEditSelect) int
	OnSet    func(*GEditSelect, int) error
	IntRef   *int
	Int32Ref *int32
	Int16Ref *int16
}

func (f *GEditSelect) getString() string {
	intval := f.getInt()
	_, found := f.Representations[int(intval)]
	if !found {
		return fmt.Sprintf("*** Illegal value %d ***", intval)
	}
	return f.Representations[intval]
}

func (f *GEditSelect) getInt() int {
	if f.OnGet != nil {
		return f.OnGet(f)
	}
	if f.IntRef != nil {
		return *f.IntRef
	}
	if f.Int32Ref != nil {
		return int(*f.Int32Ref)
	}
	if f.Int16Ref != nil {
		return int(*f.Int16Ref)
	}
	panic("GEditInt is missing reference to integer value")
}

func (f *GEditSelect) setInt(ival int) error {
	if f.OnSet != nil {
		return f.OnSet(f, ival)
	}
	if f.IntRef != nil {
		*f.IntRef = ival
		return nil
	}
	if f.Int32Ref != nil {
		*f.Int32Ref = int32(ival)
		return nil
	}
	if f.Int16Ref != nil {
		*f.Int16Ref = int16(ival)
		return nil
	}
	if f.OnGet != nil {
		slog.Error("GEditSelect component has OnGet() but not OnSet(). Value was not saved.", slog.String("debuginfo", f.DebugInfo), slog.String("msgid", "QUFJR3"))
		return nil
	}
	return UserErrorMsg("GEditInt.SetInt64() is missing reference to integer value")
}

func (f *GEditSelect) GetHtmlDisplay(Locale) string {
	return e(f.getString())
}

func (f *GEditSelect) GetHtmlEdit(idName, fieldName, extraAttrs string, ReadOnly bool) string {
	var sl []string

	if ReadOnly {
		intval := f.getInt()
		strval, ok := f.Representations[intval]
		if !ok {
			strval = ""
		}
		return fmt.Sprintf(`<input title="Read only" type="text" readonly value="%s">`+
			`<input type="hidden" id="%s" name="%s" %s value="%s">`,
			strval, e(idName), e(fieldName), extraAttrs, strconv.Itoa(intval))
	}

	sl = append(sl, fmt.Sprintf(`<select id="%s" name="%s" style="cursor:pointer; " %s>`, e(idName), e(fieldName), extraAttrs))

	currentValue := f.getInt()

	// Find order of choices
	var items []string
	for _, optionStr := range f.Representations {
		items = append(items, optionStr)
	}
	slices.Sort(items)
	askID := 0
	if f.AskUserToSelect {
		_, found := f.Representations[currentValue]
		if !found {
			for {
				_, found := f.Representations[askID]
				if !found {
					break
				}
				askID--
			}
			currentValue = askID
			items = append([]string{"-select-"}, items...)
		}
	}

	// Insert choices
	for _, item := range items {
		var optionId int
		for key, optionStr := range f.Representations {
			if optionStr == item {
				optionId = key
				break
			}
		}
		selected := ""
		if optionId == currentValue {
			selected = " selected "
		}
		sl = append(sl, fmt.Sprintf(`<option value="%s"%s>%s</option>`, e(strconv.Itoa(optionId)), selected, e(item)))
	}
	sl = append(sl, `</select>`)
	return strings.Join(sl, "\n")
}

func (f *GEditSelect) SetHtmlValue(value string) error {
	if value == "" {
		return nil
	}
	intval, err := strconv.Atoi(value)
	if err != nil {
		return err
	}
	_, found := f.Representations[intval]
	if !found {
		return UserErrorMsg("Please make a choice")
	}
	if f.ZeroIsInvalid && intval == 0 {
		return UserErrorMsg("This value cannot be selected. Please pick another value")
	}

	err = f.setInt(intval)
	f.AskUserToSelect = false
	return err
}

func (f *GEditSelect) GetSortableRepresentation() (string, float64) {
	return f.getString(), float64(*f.IntRef)
}
