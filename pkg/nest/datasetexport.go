package nest

import (
	"bytes"
	"encoding/csv"
	"slices"
)

// Exporter will return all the required data for the Downloader component
type Exporter interface {
	Export(records []RecordAccessor, description string) (ContextFrameworkFileDownload, error)
}

// DataExportCSV is the default implementation of the Exporter interface that will return CSV data.
type DataExportCSV struct {
	Filename string
}

var _ Exporter = &DataExportCSV{}

// Export returns the data required to create a file in csv format. This implementation ignores the description.
func (dataExport *DataExportCSV) Export(records []RecordAccessor, description string) (ContextFrameworkFileDownload, error) {
	fileForDownload := ContextFrameworkFileDownload{
		MimeType: "text/csv",
		FileName: dataExport.Filename + ".csv",
	}

	if len(records) == 0 {
		return fileForDownload, nil
	}

	var buf bytes.Buffer
	writer := csv.NewWriter(&buf)

	csvData := make([][]string, 0, len(records)+1)

	header := make([]string, 0)

	for _, name := range records[0].GetFieldNames() {
		header = append(header, string(name))
	}

	slices.Sort(header)

	csvData = append(csvData, header)

	for _, record := range records {
		recordData := make([]string, 0)

		for _, name := range header {
			recordData = append(recordData, record.GetFieldValue(FieldName(name)).GetAsString())
		}

		csvData = append(csvData, recordData)
	}

	if err := writer.WriteAll(csvData); err != nil {
		return fileForDownload, err
	}

	fileForDownload.FileContents = buf.Bytes()

	return fileForDownload, nil
}
