package nest

import (
	"errors"
	"strconv"
)

// Implements a Container, which shows tabs first, and then produces a page that was selected by clicking the tabs
type PagesTabbedPage struct {
	Caption string // Shown on button
	Value   string // Used in URLs, will be autommatically set if left empty
	Page    UI     // Actual page to show below tabs
}

type PagesTabbed struct {
	Component
	Value              string     // Value of current page
	Style              PanelStyle // The UI is built using a panel, styled using these settings
	Pages              []PagesTabbedPage
	InitializeAllPages bool                                           // if false, only the clicked page will be initialized
	DoNotUpdateURL     bool                                           // If true, URL will not be updated when switching tabs
	OnInitialize       func(control *PagesTabbed, ctx *Context) error // Can be used to add pages dynamically
	OnIncoming         func(control *PagesTabbed, ctx *Context) error
	OnPrepareHTML      func(control *PagesTabbed, ctx *Context) error

	// Private variables
	panel   *Panel
	tabs    *Panel
	buttons []*Button
	curPage UI
}

func (pages *PagesTabbed) GetChildren() []UI {
	var res []UI
	if pages.panel != nil {
		res = append(res, pages.panel)
	}
	for _, page := range pages.Pages {
		if page.Page != nil {
			res = append(res, page.Page)
		}
	}
	return res
}

func (pages *PagesTabbed) Init(ctx *Context) error {
	pages.Value = ctx.Get(pages.ID)

	// Check if there are any values for pages
	allValuesAbsent := true
	for _, page := range pages.Pages {
		if page.Value != "" {
			allValuesAbsent = false
			break
		}
	}

	// Set default values
	if allValuesAbsent {
		for pageIndex := range pages.Pages {
			if pageIndex == 0 {
				continue
			}
			pages.Pages[pageIndex].Value = strconv.Itoa(pageIndex)
		}
	}

	// Build UI
	pages.tabs = &Panel{
		Horizontal: true,
		Style:      PanelStyle{AvoidSpacing: true},
	}
	pages.panel = &Panel{
		Children: []UI{
			pages.tabs,
		},
	}
	for _, page := range pages.Pages {
		button := &Button{
			Caption: page.Caption,
			OnClick: func(button *Button, ctx *Context) error {
				ctx.Set(pages.ID, page.Value)
				if !pages.DoNotUpdateURL {
					ctx.SwitchURL(pages.ID)
				}
				ctx.Restart()
				return nil
			},
		}
		pages.buttons = append(pages.buttons, button)
		pages.tabs.Children = append(pages.tabs.Children, button)
	}
	if !pages.InitializeAllPages {
		for pageIndex, page := range pages.Pages {
			if pages.Value == page.Value {
				pages.curPage = page.Page
			} else {
				compGet, ok := pages.Pages[pageIndex].Page.(ComponentGetter)
				if ok {
					compGet.GetComponent().DisableInitialization = true
				}
			}
		}
	}
	ctx.InitNew(pages, pages.panel)

	return nil
}

func (pages *PagesTabbed) RunOnInitialize(ctx *Context) error {
	if pages.OnInitialize != nil {
		return pages.OnInitialize(pages, ctx)
	}
	return nil
}

func (pages *PagesTabbed) RunUserEvents(ctx *Context) error {
	if pages.OnIncoming != nil {
		return pages.OnIncoming(pages, ctx)
	}
	return nil
}

func (pages *PagesTabbed) PrepareHTML(ctx *Context) error {
	// Set classes
	pagesClass := pages.Class
	if pagesClass == "" {
		pagesClass = GenerateComponentClassName("PagesTabbed")
	}
	pages.panel.Class = pagesClass
	pages.tabs.Class = pagesClass + "ButtonBar"
	for buttonIndex, button := range pages.buttons {
		button.Class = pagesClass + "Button"
		if pages.Pages[buttonIndex].Value == pages.Value {
			button.Class = pagesClass + "ButtonSelected"
		}
	}

	var err error
	if pages.OnPrepareHTML != nil {
		err = pages.OnPrepareHTML(pages, ctx)
	}

	// Find current page
	if pages.curPage == nil {
		err = errors.Join(err, errors.New("current value for PagesTabbed component does not correspond to a page"))
	}

	// Build UI
	pages.panel.Style = pages.Style
	pages.panel.Children = append(pages.panel.Children, pages.curPage)

	// Prepare the UI
	err = errors.Join(err, pages.panel.PrepareHTML(ctx))

	return err
}

func (pages *PagesTabbed) GetHTML(ctx *Context) string {
	if pages.Omitted {
		return ""
	}

	return pages.panel.GetHTML(ctx)
}
