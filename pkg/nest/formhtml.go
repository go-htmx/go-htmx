package nest

import (
	"errors"
	"fmt"
	"log/slog"
	"strings"
)

// Deprecated: To be used with GEdit* components wrapped in LabelEdit components
type FormHtml struct {
	Component
	ButtonTitle         string // Title on the button to submit the form
	Child               UIProvider
	ErrorMessage        string                                   // If set, this contains information about an error condition that the user should see
	IsSubmitted         bool                                     // If set, the form was submitted using the save button
	DefaultErrorMessage string                                   // Used if one of the fields has a validation problem.
	OnInitialize        func(form *FormHtml, ctx *Context) error // Should load data from backend.
	OnValidate          func(form *FormHtml, ctx *Context) error // Will be called by RunUserEvents() if form validates nicely. Return error if form-post fails or validation fails.
	OnSubmit            func(form *FormHtml, ctx *Context) error // Will be called if validation passes
	OnPrepareHTML       func(form *FormHtml, ctx *Context) error
}

// Required for being a Container
func (form *FormHtml) GetChildren() []UI {
	return []UI{form.Child}
}

func getFullListOfValueEditors(item UI) []ValueEditorField {
	var res []ValueEditorField

	// Check if specified item is a Value Editor
	valedit, ok := item.(ValueEditorField)
	if ok {
		res = []ValueEditorField{valedit}
	}

	// Check subitems
	container, ok := item.(UIProvider)
	if ok {
		controlList := container.GetChildren()
		for _, control := range controlList {
			res = append(res, getFullListOfValueEditors(control)...)
		}
	}
	return res
}

func (form *FormHtml) Init(ctx *Context) error {
	if form.DefaultErrorMessage == "" {
		form.DefaultErrorMessage = T("Please correct errors.")
	}
	form.IsSubmitted = ctx.IncomingMethod == "POST"
	return nil
}

func (form *FormHtml) RunOnInitialize(htmxInfo *Context) error {
	if form.OnInitialize != nil {
		return form.OnInitialize(form, htmxInfo)
	}
	return nil
}

// If there is an error during the parsing of the incoming data, the form's ErrorMessage string will be set.
func (form *FormHtml) RunUserEvents(htmxInfo *Context) error {
	// Process incoming data
	var errList error
	if form.IsSubmitted {
		for _, field := range getFullListOfValueEditors(form) {
			formValue := htmxInfo.Get(field.GetFieldName())
			err := field.SetHtmlValue(formValue)
			errList = errors.Join(errList, err)
		}
	}
	if errList != nil {
		form.ErrorMessage = form.DefaultErrorMessage
	}
	if form.ErrorMessage == "" && htmxInfo.IncomingMethod != "GET" {
		if form.OnValidate != nil {
			err := form.OnValidate(form, htmxInfo)
			if err != nil {
				form.ErrorMessage = err.Error()
			}
		}
	}
	if form.IsSubmitted && form.ErrorMessage == "" && form.OnSubmit != nil {
		return form.OnSubmit(form, htmxInfo)
	}
	return nil
}

func (form *FormHtml) PrepareHTML(htmxInfo *Context) error {
	if form.Omitted {
		return nil
	}
	if form.ID == "" {
		slog.Error("FormHtml: ID must be specified", slog.String("msgid", "T90UL2"))
		return errors.New("internal error: form is used which does not have an ID")
	}
	if form.Child == nil {
		slog.Error("FormHtml: Container must be specified", slog.String("msgid", "T90UL3"))
		return errors.New("internal error: container is missing in form")
	}
	var err error
	if form.OnPrepareHTML != nil {
		err = form.OnPrepareHTML(form, htmxInfo)
	}
	form.Child.PrepareHTML(htmxInfo)
	return err
}

func (def *FormHtml) GetHTML(htmxInfo *Context) string {
	if def.Omitted {
		return ""
	}

	var sl []string
	formContext := htmxInfo.Copy()
	formContext.HxMethod = "POST"
	sl = append(sl, fmt.Sprintf(`<form class="%s" id="%s" %s>`, GenerateComponentClassName("FormHtml"), e(def.ID), formContext.GetHxAttrs()))
	sl = append(sl, def.Child.GetHTML(formContext))
	buttontitle := "Submit"
	if def.ButtonTitle != "" {
		buttontitle = def.ButtonTitle
	}
	sl = append(sl, fmt.Sprintf(`<p><br /><button type="submit" id="%s">%s</button></p>`, e(def.ID+"submit"), e(buttontitle)))
	if def.ErrorMessage != "" {
		sl = append(sl, fmt.Sprintf(`<br /><span style="color: red;">%s</span>`, e(def.ErrorMessage)))
	}
	sl = append(sl, "</form>")
	return strings.Join(sl, "")
}
