package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestButtonToggle(t *testing.T) {
	comp := ButtonToggle{}
	var ht UI = &comp
	assert.NotEqual(t, "", ht.GetHTML(&Context{}))
}
