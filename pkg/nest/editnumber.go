package nest

import (
	"errors"
	"fmt"
	"html"
	"log/slog"
	"strings"
)

// Component equivalent to an <input> but with editing numbers.
type EditNumber struct {
	Component
	Form               UIRegistrator      // Specify a reference to the form that this edit belongs to
	DataSource         DataSourceProvider // If connected, it will load its values from there
	DataSourceField    FieldName          // Required for DataSource
	Decimals           int                // Default is 0, i.e. integers
	Required           bool               // If true, validation will fail if there is no number.
	ReadOnly           bool               // If true, the edit box becomes read/only
	Length             uint               // Number of characters that should be visible in the input field, if possible and non-zero.
	Attrs              AttributeList      // Can be used to add attributes to the surrounding html tag
	ValidationDisabled bool               // If true, validation will return no error messages
	NumberMin          float64            // If NumberMin and NumberMax are both zero, neither will be enforced.
	NumberMax          float64            // If NumberMin and NumberMax are both zero, neither will be enforced.

	locale       Locale
	value        string // This is the value provided by the user, which may be correct or not
	OnInitialize func(ctx *Context, edit *EditNumber) error
}

var _ Editor = &EditNumber{}

// Stores the current user input, this may be something that is not a number
func (ed *EditNumber) SetEditValue(ctx *Context, value string) {
	ed.value = value
	ctx.Set(ed.ID, ed.value)
}

// Gets the current user input, this may be something that is not a number
func (ed *EditNumber) GetEditValue(ctx *Context) string {
	return ed.value
}

func (ed *EditNumber) SetValueDefault(ctx *Context) {
	ed.SetEditValue(ctx, "")
}

func (ed *EditNumber) GetValue() (float64, error) {
	if ed.Component.ID == "" {
		slog.Error("You cannot use .GetValue() on an edit component that has no ID", slog.String("msgid", "T6EQ8R"))
		return 0, errors.New("id is missing on component")
	}
	strval := strings.TrimSpace(ed.value)
	if strval == "" {
		return 0, UserErrorMsg(T("No number specified"))
	}
	val, err := ed.locale.ParseFloat(strval)
	if err != nil {
		return 0, UserErrorMsg(T("Specify a number"))
	}
	return val, nil
}

func (ed *EditNumber) GetValueDefault(def float64) float64 {
	val, err := ed.GetValue()
	if err != nil {
		return def
	}
	return val
}

func (ed *EditNumber) GetValueIntDefault(def int) int {
	val, err := ed.GetValue()
	if err != nil {
		return def
	}
	valint := int(val)
	return valint
}

func (ed *EditNumber) SetValue(ctx *Context, value float64) {
	if ed.Component.ID == "" {
		slog.Error("You cannot use .SetValue() on an edit component that has no ID", slog.String("msgid", "US9RDP"))
	}
	ed.value = ed.locale.FormatFloat(value, ed.Decimals)
}

func (ed *EditNumber) GetValidationResult(ctx *Context) error {
	if ed.ValidationDisabled && ed.DataSource == nil {
		return nil
	}
	str := strings.TrimSpace(ed.GetEditValue(ctx))
	if str == "" {
		if ed.Required {
			return UserErrorMsg(T("This field is required."))
		}
	} else {
		val, err := ed.GetValue()
		if err != nil {
			return err
		}
		if ed.NumberMin != 0 || ed.NumberMax != 0 {
			if val < ed.NumberMin || val > ed.NumberMax {
				return UserErrorMsg(fmt.Sprintf(fmt.Sprintf(T("Specify a number between %%.%df and %%.%df."), ed.Decimals, ed.Decimals), ed.NumberMin, ed.NumberMax))
			}
		}
	}
	if ed.DataSource != nil {
		if ed.DataSourceField == "" {
			slog.Error("DataSourceField is missing for EditNumber", slog.String("msgid", "435PO3"))
		}
		field := ed.DataSource.GetField(ctx, ed.DataSourceField)
		if field == nil {
			return errors.New("Unknown field " + string(ed.DataSourceField))
		}
		return field.Validate(ctx.Locale, str)
	}
	return nil
}

func (ed *EditNumber) EnableValidation(enabled bool) {
	ed.ValidationDisabled = !enabled
}

func (ed *EditNumber) Init(ctx *Context) error {
	if ed.Form != nil {
		ed.Form.RegisterComponent(ed)
	}
	ed.locale = ctx.Locale
	ed.value = ctx.Get(ed.ID)

	return nil
}

func (ed *EditNumber) RunOnInitialize(ctx *Context) error {
	if ed.DataSource != nil && ed.DataSourceField != "" {
		field := ed.DataSource.GetField(ctx, ed.DataSourceField)
		if field == nil {
			return errors.New("field unknown " + string(ed.DataSourceField))
		}
		if ed.DataSource.GetStateShouldFormsLoadNewData(ctx) {
			ed.value = field.GetAsLocalizedString(ctx.Locale)
		} else {
			ed.value = ctx.Get(ed.ID)
			_ = field.SetAsLocalizedString(ctx.Locale, ed.value)
		}
		if ed.DataSource.GetStateReadOnly(ctx) {
			ed.ReadOnly = true
		}
	}
	if ed.OnInitialize != nil {
		return ed.OnInitialize(ctx, ed)
	}
	return nil
}

func (ed *EditNumber) RunUserEvents(ctx *Context) error {
	return nil
}

func (ed *EditNumber) PrepareHTML(ctx *Context) error {
	// Remove this parameter because it is autosubmitted. But only remove it, if HTML is generated.
	ctx.Clear(ed.ID)
	return nil
}

func (ed *EditNumber) GetHTML(ctx *Context) string {
	attrSize := ""
	if ed.Length != 0 {
		attrSize = fmt.Sprintf(` size="%d" `, ed.Length)
	}
	attrReadOnly := ""
	if ed.ReadOnly {
		attrReadOnly = " readonly "
	}
	attrClass := ""
	if ed.Class != "" {
		attrClass = fmt.Sprintf(` class="%s" `, html.EscapeString(ed.Class))
	}
	res := fmt.Sprintf(`<input %s type="text" id="%s" %s%s%s value="%s">`,
		ed.Attrs.GetHTML(ctx), ed.ID, attrSize, attrClass, attrReadOnly, e(ed.GetEditValue(ctx)))

	IDenc := html.EscapeString(ed.ID)
	paramNameEnc := html.EscapeString(OTprefix + ed.ID)
	res = res + fmt.Sprintf(
		`<script>
	if (!document.body.%shasListenerAttached) {
		document.body.addEventListener('htmx:configRequest', %shandleConfigRequest);
		document.body.%shasListenerAttached = true;
	}

	function %shandleConfigRequest(evt) {
		const element = document.getElementById('%s');
		if (element.value) {
        	evt.detail.parameters['%s'] = element.value;
    	} else {
        	delete evt.detail.parameters['%s'];
	    }
	}
	</script>`,
		IDenc, IDenc, IDenc, IDenc, IDenc, paramNameEnc, paramNameEnc)
	return res
}

func (edit *EditNumber) GetSortableRepresentation() (string, float64) {
	floatvalue, err := edit.GetValue()
	if err != nil {
		return edit.value, 0
	}
	return "", floatvalue
}
