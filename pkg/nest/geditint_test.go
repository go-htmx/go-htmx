package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestControlInt(t *testing.T) {
	i := 1
	cb := GEditInt{IntRef: &i}
	var control ValueEditor = &cb
	locale := NewLocale("en_US", "UTC")
	assert.Less(t, 0, len(control.GetHtmlDisplay(locale)))
}
