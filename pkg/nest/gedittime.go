package nest

import (
	"errors"
	"fmt"
	"log/slog"
	"time"
)

var _ ValueEditor = &GEditTime{}

// Deprecated
type GEditTime struct {
	OnValidate        func(string) error // Checks if the string is ok - and if not, returns a suitable error message
	LimitedInputRange bool               // If true, inputs are validated against limits
	LimitLow          time.Time          // Lowest value that is allowed, if LimitedInputRange is true
	LimitHigh         time.Time          // Highest value that is allowed, if LimitedInputRange is true
	Required          bool               // If true, it must be specified
	DebugInfo         string             // if filled, this will be used with log messages
	TimeRelative      bool               // If set, recent timestamps will be shown as "xx ago"
	TimeFormat        string             // If not set, a default is used

	// Either set StringRef, or OnGet/OnSet
	OnGet   func(*GEditTime) time.Time
	OnSet   func(*GEditTime, time.Time) error
	TimeRef *time.Time
}

func (f *GEditTime) GetHtmlDisplay(Locale) string {
	t := f.getTime()
	if t.IsZero() {
		return ""
	}

	formatStr := f.TimeFormat
	if formatStr == "" {
		formatStr = "2006-01-02 15:04:05"
	}
	loc := time.UTC
	if f.TimeRelative {
		elapsed := time.Since(t)
		elapsedMinutes := elapsed.Minutes()
		if elapsedMinutes < 60 {
			if elapsedMinutes < 1 {
				return e("< 1 min ago")
			} else {
				return e(fmt.Sprintf("%.0f min ago", elapsedMinutes))
			}
		} else if elapsedMinutes < 3*60 {
			return e(t.In(loc).Format("15:04"))
		}
	}
	formattedTime := t.In(loc).Format(formatStr)
	return e(formattedTime)
}

func (f *GEditTime) GetHtmlEdit(idName, fieldName, extraAttrs string, ReadOnly bool) string {
	if f.LimitedInputRange {
		if !f.LimitLow.IsZero() {
			extraAttrs = fmt.Sprintf(`%s min="%s"`, extraAttrs, getRFC3339(f.LimitLow))
		}
		if !f.LimitHigh.IsZero() {
			extraAttrs = fmt.Sprintf(`%s max="%s"`, extraAttrs, getRFC3339(f.LimitHigh))
		}
	}
	if ReadOnly {
		extraAttrs += " readonly "
	}

	requiredstr := ""
	if f.Required {
		requiredstr = " required "
	}
	return fmt.Sprintf(`<input type="datetime-local" id="%s" name="%s" %s %s value="%s">`, idName, fieldName, extraAttrs, requiredstr, e(getRFC3339(f.getTime())))
}

func (f *GEditTime) SetHtmlValue(value string) error {
	if f.OnValidate != nil {
		err := f.OnValidate(value)
		if err != nil {
			return err
		}
	}

	// Convert to time
	if f.Required && value == "" {
		return errors.New("Required")
	}

	parsedTime, err := time.Parse("2006-01-02T15:04", value)
	if err != nil {
		return err
	}

	if f.OnSet != nil {
		return f.OnSet(f, parsedTime)
	}
	if f.TimeRef == nil {
		if f.OnGet != nil {
			// OnGet is present, but OnSet is not, so probably this just doesn't want to save anything.
			return nil
		}
		slog.Error("Control is missing .TimeRef", slog.String("debuginfo", f.DebugInfo), slog.String("msgid", "0KLMQX"))
		return UserErrorMsg("Internal error, see server log. 0KCPQX")
	}
	*f.TimeRef = parsedTime
	return nil
}

func (f *GEditTime) GetSortableRepresentation() (string, float64) {
	t := f.getTime()
	if t.IsZero() {
		return "-", 0
	}
	return "", float64(t.UnixMilli())
}

func getRFC3339(t time.Time) string {
	if t.IsZero() {
		return ""
	}
	return t.Format(time.RFC3339)
}

func (f *GEditTime) getTime() time.Time {
	if f.OnGet != nil {
		return f.OnGet(f)
	}
	if f.TimeRef != nil {
		return *f.TimeRef
	}
	panic("GEditTime.getTime() is missing a reference")
}
