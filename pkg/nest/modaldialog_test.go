package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGModalDialog(t *testing.T) {
	var control UI = &ModalDialog{}
	assert.NotEqual(t, "", control.GetHTML(&Context{}))
}
