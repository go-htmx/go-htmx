package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSelect(t *testing.T) {
	i := 1
	cb := GEditSelect{IntRef: &i}
	var control ValueEditor = &cb
	assert.Less(t, 0, len(control.GetHtmlDisplay(nil)))
}
