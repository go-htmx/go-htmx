package nest

import (
	"testing"
)

func TestTableCell(t *testing.T) {
	comp := &TableCell{}
	var _ UI = comp
	var _ UIProvider = comp
	var _ SortableValueProvider = comp
}
