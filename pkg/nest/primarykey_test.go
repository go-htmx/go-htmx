package nest

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPrimarykeyIntStr(t *testing.T) {
	pk := PrimaryKeyIntStr{
		Value1: 23,
		Value2: "World",
	}
	s := pk.GetAsString()
	pk2 := PrimaryKeyIntStr{}
	pk2.SetAsString(s)
	assert.Equal(t, pk, pk2)
}
