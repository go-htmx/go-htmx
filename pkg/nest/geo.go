package nest

type GeoPoint struct {
	Latitude  float64
	Longitude float64
}

func (coor GeoPoint) IsZero() bool {
	return coor.Latitude == 0 || coor.Longitude == 0
}

type GeoBounds [2]GeoPoint

func (bounds GeoBounds) IsZero() bool {
	return bounds[0].IsZero() && bounds[1].IsZero()
}

var constZeroBounds = GeoBounds{}

func (bounds *GeoBounds) Expand(coor GeoPoint) {
	if *bounds == constZeroBounds {
		(*bounds)[0] = coor
		(*bounds)[1] = coor
	} else {
		bounds[0].Latitude = min(bounds[0].Latitude, coor.Latitude)
		bounds[0].Longitude = min(bounds[0].Longitude, coor.Longitude)
		bounds[1].Latitude = max(bounds[1].Latitude, coor.Latitude)
		bounds[1].Longitude = max(bounds[1].Longitude, coor.Longitude)
	}
}

func (bounds *GeoBounds) GetCenter() GeoPoint {
	return GeoPoint{
		Latitude:  (bounds[0].Latitude + bounds[1].Latitude) / 2.0,
		Longitude: (bounds[0].Longitude + bounds[1].Longitude) / 2.0,
	}
}

func (bounds *GeoBounds) ZoomOut(factor float64) {
	center := bounds.GetCenter()
	for index := range bounds {
		bounds[index].Latitude = (bounds[index].Latitude-center.Latitude)*factor + center.Latitude
		bounds[index].Longitude = (bounds[index].Longitude-center.Longitude)*factor + center.Longitude
	}
}
