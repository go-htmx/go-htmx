package nest

import (
	"fmt"
	"html"
	"strings"
)

// GoNestCSSClassPrefix is a library-specific css class prefix that should be added to every class in go-nest.
// This way library classes do not collide with client's class names.
const GoNestCSSClassPrefix = "go-nest-"

func GenerateComponentClassName(componentName string) string {
    return fmt.Sprintf("%s%s", 
        e(GoNestCSSClassPrefix), 
        e(componentName),
    )
}

type Length struct {
	Value float64    // The numeric value
	Unit  LengthUnit // The unit, e.g. "px", "px", "%" etc.
}

type Border struct {
	Width       Length
	Color       Color
	Style       BorderStyle // If set, this is used for all 4 sides. Otherwise set for each side.
	StyleTop    BorderStyle
	StyleBottom BorderStyle
	StyleLeft   BorderStyle
	StyleRight  BorderStyle
	Radius      Length // If non-zero, the corners are rounded
}

type LengthUnit string  // Length dimensions in CSS
type BorderStyle string // E.g. "solid", "dotted", "double" etc.

// Absolute lengths
const (
	Pixels     LengthUnit = "px"
	Millimeter LengthUnit = "mm"
	Centimeter LengthUnit = "cm" // 10 mm
	Inches     LengthUnit = "in" // 72 pt, 96 px, 25.4 cm
	Picas      LengthUnit = "pc" // 12 pt
	Point      LengthUnit = "pt"
)

// Relative lengths
const (
	Percent        LengthUnit = "%"    // Relative to the parent element
	Em             LengthUnit = "em"   // Relative to the font-size of the element (2 = 2*current font-size)
	Rem            LengthUnit = "rem"  // Relative to font-size of the root element
	Char           LengthUnit = "ch"   // Width of "0" character
	Viewport       LengthUnit = "vw"   // Relative to viewport
	ViewportHeight LengthUnit = "vh"   // Relative to viewport height
	ViewportMin    LengthUnit = "vmin" // Relative to the viewport's smaller dimension
	ViewportMax    LengthUnit = "vmax" // Relative to the viewport's larger dimension
)

const (
	BsDotted BorderStyle = "dotted" // Defines a dotted border
	BsSolid  BorderStyle = "solid"  // Defines a solid border
	BsDashed BorderStyle = "dashed" // Defines a dashed border
	BsDouble BorderStyle = "double" // Defines a double border
	BsGroove BorderStyle = "groove" // Defines a 3D grooved border. The effect depends on the border-color value
	BsRidge  BorderStyle = "ridge"  // Defines a 3D ridged border. The effect depends on the border-color value
	BsInset  BorderStyle = "inset"  // Defines a 3D inset border. The effect depends on the border-color value
	BsOutset BorderStyle = "outset" // Defines a 3D outset border. The effect depends on the border-color value
	BsNone   BorderStyle = "none"   // Defines no border
	BsHidden BorderStyle = "hidden" // Defines a hidden border
)

var UnitIsRelative = map[LengthUnit]bool{
	Pixels:         false,
	Millimeter:     false,
	Centimeter:     false,
	Inches:         false,
	Picas:          false,
	Point:          false,
	Percent:        true,
	Em:             true,
	Char:           true,
	Viewport:       true,
	ViewportHeight: true,
	ViewportMax:    true,
	ViewportMin:    true,
}

// NewLength creates a new Length with the specified value and unit
func NewLength(value float64, unit LengthUnit) Length {
	return Length{Value: value, Unit: unit}
}

// Quick way to make an absolute Pixel-distance value
func Px(count float64) Length {
	return NewLength(count, Pixels)
}

// Quick way to make a percentage-distance value
func Pct(count float64) Length {
	return NewLength(count, Percent)
}

// Retruns true if value has never been set, or has been set to a distance of zero
func (l Length) IsZero() bool {
	return l.Value == 0
}

// Returns true if Length value has never been set
func (l Length) IsNull() bool {
	return l.Unit == ""
}

// String implements the Stringer interface, returning the length as a string (e.g., "10px")
// Safe for direct insertion into HTML
func (l Length) String() string {
	return html.EscapeString(fmt.Sprintf("%.2f%s", l.Value, l.Unit))
}

// Output suitable for a stylesheet, safe for direct insertion into HTML
func (b Border) String() string {
	parts := make([]string, 0, 10)
	if b.Style != "" {
		parts = append(parts, fmt.Sprintf("border-style: %s;", b.Style))
	} else {
		if b.StyleTop != "" {
			parts = append(parts, fmt.Sprintf("border-top-style: %s;", b.Style))
		}
		if b.StyleBottom != "" {
			parts = append(parts, fmt.Sprintf("border-bottom-style: %s;", b.Style))
		}
		if b.StyleLeft != "" {
			parts = append(parts, fmt.Sprintf("border-left-style: %s;", b.Style))
		}
		if b.StyleRight != "" {
			parts = append(parts, fmt.Sprintf("border-right-style: %s;", b.Style))
		}
	}
	if len(parts) == 0 {
		// No border specified, so we will add a default.
		parts = append(parts, "border-style: solid;")
	}
	if !b.Width.IsZero() {
		parts = append(parts, fmt.Sprintf("border-width: %s;", b.Width.String()))
	}
	if !b.Radius.IsZero() {
		parts = append(parts, fmt.Sprintf("border-radius: %s;", b.Radius.String()))
	}
	if b.Color != "" {
		parts = append(parts, fmt.Sprintf("border-color: %s;", b.Color))
	}
	return html.EscapeString(strings.Join(parts, " "))
}
