package nest

import (
	"fmt"
	"log/slog"
	"strconv"
)

// Deprecated, use ValueInt or EditNumber instead, with DataSource
type GEditInt struct {
	LimitedInputRange bool               // If true, inputs are validated against limits
	LimitLow          int64              // Lowest value that is allowed, if LimitedInputRange is true
	LimitHigh         int64              // Highest value that is allowed, if LimitedInputRange is true
	Length            int                // Number of characters that are allowed to be input
	ZeroIsNull        bool               // Makes zero be treated as a non-value
	DebugInfo         string             // if filled, this will be used with log messages
	Alignment         GFieldAlignment    // Default is to right-align the number
	OnValidate        func(string) error // Called for input data from browser. Return error to show to the user.

	// One of the following must be specified
	OnGet    func(*GEditInt) int64
	OnSet    func(*GEditInt, int64) error
	IntRef   *int
	Int64Ref *int64
	Int32Ref *int32
	Int16Ref *int16
}

func (f *GEditInt) GetHtmlDisplay(locale Locale) string {
	var str string
	if f.IntRef != nil {
		str = locale.FormatInt(*f.IntRef)
	} else if f.Int64Ref != nil {
		str = locale.FormatInt(int(*f.Int64Ref))
	} else if f.Int32Ref != nil {
		str = locale.FormatInt(int(*f.Int32Ref))
	} else if f.Int16Ref != nil {
		str = locale.FormatInt(int(*f.Int16Ref))
	} else {
		slog.Error("GEditInt is missing IntRef")
		return ""
	}
	return e(str)
}

func (f *GEditInt) GetHtmlEdit(idName, fieldName, extraAttrs string, ReadOnly bool) string {
	length := f.Length
	style := "text-align: right;"
	switch f.Alignment {
	case FaLeft:
		style = "text-align: left;"
	case FaCenter:
		style = "text-align: center;"
	}
	if f.LimitedInputRange {
		if length == 0 {
			minStr := strconv.Itoa(int(f.LimitLow))
			maxStr := strconv.Itoa(int(f.LimitHigh))
			length = max(len(minStr), len(maxStr))
		}
		extraAttrs = fmt.Sprintf(`%s min="%d" max="%d" `, extraAttrs, f.LimitLow, f.LimitHigh)
	}
	if length != 0 {
		extraAttrs = fmt.Sprintf(`%s maxlength="%d" `, extraAttrs, length)
		style += fmt.Sprintf(" width: %dch;", length+4)
	}
	if ReadOnly {
		extraAttrs += " readonly "
	}
	return fmt.Sprintf(`<input type="number" step="1" id="%s" name="%s" style="%s" %s value="%s">`, e(idName), e(fieldName), e(style), extraAttrs, e(f.getString()))
}

func (f *GEditInt) SetHtmlValue(value string) error {
	return f.setString(value)
}

func (f *GEditInt) GetFloat64() float64 {
	return float64(f.getInt64())
}

func (f *GEditInt) getInt64() int64 {
	if f.OnGet != nil {
		return f.OnGet(f)
	}
	if f.Int64Ref != nil {
		return *f.Int64Ref
	}
	if f.IntRef != nil {
		return int64(*f.IntRef)
	}
	if f.Int32Ref != nil {
		return int64(*f.Int32Ref)
	}
	if f.Int16Ref != nil {
		return int64(*f.Int16Ref)
	}
	slog.Error("Integer reference is missing", slog.String("msgid", "2GR5O1"))
	panic("GEditInt is missing reference to integer value")
}

func (f *GEditInt) setInt64(ival int64) {
	if f.OnSet != nil {
		f.OnSet(f, ival)
		return
	}
	if f.Int64Ref != nil {
		*f.Int64Ref = ival
		return
	}
	if f.IntRef != nil {
		*f.IntRef = int(ival)
		return
	}
	if f.Int32Ref != nil {
		*f.Int32Ref = int32(ival)
		return
	}
	if f.Int16Ref != nil {
		*f.Int16Ref = int16(ival)
		return
	}
	slog.Error("Integer reference is missing", slog.String("msgid", "2GR5O2"))
	panic("GEditInt.SetInt64() is missing reference to integer value")
}

func (f *GEditInt) getString() string {
	ival := f.getInt64()
	if f.ZeroIsNull && ival == 0 {
		return ""
	}
	return fmt.Sprintf("%d", ival)
}

func (f *GEditInt) setString(value string) error {
	if f.OnValidate != nil {
		err := f.OnValidate(value)
		if err != nil {
			return err
		}
	}
	if value == "" {
		f.setInt64(0)
		return nil
	}

	intval, err := strconv.Atoi(value)
	if err != nil {
		return err
	}
	intval64 := int64(intval)
	if f.LimitedInputRange {
		if intval64 < f.LimitLow {
			return UserErrorMsg(fmt.Sprintf(`Value %d is below lower limit %d`, intval64, f.LimitLow))
		}
		if intval64 > f.LimitHigh {
			return UserErrorMsg(fmt.Sprintf(`Value %d is above upper limit %d`, intval64, f.LimitHigh))
		}
	}
	f.setInt64(intval64)
	return nil
}

func (f *GEditInt) GetSortableRepresentation() (string, float64) {
	return "", float64(f.getInt64())
}
