package nest

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"log/slog"
	"strconv"
	"strings"
)

// This file implements handling of database table primary keys

type PrimaryKey interface {
	GetRecordIdentifier() string // Must be useful as ID in HTML and similar
	GetAsString() string         // Gets a string that makes it re-readable
	SetAsString(string)          // Sets a primary key value from stored string representation
}

//****************************************************************************************

type PrimaryKeyInt int

func (pki *PrimaryKeyInt) GetRecordIdentifier() string {
	if pki == nil {
		return "nil"
	}
	return fmt.Sprintf("i%d", *pki)
}

func (pki *PrimaryKeyInt) GetAsString() string {
	if pki == nil {
		return "nil"
	}
	return fmt.Sprintf("%d", *pki)
}

func (pki *PrimaryKeyInt) SetAsString(param string) {
	intval, err := strconv.ParseInt(param, 10, 64)
	if err != nil {
		slog.Error("Error converting primary key value to integer", slog.String("value", param), slog.String("error", err.Error()), slog.String("msgid", "S8JR1D"))
	}
	*pki = PrimaryKeyInt(intval)
}

//****************************************************************************************

type PrimaryKeyStr string

func (pks *PrimaryKeyStr) GetRecordIdentifier() string {
	hash := sha256.Sum256([]byte(*pks))
	return "s" + hex.EncodeToString(hash[:])
}

func (pki *PrimaryKeyStr) GetAsString() string {
	return string(*pki)
}

func (pki *PrimaryKeyStr) SetAsString(param string) {
	*pki = PrimaryKeyStr(param)
}

//****************************************************************************************

type PrimaryKeyIntInt struct {
	Value1 int
	Value2 int
}

func (pkii *PrimaryKeyIntInt) GetRecordIdentifier() string {
	return fmt.Sprintf("i%dz%d", pkii.Value1, pkii.Value2)
}

func (pki *PrimaryKeyIntInt) GetAsString() string {
	return fmt.Sprintf("%d;%d", pki.Value1, pki.Value2)
}

func (pki *PrimaryKeyIntInt) SetAsString(param string) {
	values := strings.Split(param, ";")
	value1, _ := strconv.ParseInt(values[0], 10, 64)
	var value2 int64
	if len(values) >= 2 {
		value2, _ = strconv.ParseInt(values[1], 10, 64)
	}
	pki.Value1 = int(value1)
	pki.Value2 = int(value2)
}

//****************************************************************************************

type PrimaryKeyStrArr []string

func (pksa *PrimaryKeyStrArr) GetRecordIdentifier() string {
	combined := strings.Join(*pksa, "\x01")
	hash := sha256.Sum256([]byte(combined))
	return "s" + hex.EncodeToString(hash[:])
}

func (pki *PrimaryKeyStrArr) GetAsString() string {
	return strings.Join(*pki, "\x01")
}

func (pki *PrimaryKeyStrArr) SetAsString(param string) {
	*pki = strings.Split(param, "\x01")
}

//****************************************************************************************

type PrimaryKeyStrInt struct {
	Value1 string
	Value2 int
}

func (value *PrimaryKeyStrInt) GetRecordIdentifier() string {
	hash := sha256.Sum256([]byte(value.Value1))
	return fmt.Sprintf("z%si%d", hex.EncodeToString(hash[:]), value.Value2)
}

func (pki *PrimaryKeyStrInt) GetAsString() string {
	return fmt.Sprintf("%s\x01%d", pki.Value1, pki.Value2)
}

func (pki *PrimaryKeyStrInt) SetAsString(param string) {
	values := strings.Split(param, "\x01")
	pki.Value1 = values[0]
	pki.Value2 = 0
	if len(values) >= 2 {
		value2, _ := strconv.ParseInt(values[1], 10, 64)
		pki.Value2 = int(value2)
	}
}

//****************************************************************************************

type PrimaryKeyIntStr struct {
	Value1 int
	Value2 string
}

func (value *PrimaryKeyIntStr) GetRecordIdentifier() string {
	hash := sha256.Sum256([]byte(value.Value2))
	return fmt.Sprintf("i%dz%s", value.Value1, hex.EncodeToString(hash[:]))
}

func (pki *PrimaryKeyIntStr) GetAsString() string {
	return fmt.Sprintf("%d\x01%s", pki.Value1, pki.Value2)
}

func (pki *PrimaryKeyIntStr) SetAsString(param string) {
	values := strings.Split(param, "\x01")
	intval, _ := strconv.ParseInt(values[0], 10, 64)
	pki.Value1 = int(intval)
	pki.Value2 = ""
	if len(values) >= 2 {
		pki.Value2 = values[1]
	}
}
