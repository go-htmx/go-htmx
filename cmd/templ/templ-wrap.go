package main

import (
	"bytes"
	"errors"

	"github.com/a-h/templ"
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// Component wrapper for Templ components
type Templ struct {
	nest.Component                 // Build-in logic from the framework
	Comp           templ.Component // Must be set to the Templ component
	html           string          // internal use only
}

func (part *Templ) Init(ctx *nest.Context) error {
	return nil
}

func (widget *Templ) RunOnInitialize(ctx *nest.Context) error {
	return nil
}

func (part *Templ) RunUserEvents(htmxInfo *nest.Context) error {
	return nil
}

// Templ component is rendered in PrepareHTML() because it may cause errors
func (widget *Templ) PrepareHTML(ctx *nest.Context) error {
	if widget.Comp == nil {
		return errors.New("templ component was not set")
	}
	var buf bytes.Buffer
	err := widget.Comp.Render(ctx.Context, &buf)
	if err != nil {
		return err
	}
	widget.html = buf.String()
	return nil
}

// HTML rendering may not cause errors in this framework
func (widget *Templ) GetHTML(ctx *nest.Context) string {
	return widget.html
}
