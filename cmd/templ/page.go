package main

import (
	"fmt"
	"math/rand"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// Example of embedding a Go Templ component into this framework
func webPageWithTemplComponent() nest.UI {
	// Data to show
	statistics := StatInfo{
		Title:       "CPU",
		Value:       fmt.Sprintf("%d%%", rand.Intn(100)),
		Description: "CPU usage",
	}

	// Encapsulation of the Stat() component that was made using Templ
	templWidget := &Templ{
		Comp: Stat(statistics),
	}

	// Web page design
	page := &nest.Panel{
		Refresh: 2,
		Children: []nest.UI{
			&nest.Heading1{Text: "Example of using a templ component"},
			templWidget,
		},
	}

	return page
}
