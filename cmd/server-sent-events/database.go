package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

type chatDatabaseNotificationSubscriber struct {
	ctx     context.Context
	handler func(context.Context)
}

type chatDatabaseMessage struct {
	message   string
	timestamp time.Time
}

type chatDatabase struct {
	mu          sync.RWMutex
	messages    []chatDatabaseMessage
	subscribers []chatDatabaseNotificationSubscriber
}

// Add message to chat
func (db *chatDatabase) Add(msg string) {
	db.mu.Lock()
	message := chatDatabaseMessage{
		message:   msg,
		timestamp: time.Now(),
	}
	db.messages = append(db.messages, message)
	db.mu.Unlock()
	db.NotifySubscribers()
}

func (db *chatDatabase) NotifySubscribers() {
	db.mu.Lock()
	// Delete subscribers that have a closed context
	subid := 0
	for subid < len(db.subscribers) {
		sub := db.subscribers[subid]
		select {
		case <-sub.ctx.Done():
			db.subscribers = append(db.subscribers[:subid], db.subscribers[subid+1:]...)
		default:
			subid++
		}
	}
	db.mu.Unlock()

	db.mu.RLock()
	for _, sub := range db.subscribers {
		sub.handler(sub.ctx)
	}
	fmt.Printf("After adding a message to the database, there are now %d subscribers to the database\n", len(db.subscribers))
	db.mu.RUnlock()
}

// Get a copy of all chat messages
func (db *chatDatabase) GetCopy() []string {
	db.mu.RLock()
	res := make([]string, 0, len(db.messages))
	for _, msg := range db.messages {
		formattedTime := msg.timestamp.Format("2006-01-02 15:04:05")
		res = append(res, fmt.Sprintf("%s: %s", formattedTime, msg.message))
	}
	db.mu.RUnlock()
	return res
}

// Add an event handler that will be called on each new message
// If event handler for the same id exists, it will be replaced.
func (db *chatDatabase) Subscribe(ctx context.Context, fun func(context.Context)) {
	db.mu.Lock()
	db.subscribers = append(db.subscribers, chatDatabaseNotificationSubscriber{
		handler: fun,
		ctx:     ctx,
	})
	db.mu.Unlock()
}

// MISSING: Unsubscribe
