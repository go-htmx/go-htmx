package main

import (
	"context"
	"fmt"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// Chat-system web page
func page2SSE(db *chatDatabase, backend *nest.SseBackend) nest.UI {

	// This component shows all chat messages using <tt</tt>
	fullChatList := &nest.SseSimple{
		Backend: backend,
		Child: &nest.Paragraph{
			OnPrepareHTML: func(control *nest.Paragraph, ctx *nest.Context) error {
				fmt.Println("OnPrepareHTML")
				chatmessages := db.GetCopy()
				control.TextParts = nil
				for _, msg := range chatmessages {
					control.TextParts = append(control.TextParts,
						&nest.Text{Text: msg},
						&nest.LineBreak{})
				}
				return nil
			},
		},
		Handler: func(ctx context.Context, triggerUpdate func(), sseid nest.SseId) {
			db.Subscribe(ctx, func(ctx context.Context) {
				fmt.Println(sseid, "Notified from DB")
				triggerUpdate()
			})
		},
	}

	// Input form for adding messages
	inputform := &nest.CustomHtml{
		OnPrepareHTML: func(control *nest.CustomHtml, ctx *nest.Context) error {
			ctx = ctx.Copy() // We need a different hx-method here
			ctx.HxMethod = "POST"
			control.Html = fmt.Sprintf(`<form %s><input type="text" autofocus name="message"><input type="submit"></form>`, ctx.GetHxAttrs())
			return nil
		},
		OnIncoming: func(control *nest.CustomHtml, ctx *nest.Context) error {
			msg := ctx.Get("message")
			if msg != "" {
				db.Add(msg)
			}
			return nil
		},
	}

	// Full page layout
	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: "Chat application"},
			&nest.Paragraph{Text: "Open multiple browsers on this URL, and see how messages can be sent between them."},
			fullChatList,
			inputform,
		},
	}

	return page
}
