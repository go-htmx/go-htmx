package main

import (
	"fmt"
	"net/http"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"

	_ "embed"

	"github.com/gorilla/mux"
)

//go:embed stylesheet.css
var stylesheet string

//go:embed template.html
var template string

// BOILERPLATE code
// This way of making a handler allows to use templates, and provide other parameters, e.g. database connection parameters.
func makehandler(page func(db *chatDatabase, backend *nest.SseBackend) nest.UI, db *chatDatabase, backend *nest.SseBackend) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Incoming HTTP request", r.Method, r.URL.String(), r.URL.Query().Get(nest.SseParameterName))
		ctx := nest.NewContext(r)
		ctx.HxTarget = "#main" // Which part of the template is replaced by HTMX, by default
		pageProcessor := func() nest.UI { return page(db, backend) }
		if ctx.IsHtmxRequest {
			ctx.RequestHandler(w, pageProcessor)
		} else {
			htmlstr := fmt.Sprintf(template,
				stylesheet,
				ctx.RequestHandlerForTemplate(pageProcessor))
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			_, err := w.Write([]byte(htmlstr))
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				fmt.Println("Incoming HTTP request completed with error")
				return
			}
		}
	}
}

func main() {
	sseBackend := nest.SseBackend{
		Url: "/sse",
	}
	db := chatDatabase{}

	r := mux.NewRouter()
	r.HandleFunc("/", makehandler(page2SSE, &db, &sseBackend))
	r.HandleFunc(sseBackend.Url, sseBackend.GetHttpHandler())

	fmt.Println("Starting server at port 7071")
	fmt.Println("Open your browser on http://127.0.0.1:7071/ to try out the user interface.")
	if err := http.ListenAndServe(":7071", r); err != nil {
		fmt.Println("Failed to start server:", err)
	}
}
