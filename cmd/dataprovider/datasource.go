package main

import (
	"strconv"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

func (value *Product) ToRecord(categorySelectionList []nest.ValueAndTitle) nest.Record[Product] {
	id := nest.PrimaryKeyInt(value.ID)
	return nest.Record[Product]{
		Data:       value,
		PrimaryKey: &id,
		Fields: map[nest.FieldName]nest.FieldValue{
			"ID":          &nest.ValueInt{IntRef: &value.ID, ZeroIsNull: true},
			"Name":        &nest.ValueStr{StrRef: &value.Name, LengthMin: 3},
			"Description": &nest.ValueStr{StrRef: &value.Description},
			"Category": &nest.ValueInt{
				ZeroIsNull:    true,
				Required:      true,
				IntRef:        (*int)(&value.Category),
				SelectionList: categorySelectionList,
			},
			"Location":     &nest.ValueStr{StrRef: &value.Location},
			"CountInStock": &nest.ValueInt{IntRef: &value.CountInStock, Min: 0, Max: 99999999},
			"IsYellow":     &nest.ValueBool{BoolRef: &value.IsYellow},

			// Used by map
			"Latitude":  &nest.ValueFloat{FloatRef: &value.GeoPoint.Latitude, Min: -180, Max: 180},
			"Longitude": &nest.ValueFloat{FloatRef: &value.GeoPoint.Longitude, Min: -360, Max: 360},

			// Used by geopointeditor
			"GeoPoint": &nest.ValueAny[nest.GeoPoint]{Ref: &value.GeoPoint}, // Demonstration of providing values that are not supported by the framework
		},
	}
}

func getCategorySelectionList() []nest.ValueAndTitle {
	var categorySelectionList []nest.ValueAndTitle
	for productCategory, title := range GetProductCategoryStrings() {
		categorySelectionList = append(categorySelectionList, nest.ValueAndTitle{
			Value: strconv.Itoa(int(productCategory)),
			Title: title,
		})
	}
	return categorySelectionList
}

func getDatasource(db *DB) *nest.DataSource[Product] {
	return &nest.DataSource[Product]{
		SortMethods: db.GetProductsSortMethods(),
		OnGetBlank: func(ctx *nest.Context, ds *nest.DataSource[Product]) nest.Record[Product] {
			prod := Product{}
			categorySelectionList := getCategorySelectionList()
			return prod.ToRecord(categorySelectionList)
		},
		OnGetRecords: func(ctx *nest.Context, ds *nest.DataSource[Product], filter nest.FilterAndSort) ([]nest.Record[Product], error) {
			categorySelectionList := getCategorySelectionList()
			// Language is used by QueryProducts in order to get sorting correct according to user's language
			prods := db.QueryProducts(filter.SortMethod, ctx.Locale.GetInfo().Language)

			// This decides what fields are filtered on
			getForFilter := func(item Product) string {
				return item.Name + " " + item.Description
			}

			// Converter for converting database-specific struct type into a framework-type that UI components understand
			converter := func(prod *Product) nest.Record[Product] {
				return prod.ToRecord(categorySelectionList)
			}

			// Apply filtering, range filtering and conversion
			return ds.DoSortFilterRangeConvert(prods, filter, nil, getForFilter, converter)
		},
		OnGetSingle: func(ctx *nest.Context, ds *nest.DataSource[Product]) (nest.Record[Product], error) {
			pk := ds.GetCurrentPrimaryKey().(*nest.PrimaryKeyInt)
			product, err := db.QueryProduct(int(*pk))
			categorySelectionList := getCategorySelectionList()
			return product.ToRecord(categorySelectionList), err
		},
		OnDelete: func(ctx *nest.Context, ds *nest.DataSource[Product]) error {
			pk := ds.GetCurrentPrimaryKey().(*nest.PrimaryKeyInt)
			return db.DeleteProduct(int(*pk))
		},
		OnInsert: func(ctx *nest.Context, ds *nest.DataSource[Product], record nest.Record[Product]) (nest.PrimaryKey, error) {
			product := record.Data
			newID, err := db.InsertProduct(*product)
			newKey := nest.PrimaryKeyInt(newID)
			return &newKey, err
		},
		OnUpdate: func(ctx *nest.Context, ds *nest.DataSource[Product], record nest.Record[Product]) error {
			product := record.Data
			return db.UpdateProduct(*product)
		},
	}
}
