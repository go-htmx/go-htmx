package main

import "gitlab.com/go-htmx/go-htmx/pkg/nest"

func (db *DB) LoadDemoData() {
	db.Products = []Product{
		{ID: 1, Name: "Screwdriver", Category: PcTools, CountInStock: 3422, Description: "Ordinary screwdriver with yellow handle"},
		{ID: 2, Name: "Milk, 1 liter", Category: PcFood, CountInStock: 55329, Description: "Milk with 1.5% fat"},
		{ID: 3, Name: "Bread, 1 loaf", Category: PcFood, CountInStock: 12257, Description: "Bread made of wheat"},
		{ID: 4, Name: "Jacket, size M", Category: PcClothes, CountInStock: 7525, Description: "Outdoor all-weather jacket size Medium"},
		{ID: 5, Name: "Pants, size M", Category: PcClothes, CountInStock: 34420, Description: "", GeoPoint: nest.GeoPoint{Latitude: 40.68927461796574, Longitude: -74.04456349932875}},
		{ID: 6, Name: "Shirts, size L", Category: PcClothes, CountInStock: 7564, Description: ""},
		{ID: 7, Name: "Socks, size 43", Category: PcClothes, CountInStock: 4317, Description: "Pairs of socks"},
		{ID: 8, Name: "Coat, size L", Category: PcClothes, CountInStock: 7712, Description: ""},
		{ID: 9, Name: "Shovel", Category: PcTools, CountInStock: 4322, Description: "Ordinary screwdriver with yellow handle"},
		{ID: 10, Name: "Saw", Category: PcTools, CountInStock: 3317, Description: "", GeoPoint: nest.GeoPoint{Latitude: 55, Longitude: 10.5}},
		{ID: 11, Name: "Hammer", Category: PcTools, CountInStock: 5413, Description: "", GeoPoint: nest.GeoPoint{Latitude: 53, Longitude: 0}},
		{ID: 12, Name: "Pipe", Category: PcTools, CountInStock: 7615, Description: "", GeoPoint: nest.GeoPoint{Latitude: 48.858299483847105, Longitude: 2.2946958731377656}},
		{ID: 13, Name: "Toolbox", Category: PcTools, CountInStock: 55514, Description: ""},
	}
}
