package main

import (
	"errors"
	"sort"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
	"golang.org/x/text/collate"
	"golang.org/x/text/language"
)

type ProductCategory int

const (
	PcFood ProductCategory = iota + 1
	PcToys
	PcClothes
	PcTools
)

func GetProductCategoryStrings() map[ProductCategory]string {
	return map[ProductCategory]string{
		PcFood:    "Food",
		PcClothes: "Clothes",
		PcTools:   "Tools",
		PcToys:    "Toys",
	}
}

type Product struct {
	ID           int
	Name         string
	Description  string
	Category     ProductCategory
	Location     string
	CountInStock int
	GeoPoint     nest.GeoPoint
	IsYellow     bool
}

// This is equivalent to having a relational database, e.g. PostgreSQL
type DB struct {
	Products []Product // Primary key is the product ID
}

func (db *DB) GetProductsSortMethods() map[nest.DatasetSortMethod]string {
	return map[nest.DatasetSortMethod]string{
		"ID":       "By ID",
		"IDrev":    "By ID reversed",
		"Name":     "By name",
		"Category": "By category",
		"Count":    "By count in stock",
	}
}

// Equivalent to a "select * from product" query into the database
func (db *DB) QueryProducts(sortby nest.DatasetSortMethod, lang language.Tag) []Product {
	var list []Product
	list = append(list, db.Products...)
	switch sortby {
	case "ID":
		sort.Slice(list, func(i, j int) bool {
			return (list[i].ID < list[j].ID)
		})
	case "IDrev":
		sort.Slice(list, func(i, j int) bool {
			return !(list[i].ID < list[j].ID)
		})
	case "Name":
		// Ensure language-specific correct sorting
		collator := collate.New(lang)
		sort.Slice(list, func(i, j int) bool {
			return collator.CompareString(list[i].Name, list[j].Name) < 0
		})
	case "Category":
		sort.Slice(list, func(i, j int) bool {
			return (list[i].Category < list[j].Category)
		})
	case "Count":
		sort.Slice(list, func(i, j int) bool {
			return (list[i].CountInStock < list[j].CountInStock)
		})
	}

	return list
}

// Queries one product, like "select * from Product where id=?"
func (db *DB) QueryProduct(id int) (Product, error) {
	for _, product := range db.Products {
		if product.ID == id {
			return product, nil
		}
	}
	return Product{}, errors.New("Product ID not found in database")
}

// Like "update product, set name=?, description=? ... where id=?"
func (db *DB) UpdateProduct(newValue Product) error {
	for index, rec := range db.Products {
		if rec.ID == newValue.ID {
			db.Products[index] = newValue
			return nil
		}
	}
	return errors.New("record does not exist in the dataset")
}

// Like "delete from product where id=?"
func (db *DB) DeleteProduct(id int) error {
	for productIndex, product := range db.Products {
		if product.ID == id {
			db.Products = append(db.Products[:productIndex], db.Products[productIndex+1:]...)
			return nil
		}
	}
	return nil
}

// Like "insert into product(name,description,...) values (?,?,?...)" - return ID of new item
func (db *DB) InsertProduct(newValue Product) (int, error) {
	// Find new id value
	id := 0
	for _, rec := range db.Products {
		id = max(id, rec.ID)
	}
	id++

	// Set primary key value
	newValue.ID = id

	// Insert into database
	db.Products = append(db.Products, newValue)
	return id, nil
}
