package main

import (
	"fmt"
	"net/url"
	"strconv"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// A data-aware table that connects to a database using a DataSource
func myPage(db *DB) nest.UI {
	// The datasource is the edit/browse/load/save logic behind all data aware components
	datasrc := getDatasource(db)

	// Table that shows a list of records, with pagination
	table := &nest.TableDS{
		Columns: []nest.TableColumn{
			{FieldName: "ID", Title: "ID"},
			{FieldName: "Name", Title: "Name"},
			{FieldName: "Category", Title: "Category"},
		},
		PaginationRowsPerPage: 6,
		SortBy:                "Name",
		AllowSelect:           true,
		Dataset:               datasrc,
		Actions: []nest.RecordAction{
			{
				Caption: "Google",
				OnClick: func(ctx *nest.Context, action *nest.RecordAction, record nest.RecordAccessor) error {
					ctx.RedirectURL(fmt.Sprintf("https://www.google.com/search?q=%s", url.QueryEscape(record.GetFieldValue("Name").GetAsString())))
					return nil
				},
				IsVisible: func(ctx *nest.Context, action *nest.RecordAction, record nest.RecordAccessor) bool {
					intval, _ := strconv.Atoi(record.GetFieldValue("ID").GetAsString())
					return intval%3 == 0 // Show on 1/3 of all records
				},
			},
			{
				Caption: "Bing",
				OnClick: func(ctx *nest.Context, action *nest.RecordAction, record nest.RecordAccessor) error {
					ctx.RedirectURL(fmt.Sprintf("https://www.bing.com/search?q=%s", url.QueryEscape(record.GetFieldValue("Name").GetAsString())))
					return nil
				},
				IsVisible: func(ctx *nest.Context, action *nest.RecordAction, record nest.RecordAccessor) bool {
					intval, _ := strconv.Atoi(record.GetFieldValue("ID").GetAsString())
					return intval%2 == 0 // Show on 50% of all records
				},
			},
		},
	}

	// Form for editing a database record
	formPanel := &nest.Panel{
		Horizontal: true,
		Children: []nest.UI{
			&nest.Panel{
				Children: []nest.UI{
					&nest.Heading2{
						// Dynamic headline, which adjusts to the current state of the datasource
						OnInitialize: func(heading *nest.Heading2, ctx *nest.Context) error {
							switch datasrc.GetState() {
							case nest.DsNew:
								heading.Text = "Creating a new record"
							case nest.DsEdit:
								heading.Text = "Editing an existing record"
							case nest.DsRead:
								heading.Text = "Viewing"
							}
							return nil
						},
					},
					&nest.LabelEdit2{
						Caption: "Name",
						Editor: &nest.EditString{
							DataSource:      datasrc,
							DataSourceField: "Name",
						},
					},
					&nest.LabelEdit2{
						Caption: "Description",
						Editor: &nest.EditString{
							Length:          50,
							DataSource:      datasrc,
							DataSourceField: "Description",
						},
					},
					&nest.LabelEdit2{
						Caption: "Category",
						Editor: &nest.Select{
							DataSource:      datasrc,
							DataSourceField: "Category",
						},
					},
					&nest.LabelEdit2{
						Caption: "Location",
						Editor: &nest.EditString{
							DataSource:      datasrc,
							DataSourceField: "Location",
						},
					},
					&nest.LabelEdit2{
						Caption: "Count in stock",
						Editor: &nest.EditNumber{
							Length:          10,
							DataSource:      datasrc,
							DataSourceField: "CountInStock",
						},
					},
					&nest.CheckBox{
						Caption:         "Is yellow",
						DataSource:      datasrc,
						DataSourceField: "IsYellow",
					},
					&GeoPointEditor{
						DataSource:      datasrc,
						DataSourceField: "GeoPoint",
					},
				},
			},
			&nest.LeafletJsMap{
				Height:              nest.Px(300),
				Width:               nest.Px(300),
				DiameterKM:          5,
				DataSource:          datasrc,
				DataSourceLatitude:  "Latitude",
				DataSourceLongitude: "Longitude",
				OnInitialize: func(mymap *nest.LeafletJsMap, ctx *nest.Context) error {
					// Only show the map, if editing or if there is any data
					mymap.Omitted = !datasrc.CanDoSave(ctx) && mymap.Data.Marker.Coor.IsZero()
					return nil
				},
			},
		},
		OnInitialize: func(panel *nest.Panel, ctx *nest.Context) error {
			panel.Omitted = datasrc.GetState() == nest.DsBrowse
			return nil
		},
	}

	// Table and Form are combined into a panel, where the viewmode decides, if they are arranged horizontally or vertically
	editPanel := &nest.Panel{
		Horizontal: true,
		Children: []nest.UI{
			table,
			formPanel,
		},
	}

	datasetPanel := &nest.DatasetPanel{
		DataSource: datasrc,
		ShowSearch: true,
	}

	// Allows the user to select between 2 different ways that the GUI works
	viewMode := &nest.Select{
		DefaultValue: "right",
		Options: []nest.ValueAndTitle{
			{Value: "bottom", Title: "Show edit below table"},
			{Value: "right", Title: "Show edit right of table"},
			{Value: "either", Title: "Show either table or the edit form"},
		},
		OnInitialize: func(control *nest.Select, ctx *nest.Context) error {
			switch control.GetValue() {
			case "either":
				// Show either table or form, but not both
				table.Omitted = datasrc.GetState() != nest.DsBrowse
				formPanel.Omitted = !table.Omitted
				datasetPanel.ShowOnlyCancelSaveOrNot = true

				// Add extra columns in table
				table.Columns = append(table.Columns, nest.TableColumn{FieldName: "Description", Title: "Description"})
				table.Columns = append(table.Columns, nest.TableColumn{FieldName: "CountInStock", Title: "Count"})
			case "right":
				editPanel.Horizontal = true
			case "bottom":
				// Add extra column in table
				table.Columns = append(table.Columns, nest.TableColumn{FieldName: "Description", Title: "Description"})
			}
			return nil
		},
	}

	// Main User Interface
	page :=
		&nest.PagesTabbed{
			Pages: []nest.PagesTabbedPage{
				{Caption: "Datasource-enabled", Page: &nest.Panel{
					Children: []nest.UI{
						datasrc,
						&nest.Heading1{Text: "Table with data from a DatasetProvider"},
						viewMode,
						datasetPanel,
						&nest.HR{},
						editPanel,
					},
				}},
				{Caption: "Other", Page: &nest.Panel{
					Children: []nest.UI{
						&nest.Heading1{Text: "Other page"},
						&nest.Paragraph{Text: "This page demonstrates what happens when switching away from a datasource-enabled UI."},
						&nest.Button{
							Caption: "Click me",
							OnClick: func(button *nest.Button, ctx *nest.Context) error {
								button.Caption = "I was clicked"
								return nil
							},
						},
					},
				}},
			},
		}

	return page
}
