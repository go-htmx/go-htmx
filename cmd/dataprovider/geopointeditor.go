package main

import (
	"errors"
	"log/slog"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// This type demonstrates how to make a custom editor based on other components, for data types that are not supported by the framework.
type GeoPointEditor struct {
	nest.Component
	DataSource      nest.DataSourceProvider
	DataSourceField nest.FieldName
	edLat           nest.EditNumber
	edLong          nest.EditNumber
	panel           nest.Panel
}

var _ nest.Editor = &GeoPointEditor{}
var _ nest.UIProvider = &GeoPointEditor{}

func (ed *GeoPointEditor) GetChildren() []nest.UI {
	if ed.panel.Children == nil {
		ed.edLat.NumberMin = -180
		ed.edLat.NumberMax = 180
		ed.edLat.Decimals = 6
		ed.edLong.NumberMin = -200
		ed.edLong.NumberMax = 200
		ed.edLong.Decimals = 6
		ed.panel.Children = []nest.UI{
			&nest.Paragraph{Text: nest.T("Remove map from source code to use this for editing")},
			&nest.LabelEdit2{
				Caption: nest.T("Latitude"),
				Editor:  &ed.edLat,
			},
			&nest.LabelEdit2{
				Caption: nest.T("Longitude"),
				Editor:  &ed.edLong,
			},
		}
	}
	return []nest.UI{&ed.panel}
}

func (ed *GeoPointEditor) GetValidationResult(ctx *nest.Context) error {
	return nil
}

func (ed *GeoPointEditor) EnableValidation(enabled bool) {
}

func (ed *GeoPointEditor) SetValueDefault(ctx *nest.Context) {
	ed.edLat.SetValueDefault(ctx)
	ed.edLong.SetValueDefault(ctx)
}

func (ed *GeoPointEditor) Init(ctx *nest.Context) error {
	return nil
}

func (ed *GeoPointEditor) RunOnInitialize(ctx *nest.Context) error {
	// After datasource has been initialized, we can access it
	field := ed.DataSource.GetField(ctx, ed.DataSourceField)
	if field == nil {
		return errors.New("field unknown " + string(ed.DataSourceField))
	}
	fieldValue, found := field.(*nest.ValueAny[nest.GeoPoint])
	if !found {
		slog.Error("Wrong field type for GeoPoint Editor")
		return nil
	}
	if ed.DataSource.GetStateShouldFormsLoadNewData(ctx) {
		ed.edLat.SetValue(ctx, fieldValue.Ref.Latitude)
		ed.edLong.SetValue(ctx, fieldValue.Ref.Longitude)
	}
	if ed.DataSource.GetStateReadOnly(ctx) {
		ed.edLat.ReadOnly = true
		ed.edLong.ReadOnly = true
	}
	lat, err1 := ed.edLat.GetValue()
	long, err2 := ed.edLong.GetValue()
	if err1 == nil && err2 == nil {
		*fieldValue.Ref = nest.GeoPoint{
			Latitude:  lat,
			Longitude: long,
		}
	}
	return nil
}

func (ed *GeoPointEditor) RunUserEvents(ctx *nest.Context) error {
	return nil
}

func (ed *GeoPointEditor) PrepareHTML(ctx *nest.Context) error {
	return ed.panel.PrepareHTML(ctx)
}

func (ed *GeoPointEditor) GetHTML(ctx *nest.Context) string {
	return ed.panel.GetHTML(ctx)
}

func (f *GeoPointEditor) GetSortableRepresentation() (string, float64) {
	return "", 0
}
