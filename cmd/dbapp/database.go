package main

import "errors"

type ProductCategory int

const (
	PcFood ProductCategory = iota + 1
	PcToys
	PcClothes
	PcTools
)

func GetProductCategoryStrings() map[ProductCategory]string {
	return map[ProductCategory]string{
		PcFood:    "Food",
		PcClothes: "Clothes",
		PcTools:   "Tools",
		PcToys:    "Toys",
	}
}

type Product struct {
	ID           int
	Name         string
	Description  string
	Category     ProductCategory
	Location     string
	CountInStock int
}

// This is equivalent to having a relational database, e.g. PostgreSQL
type DB struct {
	Products []Product // Primary key is the product ID
}

// Equivalent to a "select * from product" query into the database
func (db *DB) QueryProducts() []Product {
	var list []Product
	list = append(list, db.Products...)
	return list
}

// Queries one product, like "select * from Product where id=?"
func (db *DB) QueryProduct(id int) (Product, error) {
	for _, product := range db.Products {
		if product.ID == id {
			return product, nil
		}
	}
	return Product{}, errors.New("Product ID not found in database")
}

// Like "update product, set name=?, description=? ... where id=?"
func (db *DB) UpdateProduct(newValue Product) error {
	for productIndex, product := range db.Products {
		if product.ID == newValue.ID {
			db.Products[productIndex] = newValue
			return nil
		}
	}
	return errors.New("product ID not found")
}

// Like "delete from product where id=?"
func (db *DB) DeleteProduct(id int) error {
	for productIndex, product := range db.Products {
		if product.ID == id {
			db.Products = append(db.Products[:productIndex], db.Products[productIndex+1:]...)
			return nil
		}
	}
	return nil
}

// Like "insert into product(name,description,...) values (?,?,?...)" - return ID of new item
func (db *DB) InsertProduct(newValue Product) (int, error) {
	// Find new id value
	id := 0
	for _, rec := range db.Products {
		id = max(id, rec.ID)
	}
	id++

	// Set primary key value
	newValue.ID = id

	// Insert into database
	db.Products = append(db.Products, newValue)
	return id, nil
}
