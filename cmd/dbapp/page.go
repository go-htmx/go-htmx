package main

import (
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// DEPRECATED - see /cmd/dataprovider instead

// A minimum version of a web page showing client-side state with a button
func myPage(db *DB) nest.UI {
	// Pick an ID for this database editor
	var productList []Product

	// Table view
	table := &nest.Table{
		Columns: []nest.TableColumn{
			{FieldName: "id", Title: "ID"},
			{FieldName: "name", Title: "Name"},
			{FieldName: "category", Title: "Category"},
			{FieldName: "location", Title: "Location"},
			{FieldName: "count", Title: "Stock", ExtraInfo: nest.T("Count of items in stock")},
		},
		AllowSelect:           true,
		LastLineIsTotal:       false,
		SortBy:                "id",
		PaginationRowsPerPage: 3,
		OnPrepareHTML: func(table *nest.Table, ctx *nest.Context) error {
			productList = db.QueryProducts()
			table.PaginationTotalCount = len(productList)
			return nil
		},
		OnLoadPage: func(table *nest.Table, ctx *nest.Context, firstRow int) error {
			// Add rows of database data
			for recIndex, rec := range productList {
				if recIndex < firstRow || recIndex >= firstRow+table.PaginationRowsPerPage {
					continue
				}
				editors := getProductEditors(&rec)
				row := table.NewRowInt(rec.ID)
				row.Cells = map[nest.FieldName]*nest.TableCell{
					"id":       {Value: &editors.ID},
					"name":     {Value: &editors.Name},
					"category": {Value: &editors.Category},
					"location": {Value: &editors.Location},
					"count":    {Value: &editors.CountInStock},
				}
			}
			if table.LastLineIsTotal {
				// Add sum row
				row := table.NewRowInt(-1)
				row.Cells = map[nest.FieldName]*nest.TableCell{
					"count": nest.NewGTableTotalSum(),
				}
			}
			return nil
		},
	}

	// Form view
	var currentRecord Product
	currentRecordEditors := getProductEditors(&currentRecord)
	form := &nest.FormHtml{
		Child: &nest.Panel{
			Children: []nest.UI{
				&nest.LabelEdit{
					Caption:    "ID",
					Editor:     &currentRecordEditors.ID,
					IsReadOnly: true,
				},
				&nest.LabelEdit{
					Caption: "Name",
					Editor:  &currentRecordEditors.Name,
				},
				&nest.LabelEdit{
					Caption: "Description",
					Editor:  &currentRecordEditors.Description,
				},
				&nest.LabelEdit{
					Caption: "Category",
					Editor:  &currentRecordEditors.Category,
				},
				&nest.LabelEdit{
					Caption: "Location",
					Editor:  &currentRecordEditors.Location,
				},
				&nest.LabelEdit{
					Caption: "Count in stock",
					Editor:  &currentRecordEditors.CountInStock,
				},
			},
		},
		OnInitialize: func(form *nest.FormHtml, ctx *nest.Context) error {
			id := ctx.GetInt(form.ID + "selection")
			if id == 0 {
				return nil
			}
			var err error
			currentRecord, err = db.QueryProduct(id)
			return err
		},
	}

	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: nest.T("Dataset editor")},
			&nest.DatasetEdit{
				Table:        table,
				Form:         form,
				EnableSearch: true,
				ExtraButtons: []*nest.Button{
					{
						Caption: "Test 1",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							return nest.UserErrorMsg(nest.T("Test 1 button was not implemented, yet"))
						},
					},
					{
						Caption: "Test 2",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							button.Caption = nest.T("Clicked")
							return nil
						},
					},
					{
						Caption: "Test 3",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							button.Caption = nest.T("Clicked")
							return nil
						},
					},
				},
				OnUpdate: func(ed *nest.DatasetEdit, ctx *nest.Context, selected int) error {
					return db.UpdateProduct(currentRecord)
				},
				OnInsert: func(ed *nest.DatasetEdit, ctx *nest.Context) error {
					_, err := db.InsertProduct(currentRecord)
					return err
				},
				OnDelete: func(ed *nest.DatasetEdit, ctx *nest.Context, selected int) error {
					return db.DeleteProduct(selected)
				},
			},
		},
	}
	return page
}
