package main

func (db *DB) LoadDemoData() {
	db.Products = []Product{
		{ID: 1, Name: "Screwdriver", Category: PcTools, CountInStock: 22, Description: "Ordinary screwdriver with yellow handle"},
		{ID: 2, Name: "Milk, 1 liter", Category: PcFood, CountInStock: 329, Description: "Milk with 1.5% fat"},
		{ID: 3, Name: "Bread, 1 loaf", Category: PcFood, CountInStock: 257, Description: "Bread made of wheat"},
		{ID: 4, Name: "Jacket, size M", Category: PcClothes, CountInStock: 25, Description: "Outdoor all-weather jacket size Medium"},
		{ID: 5, Name: "Pants, size M", Category: PcClothes, CountInStock: 20, Description: ""},
		{ID: 6, Name: "Shirts, size L", Category: PcClothes, CountInStock: 64, Description: ""},
		{ID: 7, Name: "Socks, size 43", Category: PcClothes, CountInStock: 17, Description: "Pairs of socks"},
		{ID: 8, Name: "Coat, size L", Category: PcClothes, CountInStock: 12, Description: ""},
	}
}
