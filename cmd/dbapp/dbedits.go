package main

import (
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

type ProductEditors struct {
	ID           nest.GEditInt
	Name         nest.GEditString
	Description  nest.GEditString
	Category     nest.GEditSelect
	Location     nest.GEditString
	CountInStock nest.GEditInt
}

func getProductRepresentations() map[int]string {
	list := GetProductCategoryStrings()
	res := make(map[int]string)
	for pc, label := range list {
		res[int(pc)] = label
	}
	return res
}

func getProductEditors(record *Product) ProductEditors {
	return ProductEditors{
		ID: nest.GEditInt{
			IntRef:            &record.ID,
			LimitedInputRange: true,
			LimitLow:          1,
			LimitHigh:         99999,
			Length:            8,
			ZeroIsNull:        true,
		},
		Name: nest.GEditString{
			StringRef: &record.Name,
			MaxLength: 20,
			OnValidate: func(str string) error {
				if len(str) < 3 {
					return nest.UserErrorMsg("Product name must be at least 3 characters")
				}
				return nil
			},
		},
		Description: nest.GEditString{
			StringRef: &record.Description,
			Length:    80,
		},
		Category: nest.GEditSelect{
			IntRef:          (*int)(&record.Category),
			Representations: getProductRepresentations(),
			AskUserToSelect: true,
			ZeroIsInvalid:   true,
		},
		Location: nest.GEditString{
			StringRef: &record.Location,
			MaxLength: 20,
		},
		CountInStock: nest.GEditInt{
			IntRef:            &record.CountInStock,
			LimitedInputRange: true,
			LimitLow:          0,
			LimitHigh:         999999,
		},
	}
}
