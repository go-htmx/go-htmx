# Database edit example (deprecated)

Do not use this demo. Use /cmd/dataprovider instead.

This example implements a web UI for editing data in a database.

The database itself is simulated using an in-memory storage, but using a database connection initialized in the main() function.

The editor has both a table view and an editor form view:

image:screenshot-table-view.png[]

image:screenshot-edit-view.png[]

Also, the table supports sorting, searching and has a footer with an automatically calculated sum.
