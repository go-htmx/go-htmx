package main

import (
	"fmt"
	"math"
	"math/rand"
)

type gamePosition struct {
	x, y int
}

type gameFood struct {
	position gamePosition
}

type gameMode int

const (
	gmNotInitialized gameMode = 0
	gmNew            gameMode = 1
	gmRunning        gameMode = 2
	gmFinished       gameMode = 3
)

type gameDirection int

const (
	gdUp    gameDirection = 1
	gdRight gameDirection = 2
	gdDown  gameDirection = 3
	gdLeft  gameDirection = 4
)

func (a gamePosition) getDistance(b gamePosition) float64 {
	dx := float64(b.x) - float64(a.x)
	dy := float64(b.y) - float64(a.y)
	return math.Sqrt(dx*dx + dy*dy)
}

func getRandomDirection() gameDirection {
	randomint := rand.Intn(4) + 1
	return gameDirection(randomint)
}

// Assumes that lines are horizontal or vertical
func checkLinesCrossed(linea1, linea2, lineb1, lineb2 gamePosition) bool {
	// Ensure linea1 is the top or left point, and linea2 is the bottom or right point
	if linea1.x > linea2.x || linea1.y > linea2.y {
		linea1, linea2 = linea2, linea1
	}

	// Ensure lineb1 is the top or left point, and lineb2 is the bottom or right point
	if lineb1.x > lineb2.x || lineb1.y > lineb2.y {
		lineb1, lineb2 = lineb2, lineb1
	}

	// Check if lines are vertical and horizontal
	isLineaVertical := linea1.x == linea2.x
	isLineaHorizontal := linea1.y == linea2.y
	isLinebVertical := lineb1.x == lineb2.x
	isLinebHorizontal := lineb1.y == lineb2.y

	if isLineaVertical && isLineaHorizontal {
		return false
	}

	if isLinebVertical && isLinebHorizontal {
		return false
	}

	// If one line is vertical and the other is horizontal, check for intersection
	if isLineaVertical && isLinebHorizontal {
		// Linea is vertical and Lineb is horizontal
		crossed := lineb1.x < linea1.x && lineb2.x > linea1.x &&
			linea1.y < lineb1.y && linea2.y > lineb1.y
		if crossed {
			fmt.Println("Crossed: ", linea1, linea2, lineb1, lineb2)
		}
		return crossed
	} else if isLineaHorizontal && isLinebVertical {
		// Linea is horizontal and Lineb is vertical
		crossed := linea1.x < lineb1.x && linea2.x > lineb1.x &&
			lineb1.y < linea1.y && lineb2.y > linea1.y
		if crossed {
			fmt.Println("Crossed: ", linea1, linea2, lineb1, lineb2)
		}
		return crossed
	}

	// If both lines are either vertical or horizontal, they can't cross
	return false
}
