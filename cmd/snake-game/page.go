package main

import "gitlab.com/go-htmx/go-htmx/pkg/nest"

func pageGame() nest.UI {
	gameUI := &SnakeArena{
		gameEngine: &game,
	}

	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: "Snake game"},
			&nest.Panel{
				Horizontal: true,
				Children: []nest.UI{
					&nest.Button{
						Caption: "New game",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							gameUI.gameEngine.Restart(600, 300, 10)
							return nil
						},
					},
					&nest.Button{
						Caption: "Up",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							gameUI.gameEngine.SetSnakeDirection(gdUp)
							return nil
						},
					},
					&nest.Button{
						Caption: "Down",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							gameUI.gameEngine.SetSnakeDirection(gdDown)
							return nil
						},
					},
					&nest.Button{
						Caption: "Left",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							gameUI.gameEngine.SetSnakeDirection(gdLeft)
							return nil
						},
					},
					&nest.Button{
						Caption: "Right",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							gameUI.gameEngine.SetSnakeDirection(gdRight)
							return nil
						},
					},
				},
			},
			&nest.Panel{
				Refresh:         2,    // Infrequent reloads just to check if the game should have been started from another browser window
				HtmxReloadLocal: true, // Ensures that the buttons are not reloaded during gameplay
				Children: []nest.UI{
					gameUI,
				},
				OnPrepareHTML: func(p *nest.Panel, ctx *nest.Context) error {
					// Set UI framerate
					if game.gameMode == gmNew || game.gameMode == gmRunning {
						p.Refresh = 0.05
					}
					return nil
				},
			},
		},
		OnPrepareHTML: func(p *nest.Panel, ctx *nest.Context) error {
			return nil
		},
	}
	return page
}
