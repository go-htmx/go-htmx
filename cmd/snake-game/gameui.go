package main

import (
	"fmt"
	"strings"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

type SnakeArena struct {
	nest.Component
	gameEngine *SnakeGame
}

const foodSize = 10

func (part *SnakeArena) Init(ctx *nest.Context) error {
	return nil
}

func (part *SnakeArena) RunOnInitialize(htmxInfo *nest.Context) error {
	return nil
}

func (part *SnakeArena) RunUserEvents(htmxInfo *nest.Context) error {
	return nil
}

func (part *SnakeArena) PrepareHTML(htmxInfo *nest.Context) error {
	return nil
}

func (sa *SnakeArena) GetHTML(htmxInfo *nest.Context) string {
	sa.gameEngine.mu.RLock()
	isNotReady := sa.gameEngine.gameMode == gmNotInitialized
	sa.gameEngine.mu.RUnlock()
	if isNotReady {
		return "<p>Waiting for game to start"
	}
	sa.gameEngine.mu.RLock()
	background := fmt.Sprintf(
		`<rect width="%d" height="%d" x="0" y="0" fill="white" />`+
			`<line x1="0" y1="0" x2="%d" y2="0" stroke="black" stroke-width="2"/>`+
			`<line x1="0" y1="0" x2="0" y2="%d" stroke="black" stroke-width="2"/>`+
			`<line x1="%d" y1="0" x2="%d" y2="%d" stroke="black" stroke-width="2"/>`+
			`<line x1="0" y1="%d" x2="%d" y2="%d" stroke="black" stroke-width="2"/>`,
		sa.gameEngine.width, sa.gameEngine.height,
		sa.gameEngine.width, sa.gameEngine.height,
		sa.gameEngine.width, sa.gameEngine.width, sa.gameEngine.height,
		sa.gameEngine.height, sa.gameEngine.width, sa.gameEngine.height)

	var parts strings.Builder
	for _, food := range sa.gameEngine.food {
		parts.WriteString(fmt.Sprintf(`<rect width="%d" height="%d" x="%d" y="%d" rx="2" ry="2" fill="green" />`,
			foodSize, foodSize, food.position.x, food.position.y))
	}

	// Snake
	var snakeparts []string
	for _, part := range sa.gameEngine.snake {
		snakeparts = append(snakeparts, fmt.Sprintf("%d,%d", part.x, part.y))
	}
	parts.WriteString(fmt.Sprintf(`<polyline style="fill:none;stroke:red;stroke-width:3" points="%s" />`, strings.Join(snakeparts, " ")))
	parts.WriteString(fmt.Sprintf(`<circle style="fill:red;stroke:red;stroke-width:3" cx="%d" cy="%d" r="%d" />`, sa.gameEngine.snake[0].x, sa.gameEngine.snake[0].y, foodSize/2))
	htmlstr := fmt.Sprintf(`<svg width="14cm" height="7cm" viewBox="0 0 %d %d" xmlns="http://www.w3.org/2000/svg">%s%s</svg>`, sa.gameEngine.width, sa.gameEngine.height, background, parts.String())
	sa.gameEngine.mu.RUnlock()
	return htmlstr
}
