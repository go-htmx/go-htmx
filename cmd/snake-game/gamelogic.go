package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

type SnakeGame struct {
	mu             sync.RWMutex
	height, width  int
	food           []gameFood
	snake          []gamePosition // First is the head of the snake
	snakeLength    int
	snakeDirection gameDirection
	lastTurn       time.Time // Timestamp of setting snake[1]
	gameMode       gameMode
}

func (eng *SnakeGame) SetSnakeDirection(direction gameDirection) {
	// Check, if change is required
	eng.mu.RLock()
	isNewDirection := eng.snakeDirection != direction
	eng.mu.RUnlock()
	if !isNewDirection {
		return
	}

	eng.mu.Lock()
	eng.snakeDirection = direction
	eng.snake = append([]gamePosition{{x: eng.snake[0].x, y: eng.snake[0].y}}, eng.snake...)
	eng.lastTurn = time.Now()
	eng.mu.Unlock()
}

func (eng *SnakeGame) GetRandomPosition() gamePosition {
	return gamePosition{
		x: rand.Intn(eng.width),
		y: rand.Intn(eng.height),
	}
}

func (eng *SnakeGame) Restart(width, height, foodCount int) {
	eng.mu.Lock()
	eng.gameMode = gmNew
	eng.width = width
	eng.height = height
	snakePosition := eng.GetRandomPosition()
	eng.snake = []gamePosition{snakePosition, snakePosition} // Start and end are the same
	eng.snakeDirection = getRandomDirection()
	eng.snakeLength = 40 // Start length of snake
	eng.food = make([]gameFood, foodCount)
	for index := range eng.food {
		eng.food[index] = gameFood{
			position: eng.GetRandomPosition(),
		}
	}
	eng.mu.Unlock()
}

// Only works because snake only has vertical and horisontal parts
func snakePartLength(a, b gamePosition) int {
	dx := b.x - a.x
	if dx < 0 {
		dx = -dx
	}
	dy := b.y - a.y
	if dy < 0 {
		dy = -dy
	}
	return dx + dy
}

func (eng *SnakeGame) getRealSnakeLength() int {
	realLength := 0
	for index, part := range eng.snake {
		if index == 0 {
			continue
		}
		realLength += snakePartLength(part, eng.snake[index-1])
	}
	return realLength
}

const sleepInterval = 20 * time.Millisecond
const snakeSpeed float64 = 20.0 / (1.0 * float64(time.Second))

// Run this in a separate thread using "go engine.Run()"
func (eng *SnakeGame) Run() {
	for {
		// Wait for the game to start
		for eng.gameMode != gmNew {
			time.Sleep(sleepInterval)
		}

		// Start the game
		eng.mu.Lock()
		eng.lastTurn = time.Now()
		eng.gameMode = gmRunning
		eng.mu.Unlock()

		// Run the game
		for eng.gameMode == gmRunning {
			time.Sleep(sleepInterval)
			eng.mu.Lock()

			// Move snake head
			durSinceTurn := time.Since(eng.lastTurn)
			distanceSinceTurnFloat := float64(durSinceTurn) * snakeSpeed
			distanceSinceTurn := int(distanceSinceTurnFloat)
			switch eng.snakeDirection {
			case gdDown:
				eng.snake[0].y = eng.snake[1].y + distanceSinceTurn
			case gdUp:
				eng.snake[0].y = eng.snake[1].y - distanceSinceTurn
			case gdRight:
				eng.snake[0].x = eng.snake[1].x + distanceSinceTurn
			case gdLeft:
				eng.snake[0].x = eng.snake[1].x - distanceSinceTurn
			default:
				panic("Illegal direction")
			}

			// Detect if snake hits itself
			for segindex := range eng.snake {
				if segindex <= 1 {
					continue
				}
				if checkLinesCrossed(eng.snake[0], eng.snake[1], eng.snake[segindex-1], eng.snake[segindex]) {
					fmt.Println("Snake crossed itself")
					eng.gameMode = gmFinished
				}

			}

			// Detect if food was hit
			for foodindex, food := range eng.food {
				if food.position.getDistance(eng.snake[0]) < foodSize {
					fmt.Println("Food found!!")
					eng.food[foodindex].position = eng.GetRandomPosition()
					eng.snakeLength = eng.snakeLength * 3 / 2
				}
			}

			// Insert extra point in case the snake has no turns, so that lastTurn time has a well defined location
			if len(eng.snake) == 2 {
				eng.snake = []gamePosition{eng.snake[0], eng.snake[0], eng.snake[1]}
				eng.lastTurn = time.Now()
			}

			// Move snake end
			realLength := eng.getRealSnakeLength()
			extraLength := realLength - eng.snakeLength
			if extraLength > 0 {
				snakeLastIndex := len(eng.snake) - 1
				snakeLastPartLength := snakePartLength(eng.snake[snakeLastIndex], eng.snake[snakeLastIndex-1])
				if snakeLastPartLength <= extraLength {
					eng.snake = eng.snake[0:snakeLastIndex]
				} else {
					if eng.snake[snakeLastIndex].x > eng.snake[snakeLastIndex-1].x {
						eng.snake[snakeLastIndex].x -= extraLength
					} else if eng.snake[snakeLastIndex].x < eng.snake[snakeLastIndex-1].x {
						eng.snake[snakeLastIndex].x += extraLength
					} else if eng.snake[snakeLastIndex].y > eng.snake[snakeLastIndex-1].y {
						eng.snake[snakeLastIndex].y -= extraLength
					} else if eng.snake[snakeLastIndex].y < eng.snake[snakeLastIndex-1].y {
						eng.snake[snakeLastIndex].y += extraLength
					} else {
						// Snake's back part has length 0 so we remove it. Next iteration will then cut parts of the next section
						eng.snake = eng.snake[0:snakeLastIndex]
					}
				}
			}

			// Check if snake has hit the wall
			if eng.snake[0].x < 0 || eng.snake[0].x > eng.width || eng.snake[0].y < 0 || eng.snake[0].y > eng.height {
				fmt.Println("Snake hit the wall")
				eng.gameMode = gmFinished
			}

			eng.mu.Unlock()
		}
	}
}
