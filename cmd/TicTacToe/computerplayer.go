package main

import (
	"math/rand"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// Plays the computer using specified color and strength, and saves board to context
func computerPlayer(ctx *nest.Context, player player, board *boardUi, strengthPct float64) error {
	choices := board.grid.findPossibleMoves()
	if choices == nil {
		return nest.UserErrorMsg("No more choices")
	}
	if rand.Float64() < strengthPct {
		for _, choice := range choices {
			testBoard := board.grid
			testBoard[choice] = player
			if testBoard.hasWinner() != playerNone {
				board.setItem(ctx, choice, player)
				return nil
			}
		}
		for _, choice := range choices {
			testBoard := board.grid
			testBoard[choice] = player.Opponent()
			if testBoard.hasWinner() != playerNone {
				board.setItem(ctx, choice, player)
				return nil
			}
		}
	}
	randomIndex := rand.Intn(len(choices))

	// Save to board and to context
	board.setItem(ctx, choices[randomIndex], player)
	return nil
}
