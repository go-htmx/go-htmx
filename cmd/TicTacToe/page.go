package main

import (
	"strconv"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// Finds out if player should use X or O
func getPlayer(humanSelector *nest.Select, isHuman bool) player {
	humanPlaysO := humanSelector.GetValue() != ""
	player := playerO
	if humanPlaysO != isHuman {
		player = playerX
	}
	return player
}

func getStrength(sel *nest.Select) float64 {
	val, _ := strconv.ParseFloat(sel.GetValue(), 64)
	return val
}

func pageGame() nest.UI {
	humanSelector := &nest.Select{
		Options: []nest.ValueAndTitle{
			{Title: "Human plays X", Value: ""},
			{Title: "Human plays O", Value: "O"},
		},
	}
	firstPlayer := &nest.Select{
		Options: []nest.ValueAndTitle{
			{Title: "Human plays first", Value: ""},
			{Title: "Computer plays first", Value: "computer"},
		},
	}
	computerMode := &nest.Select{
		Options: []nest.ValueAndTitle{
			{Title: "Random computer", Value: ""},
			{Title: "Weak computer", Value: "0.5"},
			{Title: "Strong computer", Value: "1.0"},
		},
	}

	board := &boardUi{
		OnClick: func(board *boardUi, index int, ctx *nest.Context) {
			board.setItem(ctx, index, getPlayer(humanSelector, true))
			if board.grid.hasWinner() == playerNone {
				computerPlayer(ctx, getPlayer(humanSelector, false), board, getStrength(computerMode))
			}
		},
	}

	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: "Tic Tac Toe"},
			&nest.Panel{
				Horizontal: true,
				Children: []nest.UI{
					&nest.Button{
						Caption: "New game",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							board.clearBoard(ctx)
							ctx.Restart()
							return nil
						},
					},
					humanSelector,
					firstPlayer,
					computerMode,
				},
			},
			board,
			&nest.Paragraph{
				OnPrepareHTML: func(control *nest.Paragraph, ctx *nest.Context) error {
					switch board.grid.hasWinner() {
					case playerX:
						control.Text = "X has won"
					case playerO:
						control.Text = "O has won"
					default:
						control.Omitted = true
					}
					return nil
				},
			},
		},
		OnHandleEvents: func(p *nest.Panel, ctx *nest.Context) error {
			if board.grid.isEmpty() && firstPlayer.GetValue() == "computer" {
				computerPlayer(ctx, getPlayer(humanSelector, false), board, getStrength(computerMode))
			}
			return nil
		},
	}

	return page
}
