package main

import (
	"fmt"
	"net/http"

	_ "embed"

	"github.com/gorilla/mux"
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

//go:embed stylesheet.css
var stylesheet string

//go:embed template.html
var template string

// BOILERPLATE code
// This way of making a handler allows to use templates, and provide other parameters, e.g. database connection parameters.
func makehandler(page func() nest.UI) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Incoming HTTP request", r.Method, r.URL.String())
		ctx := nest.NewContext(r)
		ctx.HxTarget = "#main" // Which part of the template is replaced by HTMX, by default
		if ctx.IsHtmxRequest {
			ctx.RequestHandler(w, page)
		} else {
			htmlstr := fmt.Sprintf(template, stylesheet,
				`<script src="https://unpkg.com/htmx.org@2.0.1" integrity="sha384-QWGpdj554B4ETpJJC9z+ZHJcA/i59TyjxEPXiiUgN2WmTyV5OEZWCD6gQhgkdpB/" crossorigin="anonymous"></script>`,
				ctx.RequestHandlerForTemplate(page))
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			_, err := w.Write([]byte(htmlstr))
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				fmt.Println("Incoming HTTP request completed with error")
				return
			}
		}
	}
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", makehandler(pageGame))

	fmt.Println("Starting server at port 7071")
	fmt.Println("Open your browser on http://127.0.0.1:7071/ to play the game.")
	if err := http.ListenAndServe(":7071", r); err != nil {
		fmt.Println("Failed to start server:", err)
	}
}
