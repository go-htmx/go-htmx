package main

import (
	"fmt"
	"strings"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

type boardUi struct {
	nest.Component
	grid    gridStates
	OnClick func(board *boardUi, index int, ctx *nest.Context)
}

// Saves board to context
func (ui *boardUi) update(htmxInfo *nest.Context) {
	var intarr []int
	for _, value := range ui.grid {
		intarr = append(intarr, int(value))
	}
	htmxInfo.SetIntArray("board", intarr)
}

// Clears board and saves board to context
func (ui *boardUi) clearBoard(htmxInfo *nest.Context) {
	ui.grid.clear()
	ui.update(htmxInfo)
}

// Sets a field on the board to the specified color and saves to context
func (ui *boardUi) setItem(htmxInfo *nest.Context, index int, item player) {
	if ui.grid.hasWinner() != playerNone {
		return
	}
	ui.grid[index] = item
	ui.update(htmxInfo)
}

func (ui *boardUi) Init(htmxInfo *nest.Context) error {
	board := htmxInfo.GetIntArray("board")
	if len(board) != 0 {
		if len(board) != 9 {
			return nest.UserErrorMsg("Problem with board, restarting")
		}
		for i := range ui.grid {
			if board[i] >= 3 {
				return nest.UserErrorMsg("Problem with board contents, restarting")
			}
			ui.grid[i] = player(board[i])
		}
	}
	return nil
}

func (ui *boardUi) RunOnInitialize(htmxInfo *nest.Context) error {
	return nil
}

func (ui *boardUi) RunUserEvents(htmxInfo *nest.Context) error {
	action := htmxInfo.GetInt("action")
	if action != 0 {
		if action < 1 || action >= 10 || ui.grid[action-1] != playerNone {
			return nest.UserErrorMsg("Problem with action, no move done.")
		}
		if ui.OnClick != nil {
			ui.OnClick(ui, action-1, htmxInfo)
		}
	}

	return nil
}

func (*boardUi) PrepareHTML(htmxInfo *nest.Context) error {
	return nil
}

func (board *boardUi) GetHTML(ctx *nest.Context) string {
	var sl []string
	sl = append(sl,
		`<svg width="6cm" height="6cm" viewBox="0 0 60 60" xmlns="http://www.w3.org/2000/svg">
		<style>
		.clickable { cursor: pointer; }
		</style>
		<line x1="20" y1="0" x2="20" y2="60" stroke="black" stroke-width="2"/>
		<line x1="40" y1="0" x2="40" y2="60" stroke="black" stroke-width="2"/>
		<line x1="0" y1="20" x2="60" y2="20" stroke="black" stroke-width="2"/>
		<line x1="0" y1="40" x2="60" y2="40" stroke="black" stroke-width="2"/>`)
	gameWinner := board.grid.hasWinner()
	board.update(ctx)
	for row := range 3 {
		for col := range 3 {
			index := row*3 + col
			x := col*20 + 5
			y := row*20 + 5
			switch board.grid[index] {
			case playerNone:
				if gameWinner == playerNone {
					ctx2 := ctx.Copy()
					ctx2.SetOTInt("action", index+1)
					ctx2.HxMethod = "PUT"
					sl = append(sl, fmt.Sprintf(`<rect %s x="%d" y="%d" width="20" height="20" fill="transparent" style="cursor:pointer"/>`,
						ctx2.GetHxAttrs(), col*20, row*20))
				}
			case playerO:
				sl = append(sl, fmt.Sprintf(`<circle cx="%d" cy="%d" r="6" stroke="blue" stroke-width="2" fill="none"/>`, x+5, y+5))
			case playerX:
				sl = append(sl,
					fmt.Sprintf(`<line x1="%d" y1="%d" x2="%d" y2="%d" stroke="red" stroke-width="2"/>`, x, y, x+10, y+10),
					fmt.Sprintf(`<line x1="%d" y1="%d" x2="%d" y2="%d" stroke="red" stroke-width="2"/>`, x, y+10, x+10, y))
			}
		}
	}
	sl = append(sl, `</svg>`)
	return strings.Join(sl, "\n")
}
