package main

type player int //

const (
	playerNone player = 0
	playerX    player = 1
	playerO    player = 2
)

type gridStates [9]player

func (pl player) Opponent() player {
	switch pl {
	case playerO:
		return playerX
	case playerX:
		return playerO
	default:
		return playerNone
	}
}

var threeInARowPositions = [...][3]int{
	{0, 1, 2},
	{3, 4, 5},
	{6, 7, 8},
	{0, 3, 6},
	{1, 4, 7},
	{2, 5, 8},
	{0, 4, 8},
	{2, 4, 6},
}

// Returns who has won - or playerNone if none.
func (grid *gridStates) hasWinner() player {
	for _, combination := range threeInARowPositions {
		player := grid[combination[0]]
		if player != playerNone && player == grid[combination[1]] && player == grid[combination[2]] {
			return player
		}
	}
	return playerNone
}

func (grid *gridStates) clear() {
	*grid = gridStates{}
}

func (grid *gridStates) isEmpty() bool {
	return *grid == gridStates{}
}

// Find out where the next move can be made
func (grid *gridStates) findPossibleMoves() (res []int) {
	for index, value := range grid {
		if value == playerNone {
			res = append(res, index)
		}
	}
	return res
}
