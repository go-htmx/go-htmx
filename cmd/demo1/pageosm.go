package main

import (
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// OpenStreetMap component
func myPageOSMMap() nest.UI {
	return &nest.Panel{
		Children: []nest.UI{
			&nest.Heading2{Text: "OpenStreetMap component"},
			&nest.Paragraph{Text: "This does not have a moveable marker but is simple"},
			&nest.OpenStreetMap{
				Width:  nest.Px(600),
				Height: nest.Px(300),
				//Bounds:     htmxc.GeoBounds{htmxc.GeoPoint{Latitude: 55, Longitude: 0}, htmxc.GeoPoint{Latitude: 56, Longitude: 15}},
				DiameterKM: 20,
				Marker:     nest.GeoPoint{Latitude: 55.5, Longitude: 10.5},
			},
		},
	}
}
