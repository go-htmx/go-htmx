package main

import (
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// A minimum version of a web page showing client-side state with a button
func myPageDownload() nest.UI {
	// Editor for the desired filename
	editFilename := &nest.EditString{}

	// This component must be included in the UI somewhere, but doesn't produce any visible UI elements.
	download := &nest.Download{
		OnDownload: func(comp *nest.Download, ctx *nest.Context) (nest.ContextFrameworkFileDownload, error) {
			file := nest.ContextFrameworkFileDownload{
				MimeType:     "text/plain",
				FileName:     editFilename.GetValue(ctx),
				FileContents: []byte("Hello, World"),
			}
			return file, nil
		},
	}

	// This is the actual web page layout
	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading2{Text: nest.T("Demo of downloading a file")},
			&nest.Paragraph{Text: nest.T("Clicking the button below will make your browser start a download of a file that is delivered by Go code. If you enter a text in the input below, it will become the filename.")},
			&nest.LabelEdit2{
				Caption: "Filename:",
				Editor:  editFilename,
			},
			&nest.Button{
				Caption: "Download",
				OnClick: func(button *nest.Button, ctx *nest.Context) error {
					download.Enabled = true
					return nil
				},
			},
			download, // The download component must be included somewhere in the user interface.
		},
	}
	return page
}
