package main

import (
	"fmt"
	"math/rand/v2"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// page_map.go
func myPageMap() nest.UI {
	MapLeaflet := &nest.LeafletJsMap{
		Component:               nest.Component{ID: "mapEx"},
		Width:                   nest.Px(600),
		Height:                  nest.Px(300),
		DiameterKM:              5.0,
		EnableInteractiveMarker: true,
	}

	return &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: "Leaflet.js map Example"},
			&nest.Paragraph{Text: "You can select a position on the map, and get the position using a button"},

			MapLeaflet,

			&nest.Panel{
				Horizontal: true,
				Children: []nest.UI{
					&nest.Button{
						Caption: "Move marker",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							MapLeaflet.Data.Marker.Coor.Latitude = 48 + 3*rand.Float64()
							MapLeaflet.Data.Marker.Coor.Longitude = 30 + 3*rand.Float64()
							return nil
						},
					},

					&nest.Button{
						Caption: "Read location",
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							msg := fmt.Sprintf(`Location: %.5f,%.5f`, MapLeaflet.Data.Marker.Coor.Latitude, MapLeaflet.Data.Marker.Coor.Longitude)
							return nest.UserErrorMsg(msg)
						},
					},
				},
			},
		},
	}
}
