package main

import "fmt"

type LanguageCode string

var translationLanguage LanguageCode

var translations_dk map[string]string = map[string]string{
	"Form":                 "Skema",
	"Table":                "Tabel",
	"Click counter":        "Kliktæller",
	"Double click counter": "Dobbelt kliktæller",
	"Hello, World":         "Hej, verden",
	"Demo of click button and an embedded image": "Demo af kliktæller og indlejret billede",
	"The following picture was provided by Wikimedia.org under the Creative Commons Attribution 3.0 Unported license. The image was downscaled.": "Det følgende billede blev leveret af Wikimedia.org i henhold til Creative Commons Attribution 3.0 Unported license. Billedet er nedskaleret.",
	"I was clicked %d times": "Jeg blev klikket %d gange",
	"Resizable map":          "Tilpasbart kort",
	"Copenhagen":             "København",
	"New York":               "New York",
	"This part is resizable": "Denne del kan tilpasses i størrelsen",
	"Resizable":              "Tilpasbar",
	"Demo application":       "Demo program",
	"Title":                  "Titel",
	"ID":                     "ID",
	"Click counter device for manual counting": "Kliktæller apparat for manuel tælning",
	"Sign up":                        "Opskriv",
	"Name":                           "Navn",
	"E-mail":                         "Email",
	"Phone number":                   "Telefonnummer",
	"Consumer":                       "Forbruger",
	"Business":                       "Erhverv",
	"Please correct errors.":         "Korriger venligst fejl",
	"Simulated data from a database": "Simulerede data fra en database",
	"Signup Type":                    "Opskrivningstype",
	"Signup form":                    "Opskrivningsskema",
	"This version currently discards not-validated input.": "Denne version gemmer ikke data der ikke kan valideres.",
	"Thank you for submitting your data.":                  "Tak for at indsende dine data.",
	"Go.dev homepage":                                      "Go.dev hjemmeside",
	"Number %d":                                            "Nummer %d",
	"Copenhagen is the capital of Denmark. It is a very walkable city, and should be visited without bringing a car.":               "København er Danmarks hovedstad. Den er god at gå til fods i, og bør besøges uden at medbringe bil.",
	"London was the world's biggest city at a time where people did not use cars. It was therefore not designed for use with cars.": "London var verdens største by, dengang man ikke havde bil. Byen er derfor ikke designet til at køre bil i.",
	"Click the buttons to replace this text.\n":              "Tryk på knapperne for at udskifte denne tekst.",
	"After switching translation, navigate to another part:": "Efter skift af sprog, naviger i programmet for at se oversættelsen",
	"Name:":                "Navn",
	"Email:":               "Email",
	"Phone number:":        "Telefonnummer",
	"Age:":                 "Alder",
	"I agree to the terms": "Jeg er enig i betingelserne",
	"Category":             "Kategori",
	"Submit":               "Indsend",
	"Child":                "Barn",
	"Adult":                "Voksen",
	"Retired":              "Pensionist",
	"Clear form":           "Nulstil skema",
	"Load data":            "Indlæs data",
	"Next":                 "Næste",
	"Previous":             "Forrige",
	"Validation error, please correct your input.": "Valideringsfejl, korriger venligst det indtastede.",
	"Data has been submitted for %s.":              "Data er indsendt for %s.",
	"This field is required.":                      "Dette felt er obligatorisk.",
	"Checkbox must be checked.":                    "Du skal afkrydse denne.",
	"Make a choice from the list":                  "Vælg fra listen",
	"Email":                                        "Email",
	"Age":                                          "Alder",
	"Download":                                     "Download",
	"Demo of downloading a file":                   "Demo af at downloade en fil",
	"Clicking the button below will make your browser start a download of a file that is delivered by Go code. If you enter a text in the input below, it will become the filename.": "Hvis du klikker på knappen nedenfor, så begynder din browser at downloade en fil. Hvis du angiver et filnavn i tekstfeltet, så vil det blive brugt som filnavn.",
	"Various":           "Diverse",
	"Popup information": "Popup information",
	"Popup heading":     "Popup overskrift",
	"Various examples":  "Diverse eksempler",
	"The following button shows how to redirect the browser to another web page.": "Følgende knap viser hvordan man sender browseren videre til en anden webside",
	"The following button opens a popup message.":                                 "Følgende knap åbner en popup besked",
	"Open popup": "Åbn popup",
	"This text was created using an HTML template:": "Denne tekst er lavet med en HTML skabelon:",
	"Add click counter":                             "Tilføj kliktæller",
	"Map":                                           "Kort",
}

var translations map[LanguageCode]map[string]string = map[LanguageCode]map[string]string{
	"da": translations_dk,
}

func translate(message string) string {
	if translationLanguage == "" {
		return message
	}
	lookupTable, ok := translations[translationLanguage]
	if !ok {
		return message
	}
	msg, found := lookupTable[message]
	if !found || msg == "" {
		// Output untranslated items for copy&paste into the translation table above
		fmt.Printf(`"%s": "",`+"\n", message)
		return message
	}
	return msg
}
