package main

import "gitlab.com/go-htmx/go-htmx/pkg/nest"

const YoutubeVideo1 = `<iframe width="560" height="315" src="https://www.youtube.com/embed/r-GSGH2RxJs?si=44Qzvtq_wbYFej_I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`
const YoutubeVideo2 = `<iframe width="560" height="315" src="https://www.youtube.com/embed/446E-r0rXHI?si=EGkr8v8eoqPFtW_L" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`

// This demonstrates how to embed something from YouTube by copy&paste from Youtube's embed homepage
func myPageYoutube() nest.UI {
	return &nest.Panel{
		Children: []nest.UI{
			&nest.Heading2{Text: "Youtube video"},
			&nest.Paragraph{Text: "This is simply inserted according to instructions from the Youtube homepage."},
			&nest.CustomHtml{Html: YoutubeVideo1},
			&nest.CustomHtml{Html: YoutubeVideo2},
		},
	}
}
