package main

import (
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// This code shows, how parts of UIs can be used inside other UIs
func allPages() nest.UI {
	return &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: nest.T("Demo application")},
			&nest.Paragraph{Text: nest.T("After switching translation, navigate to another part:")},
			&nest.Select{
				Options: []nest.ValueAndTitle{
					{Title: "Not translated", Value: ""},
					{Title: "Danish translation", Value: "da"},
					{Title: "German translation", Value: "de"},
				},
				Sorted: true,
				OnPrepareHTML: func(control *nest.Select, ctx *nest.Context) error {
					translationLanguage = LanguageCode(ctx.Get(control.ID))
					return nil
				},
			},
			&nest.PagesTabbed{
				Component: nest.Component{ID: "maintab"},
				Pages: []nest.PagesTabbedPage{
					{Caption: nest.T("Table"), Page: myPage1()},
					{Caption: nest.T("Charts"), Page: myPageCharts()},
					{Caption: nest.T("Click counter"), Page: myPage2()},
					{Caption: nest.T("Double click counter"), Page: myPage3()},
					{Caption: nest.T("Form"), Page: myPageForm()},
					//{Caption: nest.T("DatasetEdit"), Page: myPageDatasetEdit()},  // Removed because example doesn't work
					{Caption: nest.T("Resizable"), Page: myPageResizable()},
					{Caption: nest.T("Download"), Page: myPageDownload()},
					{Caption: nest.T("Various"), Page: myPageVarious()},
					{Caption: nest.T("Map"), Page: myPageMap()},
					{Caption: nest.T("OSMap"), Page: myPageOSMMap()},
					{Caption: nest.T("Youtube"), Page: myPageYoutube()},
				},
			},
		},
	}
}
