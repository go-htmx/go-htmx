package main

import (
	"fmt"
	"strings"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

func validateEmail2(ed *nest.EditString, ctx *nest.Context, str string) error {
	if str == "" {
		return nil
	}
	if len(str) < 6 {
		return nest.UserErrorMsg(nest.T("E-mail must be at least 6 characters long"))
	}
	if !strings.Contains(str, "@") {
		return nest.UserErrorMsg(nest.T("E-mail must contain @ symbol"))
	}
	if !strings.Contains(str, ".") {
		return nest.UserErrorMsg(nest.T("E-mail must contain a dot (.)"))
	}
	return nil
}

func validatePhone2(d *nest.EditString, ctx *nest.Context, str string) error {
	if str == "" {
		return nil
	}
	if len(str) < 8 {
		return nest.UserErrorMsg(nest.T("Phone must be at least 8 digits"))
	}
	return nil
}

// ***** Form editors *****

type MyEditors struct {
	editName     *nest.EditString
	editPhone    *nest.EditString
	editEmail    *nest.EditString
	editAge      *nest.EditNumber
	editAgree    *nest.CheckBox
	editCategory *nest.Select
}

func (eds *MyEditors) Load(ctx *nest.Context, name, email, phone, category string, age int) {
	eds.editName.SetValue(ctx, name)
	eds.editEmail.SetValue(ctx, email)
	eds.editPhone.SetValue(ctx, phone)
	eds.editAgree.SetState(ctx, false)
	eds.editAge.SetEditValue(ctx, "")
	if age != 0 {
		eds.editAge.SetValue(ctx, float64(age))
	}
	eds.editCategory.SetValue(ctx, category)
}

func (eds *MyEditors) GetAsText(ctx *nest.Context) string {
	return fmt.Sprintf(
		nest.T("Name: %s\nEmail: %s\nPhone: %s\nCategory: %s\nAgrees: %t"),
		eds.editName.GetValue(ctx),
		eds.editEmail.GetValue(ctx),
		eds.editPhone.GetValue(ctx),
		eds.editCategory.GetValue(),
		eds.editAgree.Checked,
	)
}

func NewMyEditors() *MyEditors {
	return &MyEditors{
		editName: &nest.EditString{
			Length:    60,
			MaxLength: 60,
			Required:  true,
		},
		editEmail: &nest.EditString{
			Length:     40,
			MaxLength:  50,
			OnValidate: validateEmail2,
		},
		editPhone: &nest.EditString{
			Length:     30,
			MaxLength:  30,
			OnValidate: validatePhone2,
		},
		editAge: &nest.EditNumber{
			Required:  true,
			NumberMax: 150,
		},
		editAgree: &nest.CheckBox{
			Caption:  nest.T("I agree to the terms"),
			Required: true,
		},
		editCategory: &nest.Select{
			Required: true,
			Options: []nest.ValueAndTitle{
				{Title: nest.T("Child"), Value: "child"},
				{Title: nest.T("Adult"), Value: "adult"},
				{Title: nest.T("Retired"), Value: "retired"},
			},
		},
	}
}

// ***** User Interface *****

// This web page is a demonstration of how to implement a form for submitting data.
func myPageForm() nest.UI {
	popupText := &nest.Paragraph{}
	popup := &nest.Popup{
		Control: popupText,
	}

	myEditors := NewMyEditors()
	form := &nest.Form{
		OnSubmit: func(ctx *nest.Context, form *nest.Form) error {
			popupText.Text = fmt.Sprintf(nest.T("Data has been submitted for %s."), myEditors.GetAsText(ctx))
			popup.Visible = true
			return nil
		},
	}

	page := &nest.Panel{
		Component: nest.Component{ID: "xform"},
		Form:      form,
		Children: []nest.UI{
			form,  // Form is non-visual but must be here in order to save and load state
			popup, // Non-visual by default, but will be activated if form is submitted correctly
			&nest.Heading1{Text: nest.T(`Signup form`)},
			&nest.Panel{
				Horizontal: true,
				Children: []nest.UI{
					&nest.Button{
						Caption: nest.T("Clear form"),
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							form.Clear(ctx)
							return nil
						},
					},
					&nest.Button{
						Caption: nest.T("Load data"),
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							form.Clear(ctx)
							myEditors.Load(ctx, "Peter Petersen", "peter@petersen.com", "555-8888", "adult", 43)
							return nil
						},
					},
				},
			},
			&nest.Panel{
				Style: nest.PanelStyle{VisiblePanel: true},
				Children: []nest.UI{
					&nest.LabelEdit2{
						Caption: nest.T("Name"),
						Editor:  myEditors.editName,
					},
					&nest.LabelEdit2{
						Caption: nest.T("Email"),
						Editor:  myEditors.editEmail,
					},
					&nest.LabelEdit2{
						Caption: nest.T("Phone number"),
						Editor:  myEditors.editPhone,
					},
					&nest.LabelEdit2{
						Caption: nest.T("Age"),
						Editor:  myEditors.editAge,
					},
					&nest.LabelEdit2{ // The LabelEdit2 adds that validation errors are shown in the UI
						Editor: myEditors.editAgree,
					},
					&nest.LabelEdit2{
						Caption: nest.T("Category"),
						Editor:  myEditors.editCategory,
					},
					&nest.Button{
						Caption: nest.T("Submit"),
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							err := form.Submit(ctx)
							if err != nil {
								return nest.UserErrorMsg(nest.T("Please correct your input"))
							}
							return nil
						},
					},
				},
			},
		},
	}
	return page
}
