package main

import (
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// A minimum version of a web page showing client-side state with a button
func myPageCharts() nest.UI {
	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: nest.T("Various examples")},

			&nest.Heading3{Text: nest.T("QuickChart")},
			&nest.QuickChart{
				Width:    nest.Px(600),
				Height:   nest.Px(300),
				AltTitle: "Chart showing fruit sales per day",
				Data: nest.ChartJsParameters{
					Type: nest.CjBar,
					Data: nest.ChartJsData{
						Labels: []string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"},
						Datasets: []nest.ChartJsDataset{
							{
								Label: "Apples",
								Data:  []float64{1, 2, 3, 4, 5},
							},
							{
								Label: "Oranges",
								Data:  []float64{4, 3, 2, 1, 0},
							},
						},
					},
				},
			},
			&nest.Heading3{Text: nest.T("Chart.js")},
			&nest.Chartjs{
				Width:  nest.Px(600),
				Height: nest.Px(300),
				Data: nest.ChartJsParameters{
					Type: nest.CjBar,
					Data: nest.ChartJsData{
						Labels: []string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday"},
						Datasets: []nest.ChartJsDataset{
							{
								Label: "Apples",
								Data:  []float64{1, 2, 3, 4, 5},
							},
							{
								Label: "Oranges",
								Data:  []float64{4, 3, 2, 1, 0},
							},
						},
					},
				},
			},
		},
	}
	return page
}
