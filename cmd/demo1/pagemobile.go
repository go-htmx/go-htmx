package main

import "gitlab.com/go-htmx/go-htmx/pkg/nest"

func middlePartBox() nest.UI {
	return &nest.Panel{
		Children: []nest.UI{
			&nest.Heading2{Text: nest.T(`Middle part`)},
			&nest.Paragraph{Text: nest.T(`Middle part`)},
			&nest.Paragraph{Text: nest.T(`Middle part`)},
			&nest.Paragraph{Text: nest.T(`Middle part`)},
			&nest.Paragraph{Text: nest.T(`Middle part`)},
		},
	}
}

func myMobilePage() nest.UI {
	page := &nest.Panel{
		Width:  nest.NewLength(100, nest.Percent),
		Height: nest.NewLength(100, nest.ViewportHeight),
		Children: []nest.UI{
			&nest.Panel{
				Height: nest.NewLength(10, nest.ViewportHeight),
				Style: nest.PanelStyle{
					VisiblePanel: true,
				},
				Children: []nest.UI{
					&nest.Heading1{Text: nest.T(`Upper part`)},
					&nest.Paragraph{Text: nest.T(`Upper part`)},
				},
			},
			&nest.Panel{
				Style: nest.PanelStyle{
					Scrollbar:    true,
					VisiblePanel: true,
				},
				Height: nest.NewLength(70, nest.ViewportHeight),
				Children: []nest.UI{
					middlePartBox(),
					middlePartBox(),
					middlePartBox(),
					middlePartBox(),
					middlePartBox(),
					middlePartBox(),
				},
			},
			&nest.Panel{
				Height: nest.NewLength(20, nest.ViewportHeight),
				Style:  nest.PanelStyle{VisiblePanel: true},
				Children: []nest.UI{
					&nest.Heading1{Text: nest.T(`Bottom part`)},
					&nest.Paragraph{Text: nest.T(`Bottom part`)},
				},
			},
		},
	}
	return page
}
