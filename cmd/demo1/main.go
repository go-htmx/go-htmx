package main

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/go-htmx/go-htmx/pkg/nest"

	_ "embed"
)

//go:embed stylesheet.css
var stylesheet string

//go:embed template.html
var template string

// BOILERPLATE code
// This way of making a handler allows to use templates, and provide other parameters, e.g. database connection parameters.
func makehandler(page func() nest.UI) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("Incoming HTTP request", r.Method, r.URL.String())
		ctx := nest.NewContext(r)
		ctx.RequireWCAG = true
		ctx.HxTarget = "#main" // Which part of the template is replaced by HTMX, by default
		if ctx.IsHtmxRequest {
			ctx.RequestHandler(w, page)
		} else {
			htmlstr := fmt.Sprintf(
				template,
				stylesheet,
				`<script src="https://unpkg.com/htmx.org@2.0.3" integrity="sha384-0895/pl2MU10Hqc6jd4RvrthNlDiE9U1tWmX7WRESftEDRosgxNsQG/Ze9YMRzHq" crossorigin="anonymous"></script>`,
				ctx.RequestHandlerForTemplate(page),
			)
			w.Header().Set("Content-Type", "text/html; charset=utf-8")
			_, err := w.Write([]byte(htmlstr))
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				fmt.Println("Incoming HTTP request completed with error")
				return
			}
		}
	}
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/hello/{name}", makehandler(myPage1))
	r.HandleFunc("/buttondemo1/", makehandler(myPage2))
	r.HandleFunc("/buttondemo2/", makehandler(myPage3))
	r.HandleFunc("/formdemo2/", makehandler(myPageForm))
	r.HandleFunc("/tabbed/", makehandler(allPages))
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/tabbed/", http.StatusFound)
	})
	r.HandleFunc("/mobile/", makehandler(myMobilePage))
	r.HandleFunc("/map/", makehandler(myPageMap))

	// Set localization system
	nest.SetTextTranslator(translate)

	fmt.Println("Starting server at port 7070")
	fmt.Println("Try http://127.0.0.1:7070/hello/world and change sorting in the table by clicking the table column headers.")
	fmt.Println("Try http://127.0.0.1:7070/buttondemo1/ to see a minimum implementation of an interactive button with client-side state.")
	fmt.Println("Try http://127.0.0.1:7070/buttondemo2/ to see a dual click counter.")
	fmt.Println("Try http://127.0.0.1:7070/formdemo/ to see a demo of a form.")
	fmt.Println("Try http://127.0.0.1:7070/mobile/ to see a demo of a mobile page.")
	fmt.Println("Try http://127.0.0.1:7070/ to see multiple pages with tabs.")
	if err := http.ListenAndServe(":7070", r); err != nil {
		fmt.Println("Failed to start server:", err)
	}
}
