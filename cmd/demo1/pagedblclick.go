package main

import (
	"fmt"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

var clickCounterCount = 2 // Initial number of click counters

func generalizedClickCounterAction(button *nest.Button, ctx *nest.Context) (err error) {
	// Get the client-side click counter. Default is that it is zero.
	clickCountParameter := button.ID + "counter"
	clickCount := ctx.GetInt(clickCountParameter) + 1

	if clickCount == 5 {
		// Demonstrate error handling
		clickCount = 0
		err = nest.UserErrorMsg(fmt.Sprintf(nest.T("%s click count has reached 5, for demo purposes this is shown as an error"), button.ID))
	}

	ctx.SetInt(clickCountParameter, clickCount) // This will make the new value appear in the client-side HTML

	return err
}

func generalizedClickCounterLoad(button *nest.Button, ctx *nest.Context) error {
	clickCountParameter := button.ID + "counter"
	clickCount := ctx.GetInt(clickCountParameter)
	button.Caption = fmt.Sprintf(nest.T("I was clicked %d times"), clickCount)
	return nil
}

// Two click counter buttons on one web page
func myPage3() nest.UI {
	return &nest.Panel{
		Children: []nest.UI{
			&nest.Button{
				Caption: nest.T("Add click counter"),
				OnClick: func(button *nest.Button, ctx *nest.Context) error {
					clickCounterCount++
					ctx.Restart() // After increasing the number of components on the page, we need to rebuild the page in memory before rendering HTML
					return nil
				},
			},
		},
		OnInitialize: func(panel *nest.Panel, ctx *nest.Context) error {
			for range clickCounterCount {
				btn := &nest.Button{
					OnClick:       generalizedClickCounterAction,
					OnPrepareHTML: generalizedClickCounterLoad,
				}
				ctx.InitNew(panel, btn)
				panel.Children = append(panel.Children, btn)
			}
			return nil
		},
	}
}
