package main

import (
	"fmt"

	_ "embed"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

//go:embed page2clickcounter.png
var imageClickCounter []byte

// A minimum version of a web page showing client-side state with a button
func myPage2() nest.UI {
	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading2{Text: nest.T("Demo of click button and an embedded image")},
			&nest.Paragraph{Text: nest.T("The following picture was provided by Wikimedia.org under the Creative Commons Attribution 3.0 Unported license. The image was downscaled.")},
			&nest.Image{ // This image will be embedded in HTML using base64 encoding
				Image:   imageClickCounter,
				Zoom:    0.4, // The PNG image is quite big, so we want it to be smaller
				AltText: nest.T("Click counter device for manual counting"),
				Caption: &nest.Text{Text: "Click counter"},
				OnClick: func(image *nest.Image, ctx *nest.Context) error {
					// Just a demonstration of error handling.
					return nest.UserErrorMsg(nest.T("Do not click this - click on the button, instead."))
				},
			},
			&nest.Button{
				OnClick: func(button *nest.Button, ctx *nest.Context) error {
					clickCountParameter := button.ID + "counter"
					ctx.SetInt(clickCountParameter, ctx.GetInt(clickCountParameter)+1)
					return nil
				},
				OnPrepareHTML: func(button *nest.Button, ctx *nest.Context) error {
					button.Caption = fmt.Sprintf(nest.T("I was clicked %d times"), ctx.GetInt(button.ID+"counter"))
					return nil
				},
			},
		},
	}
	return page
}
