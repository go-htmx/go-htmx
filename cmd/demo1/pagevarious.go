package main

import (
	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// A minimum version of a web page showing client-side state with a button
func myPageVarious() nest.UI {
	popupInfo := &nest.Paragraph{Text: nest.T("Popup information")}
	popup := &nest.Popup{
		Control: &nest.Panel{
			Children: []nest.UI{
				&nest.Heading2{Text: nest.T("Popup heading")},
				popupInfo,
			},
		},
	}
	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: nest.T("Various examples")},

			&nest.Heading3{Text: nest.T("The following button shows how to redirect the browser to another web page.")},
			&nest.Button{
				Caption: nest.T("Go.dev homepage"),
				OnClick: func(button *nest.Button, ctx *nest.Context) error {
					ctx.RedirectURL("https://go.dev/")
					return nil
				},
			},

			&nest.Heading3{Text: nest.T("The following button opens a popup message.")},
			&nest.Button{
				Caption: nest.T("Open popup"),
				OnClick: func(button *nest.Button, ctx *nest.Context) error {
					popupInfo.Text = nest.T("This message will be shown instead of the default popup text.")
					popup.Visible = true
					return nil
				},
			},
			popup,

			&nest.Heading3{Text: nest.T("This text was created using an HTML template:")},
			&nest.Template{
				Html: `<p>{{.PrefixText}}<del>{{.MyComponent}}</del>{{.SuffixText}}</p>`,
				Children: map[string]nest.UI{
					"PrefixText":  &nest.Text{Text: "The following text is deleted: "},
					"MyComponent": &nest.Text{Text: "Hello, World"},
					"SuffixText":  &nest.Text{Text: "."},
				},
			},
			&nest.Heading3{Text: nest.T("Select test")},
			&nest.Select{
				DefaultValue: "B",
				Options: []nest.ValueAndTitle{
					{Value: "B", Title: "Banana"},
					{Value: "C", Title: "Cinnamon"},
					{Value: "A", Title: "Apple"},
				},
				Sorted: true,
				OnChange: func(control *nest.Select, ctx *nest.Context, OldValue string) error {
					return nil //errors.New("error simulation")
				},
			},
			&nest.Heading3{Text: nest.T("Tabs test")},
			&nest.Tabs{
				TabList: []nest.ValueAndTitle{
					{Value: "1", Title: "First"},
					{Value: "2", Title: "Second"},
					{Value: "3", Title: "Third"},
				},
				OnChange: func(tabs *nest.Tabs, ctx *nest.Context) error {
					if tabs.Value == "3" {
						return nest.UserErrorMsg("Third button generates this error message intentionally")
					}
					return nil
				},
			},
		},
	}
	return page
}
