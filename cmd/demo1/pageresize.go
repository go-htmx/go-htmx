package main

import (
	_ "embed"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

// A minimum version of a web page showing client-side state with a button
func myPageResizable() nest.UI {
	paragraphThatWillBeModified := &nest.Text{Text: nest.T("Click the buttons to replace this text.\n")}

	page := &nest.Panel{
		Children: []nest.UI{
			&nest.Heading1{Text: nest.T("Resizable map")},
			&nest.Panel{
				Horizontal: true,
				Children: []nest.UI{
					&nest.Button{
						Caption: nest.T("Copenhagen"),
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							txt := nest.T("Copenhagen is the capital of Denmark. It is a very walkable city, and should be visited without bringing a car.")
							paragraphThatWillBeModified.Text = txt
							return nil
						},
					},
					&nest.Button{
						Caption: nest.T("New York"),
						OnClick: func(button *nest.Button, ctx *nest.Context) error {
							txt := nest.T("London was the world's biggest city at a time where people did not use cars. It was therefore not designed for use with cars.")
							paragraphThatWillBeModified.Text = txt
							return nil
						},
					},
				},
			},
			&nest.Resizable{
				Width:  nest.Px(300),
				Height: nest.Px(300),
				Child: &nest.Panel{
					Children: []nest.UI{
						&nest.Heading1{Text: nest.T("This part is resizable")},
						paragraphThatWillBeModified,
					},
				},
			},
		},
	}
	return page
}
