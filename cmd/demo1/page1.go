package main

import (
	"fmt"
	"strconv"

	"gitlab.com/go-htmx/go-htmx/pkg/nest"
)

type dataItem struct {
	id    int
	title string
}

// Web page with headline, a body text with name from URL, and an interactive table with sorting
func myPage1() nest.UI {
	// This is where data is stored. This could have been fetched from a database in an OnPrepareHTML event.
	var data []dataItem

	// Instead of a Heading1 component, you can also use a nest.CustomHtml component, but Heading1 ensures to HTML-escape the text
	heading := &nest.Heading1{
		Text: nest.T(`Hello, World`),
	}

	// This paragraph is hidden, unless a name is specified as parameter in the URL
	bodyShowingName := &nest.Paragraph{
		Component: nest.Component{Omitted: true},
		OnPrepareHTML: func(control *nest.Paragraph, ctx *nest.Context) error {
			parameterValue := ctx.Get("name") // From URL
			if parameterValue != "" {
				control.Omitted = false
				control.Text = fmt.Sprintf(nest.T(`Value specified as parameter: %s`), parameterValue)
			}
			return nil
		},
	}

	// Interactive table, allows sorting by clicking on the table header
	interactiveTable := &nest.Table{
		Columns: []nest.TableColumn{
			{
				FieldName: "id",
				Title:     nest.T("ID"),
			},
			{
				FieldName: "title",
				Title:     nest.T("Title"),
			},
		},
		SortBy:                "id",
		PaginationRowsPerPage: 5,
		AllowSelect:           true,
		OnPrepareHTML: func(table *nest.Table, ctx *nest.Context) error {
			table.PaginationTotalCount = len(data)
			return nil
		},
		OnLoadPage: func(table *nest.Table, ctx *nest.Context, firstRow int) error {
			// Add data to table
			for itemIndex, item := range data {
				if itemIndex < firstRow || itemIndex >= firstRow+table.PaginationRowsPerPage {
					continue
				}
				rowid := strconv.Itoa(item.id)
				row := table.NewRow(rowid)
				row.Cells = map[nest.FieldName]*nest.TableCell{
					"id": {
						Value: &nest.GEditInt{ // This shows how editors can be used, this is good for sorting
							IntRef: &data[itemIndex].id,
						},
					},
					"title": nest.NewGTableCellText(data[itemIndex].title), // This is an easy function to insert just a text
				}
			}
			return nil
		},
	}

	page := &nest.Panel{ // Vertically arranged Children
		Children: []nest.UI{
			heading,
			bodyShowingName,
			interactiveTable,
		},
		OnPrepareHTML: func(gv *nest.Panel, ctx *nest.Context) error {
			// Get the data somehow, from database
			for n := range 20 {
				data = append(data, dataItem{
					id:    n + 1,
					title: fmt.Sprintf(nest.T("Number %d"), n+1000+n*(19-n)),
				})
			}

			// Error reporting, e.g. if data cannot be retrieved from database
			return nil
		},
	}
	return page
}
